/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JCo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import dbcon.dbConnection;
import structures.Messages;
import com.google.gson.*;
import com.sap.conn.jco.*;
import com.sap.conn.jco.ext.*;
import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.StellarSAP_JCo;
import static main.StellarSAP_JCo.gModalidad;
import static main.StellarSAP_JCo.gTestMode;

/**
 *
 * @author Augusto Gómez
 */
public class SapConnection extends Thread
{
    
    //The custom destination data provider implements DestinationDataProvider and
    //provides an implementation for at least getDestinationProperties(String).
    //Whenever possible the implementation should support events and notify the JCo runtime
    //if a destination is being created, changed, or deleted. Otherwise JCo runtime
    //will check regularly if a cached destination configuration is still valid which incurs
    //a performance penalty.
    private String destName;
    private SapConnection.MyDestinationDataProvider myProvider;
    public Boolean band;
    public int opc;
    public String centro;
    public boolean pingResult = false;
    public static Gson json = new GsonBuilder().disableHtmlEscaping().create();
    public static String TipoConexion = null;
    public static String CurrentPriorityMode = "0";
    public static JCoDestination ABAP_RFC = null;
    public static JCoFunction fTransVentas = null;
    public static JCoFunction fTransDonaciones = null;
    public static JCoFunction fTransDepositosCP = null;
    public static JCoFunction fConsultaStock = null;
    
    /**
     * Crea una conexión a SAP a través del SAPJCo utilizando los parámetros configurados en la base de datos.
     */
    
    public SapConnection(String pTipoConexion)
    {
        
        this.TipoConexion = pTipoConexion;
        myProvider = new SapConnection.MyDestinationDataProvider();

        //register the provider with the JCo environment;
        //catch IllegalStateException if an instance is already registered
        try{       
            com.sap.conn.jco.ext.Environment.registerDestinationDataProvider(myProvider);
        }catch(IllegalStateException providerAlreadyRegisteredException){
            //somebody else registered its implementation, 
            //stop the execution
            throw new Error(providerAlreadyRegisteredException);
        }
        
        destName = "ABAP_AS";
        
        /*
        myProvider.changeProperties(destName, getDestinationPropertiesFromUI());
        */
        
        myProvider.changeProperties(destName, getDestinationPropertiesFromStellar());
        
        try {
            
            JCoDestinationManager.getDestination(destName).ping();
            pingResult = true;
            
        } catch (JCoException ex1) {
            
            if (ex1.getGroup() == JCoException.JCO_ERROR_COMMUNICATION) {
                
                myProvider.changeProperties(destName, getDestinationPropertiesFromStellar(CurrentPriorityMode));
                
                try {
                    
                    JCoDestinationManager.getDestination(destName).ping();
                    pingResult = true;
                    
                } catch (JCoException ex2) {
                    StellarSAP_JCo.gLogger.EscribirLog(ex2, "Error conexión:");
                    System.out.println(json.toJson(new Messages('E', ex1.getMessage())));
                }
                
            }else{
                
                StellarSAP_JCo.gLogger.EscribirLog(ex1, "Error conexión:");
                System.out.println(json.toJson(new Messages('E', ex1.getMessage())));
                //System.exit(0);
                
            }
            
        }
        
     }  
    
    public SapConnection(SapConnection jco, String ndoc, int opc, String centr)
    {
        
        super(jco);
        destName = jco.destName;
        myProvider = jco.myProvider;
        this.opc = opc;
        this.centro = centr;
        this.band = true;
        
    }
    
    public void unregister(){
        com.sap.conn.jco.ext.Environment.unregisterDestinationDataProvider(myProvider);
    }
    
    static class MyDestinationDataProvider implements DestinationDataProvider
    {
        
        private DestinationDataEventListener eL;
        private HashMap<String, Properties> secureDBStorage = new HashMap<String, Properties>();
        
        public Properties getDestinationProperties(String destinationName)
        {
            try
            {
                //read the destination from DB
                Properties p = secureDBStorage.get(destinationName);

                if(p!=null)
                {
                    //check if all is correct, for example
                    if(p.isEmpty())
                        throw new DataProviderException(DataProviderException.Reason.INVALID_CONFIGURATION, "Configuración del destino incorrecta", null);

                    return p;
                }
                
                return null;
            }
            catch(RuntimeException re)
            {
                throw new DataProviderException(DataProviderException.Reason.INTERNAL_ERROR, re);
            }
            
        }

        //An implementation supporting events has to retain the eventListener instance provided
        //by the JCo runtime. This listener instance shall be used to notify the JCo runtime
        //about all changes in destination configurations.
        public void setDestinationDataEventListener(DestinationDataEventListener eventListener)
        {
            this.eL = eventListener;
        }

        public boolean supportsEvents()
        {
            return true;
        }

        //implementation that saves the properties in a very secure way
        void changeProperties(String destName, Properties properties)
        {
            synchronized(secureDBStorage)
            {
                if(properties==null)
                {
                    if(secureDBStorage.remove(destName)!=null)
                        eL.deleted(destName);
                }
                else 
                {
                    secureDBStorage.put(destName, properties);
                    eL.updated(destName); // create or updated
                }
            }
        }
    } // end of MyDestinationDataProvider
    
    //Conexion a SAP sin router
    
    static Properties getDestinationPropertiesFromUI(){
        
        Properties connectProperties = new Properties();
        
//        //dbConnection conn = new dbConnection(odbc);
////        ResultSet rs = conn.select("*", "SAPCON", "", "", "", "");
////        try {
////            while (rs.next()) {
//                connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, "192.1.60.13");
//                connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR,  "00");
//                connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, "100");
//                connectProperties.setProperty(DestinationDataProvider.JCO_USER,   "SAPPOSDM");
//                connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, "posdmp01");
//                connectProperties.setProperty(DestinationDataProvider.JCO_LANG,   "ES");
////            }
////        } catch (SQLException ex) {
////            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
////            System.exit(0);
////        }

        return connectProperties;
        
    }
//    
////    Propiedades del SAP Router
//    static Properties getDestinationPropertiesRouter() {
//        Properties connectProperties = new Properties();
//        //dbConnection conn = new dbConnection(odbc);
////        ResultSet rs = conn.select("*", "SAPCON", "", "", "", "");
////        try {
////            while (rs.next()) {
//                
//                connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, "192.1.60.13");
//                connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR,  "00");
//                connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, "100");
//                connectProperties.setProperty(DestinationDataProvider.JCO_USER,   "SAPPOSDM");
//                connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, "posdmp01");
//                connectProperties.setProperty(DestinationDataProvider.JCO_LANG,   "ES");
//                
//                connectProperties.setProperty(DestinationDataProvider.JCO_SAPROUTER, "/H/190.217.5.155");
//                
////            }
////        } catch (SQLException ex) {
////            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
////            System.exit(0);   
////        }
//        return connectProperties;
//    }
//    
    static Properties getDestinationPropertiesFromStellar(String pPriorityMode)
    {
        
        Properties connectProperties = new Properties();
        
        String JCO_CONN_PRIORITY = null;
        
        String JCO_ASHOST_UI;
        String JCO_SYSNR_UI;
        String JCO_CLIENT_UI;
        String JCO_USER_UI;
        String JCO_PASSWORD_UI;
        String JCO_LANG_UI;
        
        String JCO_ASHOST_RT;
        String JCO_SYSNR_RT;
        String JCO_CLIENT_RT;
        String JCO_USER_RT;
        String JCO_PASSWORD_RT;
        String JCO_LANG_RT;
        String JCO_ROUTER_RT;
        
        if (TipoConexion.equals("1"))
        {

            if (pPriorityMode.equalsIgnoreCase("0"))
                JCO_CONN_PRIORITY = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_Priority", "1");
            else
            {
                if (CurrentPriorityMode.equalsIgnoreCase("1"))
                    pPriorityMode = "2";
                else if(CurrentPriorityMode.equalsIgnoreCase("2"))
                    pPriorityMode = "1";

                JCO_CONN_PRIORITY = pPriorityMode;
            }

            CurrentPriorityMode = JCO_CONN_PRIORITY;

            if (JCO_CONN_PRIORITY.equalsIgnoreCase("1")) 
            {

                JCO_ASHOST_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_ASHOST");
                JCO_SYSNR_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_SYSNR");
                JCO_CLIENT_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_CLIENT");
                JCO_USER_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_USER");
                JCO_PASSWORD_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_PASSWORD");
                JCO_LANG_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_LANG");

                connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, JCO_ASHOST_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR,  JCO_SYSNR_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, JCO_CLIENT_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_USER,   JCO_USER_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, JCO_PASSWORD_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_LANG,   JCO_LANG_UI);

            }
            else if(JCO_CONN_PRIORITY.equalsIgnoreCase("2")) 
            {

                JCO_ASHOST_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_ASHOST");
                JCO_SYSNR_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_SYSNR");
                JCO_CLIENT_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_CLIENT");
                JCO_USER_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_USER");
                JCO_PASSWORD_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_PASSWORD");
                JCO_LANG_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_UI_LANG");
                JCO_ROUTER_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "POSDMCon_ROUTER_HOSTNAME");

                connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, JCO_ASHOST_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR,  JCO_SYSNR_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, JCO_CLIENT_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_USER,   JCO_USER_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, JCO_PASSWORD_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_LANG,   JCO_LANG_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_SAPROUTER, JCO_ROUTER_RT);

            }

        } else if (TipoConexion.equals("2"))
        {

            if (pPriorityMode.equalsIgnoreCase("0"))
                JCO_CONN_PRIORITY = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_Priority", "1");
            else
            {
                if (CurrentPriorityMode.equalsIgnoreCase("1"))
                    pPriorityMode = "2";
                else if(CurrentPriorityMode.equalsIgnoreCase("2"))
                    pPriorityMode = "1";

                JCO_CONN_PRIORITY = pPriorityMode;
            }

            CurrentPriorityMode = JCO_CONN_PRIORITY;

            if (JCO_CONN_PRIORITY.equalsIgnoreCase("1")) 
            {

                JCO_ASHOST_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_ASHOST");
                JCO_SYSNR_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_SYSNR");
                JCO_CLIENT_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_CLIENT");
                JCO_USER_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_USER");
                JCO_PASSWORD_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_PASSWORD");
                JCO_LANG_UI = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_LANG");

                connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, JCO_ASHOST_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR,  JCO_SYSNR_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, JCO_CLIENT_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_USER,   JCO_USER_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, JCO_PASSWORD_UI);
                connectProperties.setProperty(DestinationDataProvider.JCO_LANG,   JCO_LANG_UI);

            }
            else if(JCO_CONN_PRIORITY.equalsIgnoreCase("2")) 
            {

                JCO_ASHOST_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_ASHOST");
                JCO_SYSNR_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_SYSNR");
                JCO_CLIENT_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_CLIENT");
                JCO_USER_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_USER");
                JCO_PASSWORD_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_PASSWORD");
                JCO_LANG_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_UI_LANG");
                JCO_ROUTER_RT = StellarSAP_JCo.ReglaDeNegocio(StellarSAP_JCo.gConexion, "SAPCon_ROUTER_HOSTNAME");

                connectProperties.setProperty(DestinationDataProvider.JCO_ASHOST, JCO_ASHOST_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_SYSNR,  JCO_SYSNR_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_CLIENT, JCO_CLIENT_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_USER,   JCO_USER_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_PASSWD, JCO_PASSWORD_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_LANG,   JCO_LANG_RT);
                connectProperties.setProperty(DestinationDataProvider.JCO_SAPROUTER, JCO_ROUTER_RT);

            }
            
        }
        
        return connectProperties;
        
    }
    
    static Properties getDestinationPropertiesFromStellar(){
        return getDestinationPropertiesFromStellar("0");
    }
    
    public void verificarConexion(){        
        if(this.pingResult)
            System.out.println(json.toJson(new Messages('S', "Conexión con SAP verificada con éxito!")));     
    }
    
    public Boolean ABAP_QueryEnabled(){
        if ((gTestMode && gModalidad.equalsIgnoreCase("1"))) return true;
        try {
            ABAP_RFC = JCoDestinationManager.getDestination(destName);
            return true;
        } catch (JCoException ex) {
            StellarSAP_JCo.gLogger.EscribirLog(ex, "Error al obtener RFC_ABAP");
            return false;
        }
    }
    
    public Boolean Inicializar_BAPI_Ventas_Devoluciones()
    {
        
        if (gTestMode) return true;
        
        fTransVentas = null;
        
        // OBTENGO LA ESTRUCTURA DE LA FUNCION RFC EN SAP.
        
        try{
            fTransVentas = ABAP_RFC.getRepository().getFunction("/POSDW/BAPI_POSTR_CREATE");
            return true;
        } catch (JCoException ex){
            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
            StellarSAP_JCo.gLogger.EscribirLog(ex, "Error al obtener función /POSDW/BAPI_POSTR_CREATE");
            return false;
            //System.exit(0);
        }
        
    }
    
    public Boolean Inicializar_Func_Donaciones()
    {
        
        if (!StellarSAP_JCo.gTransferirDonaciones) return false;
        if (gTestMode) return true;
        
        fTransDonaciones = null;
        
        // OBTENGO LA ESTRUCTURA DE LA FUNCION RFC EN SAP.
        
        try{
            fTransDonaciones = ABAP_RFC.getRepository().getFunction("ZGET_DONATIONS_FROM_STELLAR");
            return true;
        } catch (JCoException ex){
            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
            StellarSAP_JCo.gLogger.EscribirLog(ex, "Error al obtener función ZGET_DONATIONS_FROM_STELLAR");
            return false;
            //System.exit(0);
        }
        
    }
    
    public Boolean Inicializar_Func_DepositosCP()
    {
        
        if (!StellarSAP_JCo.gTransferirDepositosCP) return false;
        if (gTestMode) return true;
        
        fTransDepositosCP = null;
        
        // OBTENGO LA ESTRUCTURA DE LA FUNCION RFC EN SAP.
        
        try{
            fTransDepositosCP = ABAP_RFC.getRepository().getFunction("ZGET_FINANCIAL_DATA");
            return true;
        } catch (JCoException ex){
            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
            StellarSAP_JCo.gLogger.EscribirLog(ex, "Error al obtener función ZGET_FINANCIAL_DATA");
            return false;
            //System.exit(0);
        }
        
    }
    
    public Boolean Inicializar_Func_Consulta_Stock()
    {
        
        //if (gTestMode) return true;
        
        // OBTENGO LA ESTRUCTURA DE LA FUNCION RFC EN SAP.
        
        try{
            fConsultaStock = ABAP_RFC.getRepository().getFunction("ZMM_RFC_CONSULTA_STOCK_TD");
            return true;
        } catch (JCoException ex){
            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
            StellarSAP_JCo.gLogger.EscribirLog(ex, "Error al obtener función ZMM_RFC_CONSULTA_STOCK_TD");
            return false;
            //System.exit(0);
        }
        
    }
    
    public void FillIDOCInfo(JCoDestination invoke_rfc, JCoFunction function, 
    JCoStructure TestStruc, JCoTable Transaction, JCoTable RetailLineItem, 
    JCoTable LineItemTax, JCoTable Tender, JCoTable LineItemExt,
    JCoTable TransactionExt, JCoTable Return) {
              
        // LLENAR PARAMETROS DE ENTRADA DE LA RFC (IMPORTING)
        
        TestStruc = null;
        Transaction = null;
        RetailLineItem = null;
        LineItemTax = null;
        Tender = null;
        LineItemExt = null;
        TransactionExt = null;   
        
        Return = null;
        
        function.getImportParameterList().setValue("I_LOCKWAIT", 0);
        
        TestStruc = function.getImportParameterList().getStructure("I_SOURCEDOCUMENTLINK");
        
        TestStruc.setValue("KEY", "");
        TestStruc.setValue("TYPE", "");
        TestStruc.setValue("LOGICALSYSTEM", "");
        
        function.getImportParameterList().setValue("I_SOURCEDOCUMENTLINK", TestStruc);
        function.getImportParameterList().setValue("I_COMMIT", "X");
        
        // TRANSACTION INFO
        
        Transaction = function.getTableParameterList().getTable("TRANSACTION");
        RetailLineItem = function.getTableParameterList().getTable("RETAILLINEITEM");
        LineItemTax = function.getTableParameterList().getTable("LINEITEMTAX");
        Tender = function.getTableParameterList().getTable("TENDER");
        LineItemExt = function.getTableParameterList().getTable("LINEITEMEXT");
        TransactionExt = function.getTableParameterList().getTable("TRANSACTIONEXT");
        
        Transaction.appendRow();
        Transaction.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        Transaction.setValue("BUSINESSDAYDATE", "20180918");
        Transaction.setValue("TRANSACTIONTYPECODE", "1001");
        Transaction.setValue("WORKSTATIONID", "32");
        Transaction.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        Transaction.setValue("BEGINDATETIMESTAMP", "20180918163036");
        Transaction.setValue("ENDDATETIMESTAMP", "20180918163134");
        Transaction.setValue("DEPARTMENT", "");
        Transaction.setValue("OPERATORQUALIFIER", "");
        Transaction.setValue("OPERATORID", "14196500");
        Transaction.setValue("TRANSACTIONCURRENCY", "VEF");
        Transaction.setValue("TRANSACTIONCURRENCY_ISO", "");
        Transaction.setValue("PARTNERQUALIFIER", "");
        Transaction.setValue("PARTNERID", StellarSAP_JCo.tmpNumTrans.toString());
        
        RetailLineItem.appendRow();
        RetailLineItem.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        RetailLineItem.setValue("BUSINESSDAYDATE", "20180918");
        RetailLineItem.setValue("TRANSACTIONTYPECODE", "1001");
        RetailLineItem.setValue("WORKSTATIONID", "32");
        RetailLineItem.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        RetailLineItem.setValue("RETAILSEQUENCENUMBER", "1");
        RetailLineItem.setValue("RETAILTYPECODE", "2001");
        RetailLineItem.setValue("RETAILREASONCODE", "");
        RetailLineItem.setValue("ITEMIDQUALIFIER", "1");
        RetailLineItem.setValue("ITEMID", "2022046");
        RetailLineItem.setValue("RETAILQUANTITY", "4");
        RetailLineItem.setValue("SALESUNITOFMEASURE", "ST");
        RetailLineItem.setValue("SALESUNITOFMEASURE_ISO", "");
        RetailLineItem.setValue("SALESAMOUNT", "17.24");
        RetailLineItem.setValue("NORMALSALESAMOUNT", "17.24");
        RetailLineItem.setValue("COST", "");
        RetailLineItem.setValue("BATCHID", "");
        RetailLineItem.setValue("SERIALNUMBER", "");
        RetailLineItem.setValue("PROMOTIONID", "");
        RetailLineItem.setValue("ITEMIDENTRYMETHODCODE", "");
        RetailLineItem.setValue("ACTUALUNITPRICE", "4.31");
        RetailLineItem.setValue("UNITS", "");
        RetailLineItem.setValue("SCANTIME", "");
        
        LineItemTax.appendRow();
        LineItemTax.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        LineItemTax.setValue("BUSINESSDAYDATE", "20180918");
        LineItemTax.setValue("TRANSACTIONTYPECODE", "1001");
        LineItemTax.setValue("WORKSTATIONID", "32");
        LineItemTax.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        LineItemTax.setValue("RETAILSEQUENCENUMBER", "1");
        LineItemTax.setValue("TAXSEQUENCENUMBER", "1");
        LineItemTax.setValue("TAXTYPECODE", "4305");
        LineItemTax.setValue("TAXAMOUNT", "2.76");
        
        Tender.appendRow();
        Tender.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        Tender.setValue("BUSINESSDAYDATE", "20180918");
        Tender.setValue("TRANSACTIONTYPECODE", "1001");
        Tender.setValue("WORKSTATIONID", "32");
        Tender.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        Tender.setValue("TENDERSEQUENCENUMBER", "1");
        Tender.setValue("TENDERTYPECODE", "Z401");
        Tender.setValue("TENDERAMOUNT", "20");
        Tender.setValue("TENDERCURRENCY", "VEF");
        Tender.setValue("TENDERCURRENCY_ISO", "");
        Tender.setValue("TENDERID", "");
        Tender.setValue("ACCOUNTNUMBER", "");
        Tender.setValue("REFERENCEID", "");
        
        LineItemExt.appendRow();
        LineItemExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        LineItemExt.setValue("BUSINESSDAYDATE", "20180918");
        LineItemExt.setValue("TRANSACTIONTYPECODE", "1001");
        LineItemExt.setValue("WORKSTATIONID", "32");
        LineItemExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        LineItemExt.setValue("RETAILSEQUENCENUMBER", "1");
        LineItemExt.setValue("FIELDGROUP", "");
        LineItemExt.setValue("FIELDNAME", "ART_DEPART");
        LineItemExt.setValue("FIELDVALUE", "39999");
        
        TransactionExt.appendRow();
        TransactionExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        TransactionExt.setValue("BUSINESSDAYDATE", "20180918");
        TransactionExt.setValue("TRANSACTIONTYPECODE", "1001");
        TransactionExt.setValue("WORKSTATIONID", "32");
        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        //TransactionExt.setValue("RETAILSEQUENCENUMBER", "1");
        TransactionExt.setValue("FIELDGROUP", "");
        TransactionExt.setValue("FIELDNAME", "CLI_RIFCLI");
        TransactionExt.setValue("FIELDVALUE", "V-17726247");
        
        TransactionExt.appendRow();
        TransactionExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        TransactionExt.setValue("BUSINESSDAYDATE", "20180918");
        TransactionExt.setValue("TRANSACTIONTYPECODE", "1001");
        TransactionExt.setValue("WORKSTATIONID", "32");
        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        //TransactionExt.setValue("RETAILSEQUENCENUMBER", "1");
        TransactionExt.setValue("FIELDGROUP", "");
        TransactionExt.setValue("FIELDNAME", "CLI_NOMBRE");
        TransactionExt.setValue("FIELDVALUE", "SIORELA OVALLES");
        
        TransactionExt.appendRow();
        TransactionExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        TransactionExt.setValue("BUSINESSDAYDATE", "20180918");
        TransactionExt.setValue("TRANSACTIONTYPECODE", "1001");
        TransactionExt.setValue("WORKSTATIONID", "32");
        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        //TransactionExt.setValue("RETAILSEQUENCENUMBER", "1");
        TransactionExt.setValue("FIELDGROUP", "");
        TransactionExt.setValue("FIELDNAME", "FIS_INDCNS");
        TransactionExt.setValue("FIELDVALUE", "1");
        
        TransactionExt.appendRow();
        TransactionExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        TransactionExt.setValue("BUSINESSDAYDATE", "20180918");
        TransactionExt.setValue("TRANSACTIONTYPECODE", "1001");
        TransactionExt.setValue("WORKSTATIONID", "32");
        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        //TransactionExt.setValue("RETAILSEQUENCENUMBER", "1");
        TransactionExt.setValue("FIELDGROUP", "");
        TransactionExt.setValue("FIELDNAME", "FIS_SERIMP");
        TransactionExt.setValue("FIELDVALUE", "EOD0003012");
        
        TransactionExt.appendRow();
        TransactionExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        TransactionExt.setValue("BUSINESSDAYDATE", "20180918");
        TransactionExt.setValue("TRANSACTIONTYPECODE", "1001");
        TransactionExt.setValue("WORKSTATIONID", "32");
        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        //TransactionExt.setValue("RETAILSEQUENCENUMBER", "1");
        TransactionExt.setValue("FIELDGROUP", "");
        TransactionExt.setValue("FIELDNAME", "FIS_NRODOC");
        TransactionExt.setValue("FIELDVALUE", "1921");
        
        TransactionExt.appendRow();
        TransactionExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        TransactionExt.setValue("BUSINESSDAYDATE", "20180918");
        TransactionExt.setValue("TRANSACTIONTYPECODE", "1001");
        TransactionExt.setValue("WORKSTATIONID", "32");
        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        //TransactionExt.setValue("RETAILSEQUENCENUMBER", "1");
        TransactionExt.setValue("FIELDGROUP", "");
        TransactionExt.setValue("FIELDNAME", "FIS_RPRT_Z");
        TransactionExt.setValue("FIELDVALUE", "1");
        
        TransactionExt.appendRow();
        TransactionExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        TransactionExt.setValue("BUSINESSDAYDATE", "20180918");
        TransactionExt.setValue("TRANSACTIONTYPECODE", "1001");
        TransactionExt.setValue("WORKSTATIONID", "32");
        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        //TransactionExt.setValue("RETAILSEQUENCENUMBER", "1");
        TransactionExt.setValue("FIELDGROUP", "");
        TransactionExt.setValue("FIELDNAME", "REF_SERIMP");
        TransactionExt.setValue("FIELDVALUE", "");
        
        TransactionExt.appendRow();
        TransactionExt.setValue("RETAILSTOREID", StellarSAP_JCo.gCodLocalidadSAP);
        TransactionExt.setValue("BUSINESSDAYDATE", "20180918");
        TransactionExt.setValue("TRANSACTIONTYPECODE", "1001");
        TransactionExt.setValue("WORKSTATIONID", "32");
        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", "8254");
        //TransactionExt.setValue("RETAILSEQUENCENUMBER", "1");
        TransactionExt.setValue("FIELDGROUP", "");
        TransactionExt.setValue("FIELDNAME", "REF_NRODOC");
        TransactionExt.setValue("FIELDVALUE", "");
        
        // TRANSACTION INFO
        
    }
    
    public void rfcIDOCS(String desti, String date, String wp_plu, String wp_ean, String wpdwgr){
        
        JCoDestination invoke_rfc = null;
        JCoFunction function = null;
        //dbConnection db = new dbConnection(this.odbc);
        
        JCoStructure TestStruc = null;
        JCoTable Transaction = null;
        JCoTable RetailLineItem = null;
        JCoTable LineItemTax = null;
        JCoTable Tender = null;
        JCoTable LineItemExt = null;
        JCoTable TransactionExt = null;   
        
        JCoTable Return = null;
        
        //OBTENGO LA ESTRUCTURA DE LA RFC EN SAP
        try{
            invoke_rfc = JCoDestinationManager.getDestination(destName);
            function = invoke_rfc.getRepository().getFunction("/POSDW/BAPI_POSTR_CREATE");
        } catch (JCoException ex){
            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
            System.exit(0);
        }
        
        while (true) {
            
            StellarSAP_JCo.tmpNumTrans += 1;
            
            FillIDOCInfo(invoke_rfc, function, TestStruc, Transaction, RetailLineItem, LineItemTax, 
            Tender, LineItemExt, TransactionExt, Return);
            
//        
//        //LLENAR SECCIÓN DE TABLAS 
//        //T_EDIDC
//        JCoTable t_edidc = function.getTableParameterList().getTable("T_EDIDC");
//        String  fields = "docnum",
//                table = "IDOCS_DESCARGADOS",
//                where = "fecha_descarga = '" + date + "'",
//                orderBy = fields;
//        ResultSet rs = db.select(fields, table, "", where, orderBy, "");
//        try {
//            while (rs.next()) {
//                t_edidc.appendRow();
//                t_edidc.setValue("DOCNUM", rs.getString("CODIGO_IDOC"));
//                t_edidc.setValue("CREDAT", rs.getString("FECHA_DESCARGA"));
//            }
//        } catch (SQLException ex) {
//            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
//            System.exit(0);
//        }
        
            // EJECUTAR LA FUNCION REMOTA.

            try{
                function.execute(invoke_rfc);
            } catch (JCoException ex){
                System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                System.exit(0);
            }

            Return = function.getTableParameterList().getTable("RETURN");
            
            if (Return.getNumRows() == 0)
            {
                // Marcar Enviada
            }
            else{
                // Error. No procesado.
            }

            for (int i = 0; i < Return.getNumRows(); i++) {
                System.out.println(Return.getString("TYPE"));
                System.out.println(Return.getString("ID"));
                System.out.println(Return.getString("NUMBER"));
                System.out.println(Return.getString("LOG_NO"));
                System.out.println(Return.getString("LOG_MSG_NO"));
                System.out.println(Return.getString("MESSAGE_V1"));
                System.out.println(Return.getString("MESSAGE_V2"));
                System.out.println(Return.getString("MESSAGE_V3"));
                System.out.println(Return.getString("MESSAGE_V4"));
                System.out.println(Return.getString("PARAMETER"));
                System.out.println(Return.getString("ROW"));
                System.out.println(Return.getString("FIELD"));
                System.out.println(Return.getString("SYSTEM"));
            }
            
        }
        
//        //OBTENER DATOS DE LAS VARIABLES EXPORT DE LA RFC
//        //SUBRC
//        //PRODUCTOS
//        //CODIGOS_EAN
//        //DEPARTAMENTOS
//        //GRUPOS
//        //SUBGRUPOS 
//        int subrc = function.getExportParameterList().getInt("SUBRC");
//        JCoTable productos = function.getExportParameterList().getTable("PRODUCTOS");
//        JCoTable codigos_ean = function.getExportParameterList().getTable("CODIGOS_EAN");
//        JCoTable departamentos = function.getExportParameterList().getTable("DEPARTAMENTOS");
//        JCoTable grupos = function.getExportParameterList().getTable("GRUPOS");
//        JCoTable subgrupos = function.getExportParameterList().getTable("SUBGRUPOS");
//        
//        //OBTENER DEVOLUCIÓN DE T_EDIDC
//        t_edidc = function.getTableParameterList().getTable("T_EDIDC");
//        
//        String[] query = new String[ productos.getNumRows() + codigos_ean.getNumRows() + departamentos.getNumRows() + 
//                                    grupos.getNumRows() + subgrupos.getNumRows() + t_edidc.getNumRows()];
//        
//        //DATOS DE WP_PLU
//        fields = "C_CODIGO, C_DESCRI, C_DEPARTAMENTO, C_GRUPO, C_SUBGRUPO, N_TIPOPESO, N_CANTIBUL, " +
//                 "N_COSTOACT, N_COSTOREP, N_PRECIO1, N_PRECIO2, N_PRECIO3, N_IMPUESTO1, C_CODMONEDA, " +
//                 "C_MODELO, C_MARCA, C_OBSERVACION"; 
//        String values = null;
//        for (int i = 0; i < productos.getNumRows(); i++) {
//            productos.setRow(i);
//            values = "'" + productos.getString("C_CODIGO") + "', '" + productos.getString("C_DESCRI") +
//                "', '" + productos.getString("C_DEPARTAMENTO") + "', '" + productos.getString("C_GRUPO") +
//                "', '" + productos.getString("C_SUBGRUPO") + "', '" + productos.getString("N_TIPOPESO") +
//                "', '" + productos.getString("N_CANTIBUL") + "', '" + productos.getString("N_COSTOACT") +
//                "', '" + productos.getString("N_COSTOREP") + "', '" + productos.getString("N_PRECIO1") +
//                "', '" + productos.getString("N_PRECIO2") + "', '" + productos.getString("N_PRECIO3") +
//                "', '" + productos.getString("N_IMPUESTO1") + "', '" + productos.getString("C_CODMONEDA") +
//                "', '" + productos.getString("C_MODELO") + "', '" + productos.getString("C_MARCA") +
//                "', '" + productos.getString("C_OBSERVACION") + "'";
//            
//            query[i] = "INSERT INTO TR_PENDIENTE_PROD (" + fields + ") VALUES (" + values + ")";
//        }
//        
//        //DATOS DE WP_EAN
//        fields = "C_CODNASA, C_CODIGO, C_DESCRIPCION, NU_INTERCAMBIO";
//        values = null;
//        for (int i = 0; i < codigos_ean.getNumRows(); i++) {
//            codigos_ean.setRow(i);
//            values = "'" + codigos_ean.getString("C_CODNASA") + "', '" + codigos_ean.getString("C_CODIGO") +
//                "', '" + codigos_ean.getString("C_DESCRIP") + "', '" + codigos_ean.getString("NU_INTERCA") + "'";
//            
//            query[i] = "INSERT INTO TR_PENDIENTE_CODIGO (" + fields + ") VALUES (" + values + ")";
//        }
//        
//        //DATOS DE DEPARTAMENTOS
//        fields = "C_CODIGO, C_DESCRIPCIO, C_OBSERVACIO";
//        values = null;
//        for (int i = 0; i < departamentos.getNumRows(); i++) {
//            departamentos.setRow(i);
//            values = "'" + departamentos.getString("C_CODIGO") + "', '" + departamentos.getString("C_DESCRI") +
//                "', '" + departamentos.getString("C_OBSERV") + "'";
//            
//            query[i] = "INSERT INTO TR_PEND_DEPARTAMENTOS (" + fields + ") VALUES (" + values + ")";
//        }
//        
//        //DATOS DE GRUPOS
//        fields = "C_CODIGO, C_DESCRIPCIO, C_DEPARTAMENTO, C_OBSERVACIO";
//        values = null;
//        for (int i = 0; i < grupos.getNumRows(); i++) {
//            grupos.setRow(i);
//            values = "'" + grupos.getString("C_CODIGO") + "', '" + grupos.getString("C_DESCRI") +
//                "', '" + grupos.getString("C_DEPART") + "', '" + grupos.getString("C_OBSERV") + "'";
//            
//            query[i] = "INSERT INTO TR_PEND_GRUPOS (" + fields + ") VALUES (" + values + ")";
//        }
//        
//        //DATOS DE SUBGRUPOS
//        fields = "C_CODIGO, C_DESCRIPCIO, C_DEPARTAMENTO, C_GRUPO, C_OBSERVACIO";     
//        values = null;
//        for (int i = 0; i < grupos.getNumRows(); i++) {
//            grupos.setRow(i);
//            values = "'" + grupos.getString("C_CODIGO") + "', '" + grupos.getString("C_DESCRI") +
//                "', '" + grupos.getString("C_DEPART") + "', '" + grupos.getString("C_DIVISI") +
//                "', '" + grupos.getString("C_OBSERV") + "'";
//            
//            query[i] = "INSERT INTO TR_PEND_SUBGRUPOS (" + fields + ") VALUES (" + values + ")";
//        }
//        
//        //ACTUALIZACIÓN DE TABLA CONTROL PARA DESCARGA DE IDOCS EN STELLAR
//        fields = "CODIGO_IDOC, FECHA_DESCARGA";     
//        values = null;
//        for (int i = 0; i < t_edidc.getNumRows(); i++) {
//            t_edidc.setRow(i);
//            values = "'" + t_edidc.getString("DOCNUM") + "', '" + t_edidc.getString("CREDAT") + "'";
//            
//            query[i] = "INSERT INTO IDOCS_DESCARGADOS (" + fields + ") VALUES (" + values + ")";
//        }
//        
//        db.batchInsert(query);
//        System.out.println(json.toJson(new Messages('S', "Descarga de IDOCS realizada con éxito!")));     
        //System.exit(0);
    }
        
    public String concatCerosIzq(String variable, int length){
        //1: Numero de Documento LENGTH (10)
        //2: Numero de Material LENGTH (18)
        //3: Unidad de Almacenamiento LENGTH (20)
        if (variable != null && !variable.isEmpty()) 
            return String.format("%0"+length+"d", Integer.parseInt(variable));
        
        return "";
    }      
    
    public String eliminarCeros(String variable){
        if (variable != null && !variable.isEmpty()) 
            return variable.replaceFirst("^0*", "");
        
        return "";
    }
    
    public String getTodayTimestamp ()
    {
        String timeStamp = null;
        java.sql.Date dt;
 
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(new Date());
        dt = new java.sql.Date(calendar.getTimeInMillis());
        java.sql.Time sqlTime=new java.sql.Time(calendar.getTime().getTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-dd-MM");
        timeStamp = simpleDateFormat.format(dt)+" "+sqlTime.toString();

        return timeStamp;
    }       
    
}