/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dbcon;

import java.io.*;
import java.nio.file.*;
import java.sql.*;
import java.util.*;
import structures.*;
import static JCo.SapConnection.json;
import java.util.concurrent.Executor;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Augusto Gómez
 */
public class dbConnection {

    private String ip = null;
    private String port = null;
    private String db = null;
    private String usr = null;
    private String pwd = null;
    private String url;
    private String odbc = null;
    private Connection conn = null;
    private Statement  stmt = null;
    
    public dbConnection(String ip, String port, String dbname, String user, String pass, String odbc){
        
        this.ip = ip;
        this.port = port;
        this.db = dbname;
        this.usr = user;
        this.pwd = pass;    
        this.odbc = odbc;
        
        // Create a variable for the connection string.
//        url = "jdbc:sqlserver://"+ip+":"+port+";" +
//                "databaseName="+dbname+";user="+user+";password="+pass+";";  
        if(this.odbc.equals("mysql")){
            url = "jdbc:mysql://"+ip+":"+port+"/"+dbname; 
        }else if(this.odbc.toUpperCase().equals("SQLSrv".toUpperCase())){
            url = "jdbc:sqlserver://"+ip+":"+port+";" +
                    "databaseName="+dbname+";user="+user+";password="+pass+";";
        }
    }
    
    public dbConnection(String odbc){
        this.odbc = odbc;
        List<String> list = new ArrayList<>();
        String thisRoute = new File("").getAbsolutePath();

        Path ruta = Paths.get(thisRoute+"\\SAPJCo\\config\\dbconfig.cfg");

//        Abrir archivo dbconfig.cfg
        try {
            list = Files.readAllLines(ruta);
        } catch (IOException ex) {
//            Si el archivo no existe se crea uno por defecto.
            ruta = myFiles.crearConf(ruta);
            try {
                list = Files.readAllLines(ruta);
            } catch (IOException ex1) {
                String message = ex1.getMessage() + ". No se pudo obtener la configuración del archivo dbconfig.cfg!";
                System.out.println(json.toJson(new Messages('E', message)));  
                System.exit(0);      
            }
            
        }
        
        for (ListIterator<String> iter = list.listIterator(); iter.hasNext(); ) {
            String linea = iter.next();

            if (!linea.contains("*") && !linea.isEmpty()) {
                if (linea.contains("dbip")) {
                    this.ip = linea.substring(linea.indexOf("=")+1).trim();
                }else if (linea.contains("dbport")) {
                    this.port = linea.substring(linea.indexOf("=")+1).trim();
                }else if (linea.contains("dbname")) {
                    this.db = linea.substring(linea.indexOf("=")+1).trim();
                }else if (linea.contains("dbuser")) {
                    this.usr = linea.substring(linea.indexOf("=")+1).trim();
                }else if (linea.contains("dbpwd")) {
                    this.pwd = linea.substring(linea.indexOf("=")+1).trim();
                }
            }
        }
        
        // Create a variable for the connection string.
//        url = "jdbc:sqlserver://"+ip+":"+port+";" +
//                "databaseName="+db+";user="+usr+";password="+pwd+";";
//           url = "jdbc:mysql://"+ip+":"+port+"/"+db;  
        if(this.odbc.equals("mysql")){
            url = "jdbc:mysql://"+ip+":"+port+"/"+db; 
        }else if(this.odbc.equals("sqlsrv")){
            url = "jdbc:sqlserver://"+ip+":"+port+";" +
                    "databaseName="+db+";user="+usr+";password="+pwd+";";       
        }
    }   
    
    public void createConnection() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        //try{
//DERBY       Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance
//SQLSERVER   Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();  
//            conn = DriverManager.getConnection(url);
///*MYSQL*/   Class.forName("com.mysql.jdbc.Driver").newInstance(); 
//            conn = DriverManager.getConnection(url, this.usr, this.pwd);

            if(this.odbc.equals("mysql")){
                Class.forName("com.mysql.jdbc.Driver").newInstance(); 
                conn = DriverManager.getConnection(url, this.usr, this.pwd);
            }else if(this.odbc.equalsIgnoreCase("sqlsrv")){
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();  
                conn = DriverManager.getConnection(url);
            }            
            
       // }catch(ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException ex){
           // String message = "Imposible conectar con el Servidor. Verifique la conexión. "+ex.getMessage()+"!";
            //System.out.println(json.toJson(new Messages('E', message)));  
            ////System.exit(0);            
        //}
    }
    
    public void shutdown(){
        try{
            if (stmt != null) {
                stmt.close(); 
            }
            if (conn != null) {
                conn.close();
            }
        }catch(SQLException except){
            except.printStackTrace();
        }
    }
    
    public Boolean ExisteCampoTabla(String pTabla, String pCampo, Connection pCn, String pBDOrEmpty)
    {
        String mSQL;
        String mBD = !pBDOrEmpty.trim().equals("") ? "." + pBDOrEmpty: "";
        mSQL="SELECT Column_Name FROM " + mBD + "INFORMATION_SCHEMA.Columns\n" +
        "WHERE Table_Name = '" + pTabla + "' AND Column_Name = '" + pCampo + "'";
        
        Statement mCmd = null;
        try {
            mCmd = pCn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet mRs = mCmd.executeQuery(mSQL);
            return mRs.first();
        } catch (SQLException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
        public Boolean ExisteCampoTabla(ResultSet pRs, String pCampo)
    {
        
        Object AnyData = null;
        
        try {
            AnyData = pRs.getObject(pCampo);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public ResultSet select(String fields, String tables, String join,
            String where, String order, String group){
        ResultSet result;

        result = null;
        try {
            createConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(this.conn != null){
            try{
                stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                String query = "SELECT " + fields + " FROM " + tables;
                if (join.isEmpty() == false) {
                    query = query + " INNER JOIN " + join;
                }
                if (where.isEmpty() == false) {
                    query = query + " WHERE " + where;
                }
                if (order.isEmpty() == false) {
                    query = query + " ORDER BY " + order;
                }
                if (group.isEmpty() == false) {
                    query = query + " GROUP BY " + group;
                }

                result = stmt.executeQuery(query);  

            }catch(Exception except){
                String message =  "Error al procesar la Consulta. " + except.toString()+"!";
                System.out.println(json.toJson(new Messages('E', message)));
            }
        }
        return result;
    }
    
    public boolean ejecutar(char t_op, String fields, String table, 
            String values, String where) throws SQLException{
        
        try {
            createConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        String exec = null;
        boolean flag = false;
        switch (t_op){
            // 1. Insertar
            case '1':   exec = "INSERT INTO " + table + " ( "+ fields +" ) " + "VALUES ( "+ values +" )";
                        break;
            // 2. Update
            case '2':   String[] campos  = fields.split(",");
                        String[] valores = values.split(",");
                        exec = "UPDATE " + table + " SET ";
                        for (int i = 0; i < campos.length; i++) {
                            exec = exec + campos[i] + " = " + valores[i] + ", ";
                        }
                        exec = exec.substring(0, exec.length()-2);                        
                        if (where.isEmpty() == false){
                            exec = exec + " WHERE " + where;
                        }
                        break;
            // 3. Delete
            case '3':   exec = "DELETE FROM " + table; 
                        if(where.isEmpty() == false)
                                exec += " WHERE " + where;
                        break;
            default: break;
        }
        
        try {
            
            stmt = conn.createStatement();
            conn.setAutoCommit(false);
            stmt.execute(exec);
            conn.commit();
            flag = true;
            
        } catch (SQLException ex) {
            String message =  "Error al procesar la Operación. " + ex.toString()+"!";
            System.out.println(json.toJson(new Messages('E', message)));  
            
            try{
              conn.rollback();
            } catch (SQLException ex1) {
                message =  "No se pudo deshacer la operación " + ex1.toString() + "!";
                System.out.println(json.toJson(new Messages('E', message)));                   
            }            
            
        }finally{ 
            if (stmt != null){  
                stmt.close();
            } 
        }
        
        shutdown();
        return flag;
    }
    
    public boolean batchInsert(String[] query) {
        String message = null;
        
        try {
            createConnection();
            
            stmt = conn.createStatement();
            for (String queries : query) {
                stmt.addBatch(queries);
            }
            conn.setAutoCommit(false);
            stmt.executeBatch();
            conn.commit();
        } catch (SQLException ex) {
            message =  "Error al procesar la Operación. " + ex.toString()+"!";
            System.out.println(json.toJson(new Messages('E', message)));   
            
            try{
              conn.rollback();
            } catch (SQLException ex1) {
                message =  "No se pudo deshacer la operación " + ex1.toString() + "!";
                System.out.println(json.toJson(new Messages('E', message)));                   
            }
            shutdown(); 
            System.exit(0);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            shutdown(); 
        }
        
        return true;
    }

    public Connection getConnection() throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        this.createConnection();
        return this.conn;
    }    
    
//    public static void main(String[] args) {
//       dbConnection cn = new dbConnection("mysql");
//       cn.createConnection();
//       cn.shutdown();
//    }
   
}
