/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbcon;

import java.sql.SQLException;
import javax.sql.RowSet;
import javax.sql.rowset.FilteredRowSet;
import javax.sql.rowset.Predicate;

/**
 *
 * @author programacion
 */

public class StringColumnFilter implements Predicate {
    
    private String FilterValue;

    private int ColumnIndex;

    private String FieldName;

    public StringColumnFilter(String FilterValue, String FieldName) {
        this.FilterValue = FilterValue;
        this.FieldName = FieldName;
        this.ColumnIndex = -1;
    }

    public StringColumnFilter(String FilterValue, int ColumnIndex) {
        this.FilterValue = FilterValue;
        this.ColumnIndex = ColumnIndex;
        this.FieldName = new String();
    }
    
    public boolean evaluate(Object Value, String FieldName) 
    {

        boolean evaluation = true;

        if (FieldName.equalsIgnoreCase(this.FieldName)) {

            if ((Value.toString().equalsIgnoreCase(FilterValue))) {
              evaluation = true;
            } else {
              evaluation = false;
            }

        }

        return evaluation;

    }

    public boolean evaluate(Object Value, int ColumnNumber) 
    {

        boolean evaluation = true;

        if (ColumnIndex == ColumnNumber) {

            if ((Value.toString().equalsIgnoreCase(FilterValue))) {
                evaluation = true;
            } else {
                evaluation = false;
            }

        }

        return evaluation;

    }
    
    public boolean evaluate(RowSet rs) 
    {
        
        if (rs == null) {
          return false;
        }
        
        FilteredRowSet frs = (FilteredRowSet) rs;
        
        boolean evaluation = false;
        
        try {
            
            String Value = new String();
            
            if (this.ColumnIndex > 0)
              Value = frs.getString(this.ColumnIndex);
            else if (!this.FieldName.isEmpty())
              Value = frs.getString(this.FieldName);
            
            if ((Value.equalsIgnoreCase(FilterValue))) {
              evaluation = true;
            }
            
        } catch (SQLException e) {
          return false;
        }
        
        return evaluation;
        
    }
    
}
