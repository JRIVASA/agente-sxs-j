/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structures;

/**
 *
 * @author TRK-HP
 */
public class Messages {
    public char msgtype;
    public String message; 
    
    public Messages(){
        this.msgtype = ' ';
        this.message = "";
    }
    
    public Messages(char type, String message){
        this.msgtype = type;
        this.message = convertToUTF8(message.replace("\n", "<br>"));   
        
    }
    
    // convert from internal Java String format -> UTF-8
    public String convertToUTF8(String s) {
        String out = null;
        try {
            out = new String(s.getBytes("UTF-8"), "ISO-8859-1");
        } catch (java.io.UnsupportedEncodingException e) {
            return null;
        }
        return out;
    }          

}
