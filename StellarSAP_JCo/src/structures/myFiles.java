/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structures;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import static JCo.SapConnection.json;


/**
 *
 * @author HP-TRK
 */
public class myFiles {
    private String ruta;
    
    
    public myFiles(String ruta){
        this.ruta = ruta;
    }
    
    public static Path crearConf(Path ruta) {
        List<String> list = new ArrayList<String>();
        list.add("********************************************************************************");
        list.add("*        Parámetros de conexión al Servidor de la Base de Datos");
        list.add("********************************************************************************");
        list.add("dbip   = 127.0.0.1");
        list.add("dbport = 3306");
        list.add("dbname = logistica_d01");
        list.add("dbuser = root");
        list.add("dbpwd  = root");
        
        String[] thisRoute = ruta.toString().split("dbconfig.cfg");     
        try {            
            new File(thisRoute[0]).mkdirs();
            ruta = Files.write(ruta, list, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
        } catch (IOException ex) {
                String message = ex.getMessage() + ". Error al intentar crear el archivo dbconfig.cfg!";
                System.out.println(json.toJson(new Messages('E', message)));  
                System.exit(0);   
        }
        return ruta;
    }   
    
}
