/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import JCo.SapConnection;
import static JCo.SapConnection.json;
import dbcon.dbConnection;
import com.sap.conn.jco.*;
//import com.sun.rowset.CachedRowSetImpl;
import java.sql.ResultSet;
import java.sql.*;
import java.sql.SQLException;
import javax.sql.rowset.FilteredRowSet;
import com.sun.rowset.FilteredRowSetImpl;
import dbcon.StringColumnFilter;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.lang.StringUtils;
import org.ini4j.Config;
import org.ini4j.Ini;
import org.ini4j.IniPreferences;
import org.ini4j.IniPreferencesFactory;
import structures.Messages;
/**
 *
 * @author TRK-HP
 */
public class StellarSAP_JCo {
    
    private String odbc = null;
    public String op = null;
    
    public static EnCls enc;
    public static FSLogger gLogger;
    public static Double tmpNumTrans = 0.0;
    public static String AppPath;
    public static String gSetup;
    public static String gEquipo;
    public static String gServerStellar;
    public static String gSQLPort;
    public static String gUsuarioSQLStellar;
    public static String gClaveSQLStellar;
    
    public static final String DB_ADM = "VAD10";
    public static final String DB_POS = "VAD20";
    public static final String Stellar = "STELLAR";
    public static final String SAP = "SAP";
    
    public static final String RegistrosPendientesPorCorrida = "-_-_-_-_-";
    public static final String RegistrosPendientesPorLote = "..........";
    public static final String RegistrosPendientesFallidos = "!!!!!!!!!!";
    public static final String RegistrosNuevos = "";
    
    public static String gCnStrADM;
    public static String gCnStrPOS;

    public static String gCodLocalidadStellar;
    public static String gCodLocalidadSAP;
    public static String gMonedaStellarPredeterminada;
    public static String gMonedaSAPPredeterminada;
    public static String gFormaPagoFacturaSAP_SinDetallePago;
    public static String gFormaPagoDevolucionSAP;
    
    public static String gFormaPagoAnticipoWeb;
    public static String gUnidadMedidaSAP;
    
    public static long gCnTimeOutStellar;
    public static long gCmdTimeOutStellar;
    public static long nRegistrosLote;
    public static long nLotesMaxEjecucion;
    public static long nLotesProcesados = 0;
    
    public static long nOperacionesAutorizadasPorLote;
    public static String gAutorizacionSupervisor_CodigoSAP;
    public static HashMap ListaAsociacionOperacionesAutorizadas;
    
    public static HashMap gTaxListCodes;
    public static HashMap gTaxList1;
    public static HashMap gTaxList2;
    public static HashMap gTaxList3;
    public static HashMap ListaAsociacionFormaPago;
    public static HashMap ListaAsociacionMoneda;
    public static HashMap ListaAsociacionBancos;
    public static Boolean ObtenerListasAsociacionBD;
    
    public static Boolean gDebugMode;
    public static Boolean gTestMode;
    public static Boolean gTransferirAutorizaciones;
    public static Boolean gValidarDocumentoFiscal;
    public static Boolean gEnviarTransactionDiscount;
    public static Boolean gTransferirCierres;
    public static Boolean gTransferirDonaciones;
    public static Boolean gTransferirDepositosCP;
    
    public static List<Connection> glCn = null;
    public static Connection gConexion;
    public static SapConnection gCnSAP;
    public static String gCorrelativo;
    
    public static String gModalidad;
    
    public static Boolean DepoprodSync_SoloProductosWeb;
    public static Boolean DepoprodSync_SoloProductosActivos;
    public static Boolean DepoprodSync_SoloExistenciaPositiva;
    public static Boolean DepoprodSync_TransaccionPorLote;
    public static Boolean DepoprodSync_RestarStockNoSincronizado = false;
    
    public static Boolean VentaADM_NumeroTransaccionSinPrefijo = false;
    public static String VentaADM_IndiceNumericoLocalidad;
    public static String VentaADM_POS_ID;
    
    public static Boolean TestSoloVentasADM = false;
    
    public static Boolean SincronizarEnMultiMoneda = false;
    public static String SincronizarEnMultiMoneda_CodMoneda;
    public static String SincronizarEnMultiMoneda_POS_Modalidad_Tasa;
    public static String SincronizarEnMultiMoneda_ADM_Modalidad_Tasa;
    public static String SincronizarenMultiMoneda_Limite_Decimales;
    public static String SincronizarEnMultiMoneda_Param_TransactionCurrency;
    public static String SincronizarEnMultiMoneda_Param_TransactionCurrency_ISO;
    
    public static String sGetIni(String pFilePath, String pSection, String pKey, String pDefaultValue){
        try {
            pFilePath= "file:///" + pFilePath;
            Ini mIni = new Ini(new URL(pFilePath));
            String res = mIni.get(pSection, pKey);
            if (res == null) res = pDefaultValue;
            return res;
        } catch (Exception ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return pDefaultValue;
        }
    }
    
    public static Boolean sWriteIni(String pFilePath, String pSection, String pKey, String pValue){
        try {
            pFilePath= "file:///" + pFilePath;
            URL mUrl = new URL(pFilePath);
            String mFile = mUrl.getFile();
            Ini mIni = new Ini(mUrl);
            mIni.put(pSection, pKey, pValue);
            mIni.store(new File(mFile));
            return true;
        } catch (MalformedURLException ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public StellarSAP_JCo(String[] args){
        
        String opt = null, datoA = null, datoB = null, 
                datoC = null, datoD = null, datoE = null;
        
        args = new String []{ "sqlsrv", "5" };
        /*
        for (int i = 0; i < args.length; i++) {
            switch(i){
                case 0:
                    this.odbc = args[i];
                    break;                
                case 1:
                    opt = args[i];
                    break;
                case 2:
                    datoA = args[i].toUpperCase();
                    break;   
                case 3:
                    datoB = args[i];
                    break;
            }
        }*/
        
//        String javaLibPath = System.getProperty("java.library.path");
//        Map<String, String> envVars = System.getenv();
//        System.out.println(envVars.get("Path"));
//        System.out.println(javaLibPath);
//        for (String var : envVars.keySet()) {
//            System.err.println("examining " + var);
//            if (envVars.get(var).equals(javaLibPath)) {
//                System.out.println(var);
//            }
//        }
        
        File jarFile = null;
        CodeSource codeSource = null;
        
        /*switch (opt){
            case "0"://Test de conexión
                this.testConnection(); 
                break;
            case "1"://Descargar IDOCS desde SAP a Stellar
                
                codeSource = StellarSAP_JCo.class.getProtectionDomain().getCodeSource();
                jarFile = null;
                
                try {
                    jarFile = new File(codeSource.getLocation().toURI().getPath());
                    AppPath=jarFile.getParentFile().getPath();
                } catch (URISyntaxException ex) {
                    Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
                    System.exit(0);
                }
                
                System.out.println(AppPath);
                
                gSetup = AppPath + "\\Setup.ini";
                gLogger = new FSLogger(AppPath + "\\Error.log");
                
                gCodLocalidadSAP = sGetIni(gSetup, SAP, "LOCALIDAD", "");
                
                this.downloadIDOCStoStellar(datoA, datoB, datoC, datoD, datoE);
                
                break;
                
            case "5":*/
                
                codeSource = StellarSAP_JCo.class.getProtectionDomain().getCodeSource();
                jarFile = null;
                
                try {
                    jarFile = new File(codeSource.getLocation().toURI().getPath());
                    AppPath=jarFile.getParentFile().getPath();
                } catch (URISyntaxException ex) {
                    Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
                    System.exit(0);
                }
                
                gSetup = AppPath + "\\Setup.ini";
                gLogger = new FSLogger(AppPath + "\\Error.log");
                
                gModalidad = sGetIni(gSetup, Stellar, "MODALIDAD", "1");
                
                //sWriteIni("file:///" + gSetup, "SERVER", "RemoteServer", "BIG-P-03\\L12014");
                //String TmpVal = sGetIni("file:///" + gSetup, "SERVER", "RemoteServer", "GARABATO");
                
                // Init Base Parameters
                
                gDebugMode = (Val(sGetIni(gSetup, Stellar, "DEBUGMODE", "0")) == 1);
                
                gTestMode = (Val(sGetIni(gSetup, Stellar, "TESTING", "0")) == 1);
                
                TestSoloVentasADM = (Val(sGetIni(gSetup, Stellar, "TEST_SOLO_VENTAS_ADM", "0")) == 1);
                
                gServerStellar = sGetIni(gSetup, Stellar, "SERVER", "");
                
                if (gServerStellar.isEmpty()) { gLogger.EscribirLog("El servidor Stellar no está configurado."); System.exit(0); }
                
                gSQLPort = sGetIni(gSetup, Stellar, "PORT", "1433");
                
                if (gSQLPort.isEmpty()) { gLogger.EscribirLog("El puerto del SQL no está configurado."); System.exit(0); }
                
                try { enc = new EnCls(new SecretKeySpec("GNJKN32KNK2N98L5".getBytes(), "AES")); } catch (Exception ex) { gLogger.EscribirLog(ex, ""); }
                
                gUsuarioSQLStellar = sGetIni(gSetup, Stellar, "USER", "SA");
                gClaveSQLStellar = sGetIni(gSetup, Stellar, "PASSWORD", "");
                
                if (!(gUsuarioSQLStellar.equalsIgnoreCase("SA") && gClaveSQLStellar.isEmpty()))
                {
                    try { 
                        gUsuarioSQLStellar =  enc.Decode(gUsuarioSQLStellar); 
                        gClaveSQLStellar =  enc.Decode(gClaveSQLStellar); 
                    } catch (Exception ex) {}
                }
                
                gCnTimeOutStellar = Long.parseLong(sGetIni(gSetup, Stellar, "CONNECTION_TIMEOUT", "30"));
                gCmdTimeOutStellar = Long.parseLong(sGetIni(gSetup, Stellar, "COMMAND_TIMEOUT", "60"));
                
                gEquipo = getComputerName();
                
                // End Base Parameters
                
                if (gModalidad.equalsIgnoreCase("1"))
                {
                    
                    ObtenerListasAsociacionBD = (Val(sGetIni(gSetup, Stellar, "LISTAS_ASOCIACION_POR_BD", "1")) == 1);
                    
                    gCodLocalidadStellar = sGetIni(gSetup, Stellar, "LOCALIDAD", "");
                    
                    if (gCodLocalidadStellar.isEmpty()) { gLogger.EscribirLog("La localidad Stellar no está configurada."); System.exit(0); }
                    
                    gCodLocalidadSAP = sGetIni(gSetup, SAP, "LOCALIDAD", "");
                    
                    if (gCodLocalidadSAP.isEmpty()) { gLogger.EscribirLog("La localidad SAP no está configurada."); System.exit(0); }
                    
                    if (!ObtenerListasAsociacionBD)
                    {
                        
                        ListaAsociacionFormaPago = ConvertirCadenadeAsociacion(sGetIni(gSetup, Stellar, "LISTA_ASOCIACION_FORMA_PAGO", ""));
                        
                        if (ListaAsociacionFormaPago == null) { gLogger.EscribirLog("Datos de asociaciones de Medios de Pago inválidos o no establecidos"); System.exit(0); }
                        
                        ListaAsociacionMoneda = ConvertirCadenadeAsociacion(sGetIni(gSetup, Stellar, "LISTA_ASOCIACION_MONEDA", ""));
                        
                        if (ListaAsociacionMoneda == null) { gLogger.EscribirLog("Datos de asociaciones de Monedas inválidos o no establecidos"); System.exit(0); }
                        
                        ListaAsociacionBancos = ConvertirCadenadeAsociacion(sGetIni(gSetup, Stellar, "LISTA_ASOCIACION_BANCOS", "*:*"));
                        
                        if (ListaAsociacionBancos == null) { gLogger.EscribirLog("Datos de asociaciones de Bancos inválidos o no establecidos"); System.exit(0); }
                        
                    }
                    
                    ListaAsociacionOperacionesAutorizadas = ConvertirCadenadeAsociacion(sGetIni(gSetup, Stellar, "LISTA_ASOCIACION_OPERACIONES_AUTORIZADAS", ""));
                    
                    gMonedaStellarPredeterminada = sGetIni(gSetup, Stellar, "CODIGO_MONEDA_PREDETERMINADA", "VES");
                    
                    gMonedaSAPPredeterminada = sGetIni(gSetup, SAP, "CODIGO_MONEDA_PREDETERMINADA", "VES");
                    
                    if (gMonedaSAPPredeterminada.isEmpty() || gMonedaStellarPredeterminada.isEmpty()) { 
                        gLogger.EscribirLog("Moneda predeterminada SAP/Stellar no establecida"); 
                        System.exit(0); 
                    }
                    
                    gUnidadMedidaSAP = sGetIni(gSetup, SAP, "UNIDAD_MEDIDA", "UN");
                    gFormaPagoFacturaSAP_SinDetallePago = sGetIni(gSetup, SAP, "FORMA_PAGO_FACTURA_RESUMIDA", "*");
                    gFormaPagoDevolucionSAP = sGetIni(gSetup, SAP, "FORMA_PAGO_DEVOLUCION", "Z301");
                    
                    gAutorizacionSupervisor_CodigoSAP = sGetIni(gSetup, SAP, "CODIGO_AUTORIZACION_SUPERVISOR", "Z100");
                    
                    gTaxListCodes = ConvertirCadenadeAsociacion(sGetIni(gSetup, Stellar, "TAX_LIST", "16.00:4305:12.00:4301|08.00:4302"));
                    
                    gTaxList1 = ConvertirCadenadeAsociacion(sGetIni(gSetup, Stellar, "TAX_LIST_1", "Tax1:16.00|Tax2:12.00"));
                    gTaxList2 = ConvertirCadenadeAsociacion(sGetIni(gSetup, Stellar, "TAX_LIST_2", "Tax1:08.00"));
                    gTaxList3 = ConvertirCadenadeAsociacion(sGetIni(gSetup, Stellar, "TAX_LIST_3", "Tax1:31.00|Tax2:22.00|Tax3:07.00|Tax4:09.00|TaxOld:10.00"));
                    
                    gTransferirAutorizaciones = (Val(sGetIni(gSetup, Stellar, "OPERACIONES_AUTORIZADAS", "0")) == 1);
                    gValidarDocumentoFiscal = (Val(sGetIni(gSetup, Stellar, "VALIDAR_DOCUMENTO_FISCAL", "0")) == 1);
                    gEnviarTransactionDiscount = (Val(sGetIni(gSetup, Stellar, "ENVIAR_DESCUENTO_TOTALIZADO", "0")) == 1);
                    gTransferirCierres = (Val(sGetIni(gSetup, Stellar, "ENVIAR_CIERRES", "0")) == 1);
                    gTransferirDonaciones = (Val(sGetIni(gSetup, Stellar, "ENVIAR_DONACIONES", "1")) == 1);
                    gTransferirDepositosCP = (Val(sGetIni(gSetup, Stellar, "ENVIAR_DEPOSITOS_CP", "1")) == 1);
                    
                    nRegistrosLote = (long) Val(sGetIni(gSetup, Stellar, "REGISTROS_LOTE", "-1"));
                    if (nRegistrosLote <= -1) nRegistrosLote = 50;
                    
                    nLotesMaxEjecucion = (long) Val(sGetIni(gSetup, Stellar, "LOTES_CORRIDA", "1"));
                    if (nLotesMaxEjecucion <= 1) nLotesMaxEjecucion = 10;
                    
                    nOperacionesAutorizadasPorLote = (long) Val(sGetIni(gSetup, Stellar, "REGISTROS_LOTE_OPERACIONES_AUTORIZADAS", "-1"));
                    if (nOperacionesAutorizadasPorLote <= -1) nOperacionesAutorizadasPorLote = 1000;
                    
                    gFormaPagoAnticipoWeb = sGetIni(gSetup, Stellar, "DENOMINACION_PAGO_WEB", "Efectivo");
                    
                    if (gFormaPagoAnticipoWeb.trim().isEmpty()) { 
                        gLogger.EscribirLog("La denominación para el pago de ventas web por aplicación de anticipo no ha sido establecida. Verifique el Setup."); 
                        System.exit(0); 
                    }
                    
                    VentaADM_IndiceNumericoLocalidad = sGetIni(gSetup, Stellar, "INDICE_NUMERICO_LOCALIDAD", "");
                    VentaADM_NumeroTransaccionSinPrefijo = (Val(sGetIni(gSetup, Stellar, "NUMERO_TRANSACCION_SIN_PREFIJO", "0")) == 1);
                    VentaADM_POS_ID = sGetIni(gSetup, Stellar, "VENTA_ADM_POS_ID", "WEB");
                    
                    SincronizarEnMultiMoneda = (Val(sGetIni(gSetup, Stellar, "SINCRONIZAR_EN_MULTIMONEDA", "0")) == 1);
                    
                    SincronizarEnMultiMoneda_CodMoneda = sGetIni(gSetup, Stellar, "SINCRONIZAR_EN_MULTIMONEDA_CODMONEDA", "").replace("'", "");
                    
                    if (SincronizarEnMultiMoneda_CodMoneda.trim().isEmpty())
                    {
                        gLogger.EscribirLog("La moneda para la conversión de valores multimoneda para la sincronización no está especificada. Verifique el Setup."); 
                        System.exit(0); 
                    }
                    
                    SincronizarEnMultiMoneda_POS_Modalidad_Tasa = sGetIni(gSetup, Stellar, "SINCRONIZAR_EN_MULTIMONEDA_POS_MODALIDAD_TASA", "1");
                    
                    // 1 - SI ESTA DISPONIBLE, SACAR EL FACTOR DE MA_PAGOS.n_FactorMonedaAdicional, SINO BUSCARLO EN LA TASA DEL ÚLTIMO PAGO DE MA_DETALLEPAGO EN ESA MONEDA 
                    // EN LA FECHA Y HORA DEL DOCUMENTO. ESTO SERÍA LO MAS PRECISO PARA EL POS
                    
                    // 2 - BUSCAR EN EL HISTORICO DE MONEDAS SEGUN LA FECHA Y HORA DEL DOCUMENTO, DE NO CONSEGUIR NINGUN REGISTRO ANTERIOR A LA FECHA, 
                    // TOMAR LA TASA ACTUAL DE LA MONEDA. IMPLICA ALGO DE RIESGO YA QUE SE DEBE GARANTIZAR MANTENER BIEN EL HISTORICO DE TODAS LAS MONEDAS DIARIAMENTE
                    // EN CADA TIENDA, SINO SE ESTARIAN ENVIANDO LAS TRANSACCIONES A TASA VIEJA. LA CONSISTENCIA DEPENDE DE LA ORGANIZACION.
                    
                    // 3 - BUSCAR EN EL HISTORICO DE MONEDAS SEGUN LA FECHA Y HORA DEL DOCUMENTO PERO SOLO TOMAR EL REGISTRO SI EXISTE EN ESE DIA, NO HACIA ATRAS, SINÓ,
                    // HACER FALLBACK CON TODO LO QUE HACE LA OPCION 1. AGREGA LA POSIBILIDAD OPCIONAL DE PODER INTERVENIR LA TASA CONTROLANDO EL HISTORICO
                    // Y PODER REENVIAR LOS REGISTROS CON LA TASA MODIFICADA, MAS NO REQUIERE UN ESTRICTO CONTROL DEL HISTORICO.
                    
                    if (!(SincronizarEnMultiMoneda_POS_Modalidad_Tasa.equals("1") 
                    || SincronizarEnMultiMoneda_POS_Modalidad_Tasa.equals("2") 
                    || SincronizarEnMultiMoneda_POS_Modalidad_Tasa.equals("3"))) 
                    {
                        gLogger.EscribirLog("Opcion de Busqueda de Tasa para transacciones POS invalida. Verifique el Setup."); 
                        System.exit(0);                         
                    }
                    
                    SincronizarEnMultiMoneda_ADM_Modalidad_Tasa = sGetIni(gSetup, Stellar, "SINCRONIZAR_EN_MULTIMONEDA_ADM_MODALIDAD_TASA", "1");
                    
                    // 1 - BUSCAR EN EL HISTORICO DE MONEDAS SEGUN LA FECHA Y HORA DEL DOCUMENTO, DE NO CONSEGUIR NINGUN REGISTRO ANTERIOR A LA FECHA, 
                    // TOMAR LA TASA ACTUAL DE LA MONEDA. A NIVEL DE FACTURACION ADMINISTRATIVA LO MAS CONVENIENTE ES CONTROLAR EL HISTORICO.
               
                    // 2 - IR A BUSCAR EN MA_DETALLE PAGO TAL COMO LA OPCION 1 PARA EL POS (SOLO SE PUEDE UTILIZAR SI LA TIENDA TIENE VENTAS POS)
                    
                    // 3 - LA MISMA OPCION 3 QUE PARA EL POS (SOLO SE PUEDE UTILIZAR SI LA TIENDA TIENE VENTAS POS)
                    
                    if (!(SincronizarEnMultiMoneda_ADM_Modalidad_Tasa.equals("1") 
                    || SincronizarEnMultiMoneda_ADM_Modalidad_Tasa.equals("2") 
                    || SincronizarEnMultiMoneda_ADM_Modalidad_Tasa.equals("3"))) 
                    {
                        gLogger.EscribirLog("Opcion de Busqueda de Tasa para transacciones ADM invalida. Verifique el Setup."); 
                        System.exit(0);                         
                    }
                    
                    Double TmpDbl = Val(sGetIni(gSetup, Stellar, "SINCRONIZAR_EN_MULTIMONEDA_LIMITE_DECIMALES", "2"));
                    
                    if (TmpDbl < 0.0) TmpDbl = 2.0;
                    
                    SincronizarenMultiMoneda_Limite_Decimales = String.valueOf(TmpDbl.intValue());
                    
                    SincronizarEnMultiMoneda_Param_TransactionCurrency = sGetIni(gSetup, Stellar, "SINCRONIZAR_EN_MULTIMONEDA_PARAM_TRANSACTION_CURRENCY", "");
                    
                    if (SincronizarEnMultiMoneda_Param_TransactionCurrency.trim().isEmpty()) 
                        SincronizarEnMultiMoneda_Param_TransactionCurrency = SincronizarEnMultiMoneda_CodMoneda;
                     
                    SincronizarEnMultiMoneda_Param_TransactionCurrency_ISO = sGetIni(gSetup, Stellar, "SINCRONIZAR_EN_MULTIMONEDA_PARAM_TRANSACTION_CURRENCY_ISO", "");
                    
                }
                else if (gModalidad.equalsIgnoreCase("2") || gModalidad.equalsIgnoreCase("2.1"))
                {
                    
                    gCodLocalidadStellar = sGetIni(gSetup, Stellar, "LOCALIDAD", "*");
                    
                    if (gCodLocalidadStellar.isEmpty()) { gLogger.EscribirLog("La localidad Stellar no está configurada."); System.exit(0); }
                    
                    gCodLocalidadSAP = sGetIni(gSetup, SAP, "LOCALIDAD", "*");
                    
                    if (gCodLocalidadSAP.isEmpty()) { gLogger.EscribirLog("La localidad SAP no está configurada."); System.exit(0); }
                    
                    DepoprodSync_SoloProductosActivos = (Val(sGetIni(gSetup, Stellar, "SOLO_PRODUCTOS_ACTIVOS", "0")) == 1);
                    
                    DepoprodSync_SoloProductosWeb = (Val(sGetIni(gSetup, Stellar, "SOLO_PRODUCTOS_WEB", "1")) == 1);
                    
                    DepoprodSync_SoloExistenciaPositiva =  (Val(sGetIni(gSetup, Stellar, "SOLO_EXISTENCIA_POSITIVA", "1")) == 1);
                    
                    DepoprodSync_TransaccionPorLote = (Val(sGetIni(gSetup, Stellar, "TRANSACCION_POR_LOTE", "1")) == 1);
                    
                    DepoprodSync_RestarStockNoSincronizado = (Val(sGetIni(gSetup, Stellar, "RESTAR_STOCK_NO_SINCRONIZADO", "1")) == 1);
                    
                    gTestMode = (Val(sGetIni(gSetup, Stellar, "TESTING", "0")) == 1);
                    
                }
                
                List glCn = Arrays.asList(new Connection[]{ gConexion });
                List User = Arrays.asList(new String[]{ gUsuarioSQLStellar });
                List Password = Arrays.asList(new String[]{ gClaveSQLStellar });
                List NewUser = Arrays.asList(new String[]{ "" });
                List NewPassword = Arrays.asList(new String[]{ "" });
                
                if (EstablecerConexion(glCn, gServerStellar, gSQLPort, DB_POS, gCnTimeOutStellar, 
                User, Password, true, NewUser, NewPassword)){
                    
                    gConexion = (Connection) glCn.get(0);
                    gUsuarioSQLStellar = User.get(0).toString();
                    gClaveSQLStellar = Password.get(0).toString();
                    
                    if (!NewUser.get(0).toString().isEmpty() || !NewPassword.get(0).toString().isEmpty())
                    {
                        sWriteIni(gSetup, Stellar, "USER", NewUser.get(0).toString());
                        sWriteIni(gSetup, Stellar, "PASSWORD", NewPassword.get(0).toString());
                    }
                }
                
                if (gConexion != null){
                    IniciarAgente();
                }
                
                /*
                //this.TestStellarConnection("BIG-P-03\\L12014");
                break;

        }*/
        
        this.op = opt;
        
    }
    
    public int HowManyIn(String pBaseString, String pSearchText){
        
        String str = pBaseString;
        String findStr = pSearchText;
        int lastIndex = 0;
        int count = 0;

        while(lastIndex != -1){

            lastIndex = str.indexOf(findStr, lastIndex);

            if(lastIndex != -1){
                count++;
                lastIndex += findStr.length();
            }
            
        }
        
        return count;
        
    }
    
    public static String Correlativo(Connection pCn, String pCampo)
    {

        ResultSet mRs = null;
        String mSQL;
        String mValor;

        try {

            mSQL = "SELECT * FROM MA_CORRELATIVOS WHERE (CU_Campo = '" + pCampo + "')";

            Statement Cmd = pCn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            mRs = Cmd.executeQuery(mSQL);

            if (mRs.first()) {
                mRs.updateInt("nu_Valor", mRs.getInt("nu_Valor") + 1);
                DecimalFormat fmt = new DecimalFormat(mRs.getNString("cu_Formato"));
                mValor = fmt.format(mRs.getInt("nu_Valor"));
                mRs.updateRow();
            } else {
                mRs.moveToInsertRow();
                mRs.updateNString("cu_Campo", pCampo);
                mRs.updateNString("cu_Formato", "0000000000");
                mRs.updateInt("nu_Valor", 1); 
                mRs.insertRow();
                mRs.first();
                DecimalFormat fmt = new DecimalFormat(mRs.getNString("cu_Formato"));
                mValor = fmt.format(mRs.getInt("nu_Valor"));
            }

            return mValor;

        } catch(Exception Ex) {

            gLogger.EscribirLog (Ex, "");
            return "";
        }
        
    }
    
    public static String ReglaDeNegocio(Connection pCn, String pCampo, String pValorDefault)
    {
        
        ResultSet mRs = null;
        String mSQL;
        String mValor;
        
        try {
            
            mSQL = "SELECT * FROM [VAD10].[DBO].[MA_REGLASDENEGOCIO] WHERE (Campo = '" + pCampo + "')";
            
            Statement Cmd = pCn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            
            mRs = Cmd.executeQuery(mSQL);
            
            if (mRs.first()) {
                mValor = mRs.getString("Valor");
            } else {
                mValor = pValorDefault;
            }
            
            return mValor;
            
        } catch(Exception Ex) {
            
            gLogger.EscribirLog (Ex, "");
            return pValorDefault;
        }
        
    }
    
    public static String ReglaDeNegocio(Connection pCn, String pCampo)
    {
        return ReglaDeNegocio(pCn, pCampo, new String());
    }
    
    public static Boolean ExisteCampoTabla(String pCampo, String pTabla, Connection pCn, String pBDOrEmpty)
    {
        String mSQL;
        String mBD = !pBDOrEmpty.trim().equals("") ? pBDOrEmpty + ".": "";
        mSQL="SELECT Column_Name FROM " + mBD + "INFORMATION_SCHEMA.Columns\n" +
        "WHERE Table_Name = '" + pTabla + "' AND Column_Name = '" + pCampo + "'";
        
        Statement mCmd = null;
        try {
            mCmd = pCn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet mRs = mCmd.executeQuery(mSQL);
            return mRs.first();
        } catch (Exception ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public static Boolean ExisteCampoTabla(String pCampo, String pTabla, Connection pCn)
    {
        return ExisteCampoTabla(pCampo, pTabla, pCn, "");
    }
    
    public static Boolean ExisteCampoTabla(ResultSet pRs, String pCampo)
    {
        
        Object AnyData = null;
        
        try {
            AnyData = pRs.getObject(pCampo);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public Boolean PrepararBD()
    {
        
        Boolean result = true; Boolean exec = false;
        long mCmdTOut_Original = 0; Boolean Reintentando = false;
        
        mCmdTOut_Original = gCmdTimeOutStellar;
        
    Retry:
        
        for (int i = 0; i <= 0; i++) {
            
        try {
            
        gCmdTimeOutStellar = 0;
        
        if (!ExisteCampoTabla("cs_Sync_SxS", "MA_PAGOS", gConexion, DB_POS)) {
            exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_PAGOS]" + "\n" + 
            "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_PAGOS_DEFAULT_cs_Sync_SxS] DEFAULT ('')");
            
            result = result && true;
        }
        
        if (!ExisteCampoTabla("cs_Sync_SxS", "MA_VENTAS", gConexion, DB_ADM)) {
            exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD10].[DBO].[MA_VENTAS]" + "\n" + 
            "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_VENTAS_DEFAULT_cs_Sync_SxS] DEFAULT ('')");
            
            result = result && true;
        }
        
        if (gTransferirAutorizaciones) {
            if (!ExisteCampoTabla("cs_Sync_SxS", "TR_OPERACIONES_AUTORIZADAS", gConexion, DB_POS)) {
                exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[TR_OPERACIONES_AUTORIZADAS]" + "\n" + 
                "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [TR_OPERACIONES_AUTORIZADAS_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                result = result && true;
            }
        }
        
        if (gTransferirCierres) {
            if (!ExisteCampoTabla("cs_Sync_SxS", "MA_CIERRES", gConexion, DB_POS)) {
                exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_CIERRES]" + "\n" + 
                "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_CIERRES_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                result = result && true;
            }
        }
        
        if (gTransferirDonaciones) {
            if (!ExisteCampoTabla("cs_Sync_SxS", "MA_DONACIONES", gConexion, DB_POS)) {
                exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_DONACIONES]" + "\n" + 
                "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_DONACIONES_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                result = result && true;
            }
        }
        
        if (gTransferirDepositosCP) {
            if (!ExisteCampoTabla("cs_Sync_SxS", "MA_DEPOSITOS", gConexion, DB_POS)) {
                exec = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).execute("ALTER TABLE [VAD20].[DBO].[MA_DEPOSITOS]" + "\n" + 
                "ADD cs_Sync_SxS NVARCHAR(20) NOT NULL CONSTRAINT [MA_DEPOSITOS_DEFAULT_cs_Sync_SxS] DEFAULT ('')");

                result = result && true;
            }
        }
        
        return result;
        
        } catch (Exception Ex) {
            StellarSAP_JCo.gLogger.EscribirLog(Ex, "PrepararBD:");
            if (!Reintentando) {
                Reintentando = true;
                continue Retry;
            } else {
            // Sin Exito.
        }
        
        }

    }
        return result;
    
    }
    
    public void IniciarAgente(){
        
        if (gModalidad.equalsIgnoreCase("1"))
        {
            
            if (SincronizarEnMultiMoneda)
            {
                String mCodMonedaExiste = "";
                ResultSet m;
                
                try
                {
                    
                    m = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
                    "SELECT * FROM\n" +
                    "VAD10.DBO.MA_MONEDAS WHERE c_CodMoneda = '" + SincronizarEnMultiMoneda_CodMoneda + "' \n" +
                    ""
                    );

                    if (m.first())
                    {
                        mCodMonedaExiste = isDBNull(m.getObject("c_CodMoneda"), "").toString();
                    }                    
                    
                    if (mCodMonedaExiste.isEmpty()) { gLogger.EscribirLog("La moneda especificada para la conversión es inválida o no existe en la base de datos."); System.exit(0); }
                    
                } catch (Exception ex)
                {
                    gLogger.EscribirLog(ex, "Error verificando moneda de conversión en base de datos."); System.exit(0);
                }
                
            }
            
            if (ObtenerListasAsociacionBD)
            {
                
                String mLista;
                ResultSet x;
                
                try {
                
                mLista = "";
                
                x = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_Codigo + ':' + CodigoExterno AS 'data()' FROM\n" +
                "VAD10.DBO.MA_BANCOS WHERE CodigoExterno <> ''\n" +
                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista"
                );

                if (x.first())
                {
                    mLista = isDBNull(x.getObject("Lista"), "").toString();
                }
                
                ListaAsociacionBancos = ConvertirCadenadeAsociacion(mLista);
                
                if (ListaAsociacionBancos == null) { gLogger.EscribirLog("Datos de asociaciones de Bancos inválidos o no establecidos"); System.exit(0); }
                
                mLista = "";
                
                x = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_CodMoneda + ';' + c_CodDenomina + ':' + CodigoExterno AS 'data()' FROM\n" +
                "VAD10.DBO.MA_DENOMINACIONES WHERE CodigoExterno <> ''\n" +
                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista"
                );
                
                if (x.first())
                {
                    mLista = isDBNull(x.getObject("Lista"), "").toString();
                }
                
                ListaAsociacionFormaPago = ConvertirCadenadeAsociacion(mLista);
                
                if (ListaAsociacionFormaPago == null) { gLogger.EscribirLog("Datos de asociaciones de Formas de Pago inválidos o no establecidos"); System.exit(0); }
                
                mLista = "";
                
                x = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
                "SELECT REPLACE(SUBSTRING((SELECT '|' + c_CodMoneda + ':' + CodigoExterno AS 'data()' FROM\n" +
                "VAD10.DBO.MA_MONEDAS WHERE CodigoExterno <> ''\n" +
                "FOR XML PATH ('')), 2, 9999), ' |', '|') AS Lista"
                );
                
                if (x.first())
                {
                    mLista = isDBNull(x.getObject("Lista"), "").toString();
                }
                
                ListaAsociacionMoneda = ConvertirCadenadeAsociacion(mLista);
                
                if (ListaAsociacionMoneda == null) { gLogger.EscribirLog("Datos de asociaciones de Monedas inválidos o no establecidos"); System.exit(0); }
                
                } catch (Exception ex)
                {
                    gLogger.EscribirLog(ex, "Error al obtener datos de asociaciones por BD."); System.exit(0);
                }
                
            }
            
            gCorrelativo = Correlativo(gConexion, "Corrida_Agente_SAP_Trans");
            
            if (!gCorrelativo.isEmpty())
            {
                
                Boolean result = PrepararBD();
                
                if (InitializeSAPConnection("1"))
                {
                    if (gCnSAP.ABAP_QueryEnabled())
                    {
                        if (gCnSAP.Inicializar_BAPI_Ventas_Devoluciones())
                        {
                            if (TestSoloVentasADM)
                            {
                                SincronizarVentasADM();
                            }
                            else
                            {
                                SincronizarVentas();
                                SincronizarAutorizaciones();
                                SincronizarCierres();
                                SincronizarVentasADM();
                            }
                        }
                    }
                }

                if(gCnSAP != null) gCnSAP.unregister();

                if (InitializeSAPConnection("2")){
                    if (gCnSAP.ABAP_QueryEnabled() && (!TestSoloVentasADM))
                    {
                        if (gCnSAP.Inicializar_Func_Donaciones())
                        {
                            SincronizarDonaciones();
                        }
                        if (gCnSAP.Inicializar_Func_DepositosCP())
                        {
                            SincronizarDepositosCP();
                        }
                    }
                }
                
                if(gCnSAP != null) gCnSAP.unregister();
                
            }
            
        }
        else if (gModalidad.equalsIgnoreCase("2") || gModalidad.equalsIgnoreCase("2.1"))
        {
            
            if (InitializeSAPConnection("2"))
            {
                if (gCnSAP.ABAP_QueryEnabled())
                {
                    if (gCnSAP.Inicializar_Func_Consulta_Stock())
                    {
                        SincronizarStock();
                    }
                }
            }
            
            if(gCnSAP != null) gCnSAP.unregister();
            
        }
        
    }
    
    // VENTAS Y DEVOLUCIONES POS (TRANSACCIONES POS DM)
    
    public Boolean HayDatosPendientes(String pEstatus){
        
        try {

        ResultSet RsDatosPendientes; Boolean exec;

        String ExcluirVNF = "";

        if (ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", gConexion)) {
            ExcluirVNF = " AND bDocNoFiscal = 0";
        }
        
        RsDatosPendientes = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).
        executeQuery("SELECT COUNT(*) AS Registros FROM MA_PAGOS WHERE cs_Sync_SxS = '" 
        + pEstatus + "'" + ExcluirVNF);
        RsDatosPendientes.first();
        
        return (RsDatosPendientes.getLong("Registros") > 0);
        
        } catch (Exception Ex) { return false; }
        
    }
    
    public Boolean MarcarRegistros(String pEstatus, Boolean pFallidos)
    {
        
        try {

            int RegistrosAfectados = 0; String pEstatusPrevio = ""; String mRegistrosXLote = "";

            String ExcluirVNF = ""; String SQLMarcaje = "";

            if (ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", gConexion)) {
                ExcluirVNF = " AND bDocNoFiscal = 0";
            }

            switch (pEstatus) {
                case RegistrosPendientesPorCorrida:
                    pEstatusPrevio = "'" + RegistrosNuevos + "'";
                    break;
                case RegistrosPendientesPorLote:
                    if (pFallidos)
                        pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                    else {
                        pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                        mRegistrosXLote = "AND (c_Concepto + c_Numero + c_Sucursal) IN (" + "\n" + 
                        "SELECT TOP (" + nRegistrosLote + ") (c_Concepto + c_Numero + c_Sucursal) FROM MA_PAGOS" + "\n" + 
                        "WHERE cs_Sync_SxS = " + pEstatusPrevio + ExcluirVNF + " ORDER BY ID)";
                    }
                    break;
                default: // Guardados con Exito.
                    pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                    break;
            }

            SQLMarcaje = "UPDATE MA_PAGOS SET cs_Sync_SxS = '" + pEstatus + "'" + "\n" + 
            "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ")" + mRegistrosXLote + ExcluirVNF;

            if (gDebugMode) gLogger.EscribirLog(SQLMarcaje);

            RegistrosAfectados = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeUpdate(SQLMarcaje);
            
            return (RegistrosAfectados > 0);

        } catch (Exception Ex) { return false; }

    }
    
    public Boolean MarcarRegistros(String pEstatus)
    {
        return MarcarRegistros(pEstatus, false);
    }
    
    public Boolean ObtenerDatosVentas(List<FilteredRowSet> pRsVentas, List<FilteredRowSet> pRsItems, 
    List<FilteredRowSet> pRsDetPagos, List<FilteredRowSet> pRsImpuestos, 
    List<FilteredRowSet> pRsDocumentosFiscales, Boolean pReprocesarFallidos)
    {
        
        ResultSet pRsDataVentas = null;
        ResultSet pRsDataItems = null; 
        ResultSet pRsDataDetPagos = null;
        ResultSet pRsDataImpuestos = null;
        ResultSet pRsDataDocumentosFiscales = null;
        
        try {

            pRsVentas.set(0, new FilteredRowSetImpl()) ;
            pRsItems.set(0, new FilteredRowSetImpl()) ;
            pRsDetPagos.set(0, new FilteredRowSetImpl()) ;
            pRsImpuestos.set(0, new FilteredRowSetImpl()) ;
            pRsDocumentosFiscales.set(0, new FilteredRowSetImpl()) ;
            
            String mSQL = null;

            String ExcluirVNF = new String();
            
            String mEstatus = new String();
            
            if (pReprocesarFallidos)
                mEstatus = RegistrosPendientesFallidos;
            else
                mEstatus = RegistrosPendientesPorLote;
            
            if (ExisteCampoTabla("bDocNoFiscal", "MA_PAGOS", gConexion))
                ExcluirVNF = " AND bDocNoFiscal = 0";

            mSQL = "SELECT DOC.*, isNULL(DON.n_Total, 0) AS n_MontoDonacion, " + "\n" +
            "(DOC.c_Concepto + DOC.c_Numero + DOC.c_Sucursal) AS TransID " + "\n" + 
            "FROM [VAD20].[DBO].[MA_PAGOS] DOC" + "\n" + 
            "LEFT JOIN [VAD20].[DBO].[MA_DONACIONES] DON" + "\n" + 
            "ON DOC.c_Concepto = 'VEN' " + "\n" + 
            "AND (DOC.c_Concepto + DOC.c_Numero + DOC.c_Sucursal) = ('VEN' + isNULL(DON.c_Numero_Rel, '_N_A') + isNULL(DON.c_Sucursal, '_N_A'))" + "\n" + 
            "WHERE (DOC.cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + " " +
            "ORDER BY DOC.ID";

            pRsDataVentas = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataVentas.first();
            pRsVentas.get(0).populate(pRsDataVentas);

            /*mSQL = "SELECT *, (c_Concepto + c_Numero) AS TransID " + "\n" +
            "FROM [VAD20].[DBO].[MA_TRANSACCION] WHERE (c_Concepto + c_Numero) IN (" + "\n" + 
            "SELECT (c_Concepto + c_Numero) FROM MA_PAGOS" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")" + " " +
            "ORDER BY ID";*/
            
            mSQL = "" +
            "SELECT c_Concepto, c_Numero, c_Localidad, MIN(n_Linea) AS n_Linea, Cod_Principal, Cod_Principal AS Codigo, " + "\n" + 
            "SUM(Cantidad) AS Cantidad, ABS(ROUND(Precio, 2, 0)) AS Precio, " + "\n" + 
            "ABS(ROUND(n_PrecioReal, 2, 0)) AS n_PrecioReal, " + "\n" + 
            "SUM(Subtotal) AS Subtotal, SUM(Impuesto) AS Impuesto, SUM(Total) AS Total, " + "\n" + 
            "Impuesto1, SUM(Descuento) AS Descuento, " + "\n" + 
            "(c_Concepto + c_Numero + c_Localidad) AS TransID " + "\n" + 
            "FROM [VAD20].[DBO].[MA_TRANSACCION] WHERE (c_Concepto + c_Numero + c_Localidad) IN (" + "\n" + 
            "SELECT (c_Concepto + c_Numero + c_Sucursal) FROM MA_PAGOS" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ") " + "\n" +
            "GROUP BY Cod_Principal, ABS(ROUND(Precio, 2, 0)), ABS(ROUND(n_PrecioReal, 2, 0)), " + "\n" + 
            "Impuesto1, c_Concepto, c_Numero, c_Localidad HAVING ABS(ROUND(SUM(Cantidad), 8, 0)) >= 0.0001 " + "\n" + 
            "ORDER BY MIN(ID) "; // NO ENVIAR PRODUCTOS REINTEGRADOS CUYA CANTIDAD QUEDO EN CERO.
            
            pRsDataItems = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataItems.first();
            pRsItems.get(0).populate(pRsDataItems);

            mSQL = "SELECT *, (c_Factura + c_Localidad) AS TransID " + "\n" +
            "FROM [VAD20].[DBO].[MA_DETALLEPAGO] WHERE (c_Factura + c_Localidad) IN (" + "\n" + 
            "SELECT (c_Numero + c_Sucursal) FROM MA_PAGOS" + "\n" + 
            "WHERE (c_Concepto = 'VEN') AND (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")" + " " +
            "ORDER BY c_Factura, " + "\n" + 
            "(CASE WHEN c_CodDenominacion = 'Efectivo' THEN 1 ELSE 0 END) DESC," + "\n" + 
            "(CASE WHEN c_CodMoneda = '" + gMonedaStellarPredeterminada + "' THEN 1 ELSE 0 END) DESC, ID";

            pRsDataDetPagos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataDetPagos.first();
            pRsDetPagos.get(0).populate(pRsDataDetPagos);

            mSQL = "SELECT *, (c_Concepto + c_Numero + c_Localidad) AS TransID " + "\n" +
            "FROM [VAD20].[DBO].[MA_PAGOS_IMPUESTOS] WHERE (c_Concepto + c_Numero + c_Localidad) IN (" + "\n" + 
            "SELECT (c_Concepto + c_Numero + c_Sucursal) FROM MA_PAGOS" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")";
            
            pRsDataImpuestos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataImpuestos.first();
            pRsImpuestos.get(0).populate(pRsDataImpuestos);
            
            mSQL = "SELECT *, (cu_DocumentoTipo + cu_DocumentoStellar + cu_Localidad) AS TransID " + "\n" +
            "FROM [VAD20].[DBO].[MA_DOCUMENTOS_FISCAL] " + "\n" + 
            "WHERE (cu_DocumentoTipo + cu_DocumentoStellar + cu_Localidad) IN (" + "\n" + 
            "SELECT (c_Concepto + c_Numero + c_Sucursal) FROM MA_PAGOS" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")";
            
            pRsDataDocumentosFiscales = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataDocumentosFiscales.first();
            pRsDocumentosFiscales.get(0).populate(pRsDataDocumentosFiscales);
            
            return pRsDataVentas.first();
            
        } catch (Exception ex) {
            gLogger.EscribirLog(ex, "Error al buscar registros de ventas pendientes por procesar.");
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    public void ConstruirRegistrosDeVentas(Boolean pReprocesarFallidos)
    {
        
        String mUltDoc = new String(); Double mUltTot = 0.0; String mUltFec = new String();
        java.util.Date DocTime = new java.util.Date();
        
        try {
            
            FilteredRowSet RsVentas = null, RsItems = null, RsDetPagos = null,
            RsImpuestos = null, RsDocumentosFiscales = null;
            
            List<FilteredRowSet> lRsVentas = Arrays.asList(new FilteredRowSet[]{ RsVentas }), 
            lRsItems = Arrays.asList(new FilteredRowSet[]{ RsItems }),
            lRsDetPagos = Arrays.asList(new FilteredRowSet[]{ RsDetPagos }),
            lRsImpuestos = Arrays.asList(new FilteredRowSet[]{ RsImpuestos }),
            lRsDocumentosFiscales = Arrays.asList(new FilteredRowSet[]{ RsDocumentosFiscales });
            
            ResultSet RsVentaFinal = null; ResultSet RsItemsFinal = null; ResultSet RsDetPagFinal = null;
            
            Double MontoExento; Object tmpVal; Double Cont;
            
            Double mTasaOrigen = 1.0;
            Double mTasaDestino = 1.0;
            Double mTasaConversion = 1.0; 
            
            Integer mLimiteDec = 0;
            
            //if (pReprocesarFallidos)
                //MarcarRegistros(RegistrosPendientesPorLote, true);
            
            if (ObtenerDatosVentas(lRsVentas, lRsItems, lRsDetPagos, lRsImpuestos, lRsDocumentosFiscales, pReprocesarFallidos))
            {
                if (true) //ObtenerDataSourceVentasFinal(RsVentaFinal, RsItemsFinal, RsDetPagFinal))
                {
                    
                    if (gTestMode) { // PRUEBAS
                        
                    RsVentas = lRsVentas.get(0);
                    RsItems = lRsItems.get(0);
                    RsDetPagos = lRsDetPagos.get(0);
                    RsImpuestos = lRsImpuestos.get(0);
                    RsDocumentosFiscales = lRsDocumentosFiscales.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsVentas.getMetaData();
                    
                    while (RsVentas.next())
                    {
                        
                        TestContinue: {
                        
                        mUltDoc = RsVentas.getString("TransID");
                        mUltTot = RsVentas.getDouble("n_Total");
                        mUltFec = RsVentas.getString("f_Fecha");
                        
                        try { 
                            assert (!mUltDoc.equals("VEN002000144")); 
                        } catch (AssertionError ae) 
                        { 
                            System.out.println(ae.toString()); 
                        }
                        
                        /*for (int i = 1; i <= RsVentas.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsVentas.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsVentas.getObject(i).toString());
                        }*/
                        
                        HashMap<String, Object> tmpRowTR = null;
                        HashMap<String, Object> tmpRowTRExt = null;
                        HashMap<String, Object> tmpRowDP = null;
                        HashMap<String, Object> tmpRowItm = null;
                        HashMap<String, Object> tmpRowTax = null;
                        HashMap<String, Object> tmpRowItmVoid = null;
                        
                        HashMap<String, HashMap<String, Object>> TestStruc = null;
                        HashMap<String, HashMap<String, Object>>  Transaction = null;
                        HashMap<String, HashMap<String, Object>>  TransactionDiscount = null;
                        HashMap<String, HashMap<String, Object>>  RetailLineItem = null;
                        HashMap<String, HashMap<String, Object>>  LineItemVoid = null;
                        HashMap<String, HashMap<String, Object>>  LineItemDiscount = null;
                        HashMap<String, HashMap<String, Object>>  LineItemTax = null;
                        HashMap<String, HashMap<String, Object>>  Tender = null;
                        HashMap<String, HashMap<String, Object>>  LineItemExt = null;
                        HashMap<String, HashMap<String, Object>>  TransactionExt = null;
                        HashMap<String, HashMap<String, Object>>  Additionals = null;

                        JCoTable Return = null;
                        
                        Transaction = new HashMap<String, HashMap<String, Object>>();
                        TransactionDiscount = new HashMap<String, HashMap<String, Object>>();
                        RetailLineItem = new HashMap<String, HashMap<String, Object>>();
                        LineItemVoid = new HashMap<String, HashMap<String, Object>>();
                        LineItemDiscount = new HashMap<String, HashMap<String, Object>>();
                        LineItemTax = new HashMap<String, HashMap<String, Object>>();
                        Tender = new HashMap<String, HashMap<String, Object>>();
                        LineItemExt = new HashMap<String, HashMap<String, Object>>();
                        TransactionExt = new HashMap<String, HashMap<String, Object>>();
                        Additionals = new HashMap<String, HashMap<String, Object>>();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTRExt = new HashMap<String, Object>();
                        tmpRowDP = new HashMap<String, Object>();
                        tmpRowTax = new HashMap<String, Object>();
                        tmpRowItm = new HashMap<String, Object>();
                        
                        Transaction.put("1", tmpRowTR);
                        
                        if (!gCodLocalidadSAP.isEmpty())
                            tmpRowTR.put("RETAILSTOREID", gCodLocalidadSAP);
                        else
                            tmpRowTR.put("RETAILSTOREID",  RsVentas.getString("c_Sucursal"));
                        tmpRowTR.put("BUSINESSDAYDATE", dtFmt.format(RsVentas.getDate("f_Fecha")));
                        tmpRowTR.put("TRANSACTIONTYPECODE", "1001");
                        tmpRowTR.put("WORKSTATIONID", RsVentas.getString("c_Caja"));
                        tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", RsVentas.getString("c_Numero")
                        .substring(RsVentas.getString("c_Caja").length()));
                        
                        if (RsVentas.getDouble("t_Venta") > 0)
                            tmpRowTR.put("BEGINDATETIMESTAMP", Left(dtFmtLong.format(DateAddInterval(DateTimeFrom(RsVentas.getDate("f_Fecha"), RsVentas.getDate("f_Hora")), 
                            Calendar.SECOND, ((int)((-1.0) * RsVentas.getDouble("t_Venta") * 60D)))), 14));
                        else
                            tmpRowTR.put("BEGINDATETIMESTAMP", Left(dtFmtLong.format(DateTimeFrom(RsVentas.getDate("f_Fecha"), RsVentas.getDate("f_Hora"))), 14));
                        
                        tmpRowTR.put("ENDDATETIMESTAMP", dtFmtLong.format(DateTimeFrom(RsVentas.getDate("f_Fecha"), RsVentas.getDate("f_Hora"))));
                        tmpRowTR.put("DEPARTMENT", new String());
                        tmpRowTR.put("OPERATORQUALIFIER", new String());
                        tmpRowTR.put("OPERATORID", RsVentas.getString("c_Usuario"));
                        tmpRowTR.put("TRANSACTIONCURRENCY", gMonedaSAPPredeterminada);
                        tmpRowTR.put("TRANSACTIONCURRENCY_ISO", new String());
                        tmpRowTR.put("PARTNERQUALIFIER", new String());
                        tmpRowTR.put("PARTNERID", new String());
                        
                        // DATOS ADICIONALES DE LA TRANSACCIÓN.
                        
                        int nPosTRExt = 0;
                        
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        TransactionExt.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                        tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTR.put("FIELDGROUP", new String());
                        tmpRowTR.put("FIELDNAME", "CLI_RIFCLI");
                        tmpRowTR.put("FIELDVALUE", RsVentas.getString("c_RIF"));

                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        TransactionExt.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                        tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTR.put("FIELDGROUP", new String());
                        tmpRowTR.put("FIELDNAME", "CLI_NOMBRE");
                        tmpRowTR.put("FIELDVALUE", RsVentas.getString("c_Desc_Cliente"));
                        
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                        tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTR.put("FIELDGROUP", new String());
                        tmpRowTR.put("FIELDNAME", "StellarDocID");
                        tmpRowTR.put("FIELDVALUE", "POS_" + RsVentas.getString("TransID"));
                        
//                        tmpRowTR.appendRow();
//                        tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
//                        tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
//                        tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
//                        tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
//                        tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
//                        tmpRowTR.put("FIELDGROUP", "");
//                        tmpRowTR.put("FIELDNAME", "REF_NRODOC");
//                        tmpRowTR.put("FIELDVALUE", "");
                        
                        ResultSet DatosFiscales_DocumentoRelacion = null;
                        
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV"))
                            DatosFiscales_DocumentoRelacion = DatosFiscalesDocRel("VEN" + RsVentas.getString("cs_Documento_Rel"));
                        
                        RsDocumentosFiscales.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID"));

                        if (RsDocumentosFiscales.first())
                        {
                            
//                            FieldData = RsDocumentosFiscales.getMetaData();
//                            RsDocumentosFiscales.beforeFirst();
//                            while (RsDocumentosFiscales.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsDocumentosFiscales.getObject(i).toString());
//                                }
//                            }
                            
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "FIS_INDCNS");
                            tmpRowTR.put("FIELDVALUE", "1");
                            
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "FIS_SERIMP");
                            tmpRowTR.put("FIELDVALUE", RsDocumentosFiscales.getString("cu_SerialImpresora"));
                            
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "FIS_NRODOC");
                            tmpRowTR.put("FIELDVALUE", RsDocumentosFiscales.getString("cu_DocumentoFiscal"));
                            
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "FIS_RPRT_Z");
                            if (ExisteCampoTabla(RsDocumentosFiscales, "cu_ZFiscal"))
                                tmpRowTR.put("FIELDVALUE", RsDocumentosFiscales.getString("cu_ZFiscal"));
                            else
                                tmpRowTR.put("FIELDVALUE", new String());
                            
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "REF_SERIMP");
                            if (DatosFiscales_DocumentoRelacion == null)
                                tmpRowTR.put("FIELDVALUE", new String());
                            else
                                tmpRowTR.put("FIELDVALUE", DatosFiscales_DocumentoRelacion.getString("cu_SerialImpresora"));
                            
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "REF_NRODOC");
                            if (DatosFiscales_DocumentoRelacion == null)
                                tmpRowTR.put("FIELDVALUE", new String());
                            else
                                tmpRowTR.put("FIELDVALUE", DatosFiscales_DocumentoRelacion.getString("cu_DocumentoFiscal"));
                            
                        }
                        else
                        {
                            
                            if (gValidarDocumentoFiscal && !RsVentas.getBoolean("bs_Impresa")) 
                            {
                                // Controlar Error de Registro de Ventas.
                                MarcarFallido(RsVentas.getString("TransID"));
                                break TestContinue;
                            }
                                
                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "FIS_INDCNS");
                            tmpRowTR.put("FIELDVALUE", new String());

                         nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "FIS_SERIMP");
                            tmpRowTR.put("FIELDVALUE", new String());

                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "FIS_NRODOC");
                            tmpRowTR.put("FIELDVALUE", new String());

                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "FIS_RPRT_Z");
                            tmpRowTR.put("FIELDVALUE", new String());

                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "REF_SERIMP");
                            tmpRowTR.put("FIELDVALUE", new String());

                        nPosTRExt++;
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTR.put(String.valueOf(nPosTRExt), tmpRowTR);
                        
                            tmpRowTR.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTR.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTR.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTR.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTR.put("FIELDGROUP", new String());
                            tmpRowTR.put("FIELDNAME", "REF_NRODOC");
                            tmpRowTR.put("FIELDVALUE", new String());
                            
                        }
                        
                        /*
                        RsImpuestos.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID") );

                        if (RsImpuestos.first())
                        {
//                            FieldData = RsImpuestos.getMetaData();
//                            RsImpuestos.beforeFirst();
//                            while (RsImpuestos.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsImpuestos.getObject(i).toString());
//                                }
//                            }
                        }
                        else
                        {
                            
                        }
                        */
                        
                        RsDetPagos.setFilter(new StringColumnFilter(RsVentas.getString("c_Numero"), "TransID") );
                        
                        int nPosTender = 0;
                        
                        if (RsDetPagos.first())
                        {
                            
//                            FieldData = RsDetPagos.getMetaData();
//                            RsDetPagos.beforeFirst();
//                            while (RsDetPagos.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsDetPagos.getObject(i).toString());
//                                }
//                            }
                            
                            RsDetPagos.beforeFirst();
                            
                            while (RsDetPagos.next())
                            {
                                
                            nPosTender++;
                            tmpRowDP = new HashMap<String, Object>();
                            Tender.put(String.valueOf(nPosTender), tmpRowDP);
                            
                            tmpRowDP.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowDP.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowDP.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowDP.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowDP.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowDP.put("TENDERSEQUENCENUMBER", String.valueOf(nPosTender));
                            
                            String TenderTypeCode = null;
                            
                            // Primero buscar Llave compuesta de moneda y denominación.
                            if (ListaAsociacionFormaPago.containsKey(
                            RsDetPagos.getString("c_CodMoneda") + ";" + RsDetPagos.getString("c_CodDenominacion")))
                            {
                                TenderTypeCode = ListaAsociacionFormaPago.get(
                                RsDetPagos.getString("c_CodMoneda") + ";" + RsDetPagos.getString("c_CodDenominacion")).toString();
                            } else if (ListaAsociacionFormaPago.containsKey(
                            RsDetPagos.getString("c_CodDenominacion"))){ // sino existe se intenta buscar solo por código de denominación.
                                TenderTypeCode = ListaAsociacionFormaPago.get(
                                RsDetPagos.getString("c_CodDenominacion")).toString();
                            }
                            
                            // En vez de marcar como fallidos, enviemosle lo que ellos consideren como efectivo
                            // de manera predeterminada para no trancar el proceso.
                            if (TenderTypeCode == null)
                                TenderTypeCode = "3101"; // Efectivo
                            
                            if (TenderTypeCode == null)
                            {
                                // Controlar Error Registro de Ventas.
                                MarcarFallido(RsVentas.getString("TransID"));
                                break TestContinue;
                            }
                            
                            tmpRowDP.put("TENDERTYPECODE", TenderTypeCode);
                            tmpRowDP.put("TENDERAMOUNT", String.valueOf(RsDetPagos.getDouble("n_Monto")));
                            tmpRowDP.put("TENDERCURRENCY", gMonedaSAPPredeterminada);
                            tmpRowDP.put("TENDERCURRENCY_ISO", new String());
                            tmpRowDP.put("TENDERID", RsDetPagos.getString("c_CodDenominacion"));
                            tmpRowDP.put("ACCOUNTNUMBER", new String());
                            tmpRowDP.put("REFERENCEID", RsDetPagos.getString("c_Numero"));
                            
                            }
                            
                        }
                        else
                        {
                            MarcarFallido(RsVentas.getString("TransID"));
                            break TestContinue;
                        }
                        
                        RsItems.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID") );
                        
                        int nPosDcto = 0;
                        int nPosImp = 0;
                        
                        if (RsItems.first())
                        {
                            
//                            FieldData = RsItems.getMetaData();
//                            RsItems.beforeFirst();
//                            while (RsItems.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsItems.getObject(i).toString());
//                                }
//                            }
                            
                            RsItems.beforeFirst();
                            
                            HashMap DocItmList = new HashMap();
                            HashMap DocItmList_Info;
                            
                            while(RsItems.next())
                            {
                                
                            tmpRowItm = new HashMap<String, Object>();
                            RetailLineItem.put(RsItems.getString("n_Linea"), tmpRowItm);
                            
                            tmpRowItm.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowItm.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowItm.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowItm.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowItm.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowItm.put("RETAILSEQUENCENUMBER", RsItems.getString("n_Linea"));
                            
                            if(RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN") && RsItems.getDouble("Cantidad") > 0)
                            {
                                if (!DocItmList.containsKey(RsItems.getString("Cod_Principal") + "|" + String.valueOf(Round(RsItems.getDouble("Precio"), 2))))
                                {
                                    DocItmList_Info = new HashMap();
                                    DocItmList_Info.put("Cod", RsItems.getString("Cod_Principal"));
                                    DocItmList_Info.put("Current_Ln", RsItems.getString("n_Linea"));
                                    DocItmList_Info.put("Cant_Acum", RsItems.getDouble("Cantidad"));
                                    DocItmList_Info.put("Price", RsItems.getDouble("Precio"));
                                    HashMap All_Ln_Info = new HashMap();
                                    HashMap Ln_Info = new HashMap();
                                    Ln_Info.put("Ln", DocItmList_Info.get("Current_Ln"));
                                    Ln_Info.put("Remaining_Cant", DocItmList_Info.get("Cant_Acum"));
                                    All_Ln_Info.put(RsItems.getString("n_Linea"), Ln_Info);
                                    HashMap All_R_Info = new HashMap();
                                    DocItmList_Info.put("All_Ln_Info", All_Ln_Info);
                                    DocItmList_Info.put("All_R_Info", All_R_Info);
                                    DocItmList.put(RsItems.getString("Cod_Principal") + "|" + String.valueOf(Round(RsItems.getDouble("Precio"), 2)), DocItmList_Info);
                                } else {
                                    DocItmList_Info = ((HashMap<String, Object>) DocItmList.get(RsItems.getString("Cod_Principal") + "|" + String.valueOf(Round(RsItems.getDouble("Precio"), 2))));
                                    DocItmList_Info.replace("Cant_Acum", ((Double) DocItmList_Info.get("Cant_Acum")) + RsItems.getDouble("Cantidad"));
                                    HashMap Ln_Info = new HashMap();
                                    Ln_Info.put("Ln", RsItems.getString("n_Linea"));
                                    Ln_Info.put("Remaining_Cant", RsItems.getDouble("Cantidad"));
                                    if (((HashMap)(DocItmList_Info.get("All_Ln_Info"))).size() <= 0)
                                        DocItmList_Info.replace("Current_Ln", RsItems.getString("n_Linea"));
                                    ((HashMap)(DocItmList_Info.get("All_Ln_Info"))).put(RsItems.getString("n_Linea"), Ln_Info);
                                }
                            } else if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN") && RsItems.getDouble("Cantidad") < 0)
                            {
                                                                
                                DocItmList_Info = ((HashMap) DocItmList.get(RsItems.getString("Cod_Principal") + "|" + String.valueOf(Round(RsItems.getDouble("Precio"), 2))));
                                
                                HashMap All_Ln_Info = ((HashMap)(DocItmList_Info.get("All_Ln_Info")));
                                HashMap All_R_Info = ((HashMap)(DocItmList_Info.get("All_R_Info")));
                                
                                Double Cant_R = Math.abs(RsItems.getDouble("Cantidad"));
                                DocItmList_Info.replace("Cant_Acum", Round(((Double) DocItmList_Info.get("Cant_Acum") - Cant_R), 6));
                                
                                while (Cant_R != 0)
                                {
                                    
                                    String Current_Ln = DocItmList_Info.get("Current_Ln").toString();
                                    /*try { assert (All_Ln_Info.containsKey(Current_Ln)); } catch ( AssertionError ae ) 
                                    { 
                                        if(ae != null)
                                            System.out.println(ae); 
                                    };*/
                                    Double Cant_Ln = 0D;
                                    if (All_Ln_Info.containsKey(Current_Ln))
                                        Cant_Ln = Round((Double)((HashMap) All_Ln_Info.get(Current_Ln)).get("Remaining_Cant"), 6);
                                    
                                    //if (!All_R_Info.containsKey(Current_Ln))
                                    //{
                                        //
                                        tmpRowItmVoid = new HashMap<String, Object>();
                                        LineItemVoid.put(RsItems.getString("n_Linea"), tmpRowItmVoid);

                                        tmpRowItmVoid.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                                        tmpRowItmVoid.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                                        tmpRowItmVoid.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                                        tmpRowItmVoid.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                                        tmpRowItmVoid.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                                        tmpRowItmVoid.put("RETAILSEQUENCENUMBER", RsItems.getString("n_Linea"));
                                        
                                        tmpRowItmVoid.put("VOIDEDLINE", Current_Ln);
                                        tmpRowItmVoid.put("VOIDFLAG", "X");
                                        
                                        //All_R_Info.put(Current_Ln, Current_Ln);
                                        All_R_Info.putIfAbsent(Current_Ln, Current_Ln);
                                    //}
                                    
                                    /*try { assert (Round((Cant_R - Cant_Ln), 6) != 0D); } catch ( AssertionError ae ) 
                                    { 
                                        System.out.println(ae); 
                                    };*/
                                    
                                    if (Cant_R < Cant_Ln && Cant_Ln != 0D)
                                    {
                                        ((HashMap) All_Ln_Info.get(Current_Ln)).replace("Remaining_Cant", Round((Cant_Ln - Cant_R), 6));
                                        Cant_R = 0D;
                                    } else {
                                        
                                        Cant_R = Round((Cant_R - Cant_Ln), 6);
                                        All_Ln_Info.remove(Current_Ln);
                                        if (All_Ln_Info.size() > 0)
                                        {
                                            Current_Ln = ((HashMap) All_Ln_Info.values().toArray()[0]).get("Ln").toString();
                                            DocItmList_Info.replace("Current_Ln", Current_Ln);
                                        } else {
                                            Cant_R = 0D;
                                        }
                                    }
                                    
                                }
                                
                            }
                            
                            if (RsItems.getDouble("Cantidad") < 0 &&
                            RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                tmpRowItm.put("RETAILTYPECODE", "2901");
                            else if (RsItems.getDouble("Cantidad") > 0 &&
                            RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV"))
                                tmpRowItm.put("RETAILTYPECODE", "2902");
                            else
                                if (RsItems.getDouble("Impuesto") == 0)
                                    if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                        tmpRowItm.put("RETAILTYPECODE", "2201");
                                    else
                                        tmpRowItm.put("RETAILTYPECODE", "2202");
                                else
                                    if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                        tmpRowItm.put("RETAILTYPECODE", "2001");
                                    else
                                        tmpRowItm.put("RETAILTYPECODE", "2801");
                            
                            tmpRowItm.put("RETAILREASONCODE", new String());
                            tmpRowItm.put("ITEMIDQUALIFIER", new String());
                            tmpRowItm.put("ITEMID", RsItems.getString("Codigo"));
                            tmpRowItm.put("RETAILQUANTITY", String.valueOf(RsItems.getDouble("Cantidad")));
                            tmpRowItm.put("SALESUNITOFMEASURE", gUnidadMedidaSAP);
                            tmpRowItm.put("SALESUNITOFMEASURE_ISO", new String());
                            tmpRowItm.put("SALESAMOUNT", String.valueOf(RsItems.getDouble("Total")));
                            tmpRowItm.put("NORMALSALESAMOUNT", String.valueOf(RsItems.getDouble("Total")));
                            tmpRowItm.put("COST", new String());
                            tmpRowItm.put("BATCHID", new String());
                            tmpRowItm.put("SERIALNUMBER", new String());
                            tmpRowItm.put("PROMOTIONID", new String());
                            tmpRowItm.put("ITEMIDENTRYMETHODCODE", new String());
                            tmpRowItm.put("ACTUALUNITPRICE", String.valueOf(RsItems.getDouble("Precio")));
                            tmpRowItm.put("UNITS", new String());
                            tmpRowItm.put("SCANTIME", new String());
                            
                            ResultSet Cat = BuscarCategoriasProducto(RsItems.getString("Cod_Principal"));
                            
                            tmpRowItm = new HashMap<String, Object>();
                            LineItemExt.put(RsItems.getString("n_Linea"), tmpRowItm);
                            
                            tmpRowItm.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowItm.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowItm.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowItm.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowItm.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowItm.put("RETAILSEQUENCENUMBER", RetailLineItem.get(RsItems.getString("n_Linea")).get("RETAILSEQUENCENUMBER"));
                            tmpRowItm.put("FIELDGROUP", new String());
                            tmpRowItm.put("FIELDNAME", "ART_DEPART");
                            if (Cat == null)
                                tmpRowItm.put("FIELDVALUE", new String());
                            else
                                tmpRowItm.put("FIELDVALUE", Cat.getString("c_Departamento"));
                            
                            if (RsItems.getDouble("Impuesto") != 0)
                            {
                                
                                nPosImp++;
                            tmpRowTax = new HashMap<String, Object>();
                            tmpRowTax.put(String.valueOf(nPosImp), tmpRowTax);
                                
                                tmpRowTax.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                                tmpRowTax.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                                tmpRowTax.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                                tmpRowTax.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                                tmpRowTax.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                                tmpRowTax.put("RETAILSEQUENCENUMBER", RsItems.getString("n_Linea"));
                                tmpRowTax.put("TAXSEQUENCENUMBER", String.valueOf(nPosImp));
                                
                                String TaxTypeCode = null;
                                DecimalFormatSymbols taxFmtSym = new DecimalFormatSymbols();
                                taxFmtSym.setDecimalSeparator('.');
                                taxFmtSym.setGroupingSeparator(',');
                                DecimalFormat taxFmt = new DecimalFormat("00.00", taxFmtSym);
                                
                                if (gTaxListCodes != null)
                                {
                                    if (gTaxListCodes.containsKey(taxFmt.format(RsItems.getDouble("Impuesto1"))))
                                        TaxTypeCode = gTaxListCodes.get(taxFmt.format(RsItems.getDouble("Impuesto1"))).toString();
                                } else 
                                {
                                    if (gTaxList1.containsValue(taxFmt.format(RsItems.getDouble("Impuesto1"))))
                                        TaxTypeCode = "4301";
                                    else if (gTaxList2.containsValue(taxFmt.format(RsItems.getDouble("Impuesto1"))))
                                        TaxTypeCode = "4301";
                                    else if (gTaxList3.containsValue(taxFmt.format(RsItems.getDouble("Impuesto1"))))
                                        TaxTypeCode = "4303";
                                }
                                
                                if (TaxTypeCode == null)
                                {
                                    // Controlar Error Registro de Ventas.
                                    MarcarFallido(RsVentas.getString("TransID"));
                                    break TestContinue;
                                }
                                
                                tmpRowTax.put("TAXTYPECODE", TaxTypeCode);
                                tmpRowTax.put("TAXAMOUNT", String.valueOf(RsItems.getDouble("Impuesto")));
                                
                            }
                            
                            if (RsItems.getDouble("Descuento") > 0)
                            {
                                
                                nPosDcto++;
                                tmpRowItm = new HashMap<String, Object>();
                                tmpRowItm.put(String.valueOf(nPosDcto), tmpRowItm);
                                
                                tmpRowItm.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                                tmpRowItm.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                                tmpRowItm.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                                tmpRowItm.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                                tmpRowItm.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                                tmpRowItm.put("RETAILSEQUENCENUMBER", RsItems.getString("n_Linea"));
                                tmpRowItm.put("DISCOUNTSEQUENCENUMBER", String.valueOf(nPosDcto));
                                tmpRowItm.put("DISCOUNTTYPECODE", "3101");
                                tmpRowItm.put("DISCOUNTREASONCODE", new String());
                                tmpRowItm.put("REDUCTIONAMOUNT", String.valueOf(RsItems.getDouble("Descuento")));
                                tmpRowItm.put("STOREFINANCIALLEDGERACCOUNTID", new String());
                                tmpRowItm.put("DISCOUNTID", new String());
                                tmpRowItm.put("DISCOUNTIDQUALIFIER", new String());
                                tmpRowItm.put("BONUSBUYID", new String());
                                tmpRowItm.put("OFFERID", new String());
                                
                            }
                            
                            }
                            
                        }
                        else
                        {
                            // Controlar Error Registro de Ventas.
                            MarcarFallido(RsVentas.getString("TransID"));
                            break TestContinue;
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.

                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        try{
                            
                            //gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                            
                            int x = (int) (1 + (Math.random() * 10));
                            
                            if (x <= 2)
                            {
                                throw new JCoException(0, "sendfail");
                            }
                            
                            TransaccionEnviada = true;
                            
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de transacciones.");
                            //System.exit(0);
                        }

                        //Return = gCnSAP.fTransVentas.getTableParameterList().getTable("RETURN");
                        
//                        
//                        int ReturnRowCount = Return.getNumRows();
//                        
//                        if (ReturnRowCount <= 0)
//                        {
//                            // Envío exitoso sin ningún mensaje en particular.
//                            TransaccionEnviada = true;
//                        }
//                        else
//                        {
//                            
//                            TransaccionEnviada = true;
//                            
//                            // Revisar Mensajes.
//                            
//                            String RetFullLog = "";
//                            String RetLog = "";
//                            
//                            for (int i = 0; i < ReturnRowCount; i++) 
//                            {
//                                
//                                Return.setRow(i);
//                                
//                                String RetLnLog = "";
//                                
//                                if (gDebugMode)
//                                {
//                                    RetLnLog = GetLines() +
//                                    "TYPE => " + Return.getString("TYPE") + GetTab() +
//                                    "ID => " + Return.getString("ID") + GetTab() +
//                                    "NUMBER => " + Return.getString("NUMBER") + GetTab() +
//                                    "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
//                                    "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
//                                    "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
//                                    "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
//                                    "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
//                                    "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
//                                    "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
//                                    "ROW => " + Return.getString("ROW") + GetTab() +
//                                    "FIELD => " + Return.getString("FIELD") + GetTab() +
//                                    "SYSTEM => " + Return.getString("SYSTEM");
//                                    RetFullLog += RetLnLog;
//                                }
//                                
//                                if (Return.getString("TYPE").equalsIgnoreCase("E") ||
//                                Return.getString("TYPE").equalsIgnoreCase("A")) {
//                                    
//                                    TransaccionEnviada = false;
//                                    
//                                    if (!gDebugMode)
//                                        RetLnLog = GetLines() +
//                                        "TYPE => " + Return.getString("TYPE") + GetTab() +
//                                        "ID => " + Return.getString("ID") + GetTab() +
//                                        "NUMBER => " + Return.getString("NUMBER") + GetTab() +
//                                        "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
//                                        "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
//                                        "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
//                                        "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
//                                        "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
//                                        "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
//                                        "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
//                                        "ROW => " + Return.getString("ROW") + GetTab() +
//                                        "FIELD => " + Return.getString("FIELD") + GetTab() +
//                                        "SYSTEM => " + Return.getString("SYSTEM");
//                                    
//                                    RetLog += RetLnLog;
//                                    
//                                }
//                                
//                            }
//                            
//                        }
                        
                        if (TransaccionEnviada) 
                        {
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE MA_PAGOS SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_Concepto + c_Numero) = '" + RsVentas.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("Transacción [" + RsVentas.getString("TransID") + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }

                        // Próxima Transacción.
                    
                    }
                    
                    }
                            
                    }
                    else // Real Processing - Procesamiento Real - ConstruirRegistrosDeVentas:
                    {
                    
                    RsVentas = lRsVentas.get(0);
                    RsItems = lRsItems.get(0);
                    RsDetPagos = lRsDetPagos.get(0);
                    RsImpuestos = lRsImpuestos.get(0);
                    RsDocumentosFiscales = lRsDocumentosFiscales.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsVentas.getMetaData();

                    SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                    SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                    SimpleDateFormat dtFmtLog = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    
                    while (RsVentas.next())
                    {
                        
                        NextTrans: {
                        
                        mUltDoc = RsVentas.getString("TransID");
                        mUltTot = RsVentas.getDouble("n_Total");
                        mUltFec = RsVentas.getString("f_Fecha");
                        
                        DocTime = DateTimeFrom(RsVentas.getDate("f_Fecha"), RsVentas.getDate("f_Hora"));
                        
                        mTasaDestino = BuscarTasaTransaccionPOS(RsVentas, DocTime, RsVentas.getString("TransID"));
                        
                        if (mTasaDestino <= 0)
                        {
                            gLogger.EscribirLog(mUltDoc + " [" + dtFmtLog.format(DocTime) + "] - " + "Hubo un error al obtener la tasa de conversión a moneda alterna " + 
                            "o es inválida para la fecha del documento (Valor Encontrado = " + mTasaDestino.toString() + ").");
                            // Controlar Error de Registro de Ventas.
                            MarcarFallido(RsVentas.getString("TransID"));
                            break NextTrans;
                        }
                        
                        mTasaConversion = (mTasaOrigen / mTasaDestino);
                        
                        /*for (int i = 1; i <= RsVentas.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsVentas.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsVentas.getObject(i).toString());
                        }*/
                        
                        JCoStructure TestStruc = null;
                        JCoTable Transaction = null;
                        JCoTable TransactionDiscount = null;
                        JCoTable RetailLineItem = null;
                        JCoTable LineItemVoid = null;
                        JCoTable LineItemDiscount = null;
                        JCoTable LineItemTax = null;
                        JCoTable Tender = null;
                        JCoTable LineItemExt = null;
                        JCoTable TransactionExt = null;
                        JCoTable Additionals = null;

                        JCoTable Return = null;
                        
                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_LOCKWAIT", 0);

                        TestStruc = gCnSAP.fTransVentas.getImportParameterList().getStructure("I_SOURCEDOCUMENTLINK");

                        TestStruc.setValue("KEY", new String());
                        TestStruc.setValue("TYPE", new String());
                        TestStruc.setValue("LOGICALSYSTEM", new String());

                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_SOURCEDOCUMENTLINK", TestStruc);
                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_COMMIT", "X");
                        
                        Transaction = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTION");
                        TransactionDiscount = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTIONDISCOUNT");
                        RetailLineItem = gCnSAP.fTransVentas.getTableParameterList().getTable("RETAILLINEITEM");
                        LineItemVoid = gCnSAP.fTransVentas.getTableParameterList().getTable("LINEITEMVOID");
                        LineItemDiscount = gCnSAP.fTransVentas.getTableParameterList().getTable("LINEITEMDISCOUNT");
                        LineItemTax = gCnSAP.fTransVentas.getTableParameterList().getTable("LINEITEMTAX");
                        Tender = gCnSAP.fTransVentas.getTableParameterList().getTable("TENDER");
                        LineItemExt = gCnSAP.fTransVentas.getTableParameterList().getTable("LINEITEMEXT");
                        TransactionExt = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTIONEXT");
                        Additionals = gCnSAP.fTransVentas.getTableParameterList().getTable("ADDITIONALS");
                        
                        Transaction.clear();
                        TransactionDiscount.clear();
                        RetailLineItem.clear();
                        LineItemVoid.clear();
                        LineItemDiscount.clear();
                        LineItemTax.clear();
                        Tender.clear();
                        LineItemExt.clear();
                        TransactionExt.clear();
                        Additionals.clear();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        // DATOS DEL DOCUMENTO - CABECERO
                        
                        Transaction.appendRow();
                        
                        /*if (!gCodLocalidadSAP.isEmpty())
                            Transaction.setValue("RETAILSTOREID", Left(gCodLocalidadSAP, 10));
                        else
                            Transaction.setValue("RETAILSTOREID",  Left(RsVentas.getString("c_Sucursal"), 10));*/
                        
                        if (gCodLocalidadSAP.equalsIgnoreCase("*") && gCodLocalidadStellar.equalsIgnoreCase("*"))
                            Transaction.setValue("RETAILSTOREID", Left(RsVentas.getString("C_Sucursal"), 10));
                        else
                            Transaction.setValue("RETAILSTOREID", Left(gCodLocalidadSAP, 10));
                        
                        Transaction.setValue("BUSINESSDAYDATE", Left(dtFmt.format(RsVentas.getDate("f_Fecha")), 8));
                        Transaction.setValue("TRANSACTIONTYPECODE", "1001");
                        Transaction.setValue("WORKSTATIONID", Left(RsVentas.getString("c_Caja"), 10));
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                            Transaction.setValue("TRANSACTIONSEQUENCENUMBER", Left(RsVentas.getString("c_Numero")
                            .substring(RsVentas.getString("c_Caja").length()), 20));
                        else if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV"))
                            Transaction.setValue("TRANSACTIONSEQUENCENUMBER", Left("D" + RsVentas.getString("c_Numero")
                            .substring(RsVentas.getString("c_Caja").length()), 20));
                        
                        if (RsVentas.getDouble("t_Venta") > 0)
                            Transaction.setValue("BEGINDATETIMESTAMP", Left(dtFmtLong.format(DateAddInterval(DateTimeFrom(
                            RsVentas.getDate("f_Fecha"), RsVentas.getDate("f_Hora")), 
                            Calendar.SECOND, ((int)((-1.0) * RsVentas.getDouble("t_Venta") * 60D)))), 14));
                        else
                            Transaction.setValue("BEGINDATETIMESTAMP", Left(dtFmtLong.format(DateTimeFrom(
                            RsVentas.getDate("f_Fecha"), RsVentas.getDate("f_Hora"))), 14));
                        
                        Transaction.setValue("ENDDATETIMESTAMP", Left(dtFmtLong.format(DateTimeFrom(
                        RsVentas.getDate("f_Fecha"), RsVentas.getDate("f_Hora"))), 14));
                        Transaction.setValue("DEPARTMENT", new String());
                        Transaction.setValue("OPERATORQUALIFIER", new String());
                        Transaction.setValue("OPERATORID", Left(RsVentas.getString("c_Usuario"), 30));
                        
                        if (!SincronizarEnMultiMoneda)
                        {
                            Transaction.setValue("TRANSACTIONCURRENCY", gMonedaSAPPredeterminada);
                            Transaction.setValue("TRANSACTIONCURRENCY_ISO", new String());
                        }
                        else
                        {
                            Transaction.setValue("TRANSACTIONCURRENCY", SincronizarEnMultiMoneda_Param_TransactionCurrency);
                            Transaction.setValue("TRANSACTIONCURRENCY_ISO", SincronizarEnMultiMoneda_Param_TransactionCurrency_ISO);                            
                        }
                        
                        Transaction.setValue("PARTNERQUALIFIER", new String());
                        Transaction.setValue("PARTNERID", new String());
                        
                        // DATOS ADICIONALES DE LA TRANSACCIÓN.
                        
                        int nPosRowExt = 0;
                        
                        nPosRowExt++;
                        TransactionExt.appendRow();
                        //TransactionExt.setRow(nPosRowExt);
                        
                        TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        TransactionExt.setValue("FIELDGROUP", new String());
                        TransactionExt.setValue("FIELDNAME", "CLI_RIFCLI");
                        TransactionExt.setValue("FIELDVALUE", Left(RsVentas.getString("c_Rif"), 40));
                        
                        nPosRowExt++;
                        TransactionExt.appendRow();
                        //TransactionExt.setRow(nPosRowExt);
                        
                        TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        TransactionExt.setValue("FIELDGROUP", new String());
                        TransactionExt.setValue("FIELDNAME", "CLI_NOMBRE");
                        TransactionExt.setValue("FIELDVALUE", Left(RsVentas.getString("c_Desc_Cliente"), 40));
                        
                        nPosRowExt++;
                        TransactionExt.appendRow();
                        //TransactionExt.setRow(nPosRowExt);
                        
                        TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        TransactionExt.setValue("FIELDGROUP", new String());
                        TransactionExt.setValue("FIELDNAME", "StellarDoc");
                        TransactionExt.setValue("FIELDVALUE", Left("POS_" + RsVentas.getString("TransID"), 40));
                        
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV"))
                        {
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "StellarFDV");
                            TransactionExt.setValue("FIELDVALUE", Left("POS_VEN" + RsVentas.getString("cs_Documento_Rel"), 40));
                            
                        }
                        
                        if (SincronizarEnMultiMoneda)
                        {
                            
                            mLimiteDec = Integer.parseInt(SincronizarenMultiMoneda_Limite_Decimales);
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);

                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "Cod_Moneda_Alterna");
                            TransactionExt.setValue("FIELDVALUE", Left(SincronizarEnMultiMoneda_CodMoneda, 40));
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);

                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "Tasa_Moneda_Alterna");
                            TransactionExt.setValue("FIELDVALUE", Left(RawDouble(mTasaDestino), 40));
                            
                        } else {
                            mLimiteDec = 8;
                        }
                        
                        ResultSet DatosFiscales_DocumentoRelacion = null;
                        
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV"))
                            DatosFiscales_DocumentoRelacion = DatosFiscalesDocRel(
                            "VEN" + RsVentas.getString("cs_Documento_Rel") + RsVentas.getString("c_Sucursal"));
                        
                        // DATOS FISCALES
                        
                        RsDocumentosFiscales.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID") );
                        
                        if (RsDocumentosFiscales.first())
                        {
                            
//                            FieldData = RsDocumentosFiscales.getMetaData();
//                            RsDocumentosFiscales.beforeFirst();
//                            while (RsDocumentosFiscales.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsDocumentosFiscales.getObject(i).toString());
//                                }
//                            }
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_INDCNS");
                            TransactionExt.setValue("FIELDVALUE", "1");
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_SERIMP");
                            TransactionExt.setValue("FIELDVALUE", Left(RsDocumentosFiscales.getString("cu_SerialImpresora"), 40));
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_NRODOC");
                            TransactionExt.setValue("FIELDVALUE", Left(RsDocumentosFiscales.getString("cu_DocumentoFiscal"), 40));
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                        
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_RPRT_Z");
                            if (ExisteCampoTabla(RsDocumentosFiscales, "cu_ZFiscal"))
                                TransactionExt.setValue("FIELDVALUE", Left(RsDocumentosFiscales.getString("cu_ZFiscal"), 40));
                            else
                                TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "REF_SERIMP");
                            if (DatosFiscales_DocumentoRelacion == null)
                                TransactionExt.setValue("FIELDVALUE", new String());
                            else
                                TransactionExt.setValue("FIELDVALUE", Left(DatosFiscales_DocumentoRelacion.getString("cu_SerialImpresora"), 40));
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "REF_NRODOC");
                            if (DatosFiscales_DocumentoRelacion == null)
                                TransactionExt.setValue("FIELDVALUE", new String());
                            else
                                TransactionExt.setValue("FIELDVALUE", Left(DatosFiscales_DocumentoRelacion.getString("cu_DocumentoFiscal"), 40));
                            
                        }
                        else
                        {
                            
                            if (gValidarDocumentoFiscal && !RsVentas.getBoolean("bs_Impresa")) 
                            {
                                gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "La transaccion no está impresa fiscalmente.");
                                // Controlar Error de Registro de Ventas.
                                MarcarFallido(RsVentas.getString("TransID"));
                                break NextTrans;
                            }
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_INDCNS");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_SERIMP");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_NRODOC");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_RPRT_Z");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                        
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "REF_SERIMP");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "REF_NRODOC");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                        }
                        
                        // Comentando Impuestos por Transacción. Hasta ahora no son necesarios.
                        //RsImpuestos.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID") );

                        //if (RsImpuestos.first())
                        //{
//                            FieldData = RsImpuestos.getMetaData();
//                            RsImpuestos.beforeFirst();
//                            while (RsImpuestos.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsImpuestos.getObject(i).toString());
//                                }
//                            }
                        //}
                        //else
                        //{
                            
                        //}
                        
                        if (gEnviarTransactionDiscount) 
                        {
                            if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN") && 
                            RsVentas.getDouble("n_Descuento") > 0) 
                            {
                                
                                TransactionDiscount.appendRow();
                                
                                TransactionDiscount.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                TransactionDiscount.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                TransactionDiscount.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                TransactionDiscount.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                TransactionDiscount.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                
                                TransactionDiscount.setValue("DISCOUNTSEQUENCENUMBER", "1");
                                TransactionDiscount.setValue("DISCOUNTTYPECODE", "3401");
                                TransactionDiscount.setValue("DISCOUNTREASONCODE", new String());
                                TransactionDiscount.setValue("REDUCTIONAMOUNT", Left(RawDouble(Round(
                                RsVentas.getDouble("n_Descuento") * mTasaConversion, mLimiteDec)), 28));
                                TransactionDiscount.setValue("STOREFINANCIALLEDGERACCOUNTID", new String());
                                TransactionDiscount.setValue("DISCOUNTID", new String());
                                TransactionDiscount.setValue("DISCOUNTIDQUALIFIER", new String());
                                TransactionDiscount.setValue("BONUSBUYID", new String());
                                TransactionDiscount.setValue("OFFERID", new String());
                                
                            }
                        }
                        
                        // DETALLE DE PAGOS
                        
                        int nPosTender = 0;
                        
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN")) 
                        {
                            
                            RsDetPagos.setFilter(new StringColumnFilter(
                            RsVentas.getString("c_Numero") + RsVentas.getString("c_Sucursal"), "TransID"));
                            
                            if (RsDetPagos.last())
                            {
                                
    //                            FieldData = RsDetPagos.getMetaData();
    //                            RsDetPagos.beforeFirst();
    //                            while (RsDetPagos.next()) {
    //                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
    //                                    System.out.println(FieldData.getColumnLabel(i) + 
    //                                    " => " + 
    //                                    RsDetPagos.getObject(i).toString());
    //                                }
    //                            }
                                
                                // Verificamos si se quiere manejar una forma de pago resumida
                                // por el total de la factura. Para ello la variable 
                                // gFormaPagoFacturaSAP_SinDetallePago debe tener un codigo de
                                // forma de pago en SAP establecido con el cual sera enviado
                                // ese unico registro. Este mecanismo generalmente solo es usado
                                // como contingencia, lo mas probable es que dicha variable siempre
                                // esté en blanco o con un *, lo cual indica que se debe sincronizar
                                // el detalle de pago, ignorando montos por encima a causa de vuelto y donaciones.
                                
                                if (!(gFormaPagoFacturaSAP_SinDetallePago.isEmpty() || 
                                gFormaPagoFacturaSAP_SinDetallePago.equalsIgnoreCase("*")))
                                { // Si tiene un codigo especifico definido (No esta vacía ni es "*").
                                    
                                    // 1 Solo registro con el monto de la venta.
                                    
                                    nPosTender++;
                                    Tender.appendRow();
                                    
                                    Tender.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                    Tender.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                    Tender.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                    Tender.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                    Tender.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                    Tender.setValue("TENDERSEQUENCENUMBER", String.valueOf(nPosTender));
                                    
                                    String TenderCurrency = gMonedaSAPPredeterminada;
                                    
                                    String TenderTypeCode = gFormaPagoFacturaSAP_SinDetallePago;
                                    
                                    Tender.setValue("TENDERTYPECODE", TenderTypeCode);
                                    Tender.setValue("TENDERAMOUNT", Left(RawDouble(Round(
                                    RsVentas.getDouble("n_Total") * mTasaConversion, mLimiteDec)), 28));
                                    Tender.setValue("TENDERCURRENCY", TenderCurrency);
                                    Tender.setValue("TENDERCURRENCY_ISO", new String());
                                    Tender.setValue("TENDERID", Left(TenderTypeCode, 32));
                                    Tender.setValue("ACCOUNTNUMBER", new String());
                                    Tender.setValue("REFERENCEID", new String());
                                    
                                }
                                else // Caso común. Enviar todo el detalle de pago.
                                {                                    
                                    
                                    int DetPagRowCount = RsDetPagos.getRow();

                                    double mVueltoRestante = RsVentas.getDouble("n_Vuelto");
                                    double mVueltoLn = 0;
                                    double mPagoLn = 0;
                                    double mRestanteDonacion = RsVentas.getDouble("n_MontoDonacion");
                                    
                                    RsDetPagos.beforeFirst();
                                    
                                    while (RsDetPagos.next())
                                    {
                                        
                                        String mMoneda = RsDetPagos.getString("c_CodMoneda");
                                        double mMontoLn = RsDetPagos.getDouble("n_Monto");
                                        
                                        String TenderCurrency = null;
                                        String LineCurrency = null;
                                        
                                        if (ListaAsociacionMoneda.containsKey(
                                        RsDetPagos.getString("c_CodMoneda")))
                                        {
                                            LineCurrency = ListaAsociacionMoneda.get(
                                            RsDetPagos.getString("c_CodMoneda")).toString();
                                        }
                                        
                                        if (LineCurrency == null) LineCurrency = "";
                                        
                                        String TenderTypeCode = null;

                                        // Primero buscar Llave compuesta de moneda y denominación.
                                        if (ListaAsociacionFormaPago.containsKey(
                                        RsDetPagos.getString("c_CodMoneda") + ";" + RsDetPagos.getString("c_CodDenominacion")))
                                        {
                                            TenderTypeCode = ListaAsociacionFormaPago.get(
                                            RsDetPagos.getString("c_CodMoneda") + ";" + RsDetPagos.getString("c_CodDenominacion")).toString();
                                        } else if (ListaAsociacionFormaPago.containsKey(
                                        RsDetPagos.getString("c_CodDenominacion"))){ // sino existe se intenta buscar solo por código de denominación.
                                            TenderTypeCode = ListaAsociacionFormaPago.get(
                                            RsDetPagos.getString("c_CodDenominacion")).toString();
                                        }                                        
                                        
                                        //if ((RsDetPagos.getString("c_CodDenominacion").equalsIgnoreCase("Efectivo") ||
                                        //DetPagRowCount == 1)) // && Round(RsDetPagos.getDouble("n_Factor"), 8) == 1)
                                        if (mVueltoRestante > 0)
                                        {
                                            
                                            if (LineCurrency.equalsIgnoreCase(gMonedaSAPPredeterminada) && 
                                            RsDetPagos.getString("c_CodDenominacion").equalsIgnoreCase("Efectivo"))
                                            {   
                                                
                                                // Se determinó que el POS le resta el vuelto al Efectivo de la
                                                // moneda predeterminada. En estos casos entonces, no restamos.
                                                // Esto da pie a muchas inconsistencias pero bueno, asi esta hecho.
                                                // Hay que resolver de esta manera.
                                                
                                                mVueltoLn = 0;
                                                mPagoLn = Round(mMontoLn, 8);
                                                
                                                // Es MAS!, en casos extraños pudiera quedar el Row con monto negativo...
                                                // Lo que significa que hay un vuelto que habría que restarle a otra línea.
                                                // entonces corregimos de la siguiente manera:
                                                
                                                if (mPagoLn < 0)
                                                {   // Modificamos el vuelto original por el vuelto Restante (el monto negativo)
                                                    mVueltoRestante = Math.abs(mPagoLn);
                                                }
                                                else
                                                {   // Si el monto quedo positivo entonces no quedo mas vuelto por restar a ninguna otra forma de pago.
                                                    mVueltoRestante = 0;
                                                }
                                                
                                            }
                                            else
                                            {   // Si es cualquier otra cosa pero hay vuelto, restarsela
                                                // ya que el POS no se la resto. Caso Efectivo en Otras Monedas...
                                                mVueltoLn = (Round(mMontoLn - mVueltoRestante, 8) >= 0 ? 
                                                Round(mVueltoRestante, 8) : Round(mMontoLn, 8));
                                            }
                                            
                                            mPagoLn = Round(mMontoLn - mVueltoLn, 8);
                                            mVueltoRestante = Round(mVueltoRestante - mVueltoLn, 8);
                                            
                                        }
                                        else
                                        {
                                            mVueltoLn = 0;
                                            mPagoLn = Round(mMontoLn, 8);
                                        }

                                        if (mRestanteDonacion > 0) {
                                            if (mRestanteDonacion > mPagoLn)
                                            {
                                                mRestanteDonacion = Round(mRestanteDonacion - mPagoLn, 8);
                                                mPagoLn = 0;
                                            }
                                            else
                                            {
                                                mPagoLn = Round(mPagoLn - mRestanteDonacion, 8);
                                                mRestanteDonacion = 0;
                                            }
                                        }

                                        //mPagoLn = Round(mPagoLn, 2); // Mejor enviar el pago exactamente como es.
                                        
                                        if (mPagoLn > 0) // Monto final del pago - Vuelto - Donaciones.
                                        {
                                            
                                            nPosTender++;
                                            Tender.appendRow();
                                            
                                            Tender.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                            Tender.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                            Tender.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                            Tender.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                            Tender.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                            Tender.setValue("TENDERSEQUENCENUMBER", String.valueOf(nPosTender));                                            
                                            
                                            // TenderCurrency = LineCurrency;
                                            
                                            // TRANSFERIR COMO MONEDA PREDETERMINADA FACTORIZADA.
                                            
                                            TenderCurrency = gMonedaSAPPredeterminada;
                                            
                                            /*// En vez de marcar como fallidos, enviemosle lo que ellos consideren como efectivo
                                            // de manera predeterminada para no trancar el proceso.
                                            if (TenderTypeCode == null)
                                                TenderTypeCode = "3101"; // Efectivo*/
                                            
                                            if (TenderTypeCode == null || TenderCurrency == null)
                                            {
                                                gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "Una de la(s) formas de pago / moneda del pago de la factura no está categorizada para envio a SAP.");
                                                // Controlar Error Registro de Ventas.
                                                MarcarFallido(RsVentas.getString("TransID"));
                                                break NextTrans;
                                            }
                                            
                                            Tender.setValue("TENDERTYPECODE", TenderTypeCode);
                                            
                                            /*Tender.setValue("TENDERAMOUNT", Left(String.valueOf(Round(RsDetPagos.getDouble("n_Monto") -
                                            ((RsDetPagos.getString("c_CodDenominacion").equalsIgnoreCase("Efectivo") ||
                                            DetPagRowCount == 1) && Round(RsDetPagos.getDouble("n_Factor"), 8) == 1 ? 
                                            RsVentas.getDouble("n_Vuelto"): 0), 8)), 28));*/
                                            
                                            Tender.setValue("TENDERAMOUNT", Left(RawDouble(Round(mPagoLn * mTasaConversion, mLimiteDec)), 28));
                                            Tender.setValue("TENDERCURRENCY", TenderCurrency);
                                            Tender.setValue("TENDERCURRENCY_ISO", new String());
                                            Tender.setValue("TENDERID", Left(RsDetPagos.getString("c_CodDenominacion"), 32));
                                            Tender.setValue("ACCOUNTNUMBER", new String());
                                            Tender.setValue("REFERENCEID", Left(RsDetPagos.getString("c_Numero"), 35));

                                        }
                                        
                                    }

                                }
                                
                            }
                            else
                            {
                                gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "No se encontró detalle de pago.");
                                MarcarFallido(RsVentas.getString("TransID"));
                                break NextTrans;
                            }

                        }
                        else if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV")) 
                        {
                            
                            nPosTender++;
                            Tender.appendRow();
                            
                            Tender.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            Tender.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            Tender.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            Tender.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            Tender.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            Tender.setValue("TENDERSEQUENCENUMBER", String.valueOf(nPosTender));
                            
                            String TenderCurrency = gMonedaSAPPredeterminada;
                            
                            String TenderTypeCode = gFormaPagoDevolucionSAP;
                            
                            Tender.setValue("TENDERTYPECODE", TenderTypeCode);
                            Tender.setValue("TENDERAMOUNT", Left(RawDouble(Round(
                            RsVentas.getDouble("n_Total") * mTasaConversion, mLimiteDec)), 28));
                            Tender.setValue("TENDERCURRENCY", TenderCurrency);
                            Tender.setValue("TENDERCURRENCY_ISO", new String());
                            Tender.setValue("TENDERID", Left(TenderTypeCode, 32));
                            Tender.setValue("ACCOUNTNUMBER", new String());
                            Tender.setValue("REFERENCEID", new String());
                            
                        }
                        
                        // DETALLE DE TRANSACCION - PRODUCTOS
                        
                        RsItems.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID") );
                        
                        int nPosDcto = 0;
                        int nPosImp = 0;
                        
                        HashMap DocItmList = new HashMap();
                        HashMap DocItmList_Info;
                        
                        if (RsItems.first())
                        {
                            
//                            FieldData = RsItems.getMetaData();
//                            RsItems.beforeFirst();
//                            while (RsItems.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsItems.getObject(i).toString());
//                                }
//                            }
                            
                            RsItems.beforeFirst();
                            
                            while(RsItems.next())
                            {
                            
                            RetailLineItem.appendRow();
                            
                            RetailLineItem.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            RetailLineItem.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            RetailLineItem.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            RetailLineItem.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            RetailLineItem.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            RetailLineItem.setValue("RETAILSEQUENCENUMBER", RsItems.getString("n_Linea"));
                            
                            Double SignoItem = 1D;
                            
                            // Support for LineItemVoid
                            
                            if(RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN") && RsItems.getDouble("Cantidad") > 0)
                            {
                                
                                if (!DocItmList.containsKey(RsItems.getString("Cod_Principal") + "|" + 
                                String.valueOf(Round(RsItems.getDouble("Precio"), 2))))
                                {
                                    
                                    DocItmList_Info = new HashMap();
                                    DocItmList_Info.put("Cod", RsItems.getString("Cod_Principal"));
                                    DocItmList_Info.put("Current_Ln", RsItems.getString("n_Linea"));
                                    DocItmList_Info.put("Cant_Acum", RsItems.getDouble("Cantidad"));
                                    DocItmList_Info.put("Price", RsItems.getDouble("Precio"));
                                    HashMap All_Ln_Info = new HashMap();
                                    HashMap Ln_Info = new HashMap();
                                    Ln_Info.put("Ln", DocItmList_Info.get("Current_Ln"));
                                    Ln_Info.put("Remaining_Cant", DocItmList_Info.get("Cant_Acum"));
                                    All_Ln_Info.put(RsItems.getString("n_Linea"), Ln_Info);
                                    HashMap All_R_Info = new HashMap();
                                    DocItmList_Info.put("All_Ln_Info", All_Ln_Info);
                                    DocItmList_Info.put("All_R_Info", All_R_Info);
                                    DocItmList.put(RsItems.getString("Cod_Principal") + "|" + 
                                    String.valueOf(Round(RsItems.getDouble("Precio"), 2)), DocItmList_Info);
                                    
                                } else {
                                    
                                    DocItmList_Info = ((HashMap<String, Object>) DocItmList.get(RsItems.getString("Cod_Principal") + "|" + String.valueOf(Round(RsItems.getDouble("Precio"), 2))));
                                    DocItmList_Info.replace("Cant_Acum", ((Double) DocItmList_Info.get("Cant_Acum")) + RsItems.getDouble("Cantidad"));
                                    HashMap Ln_Info = new HashMap();
                                    Ln_Info.put("Ln", RsItems.getString("n_Linea"));
                                    Ln_Info.put("Remaining_Cant", RsItems.getDouble("Cantidad"));
                                    if (((HashMap)(DocItmList_Info.get("All_Ln_Info"))).size() <= 0)
                                        DocItmList_Info.replace("Current_Ln", RsItems.getString("n_Linea"));
                                    ((HashMap)(DocItmList_Info.get("All_Ln_Info"))).put(RsItems.getString("n_Linea"), Ln_Info);
                                    
                                }
                                
                            } else if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN") && RsItems.getDouble("Cantidad") < 0)
                            {
                                
                                DocItmList_Info = ((HashMap) DocItmList.get(RsItems.getString("Cod_Principal") + "|" + 
                                String.valueOf(Round(RsItems.getDouble("Precio"), 2))));
                                
                                HashMap All_Ln_Info = ((HashMap)(DocItmList_Info.get("All_Ln_Info")));
                                HashMap All_R_Info = ((HashMap)(DocItmList_Info.get("All_R_Info")));
                                
                                Double Cant_R = Math.abs(RsItems.getDouble("Cantidad"));
                                DocItmList_Info.replace("Cant_Acum", Round(((Double) DocItmList_Info.get("Cant_Acum") - Cant_R), 6));
                                
                                while (Cant_R != 0)
                                {
                                    
                                    String Current_Ln = DocItmList_Info.get("Current_Ln").toString();
                                    /*try { assert (All_Ln_Info.containsKey(Current_Ln)); } catch ( AssertionError ae ) 
                                    { 
                                        if(ae != null)
                                            System.out.println(ae); 
                                    };*/
                                    Double Cant_Ln = 0D;
                                    if (All_Ln_Info.containsKey(Current_Ln))
                                        Cant_Ln = Round((Double)((HashMap) All_Ln_Info.get(Current_Ln)).get("Remaining_Cant"), 6);
                                    
                                    //if (!All_R_Info.containsKey(Current_Ln))
                                    //{
                                        //
                                        
                                        LineItemVoid.appendRow();

                                        LineItemVoid.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                        LineItemVoid.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                        LineItemVoid.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                        LineItemVoid.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                        LineItemVoid.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                        LineItemVoid.setValue("RETAILSEQUENCENUMBER", RsItems.getString("n_Linea"));
                                        
                                        LineItemVoid.setValue("VOIDEDLINE", Current_Ln);
                                        LineItemVoid.setValue("VOIDFLAG", "X");
                                        
                                        //All_R_Info.put(Current_Ln, Current_Ln);
                                        All_R_Info.putIfAbsent(Current_Ln, Current_Ln);
                                    //}
                                    
                                    /*try { assert (Round((Cant_R - Cant_Ln), 6) != 0D); } catch ( AssertionError ae ) 
                                    { 
                                        System.out.println(ae); 
                                    };*/
                                    
                                    if (Cant_R < Cant_Ln && Cant_Ln != 0D)
                                    {
                                        ((HashMap) All_Ln_Info.get(Current_Ln)).replace("Remaining_Cant", Round((Cant_Ln - Cant_R), 6));
                                        Cant_R = 0D;
                                    } else {
                                        
                                        Cant_R = Round((Cant_R - Cant_Ln), 6);
                                        All_Ln_Info.remove(Current_Ln);
                                        if (All_Ln_Info.size() > 0)
                                        {
                                            Current_Ln = ((HashMap) All_Ln_Info.values().toArray()[0]).get("Ln").toString();
                                            DocItmList_Info.replace("Current_Ln", Current_Ln);
                                        } else {
                                            Cant_R = 0D;
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                            if (RsItems.getDouble("Cantidad") < 0 &&
                            RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                { RetailLineItem.setValue("RETAILTYPECODE", "2901"); SignoItem = -1D; }
                            else if (RsItems.getDouble("Cantidad") > 0 &&
                            RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV"))
                                { RetailLineItem.setValue("RETAILTYPECODE", "2902"); SignoItem = -1D; }
                            else
                                
                                if (new BigDecimal(RsItems.getDouble("Impuesto")).compareTo(BigDecimal.ZERO) == 0)
                                    if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                        RetailLineItem.setValue("RETAILTYPECODE", "2201");
                                    else
                                        RetailLineItem.setValue("RETAILTYPECODE", "2202");
                                else
                                    if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                        RetailLineItem.setValue("RETAILTYPECODE", "2001");
                                    else
                                        RetailLineItem.setValue("RETAILTYPECODE", "2801");
                            
                            RetailLineItem.setValue("RETAILREASONCODE", new String());
                            RetailLineItem.setValue("ITEMIDQUALIFIER", "1");
                            RetailLineItem.setValue("ITEMID", Left(RsItems.getString("Codigo"), 18));
                            RetailLineItem.setValue("RETAILQUANTITY", Left(RawDouble(Round(
                            RsItems.getDouble("Cantidad") * SignoItem, 8)), 13));
                            RetailLineItem.setValue("SALESUNITOFMEASURE", gUnidadMedidaSAP);
                            RetailLineItem.setValue("SALESUNITOFMEASURE_ISO", new String());
                            RetailLineItem.setValue("SALESAMOUNT", Left(RawDouble(Round(
                            RsItems.getDouble("Subtotal") * SignoItem * mTasaConversion, mLimiteDec)), 28));
                            RetailLineItem.setValue("NORMALSALESAMOUNT", Left(RawDouble(Round(
                            RsItems.getDouble("Cantidad") * Math.abs(RsItems.getDouble("n_PrecioReal")) *
                            SignoItem * mTasaConversion, mLimiteDec)), 28));
                            RetailLineItem.setValue("COST", new String());
                            RetailLineItem.setValue("BATCHID", new String());
                            RetailLineItem.setValue("SERIALNUMBER", new String());
                            RetailLineItem.setValue("PROMOTIONID", new String());
                            RetailLineItem.setValue("ITEMIDENTRYMETHODCODE", new String());
                            RetailLineItem.setValue("ACTUALUNITPRICE", Left(RawDouble(Math.abs(Round(
                            RsItems.getDouble("n_PrecioReal") * mTasaConversion, mLimiteDec))), 28));
                            RetailLineItem.setValue("UNITS", new String());
                            RetailLineItem.setValue("SCANTIME", new String());
                            
                            ResultSet Cat = BuscarCategoriasProducto(RsItems.getString("Cod_Principal"));
                            
                            LineItemExt.appendRow();
                            
                            LineItemExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            LineItemExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            LineItemExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            LineItemExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            LineItemExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            LineItemExt.setValue("RETAILSEQUENCENUMBER", RetailLineItem.getValue("RETAILSEQUENCENUMBER"));
                            LineItemExt.setValue("FIELDGROUP", new String());
                            LineItemExt.setValue("FIELDNAME", "ART_DEPART");
                            if (Cat == null)
                                LineItemExt.setValue("FIELDVALUE", new String());
                            else
                                LineItemExt.setValue("FIELDVALUE", Left(Cat.getString("c_Departamento"), 40));
                            
                            /*if (RsItems.getDouble("Impuesto") != 0)
                            {*/
                                
                                nPosImp++;
                                LineItemTax.appendRow();
                                
                                LineItemTax.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                LineItemTax.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                LineItemTax.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                LineItemTax.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                LineItemTax.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                LineItemTax.setValue("RETAILSEQUENCENUMBER", RsItems.getString("n_Linea"));
                                LineItemTax.setValue("TAXSEQUENCENUMBER", String.valueOf(nPosImp));
                                
                                String TaxTypeCode = null;
                                DecimalFormatSymbols taxFmtSym = new DecimalFormatSymbols();
                                taxFmtSym.setDecimalSeparator('.');
                                taxFmtSym.setGroupingSeparator(',');
                                DecimalFormat taxFmt = new DecimalFormat("00.00", taxFmtSym);
                                
                                if (gTaxListCodes != null)
                                {
                                    if (gTaxListCodes.containsKey(taxFmt.format(RsItems.getDouble("Impuesto1"))))
                                        TaxTypeCode = gTaxListCodes.get(taxFmt.format(RsItems.getDouble("Impuesto1"))).toString();
                                } else 
                                {
                                    if (gTaxList1.containsValue(taxFmt.format(RsItems.getDouble("Impuesto1"))))
                                        TaxTypeCode = "4301";
                                    else if (gTaxList2.containsValue(taxFmt.format(RsItems.getDouble("Impuesto1"))))
                                        TaxTypeCode = "4301";
                                    else if (gTaxList3.containsValue(taxFmt.format(RsItems.getDouble("Impuesto1"))))
                                        TaxTypeCode = "4303";
                                }
                                
                                if (TaxTypeCode == null)
                                {
                                    gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "No se encontró asociacion de Codigo de Impuesto.");
                                    // Controlar Error Registro de Ventas.
                                    MarcarFallido(RsVentas.getString("TransID"));
                                    break NextTrans;
                                }
                                
                                LineItemTax.setValue("TAXTYPECODE", TaxTypeCode);
                                LineItemTax.setValue("TAXAMOUNT", Left(RawDouble(Round(
                                RsItems.getDouble("Impuesto") * SignoItem * mTasaConversion, mLimiteDec)), 28));
                                
                            //}
                            
                            if (RsItems.getDouble("Descuento") > 0)
                            {
                                
                                nPosDcto++;
                                LineItemDiscount.appendRow();
                                
                                LineItemDiscount.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                LineItemDiscount.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                LineItemDiscount.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                LineItemDiscount.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                LineItemDiscount.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                LineItemDiscount.setValue("RETAILSEQUENCENUMBER", RawDouble(RsItems.getDouble("n_Linea")));
                                LineItemDiscount.setValue("DISCOUNTSEQUENCENUMBER", String.valueOf(nPosDcto));
                                LineItemDiscount.setValue("DISCOUNTTYPECODE", "3101");
                                LineItemDiscount.setValue("DISCOUNTREASONCODE", new String());
                                LineItemDiscount.setValue("REDUCTIONAMOUNT", Left(RawDouble(Round(
                                RsItems.getDouble("Descuento") * mTasaConversion, mLimiteDec)), 28));
                                LineItemDiscount.setValue("STOREFINANCIALLEDGERACCOUNTID", new String());
                                LineItemDiscount.setValue("DISCOUNTID", new String());
                                LineItemDiscount.setValue("DISCOUNTIDQUALIFIER", new String());
                                LineItemDiscount.setValue("BONUSBUYID", new String());
                                LineItemDiscount.setValue("OFFERID", new String());
                                
                            }
                            
                            }
                            
                        }
                        else
                        {
                            gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "No se encontraron productos.");
                            // Controlar Error Registro de Ventas.
                            MarcarFallido(RsVentas.getString("TransID"));
                            break NextTrans;
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean SendError = false;
                        
                        try{
                            gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de transacciones.");
                            SendError = true;
                            //System.exit(0);
                        }

                        Return = gCnSAP.fTransVentas.getTableParameterList().getTable("RETURN");
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        int ReturnRowCount = Return.getNumRows();
                        
                        if (!SendError)
                            if (ReturnRowCount <= 0)
                            {
                                // Envío exitoso sin ningún mensaje en particular.
                                TransaccionEnviada = true;
                            }
                            else
                            {
                                
                                TransaccionEnviada = true;
                                
                                // Revisar Mensajes.
                                
                                String RetFullLog = "";
                                String RetLog = "";
                                
                                for (int i = 0; i < ReturnRowCount; i++) 
                                {
                                    
                                    Return.setRow(i);
                                    
                                    String RetLnLog = "";
                                    
                                    if (gDebugMode)
                                    {
                                        RetLnLog = GetLines() +
                                        "TYPE => " + Return.getString("TYPE") + GetTab() +
                                        "ID => " + Return.getString("ID") + GetTab() +
                                        "NUMBER => " + Return.getString("NUMBER") + GetTab() +
                                        "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
                                        "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
                                        "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
                                        "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
                                        "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
                                        "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
                                        "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
                                        "ROW => " + Return.getString("ROW") + GetTab() +
                                        "FIELD => " + Return.getString("FIELD") + GetTab() +
                                        "SYSTEM => " + Return.getString("SYSTEM");
                                        RetFullLog += RetLnLog;
                                    }
                                    
                                    if (Return.getString("TYPE").equalsIgnoreCase("E") ||
                                    Return.getString("TYPE").equalsIgnoreCase("A")) {
                                        
                                        TransaccionEnviada = false;
                                        
                                        if (!gDebugMode)
                                            RetLnLog = GetLines() +
                                            "TYPE => " + Return.getString("TYPE") + GetTab() +
                                            "ID => " + Return.getString("ID") + GetTab() +
                                            "NUMBER => " + Return.getString("NUMBER") + GetTab() +
                                            "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
                                            "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
                                            "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
                                            "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
                                            "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
                                            "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
                                            "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
                                            "ROW => " + Return.getString("ROW") + GetTab() +
                                            "FIELD => " + Return.getString("FIELD") + GetTab() +
                                            "SYSTEM => " + Return.getString("SYSTEM");
                                        
                                        RetLog += RetLnLog;
                                        
                                    }
                                    
                                }
                                
                            }
                        
                        if (TransaccionEnviada) 
                        {
                            
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE MA_PAGOS SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_Concepto + c_Numero + c_Sucursal) = '" + RsVentas.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("Transacción [" + RsVentas.getString("TransID") + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }
                        
                    // Próxima Transacción.
                     
                    }
                    
                    }
                        
                }
                
                System.out.println("Lote de Ventas Finalizado con Exito.");
                
                }
                
            }
            else
            {
                System.out.println("No se encontraron transacciones de ventas pendientes por sincronizar.");
            }
            
        } catch(Exception Ex) { // ConstruirRegistrosDeVentas:Exception - ConstruirRegistrosDeVentas:Error
            System.out.println(Ex.getMessage());
            gLogger.EscribirLog(Ex, "Error al procesar registros de Ventas o Devoluciones.");
            if (!mUltDoc.isEmpty())
                gLogger.EscribirLog("Documento en proceso => " + mUltDoc + GetTab() + 
                "Fecha => " + mUltFec + GetTab() +
                "Total => " + String.valueOf(mUltTot));
        }
        
    }
    
    public ResultSet BuscarCategoriasProducto(String pCodigo)
    {
        
        ResultSet Datos;
        
        try {
            Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
            "SELECT TOP (1) c_Departamento, c_Grupo, c_Subgrupo" + GetLines() +
            "FROM [VAD20].[DBO].[MA_PRODUCTOS] WHERE c_Codigo = '" + pCodigo + "'");
            if (Datos.first()) { return Datos; }
            return null;
        } catch (SQLException ex) {
            return null;
        }
        
    }
    
    public ResultSet DatosFiscalesDocRel(String pDocRel)
    {
        
        ResultSet Datos;
        
        try {
            Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
            "SELECT TOP (1) *" + GetLines() +
            "FROM [VAD20].[DBO].[MA_DOCUMENTOS_FISCAL] WHERE (cu_DocumentoTipo + cu_DocumentoStellar + cu_Localidad) = '" + pDocRel + "'");
            if (Datos.first()) { return Datos; }
            return null;
        } catch (SQLException ex) {
            return null;
        }
        
    }
    
    public Boolean MarcarFallido(String pTransID)
    {
        
        //ResultSet Datos;
        int RowsAffected = 0;
        
        try {
            RowsAffected = gConexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY).executeUpdate(
            "UPDATE [VAD20].[DBO].[MA_PAGOS] SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "'" + GetLines() + 
            "WHERE (c_Concepto + c_Numero + c_Sucursal) = '" + pTransID + "'");
            if (RowsAffected > 0) 
                return true;
            else
                return false;
        } catch (SQLException ex) {
            return false;
        }
        
    }
    
    public void ConstruirRegistrosDeVentas()
    {
        ConstruirRegistrosDeVentas(false);
    }
    
    public void ProcesarLote()
    {
        
        if (HayDatosPendientes(RegistrosPendientesPorLote))
        {
            // Procesar primero los pendientes
            // Antes de marcar el próximo Lote.
            ConstruirRegistrosDeVentas();
            nLotesProcesados++;
        }
        
        if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistros(RegistrosPendientesPorLote)) 
            {
                ConstruirRegistrosDeVentas();
                nLotesProcesados++;
            }
        
    }
    
    public void ProcesarCorrida()
    {

        if (HayDatosPendientes(RegistrosPendientesFallidos))
            ConstruirRegistrosDeVentas(true); // Intentar procesar los Fallidos una sola vez.
        
        while (HayDatosPendientes(RegistrosPendientesPorCorrida) ||
        HayDatosPendientes(RegistrosPendientesPorLote)) {
            if (nLotesProcesados < nLotesMaxEjecucion)
                ProcesarLote();
            else
                break;
        }

    }
    
    public void SincronizarVentas(){
        
        if (HayDatosPendientes(RegistrosPendientesPorCorrida) || 
        HayDatosPendientes(RegistrosPendientesPorLote) || 
        HayDatosPendientes(RegistrosPendientesFallidos)) {
            ProcesarCorrida(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
        }
        
        if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistros(RegistrosPendientesPorCorrida)) { // Procesar registros nuevos.
                ProcesarCorrida();
            }

    }
    
    // OPERACIONES AUTORIZADAS (TRANSACCIONES DE CONTROL POSDM)
    
    public void SincronizarAutorizaciones(){
        
        if (!gTransferirAutorizaciones) return;
        
        if (HayAutorizacionesPendientes(RegistrosPendientesPorCorrida) || 
        HayAutorizacionesPendientes(RegistrosPendientesPorLote) || 
        HayAutorizacionesPendientes(RegistrosPendientesFallidos)) {
            ProcesarAutorizaciones(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
        }
        
        //if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosDeAutorizaciones(RegistrosPendientesPorCorrida)) { // Procesar registros nuevos.
                ProcesarAutorizaciones();
            }

    }
    
    public Boolean HayAutorizacionesPendientes(String pEstatus){

        try {

        ResultSet RsDatosPendientes; Boolean exec;
        
        RsDatosPendientes = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).
        executeQuery("SELECT COUNT(*) AS Registros FROM TR_OPERACIONES_AUTORIZADAS " +
        "WHERE cs_Sync_SxS = '" + pEstatus + "'");
        RsDatosPendientes.first();
        
        long RetRows = RsDatosPendientes.getLong("Registros");
        
        return (RetRows > 0);
        
        } catch (Exception Ex) { return false; }
        
    }
    
    public Boolean MarcarFallidosOperacionesAutorizadas(String pTransID)
    {
        
        //ResultSet Datos;
        int RowsAffected = 0;
        
        try {
            RowsAffected = gConexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY).executeUpdate(
            "UPDATE [VAD20].[DBO].[TR_OPERACIONES_AUTORIZADAS] SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "'" + GetLines() + 
            "WHERE (ID) = " + pTransID + "");
            if (RowsAffected > 0) 
                return true;
            else
                return false;
        } catch (SQLException ex) {
            return false;
        }
        
    }
    
    public void ConstruirRegistrosDeAutorizaciones()
    {
        ConstruirRegistrosDeAutorizaciones(false);
    }
    
    public void ProcesarLoteDeAutorizaciones()
    {
        
        if (HayAutorizacionesPendientes(RegistrosPendientesPorLote))
        {
            // Procesar primero los pendientes
            // Antes de marcar el próximo Lote.
            ConstruirRegistrosDeAutorizaciones();
            //nLotesProcesados++;
        }
        
        //if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosDeAutorizaciones(RegistrosPendientesPorLote)) 
            {
                ConstruirRegistrosDeAutorizaciones();
                //nLotesProcesados++;
            }
        
    }
    
    public void ProcesarAutorizaciones()
    {
        
        if (HayAutorizacionesPendientes(RegistrosPendientesFallidos))
            ConstruirRegistrosDeAutorizaciones(true); // Intentar procesar los Fallidos una sola vez.
        
        while (HayAutorizacionesPendientes(RegistrosPendientesPorCorrida) ||
        HayAutorizacionesPendientes(RegistrosPendientesPorLote)) {
            ProcesarLoteDeAutorizaciones();
            break; // Para este tipo de operaciones por los momentos solo se estará soportando un solo envío (un lote).
        }

    }
    
    public Boolean MarcarRegistrosDeAutorizaciones(String pEstatus, Boolean pFallidos)
    {
        
        try {

            int RegistrosAfectados = 0; String pEstatusPrevio = ""; String mRegistrosXLote = "";

            String ExcluirVNF = ""; String SQLMarcaje = "";
            
            switch (pEstatus) {
                case RegistrosPendientesPorCorrida:
                    pEstatusPrevio = "'" + RegistrosNuevos + "'";
                    break;
                case RegistrosPendientesPorLote:
                    if (pFallidos)
                        pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                    else {
                        pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                        mRegistrosXLote = "AND ID IN (" + "\n" + 
                        "SELECT TOP (" + nOperacionesAutorizadasPorLote + ") ID FROM TR_OPERACIONES_AUTORIZADAS" + "\n" + 
                        "WHERE cs_Sync_SxS = " + pEstatusPrevio + " ORDER BY ID)";
                    }
                    break;
                default: // Guardados con Exito.
                    pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                    break;
            }

            SQLMarcaje = "UPDATE TR_OPERACIONES_AUTORIZADAS SET cs_Sync_SxS = '" + pEstatus + "'" + "\n" + 
            "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ")" + mRegistrosXLote + ExcluirVNF;

            if (gDebugMode) gLogger.EscribirLog(SQLMarcaje);

            RegistrosAfectados = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeUpdate(SQLMarcaje);
            
            return (RegistrosAfectados > 0);

        } catch (Exception Ex) { 
            return false; 
        }

    }
    
    public Boolean MarcarRegistrosDeAutorizaciones(String pEstatus)
    {
        return MarcarRegistrosDeAutorizaciones(pEstatus, false);
    }
    
    public Boolean ObtenerDatosOperacionesAutorizadas(List<FilteredRowSet> pRsAutorizaciones,
    Boolean pReprocesarFallidos)
    {
        
        ResultSet pRsDataAutorizaciones = null;
        
        try {

            pRsAutorizaciones.set(0, new FilteredRowSetImpl()) ;
            
            String mSQL = null;

            String ExcluirVNF = new String();
            
            String mEstatus;
            
            if (pReprocesarFallidos)
                mEstatus = RegistrosPendientesFallidos;
            else
                mEstatus = RegistrosPendientesPorLote;

            mSQL = "SELECT *, " + 
            "isNULL(Cajero.Descripcion, 'N/A') AS NombreCajero, isNULL(Cajero.Nivel, 0) AS NivelCajero, " + "\n" + 
            "isNULL(Supervisor.Descripcion, 'N/A') AS NombreSupervisor, isNULL(Supervisor.Nivel, 0) AS NivelSupervisor, " + "\n" + 
            "TR.ID AS TransID " + "\n" + 
            "FROM [VAD20].[DBO].[TR_OPERACIONES_AUTORIZADAS] TR" + "\n" + 
            "INNER JOIN [VAD20].[DBO].[MA_OPERACIONES_AUTORIZADAS] MA" + "\n" + 
            "ON TR.OperacionRealizada = MA.Codigo" + "\n" + 
            "LEFT JOIN [VAD10].[DBO].[MA_USUARIOS] Cajero" + "\n" + 
            "ON TR.CodigoCajero = Cajero.CodUsuario" + "\n" + 
            "INNER JOIN [VAD10].[DBO].[MA_USUARIOS] Supervisor" + "\n" + 
            "ON TR.CodigoUsuarioAutorizante = Supervisor.CodUsuario" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + " " +
            "ORDER BY TR.ID";

            pRsDataAutorizaciones = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataVentas.first();
            pRsAutorizaciones.get(0).populate(pRsDataAutorizaciones);
            
            return pRsDataAutorizaciones.first();
            
        } catch (Exception ex) {
            gLogger.EscribirLog(ex, "Error al buscar datos de operaciones autorizadas pendientes por procesar.");
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    public void ConstruirRegistrosDeAutorizaciones(Boolean pReprocesarFallidos)
    {
        
        String mUltID = new String(); String mUltFec = new String();
        
        try {
            
            FilteredRowSet RsAutorizaciones = null;
            
            List<FilteredRowSet> lRsAutorizaciones = Arrays.asList(new FilteredRowSet[]{ RsAutorizaciones });
            
            HashMap RegistrosFallidos = new HashMap();
            
            Object tmpVal;
            
            //if (pReprocesarFallidos)
                //MarcarRegistrosDeAutorizaciones(RegistrosPendientesPorLote, true);
            
            if (ObtenerDatosOperacionesAutorizadas(lRsAutorizaciones, pReprocesarFallidos))
            {
                    if (gTestMode) {
                        
                    RsAutorizaciones = lRsAutorizaciones.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsAutorizaciones.getMetaData();
                    
                    while (RsAutorizaciones.next())
                    {
                        
                        TestContinue: {
                        
                        mUltID = RsAutorizaciones.getString("TransID");
                        mUltFec = RsAutorizaciones.getString("Fecha");
                        
                        /*try { assert ((mUltID.equals("XXXXXXXXXXXXXX"))); } catch (AssertionError ae) 
                        { 
                            System.out.println(ae.toString()); 
                        }*/
                        
                        /*for (int i = 1; i <= RsVentas.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsVentas.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsVentas.getObject(i).toString());
                        }*/
                        
                        HashMap<String, Object> tmpRowTR = null;
                        HashMap<String, Object> tmpRowTRExt = null;
                        
                        HashMap<String, HashMap<String, Object>> TestStruc = null;
                        HashMap<String, HashMap<String, Object>>  Transaction = null;
                        HashMap<String, HashMap<String, Object>>  TransactionExt = null;
                        
                        JCoTable Return = null;
                        
                        Transaction = new HashMap<String, HashMap<String, Object>>();
                        TransactionExt = new HashMap<String, HashMap<String, Object>>();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTRExt = new HashMap<String, Object>();
                        
                        Transaction.put("1", tmpRowTR);
                        
                        //if (!gCodLocalidadSAP.isEmpty())
                            tmpRowTR.put("RETAILSTOREID", gCodLocalidadSAP);
                        //else
                            
                        tmpRowTR.put("BUSINESSDAYDATE", dtFmt.format(RsAutorizaciones.getDate("Fecha")));
                        
                        String TransactionTypeCode = null;
                        if (ListaAsociacionOperacionesAutorizadas.containsKey(
                        RsAutorizaciones.getString("OperacionRealizada")))
                        {
                            TransactionTypeCode = ListaAsociacionOperacionesAutorizadas.get(
                            RsAutorizaciones.getString("OperacionRealizada")).toString();
                        }

                        if (TransactionTypeCode == null)
                        {
                            // Controlar Error Registro de Ventas.
                            MarcarFallidosOperacionesAutorizadas(RsAutorizaciones.getString("TransID"));
                            break TestContinue;
                        }
                        
                        tmpRowTR.put("TRANSACTIONTYPECODE", TransactionTypeCode);
                        tmpRowTR.put("WORKSTATIONID", RsAutorizaciones.getString("Caja"));
                        tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", RsAutorizaciones.getString("NumeroDocumento")
                        .substring(RsAutorizaciones.getString("Caja").length()));
                        
                        tmpRowTR.put("BEGINDATETIMESTAMP", dtFmtLong.format(RsAutorizaciones.getDate("Fecha")));
                        tmpRowTR.put("ENDDATETIMESTAMP", dtFmtLong.format(RsAutorizaciones.getDate("Fecha")));
                        tmpRowTR.put("DEPARTMENT", new String());
                        tmpRowTR.put("OPERATORQUALIFIER", new String());
                        tmpRowTR.put("OPERATORID", RsAutorizaciones.getString("CodigoCajero"));
                        tmpRowTR.put("TRANSACTIONCURRENCY", gMonedaSAPPredeterminada);
                        tmpRowTR.put("TRANSACTIONCURRENCY_ISO", new String());
                        tmpRowTR.put("PARTNERQUALIFIER", new String());
                        tmpRowTR.put("PARTNERID", new String());
                        
                        if (TransactionTypeCode.equalsIgnoreCase(gAutorizacionSupervisor_CodigoSAP))
                        {
                            
                            // DATOS ADICIONALES DE LA TRANSACCIÓN.
                            
                            int nPosTRExt = 0;

                            nPosTRExt++;
                            tmpRowTRExt = new HashMap<String, Object>();
                            TransactionExt.put(String.valueOf(nPosTRExt), tmpRowTRExt);

                            tmpRowTRExt.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTRExt.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTRExt.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTRExt.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTRExt.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTRExt.put("FIELDGROUP", new String());
                            tmpRowTRExt.put("FIELDNAME", "MOTIVO_AUT");
                            tmpRowTRExt.put("FIELDVALUE", RsAutorizaciones.getString("Proceso"));

                            nPosTRExt++;
                            tmpRowTRExt = new HashMap<String, Object>();
                            TransactionExt.put(String.valueOf(nPosTRExt), tmpRowTRExt);

                            tmpRowTRExt.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                            tmpRowTRExt.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                            tmpRowTRExt.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                            tmpRowTRExt.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                            tmpRowTRExt.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                            tmpRowTRExt.put("FIELDGROUP", new String());
                            tmpRowTRExt.put("FIELDNAME", "NIVEL_AUTO");
                            tmpRowTRExt.put("FIELDVALUE", RsAutorizaciones.getString("NivelSupervisor"));
                            
                            //tmpRowTR.remove("OPERATORID");
                            //tmpRowTR.put("OPERATORID", RsAutorizaciones.getString("CodigoUsuarioAutorizante"));
                            tmpRowTR.replace("OPERATORID", RsAutorizaciones.getString("CodigoUsuarioAutorizante"));
                            
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.

                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        try{
                            
                            //gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                            
                            int x = (int) (1 + (Math.random() * 10));
                            
                            if (x <= 2)
                            {
                                throw new JCoException(0, "sendfail");
                            }
                            
                            TransaccionEnviada = true;
                            
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de autorizaciones.");
                            //System.exit(0);
                        }

                        //Return = gCnSAP.fTransVentas.getTableParameterList().getTable("RETURN");
                        
//                        
//                        int ReturnRowCount = Return.getNumRows();
//                        
//                        if (ReturnRowCount <= 0)
//                        {
//                            // Envío exitoso sin ningún mensaje en particular.
//                            TransaccionEnviada = true;
//                        }
//                        else
//                        {
//                            
//                            TransaccionEnviada = true;
//                            
//                            // Revisar Mensajes.
//                            
//                            String RetFullLog = "";
//                            String RetLog = "";
//                            
//                            for (int i = 0; i < ReturnRowCount; i++) 
//                            {
//                                
//                                Return.setRow(i);
//                                
//                                String RetLnLog = "";
//                                
//                                if (gDebugMode)
//                                {
//                                    RetLnLog = GetLines() +
//                                    "TYPE => " + Return.getString("TYPE") + GetTab() +
//                                    "ID => " + Return.getString("ID") + GetTab() +
//                                    "NUMBER => " + Return.getString("NUMBER") + GetTab() +
//                                    "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
//                                    "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
//                                    "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
//                                    "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
//                                    "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
//                                    "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
//                                    "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
//                                    "ROW => " + Return.getString("ROW") + GetTab() +
//                                    "FIELD => " + Return.getString("FIELD") + GetTab() +
//                                    "SYSTEM => " + Return.getString("SYSTEM");
//                                    RetFullLog += RetLnLog;
//                                }
//                                
//                                if (Return.getString("TYPE").equalsIgnoreCase("E") ||
//                                Return.getString("TYPE").equalsIgnoreCase("A")) {
//                                    
//                                    TransaccionEnviada = false;
//                                    
//                                    if (!gDebugMode)
//                                        RetLnLog = GetLines() +
//                                        "TYPE => " + Return.getString("TYPE") + GetTab() +
//                                        "ID => " + Return.getString("ID") + GetTab() +
//                                        "NUMBER => " + Return.getString("NUMBER") + GetTab() +
//                                        "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
//                                        "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
//                                        "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
//                                        "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
//                                        "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
//                                        "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
//                                        "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
//                                        "ROW => " + Return.getString("ROW") + GetTab() +
//                                        "FIELD => " + Return.getString("FIELD") + GetTab() +
//                                        "SYSTEM => " + Return.getString("SYSTEM");
//                                    
//                                    RetLog += RetLnLog;
//                                    
//                                }
//                                
//                            }
//                            
//                        }
                        
                        if (TransaccionEnviada) 
                        {
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE TR_OPERACIONES_AUTORIZADAS SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (ID) = " + RsAutorizaciones.getString("TransID") + "");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("Operacion de Autorización ID [" + RsAutorizaciones.getString("TransID") + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }

                        // Próxima Transacción.
                     
                    }
                        
                    
                    
                    }
                        
                        
                        
                        
                        
                    }
                    else // ConstruirRegistrosDeAutorizaciones:
                    {
                    
                    RsAutorizaciones = lRsAutorizaciones.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsAutorizaciones.getMetaData();

                    while (RsAutorizaciones.next())
                    {
                        
                        NextTrans: {
                        
                        mUltID = RsAutorizaciones.getString("TransID");
                        mUltFec = RsAutorizaciones.getString("Fecha");
                        
                        /*for (int i = 1; i <= RsVentas.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsVentas.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsVentas.getObject(i).toString());
                        }*/
                        
                        JCoStructure TestStruc = null;
                        JCoTable Transaction = null;
                        JCoTable TransactionExt = null;
                        
                        JCoTable Return = null;
                        
                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_LOCKWAIT", 0);

                        TestStruc = gCnSAP.fTransVentas.getImportParameterList().getStructure("I_SOURCEDOCUMENTLINK");

                        TestStruc.setValue("KEY", new String());
                        TestStruc.setValue("TYPE", new String());
                        TestStruc.setValue("LOGICALSYSTEM", new String());

                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_SOURCEDOCUMENTLINK", TestStruc);
                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_COMMIT", "X");
                        
                        Transaction = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTION");
                        
                        Transaction.clear();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        Transaction.appendRow();
                        
                        //if (!gCodLocalidadSAP.isEmpty())
                            Transaction.setValue("RETAILSTOREID", Left(gCodLocalidadSAP, 10));
                        //else
                        Transaction.setValue("BUSINESSDAYDATE", Left(dtFmt.format(RsAutorizaciones.getDate("Fecha")), 8));

                        String TransactionTypeCode = null;
                        
                        if (ListaAsociacionOperacionesAutorizadas.containsKey(
                        RsAutorizaciones.getString("OperacionRealizada")))
                        {
                            TransactionTypeCode = ListaAsociacionOperacionesAutorizadas.get(
                            RsAutorizaciones.getString("OperacionRealizada")).toString();
                        }

                        if (TransactionTypeCode == null)
                        {
                            
                            // Controlar Error Registro de Ventas.
                            MarcarFallidosOperacionesAutorizadas(RsAutorizaciones.getString("TransID"));
                            break NextTrans;
                        }
                        
                        Transaction.setValue("TRANSACTIONTYPECODE", TransactionTypeCode);
                        
                        Transaction.setValue("WORKSTATIONID", Left(RsAutorizaciones.getString("Caja"), 10));
                        Transaction.setValue("TRANSACTIONSEQUENCENUMBER", Left(RsAutorizaciones.getString("NumeroDocumento")
                        .substring(RsAutorizaciones.getString("Caja").length()), 20));
                        
                        Transaction.setValue("BEGINDATETIMESTAMP", Left(dtFmtLong.format(RsAutorizaciones.getDate("Fecha")), 14));
                        Transaction.setValue("ENDDATETIMESTAMP", Left(dtFmtLong.format(RsAutorizaciones.getDate("Fecha")), 14));
                        Transaction.setValue("DEPARTMENT", new String());
                        Transaction.setValue("OPERATORQUALIFIER", new String());
                        Transaction.setValue("OPERATORID", Left(RsAutorizaciones.getString("CodigoCajero"), 30));
                        Transaction.setValue("TRANSACTIONCURRENCY", gMonedaSAPPredeterminada);
                        Transaction.setValue("TRANSACTIONCURRENCY_ISO", new String());
                        Transaction.setValue("PARTNERQUALIFIER", new String());
                        Transaction.setValue("PARTNERID", new String());
                        
                        if (TransactionTypeCode.equalsIgnoreCase(gAutorizacionSupervisor_CodigoSAP))
                        {
                            
                            // DATOS ADICIONALES DE LA TRANSACCIÓN.
                            
                            TransactionExt = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTIONEXT");
                            
                            int nPosTRExt = 0;

                            nPosTRExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosTRExt);

                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "MOTIVO_AUT");
                            TransactionExt.setValue("FIELDVALUE", Left(RsAutorizaciones.getString("Proceso"), 40));

                            nPosTRExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosTRExt);

                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "NIVEL_AUTO");
                            TransactionExt.setValue("FIELDVALUE", Left(RsAutorizaciones.getString("NivelSupervisor"), 40));
                            
                            Transaction.setValue("OPERATORID", Left(RsAutorizaciones.getString("CodigoUsuarioAutorizante"), 30));
                            
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean SendError = false;
                        
                        try{
                            gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de autorizaciones.");
                            SendError = true;
                            //System.exit(0);
                        }

                        Return = gCnSAP.fTransVentas.getTableParameterList().getTable("RETURN");
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        int ReturnRowCount = Return.getNumRows();
                        
                        if (!SendError)
                            if (ReturnRowCount <= 0)
                            {
                                // Envío exitoso sin ningún mensaje en particular.
                                TransaccionEnviada = true;
                            }
                            else
                            {

                                TransaccionEnviada = true;

                                // Revisar Mensajes.

                                String RetFullLog = "";
                                String RetLog = "";

                                for (int i = 0; i < ReturnRowCount; i++) 
                                {

                                    Return.setRow(i);

                                    String RetLnLog = "";

                                    if (gDebugMode)
                                    {
                                        RetLnLog = GetLines() +
                                        "TYPE => " + Return.getString("TYPE") + GetTab() +
                                        "ID => " + Return.getString("ID") + GetTab() +
                                        "NUMBER => " + Return.getString("NUMBER") + GetTab() +
                                        "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
                                        "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
                                        "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
                                        "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
                                        "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
                                        "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
                                        "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
                                        "ROW => " + Return.getString("ROW") + GetTab() +
                                        "FIELD => " + Return.getString("FIELD") + GetTab() +
                                        "SYSTEM => " + Return.getString("SYSTEM");
                                        RetFullLog += RetLnLog;
                                    }

                                    if (Return.getString("TYPE").equalsIgnoreCase("E") ||
                                    Return.getString("TYPE").equalsIgnoreCase("A")) {

                                        TransaccionEnviada = false;

                                        if (!gDebugMode)
                                            RetLnLog = GetLines() +
                                            "TYPE => " + Return.getString("TYPE") + GetTab() +
                                            "ID => " + Return.getString("ID") + GetTab() +
                                            "NUMBER => " + Return.getString("NUMBER") + GetTab() +
                                            "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
                                            "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
                                            "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
                                            "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
                                            "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
                                            "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
                                            "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
                                            "ROW => " + Return.getString("ROW") + GetTab() +
                                            "FIELD => " + Return.getString("FIELD") + GetTab() +
                                            "SYSTEM => " + Return.getString("SYSTEM");

                                        RetLog += RetLnLog;

                                    }

                                }

                            }
                        
                        if (TransaccionEnviada) 
                        {
                            
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE TR_OPERACIONES_AUTORIZADAS SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (ID) = " + RsAutorizaciones.getString("TransID") + "");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("Operacion de Autorización ID [" + RsAutorizaciones.getString("TransID") + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }

                    // Próxima Transacción.
                     
                    }
                    
                    }
                
                    System.out.println("Lote de Autorizaciones Finalizado con Exito.");
                
                }
                
            }
            else
            {
                System.out.println("No se encontraron registros de autorizaciones pendientes por sincronizar.");
            }
            
        } catch(Exception Ex) {
            System.out.println(Ex.getMessage());
            gLogger.EscribirLog(Ex, "Error al procesar registros de Autorizaciones.");
            if (!mUltID.isEmpty())
                gLogger.EscribirLog("ID Autorización en proceso => " + mUltID + GetTab() + 
                "Fecha => " + mUltFec + GetTab() +
                "");
        }
        
    }
    
    // RESUMEN CIERRES DE CAJA (TOTALES POSDM)
    
    public void SincronizarCierres(){
        
        if (!gTransferirCierres) return;
        
        if (HayCierresPendientes(RegistrosPendientesPorCorrida) || 
        HayCierresPendientes(RegistrosPendientesPorLote) || 
        HayCierresPendientes(RegistrosPendientesFallidos)) {
            ProcesarCierres(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
        }
        
        //if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosDeCierres(RegistrosPendientesPorCorrida)) { // Procesar registros nuevos.
                ProcesarCierres();
            }

    }
    
    public Boolean HayCierresPendientes(String pEstatus){

        try {

        ResultSet RsDatosPendientes; Boolean exec;
        
        RsDatosPendientes = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).
        executeQuery("SELECT COUNT(*) AS Registros FROM MA_CIERRES " +
        "WHERE cs_Sync_SxS = '" + pEstatus + "' AND c_Concepto = 'CIT'");
        RsDatosPendientes.first();
        
        long RetRows = RsDatosPendientes.getLong("Registros");
        
        return (RetRows > 0);
        
        } catch (Exception Ex) { return false; }
        
    }
    
    public Boolean MarcarFallidosCierres(String pTransID)
    {
        
        //ResultSet Datos;
        int RowsAffected = 0;
        
        try {
            RowsAffected = gConexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY).executeUpdate(
            "UPDATE [VAD20].[DBO].[MA_CIERRES] SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "'" + GetLines() + 
            "WHERE (ID) = " + pTransID + "");
            if (RowsAffected > 0) 
                return true;
            else
                return false;
        } catch (SQLException ex) {
            return false;
        }
        
    }
    
    public void ConstruirRegistrosDeCierres()
    {
        ConstruirRegistrosDeCierres(false);
    }
    
    public void ProcesarLoteDeCierres()
    {
        
        if (HayCierresPendientes(RegistrosPendientesPorLote))
        {
            // Procesar primero los pendientes
            // Antes de marcar el próximo Lote.
            ConstruirRegistrosDeCierres();
            //nLotesProcesados++;
        }
        
        //if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosDeCierres(RegistrosPendientesPorLote)) 
            {
                ConstruirRegistrosDeCierres();
                //nLotesProcesados++;
            }
        
    }
    
    public void ProcesarCierres()
    {
        
        if (HayCierresPendientes(RegistrosPendientesFallidos))
            ConstruirRegistrosDeCierres(true); // Intentar procesar los Fallidos una sola vez.
        
        while (HayCierresPendientes(RegistrosPendientesPorCorrida) ||
        HayCierresPendientes(RegistrosPendientesPorLote)) {
            ProcesarLoteDeCierres();
            break; // Para este tipo de operaciones por los momentos solo se estará soportando un solo envío (un lote).
        }

    }
    
    public Boolean MarcarRegistrosDeCierres(String pEstatus, Boolean pFallidos)
    {
        
        try {

            int RegistrosAfectados = 0; String pEstatusPrevio = ""; String mRegistrosXLote = "";

            String ExcluirVNF = ""; String SQLMarcaje = "";
            
            switch (pEstatus) {
                case RegistrosPendientesPorCorrida:
                    pEstatusPrevio = "'" + RegistrosNuevos + "'";
                    break;
                case RegistrosPendientesPorLote:
                    if (pFallidos)
                        pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                    else {
                        pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                        mRegistrosXLote = "AND (c_Concepto + c_Documento + c_CodLocalidad) IN (" + "\n" + 
                        "SELECT TOP (" + nOperacionesAutorizadasPorLote + ") (c_Concepto + c_Documento + c_CodLocalidad) FROM MA_CIERRES" + "\n" + 
                        "WHERE cs_Sync_SxS = " + pEstatusPrevio + " AND c_Concepto = 'CIT' ORDER BY c_Documento)";
                    }
                    break;
                default: // Guardados con Exito.
                    pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                    break;
            }

            SQLMarcaje = "UPDATE MA_CIERRES SET cs_Sync_SxS = '" + pEstatus + "'" + "\n" + 
            "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ")" + mRegistrosXLote + ExcluirVNF;

            if (gDebugMode) gLogger.EscribirLog(SQLMarcaje);

            RegistrosAfectados = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeUpdate(SQLMarcaje);
            
            return (RegistrosAfectados > 0);

        } catch (Exception Ex) { 
            return false; 
        }

    }
    
    public Boolean MarcarRegistrosDeCierres(String pEstatus)
    {
        return MarcarRegistrosDeCierres(pEstatus, false);
    }
    
    public Boolean ObtenerDatosCierres(List<FilteredRowSet> pRsCierres,
    List<FilteredRowSet> pRsTotalesCierres, Boolean pReprocesarFallidos)
    {
        
        ResultSet pRsDataCierres = null;
        ResultSet pRsDataTotalesCierres = null;
        
        try {

            pRsCierres.set(0, new FilteredRowSetImpl());
            pRsTotalesCierres.set(0, new FilteredRowSetImpl());
            
            String mSQL = null;

            String ExcluirVNF = new String();
            
            String mEstatus;
            
            if (pReprocesarFallidos)
                mEstatus = RegistrosPendientesFallidos;
            else
                mEstatus = RegistrosPendientesPorLote;
            
            mSQL = "SELECT *, (c_Concepto + c_Documento + c_CodLocalidad) AS TransID FROM MA_CIERRES " + "\n" + 
            "WHERE c_Concepto = 'CIT' AND (cs_Sync_SxS = '" + mEstatus + "') " + "\n" + 
            "ORDER BY d_Fecha";
            
            pRsDataCierres = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            
            pRsCierres.get(0).populate(pRsDataCierres);
            
            mSQL = "" +
            "SELECT * FROM (" + "\n" + 
            "" + "\n" + 
            "SELECT ('CIT' + c_Documento + c_CodLocalidad) AS TransID, c_CodLocalidad, c_Documento, SUM(Total_2201) AS Total_2201, SUM(CantLn_2201) AS CantLn_2201, " + "\n" + 
            "SUM(Total_2001) AS Total_2001, SUM(CantLn_2001) AS CantLn_2001, " + "\n" + 
            "SUM(Total_2901) AS Total_2901, SUM(CantLn_2901) AS CantLn_2901, " + "\n" + 
            "SUM(Total_2202) AS Total_2202, SUM(CantLn_2202) AS CantLn_2202, " + "\n" + 
            "SUM(Total_2801) AS Total_2801, SUM(CantLn_2801) AS CantLn_2801, " + "\n" + 
            "SUM(Total_2902) AS Total_2902, SUM(CantLn_2902) AS CantLn_2902 " + "\n" + 
            "FROM (" + "\n" + 
            "SELECT SUM(Total) AS Total_2201, COUNT(*) AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902," + "\n" + 
            "TURN.c_Documento, TURN.c_CodLocalidad FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) = 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'VEN' AND TRANS.Impuesto = 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad " + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "SUM(Total) AS Total_2001, COUNT(*) AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento, TURN.c_CodLocalidad  FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) = 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'VEN' AND TRANS.Impuesto > 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad " + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "SUM(Total) AS Total_2901, COUNT(*) AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801, " + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento, TURN.c_CodLocalidad  FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) = 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'VEN' AND TRANS.Cantidad < 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad " + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "SUM(Total) AS Total_2202, COUNT(*) AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento, TURN.c_CodLocalidad  FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) = 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'DEV' AND TRANS.Impuesto = 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad " + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "SUM(Total) AS Total_2801, COUNT(*) AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento, TURN.c_CodLocalidad  FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) = 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'DEV' AND TRANS.Impuesto < 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad " + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "SUM(Total) AS Total_2902, COUNT(*) AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento, TURN.c_CodLocalidad  FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) = 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'DEV' AND TRANS.Cantidad > 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad " + "\n" + 
            ") TBFinal1 GROUP BY c_Documento, c_CodLocalidad " + "\n" + 
            "" + "\n" + 
            "UNION ALL" + "\n" + 
            "" + "\n" + 
            "SELECT ('CIT' + TransID) AS TransID, c_Documento, c_CodLocalidad, SUM(Total_2201) AS Total_2201, SUM(CantLn_2201) AS CantLn_2201, " + "\n" + 
            "SUM(Total_2001) AS Total_2001, SUM(CantLn_2001) AS CantLn_2001, " + "\n" + 
            "SUM(Total_2901) AS Total_2901, SUM(CantLn_2901) AS CantLn_2901, " + "\n" + 
            "SUM(Total_2202) AS Total_2202, SUM(CantLn_2202) AS CantLn_2202, " + "\n" + 
            "SUM(Total_2801) AS Total_2801, SUM(CantLn_2801) AS CantLn_2801, " + "\n" + 
            "SUM(Total_2902) AS Total_2902, SUM(CantLn_2902) AS CantLn_2902 " + "\n" + 
            "FROM (" + "\n" + 
            "SELECT SUM(Total) AS Total_2201, COUNT(*) AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902," + "\n" + 
            "TURN.c_Documento + '_' + CONVERT(NVARCHAR(MAX), CAST(TRANS.f_Fecha AS DATE), 112) AS c_Documento, " + "\n" + 
            "(TURN.c_Documento + TURN.c_CodLocalidad) AS TransID, TURN.c_CodLocalidad FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) <> 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'VEN' AND TRANS.Impuesto = 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad, CAST(TRANS.f_Fecha AS DATE)" + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "SUM(Total) AS Total_2001, COUNT(*) AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento + '_' + CONVERT(NVARCHAR(MAX), CAST(TRANS.f_Fecha AS DATE), 112) AS c_Documento, " + "\n" + 
            "TURN.c_Documento AS TransID, TURN.c_CodLocalidad FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) <> 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'VEN' AND TRANS.Impuesto > 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad, CAST(TRANS.f_Fecha AS DATE)" + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "SUM(Total) AS Total_2901, COUNT(*) AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento + '_' + CONVERT(NVARCHAR(MAX), CAST(TRANS.f_Fecha AS DATE), 112) AS c_Documento, " + "\n" + 
            "TURN.c_Documento AS TransID, TURN.c_CodLocalidad FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) <> 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'VEN' AND TRANS.Cantidad < 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad, CAST(TRANS.f_Fecha AS DATE)" + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "SUM(Total) AS Total_2202, COUNT(*) AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento + '_' + CONVERT(NVARCHAR(MAX), CAST(TRANS.f_Fecha AS DATE), 112) AS c_Documento, " + "\n" + 
            "TURN.c_Documento AS TransID, TURN.c_CodLocalidad FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) <> 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'DEV' AND TRANS.Impuesto = 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad, CAST(TRANS.f_Fecha AS DATE)" + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "SUM(Total) AS Total_2801, COUNT(*) AS CantLn_2801," + "\n" + 
            "0 AS Total_2902, 0 AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento + '_' + CONVERT(NVARCHAR(MAX), CAST(TRANS.f_Fecha AS DATE), 112) AS c_Documento, " + "\n" + 
            "TURN.c_Documento AS TransID, TURN.c_CodLocalidad FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) <> 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'DEV' AND TRANS.Impuesto < 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad, CAST(TRANS.f_Fecha AS DATE)" + "\n" + 
            "UNION ALL" + "\n" + 
            "SELECT 0 AS Total_2201, 0 AS CantLn_2201," + "\n" + 
            "0 AS Total_2001, 0 AS CantLn_2001," + "\n" + 
            "0 AS Total_2901, 0 AS CantLn_2901, " + "\n" + 
            "0 AS Total_2202, 0 AS CantLn_2202, " + "\n" + 
            "0 AS Total_2801, 0 AS CantLn_2801," + "\n" + 
            "SUM(Total) AS Total_2902, COUNT(*) AS CantLn_2902, " + "\n" + 
            "TURN.c_Documento + '_' + CONVERT(NVARCHAR(MAX), CAST(TRANS.f_Fecha AS DATE), 112) AS c_Documento, " + "\n" + 
            "TURN.c_Documento AS TransID, TURN.c_CodLocalidad FROM MA_TRANSACCION TRANS INNER JOIN MA_CIERRES TURN" + "\n" + 
            "ON TRANS.c_Caja = TURN.c_CodCaja AND TRANS.Turno = TURN.Turno" + "\n" + 
            "AND TRANS.c_Usuario = TURN.c_CodCajero AND TRANS.c_Localidad = TURN.c_CodLocalidad" + "\n" + 
            "WHERE TURN.c_Concepto = 'CIT' AND DATEDIFF(DAY, TURN.d_Fecha_Cierre, TURN.d_Fecha) <> 0" + "\n" + 
            "AND TURN.cs_Sync_SxS = '" + mEstatus + "'" + "\n" + 
            "AND TRANS.c_Concepto = 'DEV' AND TRANS.Cantidad > 0" + "\n" + 
            "GROUP BY TURN.c_Documento, TURN.c_CodLocalidad, CAST(TRANS.f_Fecha AS DATE)" + "\n" + 
            ") TBFinal2 GROUP BY c_Documento, c_CodLocalidad, TransID" + "\n" + 
            "" + "\n" + 
            ") TBFinal " + "\n" + 
            "ORDER BY c_Documento";
            
            pRsDataTotalesCierres = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            
            pRsTotalesCierres.get(0).populate(pRsDataTotalesCierres);
            
            return pRsDataCierres.first();
            
        } catch (Exception ex) {
            gLogger.EscribirLog(ex, "Error al buscar datos de cierres pendientes por procesar.");
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    public void ConstruirRegistrosDeCierres(Boolean pReprocesarFallidos)
    {
        
        String mUltDoc = new String(); String mUltID = new String(); String mUltFec = new String();
        java.util.Date mFechaCierre = null;
        
        try {
            
            FilteredRowSet RsCierres = null;
            FilteredRowSet RsTotalesCierres = null;
            
            List<FilteredRowSet> lRsCierres = Arrays.asList(new FilteredRowSet[]{ RsCierres });
            List<FilteredRowSet> lRsTotalesCierres = Arrays.asList(new FilteredRowSet[]{ RsTotalesCierres });
            
            HashMap RegistrosFallidos = new HashMap();
            
            Object tmpVal;
            
            //if (pReprocesarFallidos)
                //MarcarRegistrosDeAutorizaciones(RegistrosPendientesPorLote, true);
            
            if (ObtenerDatosCierres(lRsCierres, lRsTotalesCierres, pReprocesarFallidos))
            {
                    if (gTestMode) {
                        
                    RsCierres = lRsCierres.get(0);
                    RsTotalesCierres = lRsTotalesCierres.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsTotalesCierres.getMetaData();
                    
                    while (RsTotalesCierres.next())
                    {
                        
                        TestContinue: {
                        
                        RsCierres.setFilter(new StringColumnFilter(RsTotalesCierres.getString("TransID"), "TransID"));
                        
                        RsCierres.first();
                        
                        mUltDoc = RsCierres.getString("c_Documento");
                        mUltID = RsTotalesCierres.getString("TransID");
                        mUltFec = RsCierres.getString("d_Fecha");
                        
                        String[] StrFechaCierre = mUltID.split("_", 1);
                        
                        java.util.Date HoraInicio = null; java.util.Date HoraFin = null;
                        
                        if (StrFechaCierre.length == 2)
                        {
                            mFechaCierre = DateSerial(Integer.valueOf(StrFechaCierre[1].substring(1, 4)), 
                            Integer.valueOf(StrFechaCierre[1].substring(5, 6)),
                            Integer.valueOf(StrFechaCierre[1].substring(7, 8)));
                            HoraInicio = TimeSerial(0, 0, 0);
                            HoraFin = TimeSerial(23, 59, 59);
                        } else {
                            mFechaCierre = RsCierres.getDate("d_Fecha");
                            HoraInicio = RsCierres.getDate("h_Inicio");
                            HoraFin = RsCierres.getDate("h_Final");
                        }
                        
                        /*try { assert ((mUltID.equals("XXXXXXXXXXXXXX"))); } catch (AssertionError ae) 
                        { 
                            System.out.println(ae.toString()); 
                        }*/
                        
                        /*for (int
                            RsVentas.getObject(i).toString()); i = 1; i <= RsVentas.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsVentas.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsVentas.getObject(i).toString());
                        }*/
                        
                        HashMap<String, Object> tmpRowTR = null;
                        HashMap<String, Object> tmpRowTRExt = null;
                        
                        HashMap<String, HashMap<String, Object>> TestStruc = null;
                        HashMap<String, HashMap<String, Object>>  Transaction = null;
                        HashMap<String, HashMap<String, Object>>  RetailTotals = null;
                        
                        JCoTable Return = null;
                        
                        Transaction = new HashMap<String, HashMap<String, Object>>();
                        RetailTotals = new HashMap<String, HashMap<String, Object>>();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTRExt = new HashMap<String, Object>();
                        
                        Transaction.put("1", tmpRowTR);
                        
                        tmpRowTR.put("RETAILSTOREID", Left(gCodLocalidadSAP, 10));
                        tmpRowTR.put("BUSINESSDAYDATE", Left(dtFmt.format(mFechaCierre), 8));
                        
                        String TransactionTypeCode = "1602";
                        
                        tmpRowTR.put("TRANSACTIONTYPECODE", TransactionTypeCode);
                        
                        tmpRowTR.put("WORKSTATIONID", Left(RsCierres.getString("c_CodCaja"), 10));
                        tmpRowTR.put("TRANSACTIONSEQUENCENUMBER", Left(RsTotalesCierres.getString("c_Documento"), 20));
                        
                        tmpRowTR.put("BEGINDATETIMESTAMP", Left(dtFmtLong.format(
                        DateTimeFrom(mFechaCierre, HoraInicio)), 14));
                        tmpRowTR.put("ENDDATETIMESTAMP", Left(dtFmtLong.format(
                        DateTimeFrom(mFechaCierre, HoraFin)), 14));
                        tmpRowTR.put("DEPARTMENT", new String());
                        tmpRowTR.put("OPERATORQUALIFIER", new String());
                        tmpRowTR.put("OPERATORID", Left(RsCierres.getString("c_CodCajero"), 30));
                        tmpRowTR.put("TRANSACTIONCURRENCY", gMonedaSAPPredeterminada);
                        tmpRowTR.put("TRANSACTIONCURRENCY_ISO", new String());
                        tmpRowTR.put("PARTNERQUALIFIER", new String());
                        tmpRowTR.put("PARTNERID", new String());
                        
                        // LLENAR EL DETALLE
                        
                        RetailTotals.put("1", tmpRowTRExt);
                        
                        tmpRowTRExt.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTRExt.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTRExt.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTRExt.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTRExt.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTRExt.put("RETAILTYPECODE", "2201");
                        tmpRowTRExt.put("SALESAMOUNT", Round(RsTotalesCierres.getDouble("Total_2201"), 8));
                        tmpRowTRExt.put("LINEITEMCOUNT", (RsTotalesCierres.getInt("CantLn_2201")));
                        
                        RetailTotals.put("2", tmpRowTRExt);
                        
                        tmpRowTRExt.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTRExt.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTRExt.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTRExt.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTRExt.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTRExt.put("RETAILTYPECODE", "2001");
                        tmpRowTRExt.put("SALESAMOUNT", Round(RsTotalesCierres.getDouble("Total_2001"), 8));
                        tmpRowTRExt.put("LINEITEMCOUNT", (RsTotalesCierres.getInt("CantLn_2001")));
                        
                        RetailTotals.put("3", tmpRowTRExt);
                        
                        tmpRowTRExt.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTRExt.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTRExt.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTRExt.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTRExt.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTRExt.put("RETAILTYPECODE", "2901");
                        tmpRowTRExt.put("SALESAMOUNT", Round(RsTotalesCierres.getDouble("Total_2901"), 8));
                        tmpRowTRExt.put("LINEITEMCOUNT", (RsTotalesCierres.getInt("CantLn_2901")));
                        
                        RetailTotals.put("4", tmpRowTRExt);
                        
                        tmpRowTRExt.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTRExt.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTRExt.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTRExt.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTRExt.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTRExt.put("RETAILTYPECODE", "2202");
                        tmpRowTRExt.put("SALESAMOUNT", Round(RsTotalesCierres.getDouble("Total_2202"), 8));
                        tmpRowTRExt.put("LINEITEMCOUNT", (RsTotalesCierres.getInt("CantLn_2202")));
                        
                        RetailTotals.put("5", tmpRowTRExt);
                        
                        tmpRowTRExt.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTRExt.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTRExt.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTRExt.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTRExt.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTRExt.put("RETAILTYPECODE", "2801");
                        tmpRowTRExt.put("SALESAMOUNT", Round(RsTotalesCierres.getDouble("Total_2801"), 8));
                        tmpRowTRExt.put("LINEITEMCOUNT", (RsTotalesCierres.getInt("CantLn_2801")));
                        
                        RetailTotals.put("6", tmpRowTRExt);
                        
                        tmpRowTRExt.put("RETAILSTOREID", Transaction.get("1").get("RETAILSTOREID"));
                        tmpRowTRExt.put("BUSINESSDAYDATE", Transaction.get("1").get("BUSINESSDAYDATE"));
                        tmpRowTRExt.put("TRANSACTIONTYPECODE", Transaction.get("1").get("TRANSACTIONTYPECODE"));
                        tmpRowTRExt.put("WORKSTATIONID", Transaction.get("1").get("WORKSTATIONID"));
                        tmpRowTRExt.put("TRANSACTIONSEQUENCENUMBER", Transaction.get("1").get("TRANSACTIONSEQUENCENUMBER"));
                        tmpRowTRExt.put("RETAILTYPECODE", "2902");
                        tmpRowTRExt.put("SALESAMOUNT", Round(RsTotalesCierres.getDouble("Total_2902"), 8));
                        tmpRowTRExt.put("LINEITEMCOUNT", (RsTotalesCierres.getInt("CantLn_2902")));
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        try{
                            
                            //gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                            
                            int x = (int) (1 + (Math.random() * 10));
                            
                            if (x <= 2)
                            {
                                throw new JCoException(0, "sendfail");
                            }
                            
                            TransaccionEnviada = true;
                            
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de cierres.");
                            //System.exit(0);
                        }

                        //Return = gCnSAP.fTransVentas.getTableParameterList().getTable("RETURN");
                        
//                        
//                        int ReturnRowCount = Return.getNumRows();
//                        
//                        if (ReturnRowCount <= 0)
//                        {
//                            // Envío exitoso sin ningún mensaje en particular.
//                            TransaccionEnviada = true;
//                        }
//                        else
//                        {
//                            
//                            TransaccionEnviada = true;
//                            
//                            // Revisar Mensajes.
//                            
//                            String RetFullLog = "";
//                            String RetLog = "";
//                            
//                            for (int i = 0; i < ReturnRowCount; i++) 
//                            {
//                                
//                                Return.setRow(i);
//                                
//                                String RetLnLog = "";
//                                
//                                if (gDebugMode)
//                                {
//                                    RetLnLog = GetLines() +
//                                    "TYPE => " + Return.getString("TYPE") + GetTab() +
//                                    "ID => " + Return.getString("ID") + GetTab() +
//                                    "NUMBER => " + Return.getString("NUMBER") + GetTab() +
//                                    "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
//                                    "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
//                                    "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
//                                    "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
//                                    "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
//                                    "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
//                                    "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
//                                    "ROW => " + Return.getString("ROW") + GetTab() +
//                                    "FIELD => " + Return.getString("FIELD") + GetTab() +
//                                    "SYSTEM => " + Return.getString("SYSTEM");
//                                    RetFullLog += RetLnLog;
//                                }
//                                
//                                if (Return.getString("TYPE").equalsIgnoreCase("E") ||
//                                Return.getString("TYPE").equalsIgnoreCase("A")) {
//                                    
//                                    TransaccionEnviada = false;
//                                    
//                                    if (!gDebugMode)
//                                        RetLnLog = GetLines() +
//                                        "TYPE => " + Return.getString("TYPE") + GetTab() +
//                                        "ID => " + Return.getString("ID") + GetTab() +
//                                        "NUMBER => " + Return.getString("NUMBER") + GetTab() +
//                                        "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
//                                        "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
//                                        "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
//                                        "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
//                                        "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
//                                        "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
//                                        "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
//                                        "ROW => " + Return.getString("ROW") + GetTab() +
//                                        "FIELD => " + Return.getString("FIELD") + GetTab() +
//                                        "SYSTEM => " + Return.getString("SYSTEM");
//                                    
//                                    RetLog += RetLnLog;
//                                    
//                                }
//                                
//                            }
//                            
//                        }
                        
                        if (TransaccionEnviada) 
                        {
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE MA_CIERRES SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_Concepto + c_Documento) = '" + RsCierres.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("Cierre ID [" + mUltID + "] no pudo ser marcado como procesado, pero si fue enviado.");
                            }
                            
                        }

                        // Próxima Transacción.
                     
                    }
                        
                    
                    
                    }
                        
                        
                        
                    }
                    else // ConstruirRegistrosDeCierres:
                    {
                    
                    RsCierres = lRsCierres.get(0);
                    RsTotalesCierres = lRsTotalesCierres.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsTotalesCierres.getMetaData();
                    
                    while (RsTotalesCierres.next())
                    {
                        
                        TestContinue: {
                        
                        RsCierres.setFilter(new StringColumnFilter(RsTotalesCierres.getString("TransID"), "TransID"));
                        
                        RsCierres.first();
                        
                        mUltDoc = RsCierres.getString("c_Documento");
                        mUltID = RsTotalesCierres.getString("TransID");
                        mUltFec = RsCierres.getString("d_Fecha");
                        
                        String[] StrFechaCierre = mUltID.split("_", 1);
                        
                        java.util.Date HoraInicio = null; java.util.Date HoraFin = null;
                        
                        if (StrFechaCierre.length == 2)
                        {
                            mFechaCierre = DateSerial(Integer.valueOf(StrFechaCierre[1].substring(1, 4)), 
                            Integer.valueOf(StrFechaCierre[1].substring(5, 6)),
                            Integer.valueOf(StrFechaCierre[1].substring(7, 8)));
                            HoraInicio = TimeSerial(0, 0, 0);
                            HoraFin = TimeSerial(23, 59, 59);
                        } else {
                            mFechaCierre = RsCierres.getDate("d_Fecha");
                            HoraInicio = RsCierres.getDate("h_Inicio");
                            HoraFin = RsCierres.getDate("h_Final");
                        }
                        
                        /*for (int i = 1; i <= RsCierres.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsCierres.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsCierres.getObject(i).toString());
                        }*/
                        
                        JCoStructure TestStruc = null;
                        JCoTable Transaction = null;
                        JCoTable RetailTotals = null;
                        
                        JCoTable Return = null;
                        
                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_LOCKWAIT", 0);

                        TestStruc = gCnSAP.fTransVentas.getImportParameterList().getStructure("I_SOURCEDOCUMENTLINK");

                        TestStruc.setValue("KEY", new String());
                        TestStruc.setValue("TYPE", new String());
                        TestStruc.setValue("LOGICALSYSTEM", new String());

                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_SOURCEDOCUMENTLINK", TestStruc);
                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_COMMIT", "X");
                        
                        Transaction = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTION");
                        RetailTotals = gCnSAP.fTransVentas.getTableParameterList().getTable("RETAILTOTALS");
                        
                        Transaction.clear();
                        RetailTotals.clear();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        Transaction.appendRow();
                        
                        if (gCodLocalidadSAP.equalsIgnoreCase("*") && gCodLocalidadStellar.equalsIgnoreCase("*"))
                            Transaction.setValue("RETAILSTOREID", Left(RsTotalesCierres.getString("c_CodLocalidad"), 10));
                        else
                            Transaction.setValue("RETAILSTOREID", Left(gCodLocalidadSAP, 10));
                        
                        Transaction.setValue("BUSINESSDAYDATE", Left(dtFmt.format(mFechaCierre), 8));
                        
                        String TransactionTypeCode = "1602";
                        
                        Transaction.setValue("TRANSACTIONTYPECODE", TransactionTypeCode);
                        
                        Transaction.setValue("WORKSTATIONID", Left(RsCierres.getString("c_CodCaja"), 10));
                        Transaction.setValue("TRANSACTIONSEQUENCENUMBER", Left(RsTotalesCierres.getString("c_Documento"), 20));
                        
                        Transaction.setValue("BEGINDATETIMESTAMP", Left(dtFmtLong.format(
                        DateTimeFrom(mFechaCierre, HoraInicio)), 14));
                        Transaction.setValue("ENDDATETIMESTAMP", Left(dtFmtLong.format(
                        DateTimeFrom(mFechaCierre, HoraFin)), 14));
                        Transaction.setValue("DEPARTMENT", new String());
                        Transaction.setValue("OPERATORQUALIFIER", new String());
                        Transaction.setValue("OPERATORID", Left(RsCierres.getString("c_CodCajero"), 30));
                        Transaction.setValue("TRANSACTIONCURRENCY", gMonedaSAPPredeterminada);
                        Transaction.setValue("TRANSACTIONCURRENCY_ISO", new String());
                        Transaction.setValue("PARTNERQUALIFIER", new String());
                        Transaction.setValue("PARTNERID", new String());
                        
                        // LLENAR EL DETALLE
                        
                        RetailTotals.appendRow();
                        
                        RetailTotals.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        RetailTotals.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        RetailTotals.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        RetailTotals.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        RetailTotals.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        RetailTotals.setValue("RETAILTYPECODE", "2201");
                        RetailTotals.setValue("SALESAMOUNT", RawDouble(Round(RsTotalesCierres.getDouble("Total_2201"), 8)));
                        //RetailTotals.setValue("LINEITEMCOUNT", RawDouble((RsTotalesCierres.getDouble("CantLn_2201"))));
                        RetailTotals.setValue("LINEITEMCOUNT", RsTotalesCierres.getInt("CantLn_2201"));
                        
                        RetailTotals.appendRow();
                        
                        RetailTotals.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        RetailTotals.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        RetailTotals.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        RetailTotals.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        RetailTotals.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        RetailTotals.setValue("RETAILTYPECODE", "2001");
                        RetailTotals.setValue("SALESAMOUNT", RawDouble(Round(RsTotalesCierres.getDouble("Total_2001"), 8)));
                        //RetailTotals.setValue("LINEITEMCOUNT", RawDouble((RsTotalesCierres.getDouble("CantLn_2001"))));
                        RetailTotals.setValue("LINEITEMCOUNT", RsTotalesCierres.getInt("CantLn_2001"));
                        
                        RetailTotals.appendRow();
                        
                        RetailTotals.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        RetailTotals.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        RetailTotals.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        RetailTotals.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        RetailTotals.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        RetailTotals.setValue("RETAILTYPECODE", "2901");
                        RetailTotals.setValue("SALESAMOUNT", RawDouble(Round(RsTotalesCierres.getDouble("Total_2901"), 8)));
                        //RetailTotals.setValue("LINEITEMCOUNT", RawDouble((RsTotalesCierres.getDouble("CantLn_2901"))));
                        RetailTotals.setValue("LINEITEMCOUNT", RsTotalesCierres.getInt("CantLn_2901"));
                        
                        RetailTotals.appendRow();
                        
                        RetailTotals.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        RetailTotals.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        RetailTotals.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        RetailTotals.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        RetailTotals.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        RetailTotals.setValue("RETAILTYPECODE", "2202");
                        RetailTotals.setValue("SALESAMOUNT", RawDouble(Round(RsTotalesCierres.getDouble("Total_2202"), 8)));
                        //RetailTotals.setValue("LINEITEMCOUNT", RawDouble((RsTotalesCierres.getDouble("CantLn_2202"))));
                        RetailTotals.setValue("LINEITEMCOUNT", RsTotalesCierres.getInt("CantLn_2202"));
                        
                        RetailTotals.appendRow();
                        
                        RetailTotals.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        RetailTotals.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        RetailTotals.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        RetailTotals.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        RetailTotals.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        RetailTotals.setValue("RETAILTYPECODE", "2801");
                        RetailTotals.setValue("SALESAMOUNT", RawDouble(Round(RsTotalesCierres.getDouble("Total_2801"), 8)));
                        //RetailTotals.setValue("LINEITEMCOUNT", RawDouble((RsTotalesCierres.getDouble("CantLn_2801"))));
                        RetailTotals.setValue("LINEITEMCOUNT", RsTotalesCierres.getInt("CantLn_2801"));
                        
                        RetailTotals.appendRow();
                        
                        RetailTotals.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        RetailTotals.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        RetailTotals.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        RetailTotals.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        RetailTotals.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        RetailTotals.setValue("RETAILTYPECODE", "2902");
                        RetailTotals.setValue("SALESAMOUNT", RawDouble(Round(RsTotalesCierres.getDouble("Total_2902"), 8)));
                        //RetailTotals.setValue("LINEITEMCOUNT", RawDouble((RsTotalesCierres.getDouble("CantLn_2902"))));
                        RetailTotals.setValue("LINEITEMCOUNT", RsTotalesCierres.getInt("CantLn_2902"));
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean SendError = false;
                        
                        try{
                            gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de cierres.");
                            SendError = true;
                            //System.exit(0);
                        }

                        Return = gCnSAP.fTransVentas.getTableParameterList().getTable("RETURN");
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        int ReturnRowCount = Return.getNumRows();
                        
                        if (!SendError)
                            if (ReturnRowCount <= 0)
                            {
                                // Envío exitoso sin ningún mensaje en particular.
                                TransaccionEnviada = true;
                            }
                            else
                            {

                                TransaccionEnviada = true;

                                // Revisar Mensajes.

                                String RetFullLog = "";
                                String RetLog = "";

                                for (int i = 0; i < ReturnRowCount; i++) 
                                {

                                    Return.setRow(i);

                                    String RetLnLog = "";

                                    if (gDebugMode)
                                    {
                                        RetLnLog = GetLines() +
                                        "TYPE => " + Return.getString("TYPE") + GetTab() +
                                        "ID => " + Return.getString("ID") + GetTab() +
                                        "NUMBER => " + Return.getString("NUMBER") + GetTab() +
                                        "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
                                        "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
                                        "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
                                        "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
                                        "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
                                        "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
                                        "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
                                        "ROW => " + Return.getString("ROW") + GetTab() +
                                        "FIELD => " + Return.getString("FIELD") + GetTab() +
                                        "SYSTEM => " + Return.getString("SYSTEM");
                                        RetFullLog += RetLnLog;
                                    }

                                    if (Return.getString("TYPE").equalsIgnoreCase("E") ||
                                    Return.getString("TYPE").equalsIgnoreCase("A")) {

                                        TransaccionEnviada = false;

                                        if (!gDebugMode)
                                            RetLnLog = GetLines() +
                                            "TYPE => " + Return.getString("TYPE") + GetTab() +
                                            "ID => " + Return.getString("ID") + GetTab() +
                                            "NUMBER => " + Return.getString("NUMBER") + GetTab() +
                                            "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
                                            "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
                                            "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
                                            "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
                                            "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
                                            "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
                                            "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
                                            "ROW => " + Return.getString("ROW") + GetTab() +
                                            "FIELD => " + Return.getString("FIELD") + GetTab() +
                                            "SYSTEM => " + Return.getString("SYSTEM");

                                        RetLog += RetLnLog;

                                    }

                                }
                            
                            }
                        
                        if (TransaccionEnviada) 
                        {
                            
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE MA_CIERRES SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_Concepto + c_Documento + c_CodLocalidad) = '" + RsCierres.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("Cierre ID [" + mUltID + "] no pudo ser marcado como procesado, pero si fue enviado.");
                            }
                            
                        }

                    // Próxima Transacción.
                     
                    }
                    
                    }
                
                    System.out.println("Lote de Cierres Finalizado con Exito.");
                    
                }
                    
            }
            else
            {
                System.out.println("No se encontraron registros de cierres pendientes por sincronizar.");
            }
            
        } catch(Exception Ex) {
            System.out.println(Ex.getMessage());
            gLogger.EscribirLog(Ex, "Error al procesar registros de Cierres.");
            if (!mUltID.isEmpty())
                gLogger.EscribirLog("Cierre en proceso => " + mUltID + GetTab() + 
                mUltDoc + GetTab() +
                "Fecha => " + mUltFec + GetTab() +
                "");
        }
        
    }
    
    // DONACIONES POR VENTA POS - DONACIONES SAP
    
    public void SincronizarDonaciones(){
        
        if (!gTransferirDonaciones) return;
        
        if (HayDonacionesPendientes(RegistrosPendientesPorCorrida) || 
        HayDonacionesPendientes(RegistrosPendientesPorLote) || 
        HayDonacionesPendientes(RegistrosPendientesFallidos)) {
            ProcesarDonaciones(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
        }
        
        //if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosDeDonaciones(RegistrosPendientesPorCorrida)) { // Procesar registros nuevos.
                ProcesarDonaciones();
            }

    }
    
    public Boolean HayDonacionesPendientes(String pEstatus){

        try {

        ResultSet RsDatosPendientes; Boolean exec;
        
        RsDatosPendientes = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).
        executeQuery("SELECT COUNT(*) AS Registros FROM MA_DONACIONES " +
        "WHERE cs_Sync_SxS = '" + pEstatus + "'");
        RsDatosPendientes.first();
        
        long RetRows = RsDatosPendientes.getLong("Registros");
        
        return (RetRows > 0);
        
        } catch (Exception Ex) { return false; }
        
    }
    
    public Boolean MarcarFallidosDonaciones(String pTransID)
    {
        
        //ResultSet Datos;
        int RowsAffected = 0;
        
        try {
            RowsAffected = gConexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY).executeUpdate(
            "UPDATE [VAD20].[DBO].[MA_DONACIONES] SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "'" + GetLines() + 
            "WHERE (c_Documento + c_Sucursal) = " + pTransID + "");
            if (RowsAffected > 0) 
                return true;
            else
                return false;
        } catch (SQLException ex) {
            return false;
        }
        
    }
    
    public void ConstruirRegistrosDeDonaciones()
    {
        ConstruirRegistrosDeDonaciones(false);
    }
    
    public void ProcesarLoteDeDonaciones()
    {
        
        if (HayDonacionesPendientes(RegistrosPendientesPorLote))
        {
            // Procesar primero los pendientes
            // Antes de marcar el próximo Lote.
            ConstruirRegistrosDeDonaciones();
            //nLotesProcesados++;
        }
        
        //if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosDeDonaciones(RegistrosPendientesPorLote)) 
            {
                ConstruirRegistrosDeDonaciones();
                //nLotesProcesados++;
            }
        
    }
    
    public void ProcesarDonaciones()
    {
        
        if (HayDonacionesPendientes(RegistrosPendientesFallidos))
            ConstruirRegistrosDeDonaciones(true); // Intentar procesar los Fallidos una sola vez.
        
        while (HayDonacionesPendientes(RegistrosPendientesPorCorrida) ||
        HayDonacionesPendientes(RegistrosPendientesPorLote)) {
            ProcesarLoteDeDonaciones();
            break; // Para este tipo de operaciones por los momentos solo se estará soportando un solo envío (un lote).
        }

    }
    
    public Boolean MarcarRegistrosDeDonaciones(String pEstatus, Boolean pFallidos)
    {
        
        try {

            int RegistrosAfectados = 0; String pEstatusPrevio = ""; String mRegistrosXLote = "";

            String ExcluirVNF = ""; String SQLMarcaje = "";
            
            switch (pEstatus) {
                case RegistrosPendientesPorCorrida:
                    pEstatusPrevio = "'" + RegistrosNuevos + "'";
                    break;
                case RegistrosPendientesPorLote:
                    if (pFallidos)
                        pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                    else {
                        pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                        mRegistrosXLote = "AND ID IN (" + "\n" + 
                        "SELECT TOP (" + nOperacionesAutorizadasPorLote + ") ID FROM MA_DONACIONES" + "\n" + 
                        "WHERE cs_Sync_SxS = " + pEstatusPrevio + " ORDER BY c_Documento)";
                    }
                    break;
                default: // Guardados con Exito.
                    pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                    break;
            }

            SQLMarcaje = "UPDATE MA_DONACIONES SET cs_Sync_SxS = '" + pEstatus + "'" + "\n" + 
            "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ")" + mRegistrosXLote + ExcluirVNF;

            if (gDebugMode) gLogger.EscribirLog(SQLMarcaje);

            RegistrosAfectados = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeUpdate(SQLMarcaje);
            
            return (RegistrosAfectados > 0);

        } catch (Exception Ex) { 
            return false; 
        }

    }
    
    public Boolean MarcarRegistrosDeDonaciones(String pEstatus)
    {
        return MarcarRegistrosDeDonaciones(pEstatus, false);
    }
    
    public Boolean ObtenerDatosDonaciones(List<FilteredRowSet> pRsDonaciones,
    List<FilteredRowSet> pRsDetalleDonaciones, Boolean pReprocesarFallidos)
    {
        
        ResultSet pRsDataDonaciones = null;
        ResultSet pRsDataDetalleDonaciones = null;
        
        try {

            pRsDonaciones.set(0, new FilteredRowSetImpl());
            pRsDetalleDonaciones.set(0, new FilteredRowSetImpl());
            
            String mSQL = null;

            String ExcluirVNF = new String();
            
            String mEstatus;
            
            if (pReprocesarFallidos)
                mEstatus = RegistrosPendientesFallidos;
            else
                mEstatus = RegistrosPendientesPorLote;
            
            mSQL = "SELECT DON.*, isNULL(USR.Descripcion, 'N/A') AS Usuario, " + "\n" + 
            "(c_Documento + c_Sucursal) AS TransID FROM MA_DONACIONES DON " + "\n" + 
            "LEFT JOIN VAD10.DBO.MA_USUARIOS USR " + "\n" + 
            "ON DON.c_Usuario = USR.CodUsuario" + "\n" + 
            "WHERE (DON.cs_Sync_SxS = '" + mEstatus + "') " + "\n" + 
            "ORDER BY DON.ID";
            
            pRsDataDonaciones = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            
            pRsDonaciones.get(0).populate(pRsDataDonaciones);
            
            mSQL = "SELECT *, (c_Documento + c_Localidad) AS TransID " + "\n" +
            "FROM [VAD20].[DBO].[MA_DONACIONES_DETPAG] WHERE (c_Documento + c_Localidad) IN (" + "\n" + 
            "SELECT (c_Documento + c_Sucursal) FROM MA_DONACIONES" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")" + " " +
            "ORDER BY ID";
            
            pRsDataDetalleDonaciones = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            
            pRsDetalleDonaciones.get(0).populate(pRsDataDetalleDonaciones);
            
            return pRsDataDonaciones.first();
            
        } catch (Exception ex) {
            gLogger.EscribirLog(ex, "Error al buscar datos de donaciones pendientes por procesar.");
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    public void ConstruirRegistrosDeDonaciones(Boolean pReprocesarFallidos)
    {
        
        String mUltDoc = new String(); String mUltID = new String(); String mUltFec = new String();
        java.util.Date mFechaDonacion = null;
        
        try {
            
            FilteredRowSet RsDonaciones = null;
            FilteredRowSet RsDetalleDonaciones = null;
            
            List<FilteredRowSet> lRsDonaciones = Arrays.asList(new FilteredRowSet[]{ RsDonaciones });
            List<FilteredRowSet> lRsDetalleDonaciones = Arrays.asList(new FilteredRowSet[]{ RsDetalleDonaciones });
            
            HashMap RegistrosFallidos = new HashMap();
            
            Object tmpVal;
            
            //if (pReprocesarFallidos)
                //MarcarRegistrosDeAutorizaciones(RegistrosPendientesPorLote, true);
            
            if (ObtenerDatosDonaciones(lRsDonaciones, lRsDetalleDonaciones, pReprocesarFallidos))
            {    
                    if (gTestMode) {
                        
                    RsDonaciones = lRsDonaciones.get(0);
                    RsDetalleDonaciones = lRsDetalleDonaciones.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsDonaciones.getMetaData();
                    
                    while (RsDonaciones.next())
                    {
                        
                        TestContinue: {
                        
                        mUltDoc = RsDonaciones.getString("c_Documento");
                        mUltID = RsDonaciones.getString("ID");
                        mUltFec = RsDonaciones.getString("f_Fecha");
                        
                        /*try { assert ((mUltID.equals("XXXXXXXXXXXXXX"))); } catch (AssertionError ae) 
                        { 
                            System.out.println(ae.toString()); 
                        }*/
                        
                        /*for (int
                            RsVentas.getObject(i).toString()); i = 1; i <= RsVentas.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsVentas.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsVentas.getObject(i).toString());
                        }*/
                        
                        HashMap<String, Object> tmpRowTR = null;
                        HashMap<String, Object> tmpRowTRExt = null;
                        
                        HashMap<String, HashMap<String, Object>> TestStruc = null;
                        HashMap<String, HashMap<String, Object>>  Transaction = null;
                        HashMap<String, HashMap<String, Object>>  Detail = null;
                        
                        JCoTable Return = null;
                        
                        Transaction = new HashMap<String, HashMap<String, Object>>();
                        Detail = new HashMap<String, HashMap<String, Object>>();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTRExt = new HashMap<String, Object>();
                        
                        Transaction.put("1", tmpRowTR);
                        
                        tmpRowTR.put("c_Sucursal", RsDonaciones.getString("c_Sucursal"));
                        tmpRowTR.put("c_Caja", RsDonaciones.getString("c_Caja"));
                        tmpRowTR.put("Turno", RsDonaciones.getDouble("Turno"));
                        tmpRowTR.put("c_Documento", RsDonaciones.getString("c_Documento"));
                        tmpRowTR.put("c_Concepto", RsDonaciones.getString("c_Concepto"));
                        tmpRowTR.put("f_Fecha", RsDonaciones.getDate("f_Fecha"));
                        tmpRowTR.put("f_Hora", RsDonaciones.getDate("f_Hora"));
                        tmpRowTR.put("c_Usuario", RsDonaciones.getString("c_Usuario"));
                        tmpRowTR.put("c_Cliente", RsDonaciones.getString("c_Cliente"));
                        tmpRowTR.put("n_Subtotal", RsDonaciones.getDouble("n_Subtotal"));
                        tmpRowTR.put("n_Impuesto", RsDonaciones.getDouble("n_Impuesto"));
                        tmpRowTR.put("n_Total", RsDonaciones.getDouble("n_Total"));
                        tmpRowTR.put("n_Transacciones", RsDonaciones.getInt("n_Transacciones"));
                        tmpRowTR.put("c_Numero_Rel", RsDonaciones.getString("c_Numero_Rel"));
                        tmpRowTR.put("ID", RsDonaciones.getLong("ID"));
                        
                        RsDetalleDonaciones.setFilter(new StringColumnFilter(RsDonaciones.getString("TransID"), "TransID"));
                        
                        if (RsDetalleDonaciones.first())
                        {
                            
                            RsDetalleDonaciones.beforeFirst();
                            
                            while (RsDetalleDonaciones.next())
                            {
                                
                                tmpRowTRExt = new HashMap<String, Object>();
                                Detail.put(RsDetalleDonaciones.getString("ID"), tmpRowTRExt);
                                
                                tmpRowTRExt.put("c_Localidad", RsDetalleDonaciones.getString("c_Localidad"));
                                tmpRowTRExt.put("c_Caja", RsDetalleDonaciones.getString("c_Caja"));
                                tmpRowTRExt.put("Turno", RsDetalleDonaciones.getDouble("Turno"));
                                tmpRowTRExt.put("c_Documento", RsDetalleDonaciones.getString("c_Documento"));
                                tmpRowTRExt.put("c_Concepto", RsDetalleDonaciones.getString("c_Concepto"));
                                tmpRowTRExt.put("c_CodMoneda", RsDetalleDonaciones.getString("c_CodMoneda"));
                                tmpRowTRExt.put("c_CodDenominacion", RsDetalleDonaciones.getString("c_CodDenominacion"));
                                tmpRowTRExt.put("n_Factor", RsDetalleDonaciones.getDouble("n_Factor"));
                                tmpRowTRExt.put("d_Fecha", RsDetalleDonaciones.getDate("d_Fecha"));
                                tmpRowTRExt.put("c_CodBanco", RsDetalleDonaciones.getString("c_CodBanco"));
                                tmpRowTRExt.put("c_Banco", RsDetalleDonaciones.getString("c_Banco"));
                                tmpRowTRExt.put("c_Referencia", RsDetalleDonaciones.getString("c_Referencia"));
                                tmpRowTRExt.put("n_Monto", RsDetalleDonaciones.getDouble("n_Monto"));
                                tmpRowTRExt.put("c_CodCajero", RsDetalleDonaciones.getString("c_CodCajero"));
                                tmpRowTRExt.put("c_Util", RsDetalleDonaciones.getString("c_Util"));
                                tmpRowTRExt.put("RelacionDetPag", RsDetalleDonaciones.getLong("RelacionDetPag"));
                                tmpRowTRExt.put("ID", RsDetalleDonaciones.getLong("ID"));
                                
                            }
                            
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        try{
                            
                            //gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                            
                            int x = (int) (1 + (Math.random() * 10));
                            
                            if (x <= 2)
                            {
                                throw new JCoException(0, "sendfail");
                            }
                            
                            TransaccionEnviada = true;
                            
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de cierres.");
                            //System.exit(0);
                        }
                        
                        if (TransaccionEnviada) 
                        {
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE MA_DONACIONES SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_Documento) = '" + RsDonaciones.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("Donacion ID [" + mUltDoc + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }

                        // Próxima Transacción.
                     
                    }
                        
                    
                    
                    }
                        
                        
                        
                        
                        
                    }
                    else // ConstruirRegistrosDeDonaciones:
                    {
                    
                    RsDonaciones = lRsDonaciones.get(0);
                    RsDetalleDonaciones = lRsDetalleDonaciones.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsDonaciones.getMetaData();
                    
                    while (RsDonaciones.next())
                    {
                        
                        NextTrans: {
                        
                        mUltDoc = RsDonaciones.getString("TransID");
                        mUltID = RsDonaciones.getString("ID");
                        mUltFec = RsDonaciones.getString("f_Fecha");
                        
                        /*for (int i = 1; i <= RsCierres.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsCierres.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsCierres.getObject(i).toString());
                        }*/
                        
                        JCoStructure TestStruc = null;
                        JCoTable Transaction = null;
                        JCoTable Detail = null;
                        
                        JCoTable Return = null;
                        
                        Transaction = gCnSAP.fTransDonaciones.getImportParameterList().getTable("T_DONACIONES");
                        Detail = gCnSAP.fTransDonaciones.getImportParameterList().getTable("T_DONACIONES_DET");
                        
                        Transaction.clear();
                        Detail.clear();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        Transaction.appendRow();
                        
                        //Transaction.setValue("c_Sucursal".toUpperCase(), gCodLocalidadSAP);
                        if (gCodLocalidadSAP.equalsIgnoreCase("*") && gCodLocalidadStellar.equalsIgnoreCase("*"))
                            Transaction.setValue("c_Sucursal".toUpperCase(), Left(RsDonaciones.getString("c_Sucursal"), 10));
                        else
                            Transaction.setValue("c_Sucursal".toUpperCase(), Left(gCodLocalidadSAP, 10));
                        
                        Transaction.setValue("c_Caja".toUpperCase(), RsDonaciones.getString("c_Caja"));
                        Transaction.setValue("Turno".toUpperCase(), RsDonaciones.getString("Turno"));
                        Transaction.setValue("c_Documento".toUpperCase(), RsDonaciones.getString("c_Documento"));
                        Transaction.setValue("c_Concepto".toUpperCase(), RsDonaciones.getString("c_Concepto"));
                        Transaction.setValue("f_Fecha".toUpperCase(), dtFmtLong.format(RsDonaciones.getDate("f_Fecha")));
                        Transaction.setValue("f_Hora".toUpperCase(), dtFmtLong.format(RsDonaciones.getDate("f_Hora")));
                        Transaction.setValue("c_Usuario".toUpperCase(), RsDonaciones.getString("c_Usuario"));
                        Transaction.setValue("Cajero".toUpperCase(), Left(RsDonaciones.getString("Usuario"), 50));
                        Transaction.setValue("c_Cliente".toUpperCase(), RsDonaciones.getString("c_Cliente"));
                        Transaction.setValue("n_Subtotal".toUpperCase(), RawDouble(Round(RsDonaciones.getDouble("n_Subtotal"), 8)));
                        Transaction.setValue("n_Impuesto".toUpperCase(), RawDouble(Round(RsDonaciones.getDouble("n_Impuesto"), 8)));
                        Transaction.setValue("n_Total".toUpperCase(), RawDouble(Round(RsDonaciones.getDouble("n_Total"), 8)));
                        Transaction.setValue("n_Transacciones".toUpperCase(), RsDonaciones.getString("n_Transacciones"));
                        Transaction.setValue("c_Numero_Rel".toUpperCase(), RsDonaciones.getString("c_Numero_Rel"));
                        Transaction.setValue("ID".toUpperCase(), RsDonaciones.getString("ID"));
                        
                        RsDetalleDonaciones.setFilter(new StringColumnFilter(RsDonaciones.getString("TransID"), "TransID"));
                        
                        if (RsDetalleDonaciones.first())
                        {
                            
                            RsDetalleDonaciones.beforeFirst();
                            
                            while (RsDetalleDonaciones.next())
                            {
                                
                                Detail.appendRow();
                                
                                String TenderCurrency = null;

                                if (ListaAsociacionMoneda.containsKey(
                                RsDetalleDonaciones.getString("c_CodMoneda")))
                                {
                                    TenderCurrency = ListaAsociacionMoneda.get(
                                    RsDetalleDonaciones.getString("c_CodMoneda")).toString();
                                }

                                String TenderBank = null;

                                if (ListaAsociacionBancos.containsKey(
                                RsDetalleDonaciones.getString("c_CodBanco")))
                                {
                                    TenderBank = ListaAsociacionBancos.get(
                                    RsDetalleDonaciones.getString("c_CodBanco")).toString();
                                }
                                
                                String TenderTypeCode = null;

                                // Primero buscar Llave compuesta de moneda y denominación.
                                if (ListaAsociacionFormaPago.containsKey(
                                RsDetalleDonaciones.getString("c_CodMoneda") + ";" + RsDetalleDonaciones.getString("c_CodDenominacion")))
                                {
                                    TenderTypeCode = ListaAsociacionFormaPago.get(
                                    RsDetalleDonaciones.getString("c_CodMoneda") + ";" + RsDetalleDonaciones.getString("c_CodDenominacion")).toString();
                                } else if (ListaAsociacionFormaPago.containsKey(
                                RsDetalleDonaciones.getString("c_CodDenominacion"))){ // sino existe se intenta buscar solo por código de denominación.
                                    TenderTypeCode = ListaAsociacionFormaPago.get(
                                    RsDetalleDonaciones.getString("c_CodDenominacion")).toString();
                                }

                                /*// En vez de marcar como fallidos, enviemosle lo que ellos consideren como efectivo
                                // de manera predeterminada para no trancar el proceso.
                                if (TenderTypeCode == null)
                                    TenderTypeCode = "3101"; // Efectivo*/

                                if (TenderTypeCode == null)
                                    TenderTypeCode = RsDetalleDonaciones.getString("c_CodDenominacion").toString();

                                if (TenderCurrency == null)
                                    TenderCurrency = RsDetalleDonaciones.getString("c_CodMoneda").toString();
                                    
                                if (TenderBank == null)
                                    TenderBank = RsDetalleDonaciones.getString("c_CodBanco").toString();
                                
                                //Detail.setValue("c_Localidad".toUpperCase(), RsDetalleDonaciones.getString("c_Localidad"));
                                if (gCodLocalidadSAP.equalsIgnoreCase("*") && gCodLocalidadStellar.equalsIgnoreCase("*"))
                                    Detail.setValue("c_Localidad".toUpperCase(), Left(RsDetalleDonaciones.getString("c_Localidad"), 10));
                                else
                                    Detail.setValue("c_Localidad".toUpperCase(), Left(gCodLocalidadSAP, 10));
                                
                                Detail.setValue("c_Caja".toUpperCase(), RsDetalleDonaciones.getString("c_Caja"));
                                Detail.setValue("Turno".toUpperCase(), RsDetalleDonaciones.getString("Turno"));
                                Detail.setValue("c_Documento".toUpperCase(), RsDetalleDonaciones.getString("c_Documento"));
                                Detail.setValue("c_CodMoneda".toUpperCase(), TenderCurrency);
                                Detail.setValue("c_CodDenominacion".toUpperCase(), TenderTypeCode);
                                Detail.setValue("n_Factor".toUpperCase(), RawDouble(Round(RsDetalleDonaciones.getDouble("n_Factor"), 8)));
                                Detail.setValue("d_Fecha".toUpperCase(), dtFmtLong.format(RsDetalleDonaciones.getDate("d_Fecha")));
                                Detail.setValue("c_CodBanco".toUpperCase(), TenderBank);
                                Detail.setValue("c_Banco".toUpperCase(), RsDetalleDonaciones.getString("c_Banco"));
                                Detail.setValue("c_Referencia".toUpperCase(), RsDetalleDonaciones.getString("c_Referencia"));
                                Detail.setValue("n_Monto".toUpperCase(), RawDouble(Round(RsDetalleDonaciones.getDouble("n_Monto"), 8)));
                                Detail.setValue("c_CodCajero".toUpperCase(), RsDetalleDonaciones.getString("c_CodCajero"));
                                Detail.setValue("c_Util".toUpperCase(), RsDetalleDonaciones.getString("c_Util"));
                                Detail.setValue("RelacionDetPag".toUpperCase(), RsDetalleDonaciones.getString("RelacionDetPag"));
                                Detail.setValue("ID".toUpperCase(), RsDetalleDonaciones.getString("ID"));
                                
                            }
                            
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean SendError = false;
                        
                        try{
                            gCnSAP.fTransDonaciones.execute(gCnSAP.ABAP_RFC);
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de transacciones.");
                            SendError = true;
                            //System.exit(0);
                        }

                        Return = gCnSAP.fTransDonaciones.getTableParameterList().getTable("T_MESSTAB");
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        int ReturnRowCount = Return.getNumRows();
                        
                        if (!SendError)
                            if (ReturnRowCount <= 0)
                            {
                                // Envío exitoso sin ningún mensaje en particular.
                                TransaccionEnviada = true;
                            }
                            else
                            {

                                TransaccionEnviada = true;

                                // Revisar Mensajes.

                                String RetFullLog = "";
                                String RetLog = "";

                                for (int i = 0; i < ReturnRowCount; i++) 
                                {

                                    Return.setRow(i);

                                    String RetLnLog = "";

                                    if (gDebugMode)
                                    {
                                        RetLnLog = GetLines() +
                                        "Msgtyp => " + Return.getString("Msgtyp".toUpperCase()) + GetTab() +
                                        "Msgv1 => " + Return.getString("Msgv1".toUpperCase()) + GetTab() +
                                        "Msgv2 => " + Return.getString("Msgv2".toUpperCase()) + GetTab() +
                                        "Msgv3 => " + Return.getString("Msgv3".toUpperCase()) + GetTab() +
                                        "Msgv4 => " + Return.getString("Msgv4".toUpperCase());
                                        RetFullLog += RetLnLog;
                                    }

                                    if (Return.getString("Msgtyp".toUpperCase()).equalsIgnoreCase("E")) {

                                        TransaccionEnviada = false;

                                        if (!gDebugMode)
                                            RetLnLog = GetLines() +
                                        "Msgtyp => " + Return.getString("Msgtyp".toUpperCase()) + GetTab() +
                                        "Msgv1 => " + Return.getString("Msgv1".toUpperCase()) + GetTab() +
                                        "Msgv2 => " + Return.getString("Msgv2".toUpperCase()) + GetTab() +
                                        "Msgv3 => " + Return.getString("Msgv3".toUpperCase()) + GetTab() +
                                        "Msgv4 => " + Return.getString("Msgv4".toUpperCase());

                                        RetLog += RetLnLog;

                                    }

                                }
                            
                            }
                        
                        if (TransaccionEnviada) 
                        {
                            
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE MA_DONACIONES SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_Documento + c_Sucursal) = '" + RsDonaciones.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("Donación ID [" + mUltDoc + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }

                    // Próxima Transacción.
                     
                    }
                    
                    }
                    
                    System.out.println("Lote de Donaciones Finalizado con Exito.");
                
                }
            
            }
            else
            {
                System.out.println("No se encontraron registros de donaciones pendientes por sincronizar.");
            }
            
        } catch(Exception Ex) {
            System.out.println(Ex.getMessage());
            gLogger.EscribirLog(Ex, "Error al procesar registros de Donaciones.");
            if (!mUltID.isEmpty())
                gLogger.EscribirLog("Donaciones en proceso => " + mUltDoc + GetTab() + 
                mUltID + GetTab() +
                "Fecha => " + mUltFec + GetTab() +
                "");
        }
        
    }
    
    // DEPOSITOS DE CAJA PRINCIPAL - ARQUEO SAP
    
    
    public void SincronizarDepositosCP(){
        
        if (!gTransferirDepositosCP) return;
        
        if (HayDepositosCPPendientes(RegistrosPendientesPorCorrida) || 
        HayDepositosCPPendientes(RegistrosPendientesPorLote) || 
        HayDepositosCPPendientes(RegistrosPendientesFallidos)) {
            ProcesarDepositosCP(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
        }
        
        //if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosDeDepositosCP(RegistrosPendientesPorCorrida)) { // Procesar registros nuevos.
                ProcesarDepositosCP();
            }

    }
    
    public Boolean HayDepositosCPPendientes(String pEstatus){

        try {

        ResultSet RsDatosPendientes; Boolean exec;
        
        RsDatosPendientes = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).
        executeQuery("SELECT COUNT(*) AS Registros FROM MA_DEPOSITOS " +
        "WHERE cs_Sync_SxS = '" + pEstatus + "'");
        RsDatosPendientes.first();
        
        long RetRows = RsDatosPendientes.getLong("Registros");
        
        return (RetRows > 0);
        
        } catch (Exception Ex) { return false; }
        
    }
    
    public Boolean MarcarFallidosDepositosCP(String pTransID)
    {
        
        //ResultSet Datos;
        int RowsAffected = 0;
        
        try {
            RowsAffected = gConexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY).executeUpdate(
            "UPDATE [VAD20].[DBO].[MA_DEPOSITOS] SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "'" + GetLines() + 
            "WHERE (c_Documento + c_CodLocalidad) = " + pTransID + "");
            if (RowsAffected > 0) 
                return true;
            else
                return false;
        } catch (SQLException ex) {
            return false;
        }
        
    }
    
    public void ConstruirRegistrosDeDepositosCP()
    {
        ConstruirRegistrosDeDepositosCP(false);
    }
    
    public void ProcesarLoteDeDepositosCP()
    {
        
        if (HayDepositosCPPendientes(RegistrosPendientesPorLote))
        {
            // Procesar primero los pendientes
            // Antes de marcar el próximo Lote.
            ConstruirRegistrosDeDepositosCP();
            //nLotesProcesados++;
        }
        
        //if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosDeDepositosCP(RegistrosPendientesPorLote)) 
            {
                ConstruirRegistrosDeDepositosCP();
                //nLotesProcesados++;
            }
        
    }
    
    public void ProcesarDepositosCP()
    {
        
        if (HayDepositosCPPendientes(RegistrosPendientesFallidos))
            ConstruirRegistrosDeDepositosCP(true); // Intentar procesar los Fallidos una sola vez.
        
        while (HayDepositosCPPendientes(RegistrosPendientesPorCorrida) ||
        HayDepositosCPPendientes(RegistrosPendientesPorLote)) {
            ProcesarLoteDeDepositosCP();
            break; // Para este tipo de operaciones por los momentos solo se estará soportando un solo envío (un lote).
        }

    }
    
    public Boolean MarcarRegistrosDeDepositosCP(String pEstatus, Boolean pFallidos)
    {
        
        try {

            int RegistrosAfectados = 0; String pEstatusPrevio = ""; String mRegistrosXLote = "";

            String ExcluirVNF = ""; String SQLMarcaje = "";
            
            switch (pEstatus) {
                case RegistrosPendientesPorCorrida:
                    pEstatusPrevio = "'" + RegistrosNuevos + "'";
                    break;
                case RegistrosPendientesPorLote:
                    if (pFallidos)
                        pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                    else {
                        pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                        mRegistrosXLote = "AND (c_Documento + c_CodLocalidad) IN (" + "\n" + 
                        "SELECT TOP (" + nOperacionesAutorizadasPorLote + ") (c_Documento + c_CodLocalidad) FROM MA_DEPOSITOS" + "\n" + 
                        "WHERE cs_Sync_SxS = " + pEstatusPrevio + " ORDER BY c_Documento)";
                    }
                    break;
                default: // Guardados con Exito.
                    pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                    break;
            }

            SQLMarcaje = "UPDATE MA_DEPOSITOS SET cs_Sync_SxS = '" + pEstatus + "'" + "\n" + 
            "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ")" + mRegistrosXLote + ExcluirVNF;

            if (gDebugMode) gLogger.EscribirLog(SQLMarcaje);

            RegistrosAfectados = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeUpdate(SQLMarcaje);
            
            return (RegistrosAfectados > 0);

        } catch (Exception Ex) { 
            return false; 
        }

    }
    
    public Boolean MarcarRegistrosDeDepositosCP(String pEstatus)
    {
        return MarcarRegistrosDeDepositosCP(pEstatus, false);
    }
    
    public Boolean ObtenerDatosDepositosCP(List<FilteredRowSet> pRsDepositosCP,
    List<FilteredRowSet> pRsDetalleDepositosCP, Boolean pReprocesarFallidos)
    {
        
        ResultSet pRsDataDepositosCP = null;
        ResultSet pRsDataDetalleDepositosCP = null;
        
        try {

            pRsDepositosCP.set(0, new FilteredRowSetImpl());
            pRsDetalleDepositosCP.set(0, new FilteredRowSetImpl());
            
            String mSQL = null;

            String ExcluirVNF = new String();
            
            String mEstatus;
            
            if (pReprocesarFallidos)
                mEstatus = RegistrosPendientesFallidos;
            else
                mEstatus = RegistrosPendientesPorLote;
            
            mSQL = "SELECT *, (c_Documento + c_CodLocalidad) AS TransID FROM MA_DEPOSITOS " + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "') " + "\n" + 
            "ORDER BY c_Documento";
            
            pRsDataDepositosCP = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            
            pRsDepositosCP.get(0).populate(pRsDataDepositosCP);
            
            mSQL = "SELECT *, (c_Documento + c_CodLocalidad) AS TransID " + "\n" +
            "FROM [VAD20].[DBO].[TR_CIERRES] WHERE c_Concepto = 'DEP' AND (c_Documento + c_CodLocalidad) IN (" + "\n" + 
            "SELECT (c_Documento + c_CodLocalidad) FROM MA_DEPOSITOS" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "')" + ExcluirVNF + ")" + " " +
            "ORDER BY ID";
            
            pRsDataDetalleDepositosCP = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            
            pRsDetalleDepositosCP.get(0).populate(pRsDataDetalleDepositosCP);
            
            return pRsDataDepositosCP.first();
            
        } catch (Exception ex) {
            gLogger.EscribirLog(ex, "Error al buscar datos de depósitos pendientes por procesar.");
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    public void ConstruirRegistrosDeDepositosCP(Boolean pReprocesarFallidos)
    {
        
        String mUltDoc = new String(); String mUltID = new String(); String mUltFec = new String();
        java.util.Date mFecha = null;
        
        try {
            
            FilteredRowSet RsDepositosCP = null;
            FilteredRowSet RsDetalleDepositosCP = null;
            
            List<FilteredRowSet> lRsDepositosCP = Arrays.asList(new FilteredRowSet[]{ RsDepositosCP });
            List<FilteredRowSet> lRsDetalleDepositosCP = Arrays.asList(new FilteredRowSet[]{ RsDetalleDepositosCP });
            
            HashMap RegistrosFallidos = new HashMap();
            
            Object tmpVal;
            
            //if (pReprocesarFallidos)
                //MarcarRegistrosDeAutorizaciones(RegistrosPendientesPorLote, true);
            
            if (ObtenerDatosDepositosCP(lRsDepositosCP, lRsDetalleDepositosCP, pReprocesarFallidos))
            {
                    if (gTestMode) {
                        
                    RsDepositosCP = lRsDepositosCP.get(0);
                    RsDetalleDepositosCP = lRsDetalleDepositosCP.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsDepositosCP.getMetaData();
                    
                    while (RsDepositosCP.next())
                    {
                        
                        TestContinue: {
                        
                        mUltDoc = RsDepositosCP.getString("c_Documento");
                        mUltID = RsDepositosCP.getString("c_Documento");//("ID");
                        mUltFec = RsDepositosCP.getString("d_Fecha");
                        
                        /*try { assert ((mUltID.equals("XXXXXXXXXXXXXX"))); } catch (AssertionError ae) 
                        { 
                            System.out.println(ae.toString()); 
                        }*/
                        
                        /*for (int
                            RsVentas.getObject(i).toString()); i = 1; i <= RsVentas.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsVentas.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsVentas.getObject(i).toString());
                        }*/
                        
                        HashMap<String, Object> tmpRowTR = null;
                        HashMap<String, Object> tmpRowTRExt = null;
                        
                        HashMap<String, HashMap<String, Object>> TestStruc = null;
                        HashMap<String, HashMap<String, Object>>  Transaction = null;
                        HashMap<String, HashMap<String, Object>>  Detail = null;
                        
                        JCoTable Return = null;
                        
                        Transaction = new HashMap<String, HashMap<String, Object>>();
                        Detail = new HashMap<String, HashMap<String, Object>>();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        tmpRowTR = new HashMap<String, Object>();
                        tmpRowTRExt = new HashMap<String, Object>();
                        
//                        Transaction.put("1", tmpRowTR);
//                        
//                        tmpRowTR.put("c_CodLocalidad", RsDepositosCP.getString("c_CodLocalidad"));
//                        tmpRowTR.put("c_Documento", RsDepositosCP.getString("c_Documento"));
//                        tmpRowTR.put("Turno", RsDepositosCP.getDouble("Turno"));
//                        tmpRowTR.put("c_Documento", RsDonaciones.getString("c_Documento"));
//                        tmpRowTR.put("c_Concepto", RsDonaciones.getString("c_Concepto"));
//                        tmpRowTR.put("d_Fecha", RsDonaciones.getDate("d_Fecha"));
//                        tmpRowTR.put("f_Hora", RsDonaciones.getDate("f_Hora"));
//                        tmpRowTR.put("c_Usuario", RsDonaciones.getString("c_Usuario"));
//                        tmpRowTR.put("c_Cliente", RsDonaciones.getString("c_Cliente"));
//                        tmpRowTR.put("n_Subtotal", RsDonaciones.getDouble("n_Subtotal"));
//                        tmpRowTR.put("n_Impuesto", RsDonaciones.getDouble("n_Impuesto"));
//                        tmpRowTR.put("n_Total", RsDonaciones.getDouble("n_Total"));
//                        tmpRowTR.put("n_Transacciones", RsDonaciones.getInt("n_Transacciones"));
//                        tmpRowTR.put("c_Numero_Rel", RsDonaciones.getString("c_Numero_Rel"));
//                        tmpRowTR.put("ID", RsDonaciones.getLong("ID"));
                        
                        RsDetalleDepositosCP.setFilter(new StringColumnFilter(RsDepositosCP.getString("TransID"), "TransID"));
                        
                        if (RsDetalleDepositosCP.first())
                        {
                            
                            RsDetalleDepositosCP.beforeFirst();
                            
                            while (RsDetalleDepositosCP.next())
                            {
                                
                                tmpRowTRExt = new HashMap<String, Object>();
                                Detail.put(RsDetalleDepositosCP.getString("ID"), tmpRowTRExt);
                                
                                tmpRowTRExt.put("c_CodLocalidad", RsDepositosCP.getString("c_CodLocalidad"));
                                tmpRowTRExt.put("c_Documento", RsDepositosCP.getString("c_Documento"));
                                tmpRowTRExt.put("d_Fecha", RsDepositosCP.getDate("d_Fecha"));
                                tmpRowTRExt.put("c_Planilla", RsDepositosCP.getString("c_Planilla"));
                                tmpRowTRExt.put("c_CodBanco", RsDepositosCP.getString("c_CodBanco"));
                                tmpRowTRExt.put("c_Cuenta", RsDepositosCP.getString("c_Cuenta"));
                                tmpRowTRExt.put("c_CodMoneda", RsDetalleDepositosCP.getString("c_CodMoneda"));
                                tmpRowTRExt.put("c_CodDenomina", RsDetalleDepositosCP.getString("c_CodDenomina"));
                                tmpRowTRExt.put("n_Cantidad", RsDetalleDepositosCP.getDouble("n_Cantidad"));
                                tmpRowTRExt.put("n_Monto", RsDetalleDepositosCP.getDouble("n_Monto"));
                                tmpRowTRExt.put("n_Factor", RsDetalleDepositosCP.getDouble("n_Factor"));
                                
                            }
                            
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        try{
                            
                            //gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                            
                            int x = (int) (1 + (Math.random() * 10));
                            
                            if (x <= 2)
                            {
                                throw new JCoException(0, "sendfail");
                            }
                            
                            TransaccionEnviada = true;
                            
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de cierres.");
                            //System.exit(0);
                        }
                        
                        if (TransaccionEnviada) 
                        {
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE MA_DEPOSITOS SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_Documento) = '" + RsDepositosCP.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("DEPOSITO DE CAJA PRINCIPAL ID [" + mUltDoc + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }

                        // Próxima Transacción.
                     
                    }
                        
                    
                    
                    }
                        
                        
                        
                        
                        
                    }
                    else // ConstruirRegistrosDeDepositosCP:
                    {
                    
                    RsDepositosCP = lRsDepositosCP.get(0);
                    RsDetalleDepositosCP = lRsDetalleDepositosCP.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsDepositosCP.getMetaData();
                    
                    while (RsDepositosCP.next())
                    {
                        
                        NextTrans: {
                        
                        mUltDoc = RsDepositosCP.getString("TransID");
                        mUltID = RsDepositosCP.getString("c_Documento");//("ID");
                        mUltFec = RsDepositosCP.getString("d_Fecha");
                        
                        /*for (int i = 1; i <= RsCierres.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsCierres.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsCierres.getObject(i).toString());
                        }*/
                        
                        JCoStructure TestStruc = null;
                        JCoTable Transaction = null;
                        JCoTable Detail = null;
                        
                        JCoTable Return = null;
                        
                        //Transaction = gCnSAP.fTransDonaciones.getImportParameterList().getTable("T_DEPOSITOS");
                        Detail = gCnSAP.fTransDepositosCP.getImportParameterList().getTable("T_FINANCIALS");
                        
                        //Transaction.clear();
                        Detail.clear();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                        
                        /*Transaction.appendRow();
                        
                        Transaction.setValue("c_Sucursal".toUpperCase(), gCodLocalidadSAP);
                        Transaction.setValue("c_Caja".toUpperCase(), RsDonaciones.getString("c_Caja"));
                        Transaction.setValue("Turno".toUpperCase(), RsDonaciones.getString("Turno"));
                        Transaction.setValue("c_Documento".toUpperCase(), RsDonaciones.getString("c_Documento"));
                        Transaction.setValue("c_Concepto".toUpperCase(), RsDonaciones.getString("c_Concepto"));
                        Transaction.setValue("f_Fecha".toUpperCase(), dtFmtLong.format(RsDonaciones.getDate("f_Fecha")));
                        Transaction.setValue("f_Hora".toUpperCase(), dtFmtLong.format(RsDonaciones.getDate("f_Hora")));
                        Transaction.setValue("c_Usuario".toUpperCase(), RsDonaciones.getString("c_Usuario"));
                        Transaction.setValue("c_Cliente".toUpperCase(), RsDonaciones.getString("c_Cliente"));
                        Transaction.setValue("n_Subtotal".toUpperCase(), String.valueOf(Round(RsDonaciones.getDouble("n_Subtotal"), 8)));
                        Transaction.setValue("n_Impuesto".toUpperCase(), String.valueOf(Round(RsDonaciones.getDouble("n_Impuesto"), 8)));
                        Transaction.setValue("n_Total".toUpperCase(), String.valueOf(Round(RsDonaciones.getDouble("n_Total"), 8)));
                        Transaction.setValue("n_Transacciones".toUpperCase(), RsDonaciones.getString("n_Transacciones"));
                        Transaction.setValue("c_Numero_Rel".toUpperCase(), RsDonaciones.getString("c_Numero_Rel"));
                        Transaction.setValue("ID".toUpperCase(), RsDonaciones.getString("ID"));
                        */ 
                        
                        RsDetalleDepositosCP.setFilter(new StringColumnFilter(RsDepositosCP.getString("TransID"), "TransID"));
                        
                        if (RsDetalleDepositosCP.first())
                        {
                            
                            RsDetalleDepositosCP.beforeFirst();
                            
                            while (RsDetalleDepositosCP.next())
                            {
                                
                                Detail.appendRow();
                                
                                String TenderCurrency = null;

                                if (ListaAsociacionMoneda.containsKey(
                                RsDetalleDepositosCP.getString("c_CodMoneda")))
                                {
                                    TenderCurrency = ListaAsociacionMoneda.get(
                                    RsDetalleDepositosCP.getString("c_CodMoneda")).toString();
                                }
                                
                                String TenderBank = null;

                                if (ListaAsociacionBancos.containsKey(
                                RsDepositosCP.getString("c_CodBanco")))
                                {
                                    TenderBank = ListaAsociacionBancos.get(
                                    RsDepositosCP.getString("c_CodBanco")).toString();
                                }

                                String TenderTypeCode = null;

                                // Primero buscar Llave compuesta de moneda y denominación.
                                if (ListaAsociacionFormaPago.containsKey(
                                RsDetalleDepositosCP.getString("c_CodMoneda") + ";" + RsDetalleDepositosCP.getString("c_CodDenomina")))
                                {
                                    TenderTypeCode = ListaAsociacionFormaPago.get(
                                    RsDetalleDepositosCP.getString("c_CodMoneda") + ";" + RsDetalleDepositosCP.getString("c_CodDenomina")).toString();
                                } else if (ListaAsociacionFormaPago.containsKey(
                                RsDetalleDepositosCP.getString("c_CodDenomina"))){ // sino existe se intenta buscar solo por código de denominación.
                                    TenderTypeCode = ListaAsociacionFormaPago.get(
                                    RsDetalleDepositosCP.getString("c_CodDenomina")).toString();
                                }

                                /*// En vez de marcar como fallidos, enviemosle lo que ellos consideren como efectivo
                                // de manera predeterminada para no trancar el proceso.
                                if (TenderTypeCode == null)
                                    TenderTypeCode = "3101"; // Efectivo*/

                                if (TenderTypeCode == null)
                                    TenderTypeCode = RsDetalleDepositosCP.getString("c_CodDenomina").toString();

                                if (TenderCurrency == null)
                                    TenderCurrency = RsDetalleDepositosCP.getString("c_CodMoneda").toString();
                                    
                                if (TenderBank == null)
                                    TenderBank = RsDetalleDepositosCP.getString("c_CodBanco").toString();
                                
                                //Detail.setValue("c_CodLocalidad".toUpperCase(), RsDepositosCP.getString("c_CodLocalidad"));
                                
                                if (gCodLocalidadSAP.equalsIgnoreCase("*") && gCodLocalidadStellar.equalsIgnoreCase("*"))
                                    Detail.setValue("c_CodLocalidad".toUpperCase(), Left(RsDepositosCP.getString("c_CodLocalidad"), 10));
                                else
                                    Detail.setValue("c_CodLocalidad".toUpperCase(), Left(gCodLocalidadSAP, 10));
                                
                                Detail.setValue("c_Documento".toUpperCase(), RsDepositosCP.getString("c_Documento"));
                                Detail.setValue("d_Fecha".toUpperCase(), dtFmtLong.format(RsDepositosCP.getDate("d_Fecha")));
                                Detail.setValue("d_Fecha_Registro".toUpperCase(), dtFmtLong.format(RsDetalleDepositosCP.getDate("d_Fecha")));
                                Detail.setValue("c_Planilla".toUpperCase(), RsDepositosCP.getString("c_Planilla"));
                                Detail.setValue("c_CodBanco".toUpperCase(), TenderBank);
                                Detail.setValue("c_Cuenta".toUpperCase(), RsDepositosCP.getString("c_Cuenta"));
                                Detail.setValue("c_CodMoneda".toUpperCase(), TenderCurrency);
                                Detail.setValue("c_CodDenomina".toUpperCase(), TenderTypeCode);
                                Detail.setValue("n_Cantidad".toUpperCase(), RawDouble(Round(Math.abs(RsDetalleDepositosCP.getDouble("n_Cantidad")), 8)));
                                Detail.setValue("n_Monto".toUpperCase(), RawDouble(Round(Math.abs(RsDetalleDepositosCP.getDouble("n_Bolivares")), 8)));
                                Detail.setValue("n_Factor".toUpperCase(), RawDouble(Round(Math.abs(RsDetalleDepositosCP.getDouble("n_Factor")), 8)));
                                Detail.setValue("n_MontoFactorizado".toUpperCase(), RawDouble(Round(
                                (Round(Math.abs(RsDetalleDepositosCP.getDouble("n_Bolivares")), 8) * Round(Math.abs(RsDetalleDepositosCP.getDouble("n_Factor")), 8))
                                , 8)));
                                Detail.setValue("ID".toUpperCase(), RsDetalleDepositosCP.getString("ID"));
                                
                            }
                            
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean SendError = false;
                        
                        try{
                            gCnSAP.fTransDepositosCP.execute(gCnSAP.ABAP_RFC);
                        } catch (JCoException ex){
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de depósitos de caja principal.");
                            SendError = true;
                            //System.exit(0);
                        }

                        Return = gCnSAP.fTransDepositosCP.getTableParameterList().getTable("T_MESSTAB");
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        int ReturnRowCount = Return.getNumRows();
                        
                        if (!SendError)
                            if (ReturnRowCount <= 0)
                            {
                                // Envío exitoso sin ningún mensaje en particular.
                                TransaccionEnviada = true;
                            }
                            else
                            {

                                TransaccionEnviada = true;

                                // Revisar Mensajes.

                                String RetFullLog = "";
                                String RetLog = "";

                                for (int i = 0; i < ReturnRowCount; i++) 
                                {

                                    Return.setRow(i);

                                    String RetLnLog = "";

                                    if (gDebugMode)
                                    {
                                        RetLnLog = GetLines() +
                                        "Msgtyp => " + Return.getString("Msgtyp".toUpperCase()) + GetTab() +
                                        "Msgv1 => " + Return.getString("Msgv1".toUpperCase()) + GetTab() +
                                        "Msgv2 => " + Return.getString("Msgv2".toUpperCase()) + GetTab() +
                                        "Msgv3 => " + Return.getString("Msgv3".toUpperCase()) + GetTab() +
                                        "Msgv4 => " + Return.getString("Msgv4".toUpperCase());
                                        RetFullLog += RetLnLog;
                                    }

                                    if (Return.getString("Msgtyp".toUpperCase()).equalsIgnoreCase("E")) {

                                        TransaccionEnviada = false;

                                        if (!gDebugMode)
                                            RetLnLog = GetLines() +
                                        "Msgtyp => " + Return.getString("Msgtyp".toUpperCase()) + GetTab() +
                                        "Msgv1 => " + Return.getString("Msgv1".toUpperCase()) + GetTab() +
                                        "Msgv2 => " + Return.getString("Msgv2".toUpperCase()) + GetTab() +
                                        "Msgv3 => " + Return.getString("Msgv3".toUpperCase()) + GetTab() +
                                        "Msgv4 => " + Return.getString("Msgv4".toUpperCase());

                                        RetLog += RetLnLog;

                                    }

                                }
                            
                            }
                        
                        if (TransaccionEnviada) 
                        {
                            
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE MA_DEPOSITOS SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_Documento + c_CodLocalidad) = '" + RsDepositosCP.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                gLogger.EscribirLog("TRANSACCION DEPOSITO DE CAJA PRINCIPAL ID [" + mUltDoc + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }

                    // Próxima Transacción.
                     
                    }
                    
                    }
                    
                    System.out.println("Lote de Depósitos de Caja Principal Finalizado con Exito.");
                    
                }
                    
            }
            else
            {
                System.out.println("No se encontraron registros de depositos pendientes por sincronizar.");
            }
            
        } catch(Exception Ex) {
            System.out.println(Ex.getMessage());
            gLogger.EscribirLog(Ex, "Error al procesar registros de Depositos CP.");
            if (!mUltID.isEmpty())
                gLogger.EscribirLog("Depositos CP en proceso => " + mUltDoc + GetTab() + 
                "Fecha => " + mUltFec + GetTab() +
                "");
        }
        
    }

    public Double BuscarStockNoSincronizado(String pCodProducto)
    {
        
        if (!DepoprodSync_RestarStockNoSincronizado) 
        {
            return 0D;
        }
        
        ResultSet Datos;
        
        try {
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.000");
            
            java.util.Date FechaActual = new java.util.Date();
            
            Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
            "SELECT c_CodArticulo, isNULL(SUM(n_Cantidad), 0) AS CantRestante \n" +
            "FROM VAD10.DBO.TR_INVENTARIO TR \n" +
            "INNER JOIN VAD10.DBO.MA_VENTAS MA \n" +
            "ON TR.c_Concepto = MA.c_Concepto \n" +
            "AND TR.c_CodLocalidad = MA.c_CodLocalidad \n" +
            "AND TR.c_Documento = MA.c_Documento \n" +
            "LEFT JOIN VAD10.DBO.MA_VENTAS VENTA \n" +
            "ON (VENTA.c_CodLocalidad + VENTA.c_Relacion) = (MA.c_CodLocalidad + MA.c_Concepto + ' ' + MA.c_Documento) \n" +
            "WHERE MA.c_Concepto IN ('NDE') AND MA.c_Status IN ('DPE', 'DCO') \n" +
            "AND MA.d_Fecha between '" + dateFormat.format(DateAddInterval(FechaActual, Calendar.MONTH, (-1))) + "' " + 
            "AND '" + dateFormat.format(FechaActual) + "' \n" +
            "AND TR.c_CodArticulo = '" + pCodProducto +  "' \n" +
            "AND (VENTA.c_Documento IS NULL \n" +
            "OR (VENTA.cs_Sync_SxS IN ('', '-_-_-_-_-_', '!!!!!!!!!!', '..........')) \n" +
            "OR (VENTA.c_Documento IS NOT NULL AND TR.n_Cant_Teorica > 0)) \n" +
            "GROUP BY c_CodArticulo \n");
            
            if (Datos.first()) { return Datos.getDouble("CantRestante"); } else { return 0D; }
            
        } catch (SQLException ex) {
            return 0D;
        }
        
    }
    
    public void SincronizarStock()
    {
        
        String mEtapa = "";
        
        HashMap <String, HashMap <String, String> > Localidades = new HashMap <String, HashMap <String, String> > ();
        HashMap <String, String> Existencias = new HashMap <String, String> ();
        
        ResultSet pRsLocalidades = null;
        ResultSet pRsProductos = null; 
        ResultSet pRsDataDepoprod = null;
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        
        String mTabla;
        
        try 
        {
            
            System.out.println("  ** Tiempo Inicial:   " + Instant.now().toString());
            java.util.Date Start = new java.util.Date();
            System.out.println(dateFormat.format(Start)); //2016/11/16 12:08:43
            
            FilteredRowSet pRsDepoprod = new FilteredRowSetImpl();
            
            mEtapa = "Buscando Localidades";
            System.out.println(mEtapa);
            
            String mSQL = null;
            
            mSQL = "SELECT c_Codigo AS CodLocalidad, c_Descripcion AS DesLocalidad " + "\n" +
            "FROM [VAD10].[DBO].[MA_SUCURSALES] SUC" + "\n" + 
            "WHERE c_Codigo LIKE '%TD%'" + "\n" + 
            "ORDER BY SUC.c_Descripcion";

            pRsLocalidades = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);
            
            mEtapa = "Buscando Productos";
            System.out.println(mEtapa);
            
            mSQL = "" +
            "SELECT c_Codigo AS CodProducto, c_Descri AS DesProducto " + "\n" + 
            "FROM [VAD10].[DBO].[MA_PRODUCTOS] WHERE 1 = 1 " + "\n" + 
            (DepoprodSync_SoloProductosActivos ? "AND n_Activo = 1 " + "\n" : "") + 
            (DepoprodSync_SoloProductosWeb ? "AND c_Codigo IN (SELECT Codigo FROM [VAD10].[DBO].[MA_PRODUCTOS_WEB]) " + "\n" : "") + 
            "";
            
            pRsProductos = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);
                
            //ResultSetMetaData FieldData = null;
            //FieldData = pRsProductos.getMetaData();
            
            mEtapa = "Armando estructura Productos x Localidad";
            System.out.println(mEtapa);
            
            if (gModalidad.equalsIgnoreCase("2"))
            while (pRsLocalidades.next())
            {
                
                Boolean mLocalidadAplica;
                
                mLocalidadAplica = (gCodLocalidadStellar).equals("*");
                
                if (!mLocalidadAplica)
                    mLocalidadAplica = pRsLocalidades.getString("CodLocalidad").equals(gCodLocalidadStellar);
                
                if (mLocalidadAplica)
                {
                    
                    Existencias = new HashMap <String, String> ();
                    
                    Localidades.put(pRsLocalidades.getString("CodLocalidad"), Existencias);
                    
                    pRsProductos.first();

                    while (pRsProductos.next())
                    {
                        Existencias.put(pRsProductos.getString("CodProducto"), "");
                    }
                    
                    /*try { 
                        //assert (!mUltDoc.equals("VEN002000144")); 
                    } catch (AssertionError ae) 
                    { 
                        System.out.println(ae.toString()); 
                    }*/
                    
                }
                
            }
            
            mEtapa = "Llenando datos en RFC para obtener Existencia de Producto por Localidad";
            System.out.println(mEtapa);
            
            if (gModalidad.equalsIgnoreCase("2"))
            for (Map.Entry <String, HashMap < String, String > > Localidad : Localidades.entrySet()) 
            {
                
                JCoStructure TestStruc = null;
                JCoTable TProductos = null;
                JCoTable Detail = null;

                JCoTable Return = null;
                
                mEtapa = "Obtener Parametros de Entrada para el RFC";
                
                gCnSAP.fConsultaStock.getImportParameterList().setValue("WERKS", 
                Localidad.getKey().toUpperCase().replaceAll("TD", ""));
                
                TProductos = gCnSAP.fConsultaStock.getImportParameterList().getTable("T_MATNR");
                
                TProductos.clear();
                
                for (Map.Entry <String, String > Producto : Localidad.getValue().entrySet()) 
                {
                    
                    TProductos.appendRow();
                    TProductos.setValue("MATNR", Producto.getKey());
                    //TProductos.setValue("WERKS", Localidad.getKey());
                    //TProductos.setValue("STKTOT", "");
                    
                }
                
                Boolean SendError = false;
                
                mEtapa = "Ejecutando llamado a RFC de Consulta de Stock. Localidad: " + Localidad.getKey();
                
                //try{
                gCnSAP.fConsultaStock.execute(gCnSAP.ABAP_RFC);
                //} catch (JCoException ex){
                    //SendError = true;
                    //System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                    //gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de transacciones. " +
                    ///"Localidad: " +  Localidad.getKey());
                    //return;
                //}
                
                mEtapa = "Recuperando parámetros devueltos por la función. Localidad: " + Localidad.getKey();;
                
                Return = gCnSAP.fConsultaStock.getExportParameterList().getTable("T_STOCK");
                
                int ReturnRowCount = Return.getNumRows();
                
                for (int i = 0; i < ReturnRowCount; i++) 
                {
                    
                    Return.setRow(i);
                    
                    String CodigoDevuelto = eliminarCeros(Return.getString("MATNR")); 
                    String CantidadDevuelta = Return.getString("STKTOT");
                    
                    /*if (Double.parseDouble(CantidadDevuelta) > 12)
                    {
                        System.out.println(CantidadDevuelta);
                    }*/
                    
                    Localidad.getValue().put(CodigoDevuelto, CantidadDevuelta);
                    
                }
                
            }
            else if (gModalidad.equalsIgnoreCase("2.1"))
            {
                
                String mLocalidadTemp = "";
                String mProductoTemp = "";
                
                Boolean mLocalidadAplica;
                
                mLocalidadAplica = (gCodLocalidadStellar).equals("*");
                
                if (mLocalidadAplica)
                {
                    
                    while (mLocalidadTemp.trim().isEmpty()) 
                    {                        
                        
                        mLocalidadTemp = JOptionPane.showInputDialog(null, "Introduzca un Código de Localidad o Dejela Vacía para salir.",
                        "", JOptionPane.INFORMATION_MESSAGE);
                        
                        if (mLocalidadTemp == null) mLocalidadTemp = "";
                        
                        if (!mLocalidadTemp.trim().isEmpty())
                        {
                            
                            if (!Localidades.containsKey(mLocalidadTemp))
                            {
                                
                                Existencias = new HashMap <String, String> ();
                                
                                Localidades.put(mLocalidadTemp, Existencias);
                                
                            }else
                                Existencias = Localidades.get(mLocalidadTemp);
                                
                            mProductoTemp = JOptionPane.showInputDialog(null, "Introduzca un Código de Producto o Dejelo Vacío para salir.",
                            "", JOptionPane.INFORMATION_MESSAGE);
                            
                            if (mProductoTemp == null) mProductoTemp = "";
                            
                            while (!mProductoTemp.trim().isEmpty()) 
                            {                                    
                                
                                if (!Existencias.containsKey(mProductoTemp))
                                    Existencias.put(mProductoTemp, "");
                                
                                mProductoTemp = JOptionPane.showInputDialog(null, "Introduzca un Código de Producto o Dejelo Vacío para salir.",
                                "", JOptionPane.INFORMATION_MESSAGE);
                                
                                if (mProductoTemp == null) mProductoTemp = "";
                                
                            }
                            
                        }
                        
                    }
                    
                }
                else
                {
                    
                    mLocalidadTemp = gCodLocalidadStellar;
                    
                    Existencias = new HashMap <String, String> ();

                    Localidades.put(mLocalidadTemp, Existencias);
                    
                    mProductoTemp = JOptionPane.showInputDialog(null, "Introduzca un Código de Producto o Dejelo Vacío para salir.",
                    "", JOptionPane.INFORMATION_MESSAGE);
                    
                    if (mProductoTemp == null) mProductoTemp = "";
                    
                    while (!mProductoTemp.trim().isEmpty()) 
                    {                                    
                        
                        if (!Existencias.containsKey(mProductoTemp))
                            Existencias.put(mProductoTemp, "");
                        
                        mProductoTemp = JOptionPane.showInputDialog(null, "Introduzca un Código de Producto o Dejelo Vacío para salir.",
                        "", JOptionPane.INFORMATION_MESSAGE);
                        
                        if (mProductoTemp == null) mProductoTemp = "";
                        
                    }
                                                
                }
                
                for (Map.Entry <String, HashMap < String, String > > Localidad : Localidades.entrySet()) 
                {
                    
                    JCoStructure TestStruc = null;
                    JCoTable TProductos = null;
                    JCoTable Detail = null;

                    JCoTable Return = null;

                    mEtapa = "Obtener Parametros de Entrada para el RFC";

                    gCnSAP.fConsultaStock.getImportParameterList().setValue("WERKS", 
                    Localidad.getKey().toUpperCase().replaceAll("TD", ""));

                    TProductos = gCnSAP.fConsultaStock.getImportParameterList().getTable("T_MATNR");
                    
                    TProductos.clear();
                    
                    for (Map.Entry <String, String > Producto : Localidad.getValue().entrySet()) 
                    {
                        
                        TProductos.appendRow();
                        TProductos.setValue("MATNR", Producto.getKey());
                        //TProductos.setValue("WERKS", Localidad.getKey());
                        //TProductos.setValue("STKTOT", "");
                        
                    }
                    
                    Boolean SendError = false;
                    
                    mEtapa = "Ejecutando llamado a RFC de Consulta de Stock. Localidad: " + Localidad.getKey();
                    
                    //try{
                    gCnSAP.fConsultaStock.execute(gCnSAP.ABAP_RFC);
                    //} catch (JCoException ex){
                        //SendError = true;
                        //System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                        //gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de transacciones. " +
                        ///"Localidad: " +  Localidad.getKey());
                        //return;
                    //}

                    mEtapa = "Recuperando parámetros devueltos por la función. Localidad: " + Localidad.getKey();
                    
                    Return = gCnSAP.fConsultaStock.getExportParameterList().getTable("T_STOCK");

                    int ReturnRowCount = Return.getNumRows();

                    for (int i = 0; i < ReturnRowCount; i++) 
                    {
                        
                        Return.setRow(i);
                        
                        String CodigoDevuelto = eliminarCeros(Return.getString("MATNR")); 
                        String CantidadDevuelta = Return.getString("STKTOT");
                        
                        /*if (Double.parseDouble(CantidadDevuelta) > 12)
                        {
                            System.out.println(CantidadDevuelta);
                        }*/
                        
                        Localidad.getValue().put(CodigoDevuelto, CantidadDevuelta);
                        
                    }
                    
                }
                
            }
            
            if (gTestMode)
            {
                
                mTabla = "MA_DEPOPROD_TEST";
                
                gConexion.createStatement().execute("IF NOT EXISTS(SELECT * FROM VAD10.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + mTabla + "')\n" +
                "BEGIN\n" + 
                "SELECT * INTO VAD10.DBO." + mTabla + " FROM VAD10.DBO.MA_DEPOPROD\n" + 
                "TRUNCATE TABLE VAD10.DBO." + mTabla + "\n" + 
                "END\n");
                
                try {
                    gConexion.createStatement().execute("IF EXISTS(SELECT * FROM VAD10.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + mTabla + "')\n" +
                    "   ALTER TABLE VAD10.DBO." + mTabla + " ADD CONSTRAINT " + mTabla + "_DEFAULT_n_Cantidad DEFAULT 0 FOR n_Cantidad\n");
                } catch(Exception tempex1) {}
                try {
                    gConexion.createStatement().execute("IF EXISTS(SELECT * FROM VAD10.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + mTabla + "')\n" +
                    "   ALTER TABLE VAD10.DBO." + mTabla + " ADD CONSTRAINT " + mTabla + "_DEFAULT_n_Cant_Comprometida DEFAULT 0 FOR n_Cant_Comprometida\n");
                } catch(Exception tempex2) {}
                try {
                    gConexion.createStatement().execute("IF EXISTS(SELECT * FROM VAD10.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + mTabla + "')\n" +
                    "   ALTER TABLE VAD10.DBO." + mTabla + " ADD CONSTRAINT " + mTabla + "_DEFAULT_n_Cant_Ordenada DEFAULT 0 FOR n_Cant_Ordenada\n");
                } catch(Exception tempex3) {}
                try {
                    gConexion.createStatement().execute("IF EXISTS(SELECT * FROM VAD10.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '" + mTabla + "')\n" +
                    "   ALTER TABLE VAD10.DBO." + mTabla + " ADD CONSTRAINT " + mTabla + "_DEFAULT_c_Descripcion DEFAULT '' FOR c_Descripcion\n");
                } catch(Exception tempex4) {}

            }            
            else
            {
                mTabla = "MA_DEPOPROD";
            }
            
            try {
                
                mEtapa = "Validando Depósitos faltantes";
                System.out.println(mEtapa);
                
                mSQL = "" + 
                "INSERT INTO MA_DEPOSITO (c_CodDeposito, c_Descripcion, c_CodLocalidad, c_Responsable, c_Observacion, Update_Date, Add_Date)\n" +
                "SELECT S.c_Codigo + '0001' AS c_CodDeposito, 'PISO DE VENTA' AS c_Descripcion, S.c_Codigo AS c_CodLocalidad, \n" +
                "'' AS c_Responsable, '' AS c_Observacion, GETDATE() AS Update_Date, GETDATE() AS Add_Date\n" +
                "FROM VAD10.DBO.MA_SUCURSALES S\n" +
                "LEFT JOIN VAD10.DBO.MA_DEPOSITO D\n" +
                "ON S.c_Codigo = D.c_CodLocalidad\n" +
                "AND D.c_CodDeposito = S.c_Codigo + '0001'\n" +
                "WHERE D.c_CodDeposito IS NULL\n" +
                "AND S.c_Codigo LIKE '%TD%'";
                
                gConexion.createStatement().execute(mSQL);
                
            } catch(Exception tempex5) {}
            
            mEtapa = "Validando Registros de Existencia por Localidad faltantes";
            System.out.println(mEtapa);
            
            // Insert Missing Rows
            
            mSQL = "" +
            ";WITH Localidades AS (" + "\n" +  
            "SELECT c_Codigo AS CodDeposito FROM [VAD10].[DBO].[MA_SUCURSALES]" + "\n" + 
            "), PRODUCTOS AS (" + "\n" + 
            "SELECT c_Codigo, LEFT(c_Descri, 40) AS c_Descri FROM [VAD10].[DBO].[MA_PRODUCTOS]" + "\n" + 
            "WHERE 1 = 1 " + "\n" + 
            (DepoprodSync_SoloProductosActivos ? "AND n_Activo = 1 " + "\n" : "") + 
            (DepoprodSync_SoloProductosWeb ? "AND c_Codigo IN (SELECT Codigo FROM [VAD10].[DBO].[MA_PRODUCTOS_WEB]) " + "\n" : "") + 
            "), SELECCION AS (" + "\n" + 
            "SELECT P.*, L.CodDeposito, D.n_Cantidad" + "\n" + 
            "FROM PRODUCTOS P CROSS JOIN LOCALIDADES L" + "\n" + 
            "LEFT JOIN [VAD10].[DBO].[" + mTabla + "] D" + "\n" + 
            "ON P.c_Codigo = D.c_CodArticulo" + "\n" + 
            "AND D.c_CodDeposito = L.CodDeposito + '0001'" + "\n" + 
            "WHERE 1 = 1 AND D.n_Cantidad IS NULL" + "\n" + 
            (gCodLocalidadStellar.equals("*") ? "" : "AND L.CodDeposito = '" + gCodLocalidadStellar + "'" + "\n") + 
            ") INSERT INTO [VAD10].[DBO].[" + mTabla + "] (c_CodArticulo, c_Descripcion, c_CodDeposito, n_Cantidad)" + "\n" + 
            "SELECT S.c_Codigo, S.c_Descri, (S.CodDeposito + '0001'), 0 FROM SELECCION S;";
            
            gConexion.createStatement().execute(mSQL);
            
            mEtapa = "Obteniendo último registro de cantidad en Productos por Deposito";
            System.out.println(mEtapa);
            
            // Get Current Data
            
            mSQL = "" + 
            "SELECT (D.c_CodDeposito + ';' + D.c_CodArticulo) AS PK, n_Cantidad FROM [VAD10].[DBO].[" + mTabla + "] D" + "\n" +
            "INNER JOIN [VAD10].[DBO].[MA_PRODUCTOS] P" + "\n" + 
            "ON D.c_CodArticulo = P.c_Codigo" + "\n" + 
            "WHERE 1 = 1" + "\n" + 
            "AND D.c_CodDeposito LIKE '%0001'" + "\n" + 
            (gCodLocalidadStellar.equals("*") ? "AND D.c_CodDeposito LIKE '%0001'" + "\n"
            : "AND D.c_CodDeposito = '" + gCodLocalidadStellar + "' + '0001'" + "\n") + 
            (DepoprodSync_SoloProductosActivos ? "AND P.n_Activo = 1 " + "\n" : "") + 
            (DepoprodSync_SoloProductosWeb ? "AND P.c_Codigo IN (SELECT Codigo FROM [VAD10].[DBO].[MA_PRODUCTOS_WEB]) " + "\n" : "");
            
            pRsDataDepoprod = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);            
            
            pRsDepoprod.populate(pRsDataDepoprod);
            
            // Begin Compare
            
            mEtapa = "Creando Statement. Ejecutando actualizaciones";
            System.out.println(mEtapa);
            
            Statement PendingUpdates = gConexion.createStatement();
            
            Boolean ActiveTrans = false;
            
            double RegistroActual = 0; double RegistrosProcesados = 0; double TmpDiv; Boolean TmpRes;
            
            if (gModalidad.equalsIgnoreCase("2.1"))
            {
                
                JTable table = new JTable();
                
                DefaultTableModel dtm = new DefaultTableModel(0, 0);
                
                Object[] cols = {
                    "Localidad", "Producto", "Stock"
                };
                
                // add header in table model     
                dtm.setColumnIdentifiers(cols);
                //set model into the table object
                table.setModel(dtm);
                
                for (Map.Entry <String, HashMap < String, String > > Localidad : Localidades.entrySet()) 
                {

                    for (Map.Entry <String, String > Producto : Localidad.getValue().entrySet()) 
                    {
                        dtm.addRow( new Object[] { Localidad.getKey(), Producto.getKey(), Producto.getValue() });
                    }
                
                }
                
                //JTable table = new JTable(rows, cols);
                
                JOptionPane.showMessageDialog(null, new JScrollPane(table));
                
                String[] options = {"Grabar", "Descartar y salir (Solo consulta)"};
                
                int x = JOptionPane.showOptionDialog(null, "¿Que desea hacer con los datos obtenidos?",
                "Indique una acción",
                JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
                
                if (x == 1)
                    return;
                
            }
            
            for (Map.Entry <String, HashMap < String, String > > Localidad : Localidades.entrySet()) 
            {
            
                for (Map.Entry <String, String > Producto : Localidad.getValue().entrySet()) 
                {
                    
                    if (StringUtils.isNumeric(Producto.getValue()))
                    {
                        
                        RegistroActual++;
                        
                        TmpDiv = (RegistroActual / 1000);
                        
                        TmpRes = ((new BigDecimal(Math.round(TmpDiv) - TmpDiv).compareTo(BigDecimal.ZERO) == 0));
                        
                        if (TmpRes)
                        {
                            if(DepoprodSync_TransaccionPorLote)
                                System.out.println("Cargando..." + RegistroActual);
                            else
                                System.out.println("Analizados..." + RegistroActual);
                        }
                        
                        String CurrentPK = Localidad.getKey() + "0001" + ";" + Producto.getKey();
                        Double NewCant = Double.parseDouble(Producto.getValue()) - 
                        BuscarStockNoSincronizado(Producto.getKey());
                        
                        Boolean Modificar;
                        
                        Modificar = true;
                        
                        mEtapa = "Buscando cantidad del Registro [" + CurrentPK + "]";
                        
                        pRsDepoprod.setFilter(new StringColumnFilter(CurrentPK, "PK"));
                        
                        if (pRsDepoprod.first())
                        {
                            
                            Double CantDP = pRsDepoprod.getDouble("n_Cantidad");
                            
                            if (gModalidad.equalsIgnoreCase("2.1"))
                                JOptionPane.showMessageDialog(null, "Localidad: " + 
                                Localidad.getKey() + " \n" + "Producto: " +
                                Producto.getKey()  + " \n" + "Cantidad Actual Depoprod: " + 
                                String.valueOf(CantDP) + " \n" + "Cantidad Devuelta RFC: " + 
                                String.valueOf(NewCant) + " \n" + "Modificar: " + 
                                String.valueOf(Modificar));
                            
                            if  (Objects.equals(NewCant, CantDP))
                                Modificar = false;
                            
                            if (Modificar && DepoprodSync_SoloExistenciaPositiva)
                            {
                                if(CantDP <= 0 && NewCant <= 0)
                                {
                                    Modificar = false;
                                }
                            }
                            
                        }
                            
                        if (Modificar)
                        {
                            
                            RegistrosProcesados++;
                            
                            String mSQLUpdate = "UPDATE [VAD10].[DBO].[" + mTabla + "] SET" + "\n" + 
                            "n_Cantidad = (" + String.valueOf(NewCant) + ")" + "\n" + 
                            "WHERE c_CodArticulo = '" + Producto.getKey() + "'" + "\n" + 
                            "AND c_CodDeposito = '" + Localidad.getKey() + "0001'";
                            
                            if (gDebugMode && gModalidad.equalsIgnoreCase("2.1"))
                            {
                                System.out.println(mSQLUpdate);
                                JOptionPane.showMessageDialog(null, mSQLUpdate);
                            }
                            
                            if(DepoprodSync_TransaccionPorLote)
                            {
                                mEtapa = "Cargando Registro al Lote\n\n. " + mSQLUpdate;
                                PendingUpdates.addBatch(mSQLUpdate);
                            }
                            else
                            {
                                mEtapa = "Procesando registro...\n\n. " + mSQLUpdate;
                                PendingUpdates.execute(mSQLUpdate);
                            }
                            
                        }
                        else
                        {
                            //System.out.println("Igual");
                            // No es necesario hacer un update....
                        }
                        
                    }
                    
                }
                
            }
            
            if (DepoprodSync_TransaccionPorLote)
            {
                
                mEtapa = "Finalizando transacción de Base de Datos";
                System.out.println(mEtapa);
                
                gConexion.setAutoCommit(false);
                
                PendingUpdates.executeBatch();
                
                gConexion.commit();
                
            }
            
            mEtapa = "Proceso Finalizado... Registros procesados: " + RegistrosProcesados;
            System.out.println(mEtapa);
            
        }catch (SQLException sqlex) {
            gLogger.EscribirLog(sqlex, "Error al recuperar inventario. " + mEtapa);
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, sqlex);
        }
        catch (JCoException jcoex) {
            gLogger.EscribirLog(jcoex, "Error al recuperar inventario. " + mEtapa);
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, jcoex);
        }
        catch (Exception ex) {
            gLogger.EscribirLog(ex, "Error al recuperar inventario. " + mEtapa);
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("  ** Tiempo Final:   " + Instant.now().toString());
        java.util.Date Fin = new java.util.Date();
        System.out.println(dateFormat.format(Fin)); //2016/11/16 12:08:43
        
    }

    public void SincronizarVentasADM(){
        
        if (HayDatosPendientesVentasADM(RegistrosPendientesPorCorrida) || 
        HayDatosPendientesVentasADM(RegistrosPendientesPorLote) || 
        HayDatosPendientesVentasADM(RegistrosPendientesFallidos)) {
            ProcesarLoteVentasADM(); // Intentar Procesar una corrida anterior en caso de que hayan datos pendientes.
        }
        
        if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosVentasADM(RegistrosPendientesPorCorrida)) { // Procesar registros nuevos.
                ProcesarLoteVentasADM();
            }

    }
    
    public Boolean HayDatosPendientesVentasADM(String pEstatus){
        
        try {
            
        ResultSet RsDatosPendientes; Boolean exec;
        
        String ExcluirVNF = "";
        
        System.out.println("Buscando Transacciones Administrativas....");
        
        RsDatosPendientes = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).
        executeQuery("SELECT COUNT(*) AS Registros FROM [VAD10].[DBO].[MA_VENTAS] V \n " +
        "WHERE c_Concepto IN ('VEN', 'DEV') \nAND NOT LEFT(c_Documento, 1) = 'P' \nAND c_Status = 'DCO' \nAND cs_Sync_SxS = '" 
        + pEstatus + "' " + ExcluirVNF);
        RsDatosPendientes.first();
        
        if (gDebugMode)
        {
            System.out.println("SELECT COUNT(*) AS Registros FROM [VAD10].[DBO].[MA_VENTAS] V \n " +
        "WHERE c_Concepto IN ('VEN', 'DEV') \nAND NOT LEFT(c_Documento, 1) = 'P' \nAND c_Status = 'DCO' \nAND cs_Sync_SxS = '" 
        + pEstatus + "' " + ExcluirVNF);
            
            System.out.println(RsDatosPendientes.getLong("Registros"));
        }
        
        return (RsDatosPendientes.getLong("Registros") > 0);
        
        } catch (Exception Ex) { 
            gLogger.EscribirLog(Ex, "Buscando Cantidad de Ventas ADM Pendientes");
            return false;
        }
        
    }
    
    public void ProcesarCorridaVentasADM()
    {
        
        if (HayDatosPendientesVentasADM(RegistrosPendientesFallidos))
            ConstruirRegistrosDeVentasADM(true); // Intentar procesar los Fallidos una sola vez.
        
        while (HayDatosPendientesVentasADM(RegistrosPendientesPorCorrida) ||
        HayDatosPendientesVentasADM(RegistrosPendientesPorLote)) {
            if (nLotesProcesados < nLotesMaxEjecucion)
                ProcesarLoteVentasADM();
            else
                break;
        }
        
    }
    
    public void ProcesarLoteVentasADM()
    {
        
        if (HayDatosPendientesVentasADM(RegistrosPendientesPorLote))
        {
            // Procesar primero los pendientes
            // Antes de marcar el próximo Lote.
            ConstruirRegistrosDeVentasADM();
            nLotesProcesados++;
        }
        
        if (nLotesProcesados < nLotesMaxEjecucion)
            if (MarcarRegistrosVentasADM(RegistrosPendientesPorLote)) 
            {
                ConstruirRegistrosDeVentasADM();
                nLotesProcesados++;
            }
        
    }
    
    public Boolean MarcarRegistrosVentasADM(String pEstatus, Boolean pFallidos)
    {
        
        try {
            
            int RegistrosAfectados = 0; String pEstatusPrevio = ""; String mRegistrosXLote = "";
            
            String ExcluirVNF = ""; String SQLMarcaje = "";
            
            switch (pEstatus) {
                case RegistrosPendientesPorCorrida:
                    pEstatusPrevio = "'" + RegistrosNuevos + "'";
                    break;
                case RegistrosPendientesPorLote:
                    if (pFallidos)
                        pEstatusPrevio = "'" + RegistrosPendientesFallidos + "'";
                    else {
                        pEstatusPrevio = "'" + RegistrosPendientesPorCorrida + "'";
                        mRegistrosXLote = "AND (c_CodLocalidad + c_Concepto + c_Documento) IN (" + " \n" + 
                        "SELECT TOP (" + nRegistrosLote + ") (V.c_CodLocalidad + V.c_Concepto + V.c_Documento) FROM [VAD10].[DBO].[MA_VENTAS] V " + " \n" + 
                        "WHERE V.c_Concepto IN ('VEN', 'DEV') AND V.c_Status = 'DCO' AND NOT LEFT(V.c_Documento, 1) = 'P' AND V.cs_Sync_SxS = " + pEstatusPrevio + ExcluirVNF + " ORDER BY V.ID)";
                    }
                    break;
                default: // Guardados con Exito.
                    pEstatusPrevio = "'" + RegistrosPendientesPorLote + "'";
                    break;
            }
            
            SQLMarcaje = "UPDATE [VAD10].[DBO].[MA_VENTAS] SET cs_Sync_SxS = '" + pEstatus + "' " + "\n" + 
            "WHERE cs_Sync_SxS IN (" + pEstatusPrevio + ") " + mRegistrosXLote + ExcluirVNF;
            
            RegistrosAfectados = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeUpdate(SQLMarcaje);
            
            if (gDebugMode) 
            {
                System.out.println(SQLMarcaje);
                System.out.println(RegistrosAfectados);
            }
            
            return (RegistrosAfectados > 0);
            
        } catch (Exception Ex) { 
            gLogger.EscribirLog(Ex, "Error marcando Ventas ADM pendientes");
            return false;
        }
        
    }
    
    public Boolean MarcarRegistrosVentasADM(String pEstatus)
    {
        return MarcarRegistrosVentasADM(pEstatus, false);
    }
    
    public Boolean ObtenerDatosVentasADM(List<FilteredRowSet> pRsVentas, List<FilteredRowSet> pRsItems, 
    List<FilteredRowSet> pRsDetPagos, List<FilteredRowSet> pRsImpuestos, 
    List<FilteredRowSet> pRsDocumentosFiscales, Boolean pReprocesarFallidos)
    {
        
        ResultSet pRsDataVentas = null;
        ResultSet pRsDataItems = null; 
        ResultSet pRsDataDetPagos = null;
        ResultSet pRsDataImpuestos = null;
        ResultSet pRsDataDocumentosFiscales = null;
        
        try {

            pRsVentas.set(0, new FilteredRowSetImpl()) ;
            pRsItems.set(0, new FilteredRowSetImpl()) ;
            pRsDetPagos.set(0, new FilteredRowSetImpl()) ;
            pRsImpuestos.set(0, new FilteredRowSetImpl()) ;
            pRsDocumentosFiscales.set(0, new FilteredRowSetImpl()) ;
            
            String mSQL = null;

            String ExcluirVNF = new String();
            
            String mEstatus = new String();
            
            if (pReprocesarFallidos)
                mEstatus = RegistrosPendientesFallidos;
            else
                mEstatus = RegistrosPendientesPorLote;
            
            mSQL = "SELECT (c_CodLocalidad + c_Concepto + c_Documento) AS TransID, * " + "\n" + 
            "FROM [VAD10].[DBO].[MA_VENTAS] DOC" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "') " + "\n" + 
            "AND c_Concepto IN ('VEN', 'DEV') " + "\n" + 
            "AND NOT LEFT(c_Documento, 1) = 'P' " + "\n" + 
            "AND c_Status IN ('DCO')" + "\n" + 
            "ORDER BY DOC.ID";
            
            pRsDataVentas = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataVentas.first();
            pRsVentas.get(0).populate(pRsDataVentas);
            
            mSQL = "" +
            "SELECT (c_CodLocalidad + c_Concepto + c_Documento) AS TransID, * " + "\n" + 
            "FROM [VAD10].[DBO].[TR_INVENTARIO] " + "\n" + 
            "WHERE c_Concepto IN ('VEN', 'DEV') " + "\n" + 
            "AND NOT LEFT(c_Documento, 1) = 'P' " + "\n" + 
            "AND (c_CodLocalidad + c_Concepto + c_Documento) IN (" + "\n" + 
            "SELECT (c_CodLocalidad + c_Concepto + c_Documento) " + "\n" + 
            "FROM [VAD10].[DBO].[MA_VENTAS]" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "')) " + "\n" + 
            "ORDER BY c_Linea ";
            
            pRsDataItems = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataItems.first();
            pRsItems.get(0).populate(pRsDataItems);

            mSQL = "SELECT (V.c_CodLocalidad + V.c_Concepto + V.c_Documento) AS TransID, " + "\n" +
            "V.c_Documento, V.c_Concepto, PAGRel.c_Concepto AS ConceptoPago, PAGRel.f_FechaE AS FechaPago, " + "\n" + 
            "CASE WHEN DetAP.DenominacionAnticipo IS NULL THEN DetPag.c_CodDenomina " + "\n" + 
            "ELSE DetAP.DenominacionAnticipo END AS DenominacionPago, " + "\n" + 
            "CASE WHEN DetAP.DenominacionAnticipo IS NULL THEN DetPag.c_CodMoneda " + "\n" + 
            "ELSE PAGRel.c_CodMoneda END AS MonedaPago, " + "\n" + 
            "CASE WHEN DetAP.DenominacionAnticipo IS NULL THEN (DetPag.n_Monto * DetPag.n_Factor) " + "\n" + 
            "ELSE PAGRel.n_Total END AS MontoPago, " + "\n" + 
            "CASE WHEN DetAP.DenominacionAnticipo IS NULL THEN " + "\n" + 
            "DetPag.c_Num_Cheque ELSE '' END AS ReferenciaPago" + "\n" + 
            "FROM [VAD10].[DBO].[MA_VENTAS] V " + "\n" + 
            "INNER JOIN [VAD10].[DBO].[MA_CXC] PAGRel ON V.c_Documento = PAGRel.C_Relacion AND V.c_Concepto = 'VEN' AND PAGRel.c_Concepto IN ('PAGxVEN', 'A_PxVEN') " + "\n" + 
            "LEFT JOIN [VAD10].[DBO].[MA_CXC_DETPAG] DetPag ON PAGRel.c_Documento = DetPag.c_Documento AND PAGRel.c_Concepto = 'PAGxVEN' AND DetPag.c_Concepto = 'PAG' " + "\n" + 
            "LEFT JOIN (SELECT '" + gFormaPagoAnticipoWeb + "' AS DenominacionAnticipo, 'A_PxVEN' AS c_Concepto) DetAP ON PAGRel.c_Concepto = DetAP.c_Concepto " + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "') " + "\n" + 
            "AND V.c_Concepto IN ('VEN') AND NOT LEFT(V.c_Documento, 1) = 'P' AND V.c_Status IN ('DCO') " + "\n" + 
            "ORDER BY V.c_Documento, " + "\n" + 
            "(CASE WHEN CASE WHEN DetAP.DenominacionAnticipo IS NULL THEN DetPag.c_CodDenomina " + "\n" + 
            "ELSE DetAP.DenominacionAnticipo END = 'Efectivo' THEN 1 ELSE 0 END) DESC, " + "\n" + 
            "(CASE WHEN CASE WHEN DetAP.DenominacionAnticipo IS NULL THEN DetPag.c_CodMoneda " + "\n" + 
            "ELSE PAGRel.c_CodMoneda END = '" + gMonedaStellarPredeterminada + "' THEN 1 ELSE 0 END) DESC, DetPag.ID";
            
            pRsDataDetPagos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataDetPagos.first();
            pRsDetPagos.get(0).populate(pRsDataDetPagos);
            
            mSQL = "SELECT *, (cs_CodLocalidad + cs_TipoDoc + cs_Documento) AS TransID " + "\n" +
            "FROM [VAD10].[DBO].[MA_VENTAS_IMPUESTOS] WHERE (cs_TipoDoc + cs_Documento) IN (" + "\n" + 
            "SELECT (c_Concepto + c_Documento) FROM [VAD10].[DBO].[MA_VENTAS]" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "') " + "\n" + 
            "AND c_Concepto IN ('VEN', 'DEV') " + "\n" + 
            "AND NOT LEFT(c_Documento, 1) = 'P' " + "\n" + 
            "AND c_Status IN ('DCO'))" + "\n" + 
            "";
            
            pRsDataImpuestos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataImpuestos.first();
            pRsImpuestos.get(0).populate(pRsDataImpuestos);
            
            mSQL = "SELECT *, (cu_Localidad + cu_DocumentoTipo + cu_DocumentoStellar) AS TransID " + "\n" +
            "FROM [VAD10].[DBO].[MA_DOCUMENTOS_FISCAL] " + "\n" + 
            "WHERE (cu_Localidad + cu_DocumentoTipo + cu_DocumentoStellar) IN (" + "\n" + 
            "SELECT (c_CodLocalidad + c_Concepto + c_Documento) FROM [VAD10].[DBO].[MA_VENTAS]" + "\n" + 
            "WHERE (cs_Sync_SxS = '" + mEstatus + "') " + "\n" + 
            "AND c_Concepto IN ('VEN', 'DEV') " + "\n" + 
            "AND NOT LEFT(c_Documento, 1) = 'P' " + "\n" + 
            "AND c_Status IN ('DCO'))" + "\n" + 
            "";
            
            pRsDataDocumentosFiscales = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            //pRsDataDocumentosFiscales.first();
            pRsDocumentosFiscales.get(0).populate(pRsDataDocumentosFiscales);
            
            return pRsDataVentas.first();
            
        } catch (Exception ex) {
            gLogger.EscribirLog(ex, "Error al buscar registros de ventas administrativas pendientes por procesar.");
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    public ResultSet DatosFiscalesDocRelVentasADM(String pDocRel)
    {
        
        ResultSet Datos;
        
        try {
            Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
            "SELECT TOP (1) *" + GetLines() +
            "FROM [VAD10].[DBO].[MA_DOCUMENTOS_FISCAL] WHERE (cu_Localidad + cu_DocumentoTipo + cu_DocumentoStellar) = '" + pDocRel + "'");
            if (Datos.first()) { return Datos; }
            return null;
        } catch (SQLException ex) {
            return null;
        }
        
    }
    
    public Boolean MarcarFallidoVentasADM(String pTransID)
    {
        
        //ResultSet Datos;
        int RowsAffected = 0;
        
        try {
            RowsAffected = gConexion.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY).executeUpdate(
            "UPDATE [VAD10].[DBO].[MA_VENTAS] SET cs_Sync_SxS = '" + RegistrosPendientesFallidos + "' \n" + 
            "WHERE (c_CodLocalidad + c_Concepto + c_Documento) = '" + pTransID + "'");
            if (RowsAffected > 0) 
                return true;
            else
                return false;
        } catch (SQLException ex) {
            gLogger.EscribirLog(ex, "Error marcando venta adm fallida " + pTransID);
            return false;
        }
        
    }
    
    public ResultSet BuscarCategoriasProductoADM(String pCodigo)
    {
        
        ResultSet Datos;
        
        try {
            Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
            "SELECT TOP (1) c_Departamento, c_Grupo, c_Subgrupo" + " \n" +
            "FROM [VAD10].[DBO].[MA_PRODUCTOS] WHERE c_Codigo = '" + pCodigo + "'");
            if (Datos.first()) { return Datos; }
            return null;
        } catch (SQLException ex) {
            return null;
        }
        
    }
    
    public String BuscarPedidoWebVentaADM(String pTransID)
    {
        
        ResultSet Datos;
        
        try {
            Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(
            "SELECT * FROM (\n" +
            "SELECT\n" +
            "isNULL(\n" +
            "CASE WHEN DOCREL.c_Concepto = 'PED' AND LEFT(DOCREL.c_Relacion, 3) = 'WEB' THEN DOCREL.c_Relacion ELSE\n" +
            "CASE WHEN DR2.c_Concepto = 'PED' AND LEFT(DR2.c_Relacion, 3) = 'WEB' THEN DR2.c_Relacion ELSE\n" +
            "CASE WHEN DR3.c_Concepto = 'PED' AND LEFT(DR3.c_Relacion, 3) = 'WEB' THEN DR3.c_Relacion ELSE \n" +
            "NULL END END END, ''\n" +
            ") AS PedidoWeb\n" +
            "--, VEN.*, DOCREL.*, DR2.* \n" +
            "FROM [VAD10].DBO.MA_VENTAS VEN \n" +
            "LEFT JOIN [VAD10].DBO.MA_VENTAS DOCREL\n" +
            "ON (DOCREL.c_Concepto + ' ' + DOCREL.c_Documento) \n" +
            "= VEN.c_Relacion\n" +
            "LEFT JOIN [VAD10].DBO.MA_VENTAS DR2\n" +
            "ON (DR2.c_Concepto + ' ' + DR2.c_Documento) \n" +
            "= DOCREL.c_Relacion\n" +
            "LEFT JOIN [VAD10].DBO.MA_VENTAS DR3\n" +
            "ON (DR3.c_Concepto + ' ' + DR3.c_Documento) \n" +
            "= DR2.c_Relacion\n" +
            "WHERE VEN.c_Concepto IN ('VEN', 'DEV')\n" +
            "AND NOT LEFT(VEN.c_Documento, 1) = 'P'\n" +
            "AND VEN.c_Status = 'DCO'\n" +
            "AND (VEN.c_CodLocalidad + VEN.c_Concepto + VEN.c_Documento) = '" + pTransID + "'\n" +
            ") TB ");
            if (Datos.first()) { return Datos.getString("PedidoWeb"); } else { return ""; }
        } catch (SQLException ex) {
            return "";
        }
        
    }

    public Boolean BuscarDatosPagosWeb(List<FilteredRowSet> pRsDetPagos, String pTransID)
    {
        
        ResultSet Datos;
        
        try {
            
            pRsDetPagos.set(0, new FilteredRowSetImpl()) ;
            
            String mSQL = 
            "SELECT '" + pTransID + "' AS TransID, '' AS c_Documento, '' AS c_Concepto,\n" +
            "'A_P' AS ConceptoPago, CXCDP.f_Fecha AS FechaPago,\n" +
            "CXCDP.c_CodDenomina AS DenominacionPago,\n" +
            "CXCDP.c_CodMoneda AS MonedaPago,\n" +
            "(CXCDP.n_Monto * CXCDP.n_Factor) AS MontoPago,\n" +
            "CXCDP.c_Num_Cheque AS ReferenciaPago\n" +
            "FROM (\n" +
            "SELECT\n" +
            "isNULL(\n" +
            "CASE WHEN DOCREL.c_Concepto = 'PED' AND LEFT(DOCREL.c_Relacion, 3) = 'WEB' THEN DOCREL.c_Relacion ELSE\n" +
            "CASE WHEN DR2.c_Concepto = 'PED' AND LEFT(DR2.c_Relacion, 3) = 'WEB' THEN DR2.c_Relacion ELSE\n" +
            "CASE WHEN DR3.c_Concepto = 'PED' AND LEFT(DR3.c_Relacion, 3) = 'WEB' THEN DR3.c_Relacion ELSE \n" +
            "NULL END END END, ''\n" +
            ") AS PedidoWeb\n" +
            "--, VEN.*, DOCREL.*, DR2.* \n" +
            "FROM [VAD10].DBO.MA_VENTAS VEN \n" +
            "LEFT JOIN [VAD10].DBO.MA_VENTAS DOCREL\n" +
            "ON (DOCREL.c_Concepto + ' ' + DOCREL.c_Documento) \n" +
            "= VEN.c_Relacion\n" +
            "LEFT JOIN [VAD10].DBO.MA_VENTAS DR2\n" +
            "ON (DR2.c_Concepto + ' ' + DR2.c_Documento) \n" +
            "= DOCREL.c_Relacion\n" +
            "LEFT JOIN [VAD10].DBO.MA_VENTAS DR3\n" +
            "ON (DR3.c_Concepto + ' ' + DR3.c_Documento) \n" +
            "= DR2.c_Relacion\n" +
            "WHERE VEN.c_Concepto IN ('VEN', 'DEV')\n" +
            "AND NOT LEFT(VEN.c_Documento, 1) = 'P'\n" +
            "AND VEN.c_Status = 'DCO'\n" +
            "AND (VEN.c_CodLocalidad + VEN.c_Concepto + VEN.c_Documento) = '" + pTransID + "'\n" +
            ") TB INNER JOIN [VAD10].DBO.MA_VENTAS_WEB_DETPAG DP\n" +
            "ON DP.Code = SUBSTRING(TB.PedidoWeb, 5, 9999)\n" +
            "AND DP.Amount > 0\n" +
            "INNER JOIN [VAD10].DBO.MA_CXC_DETPAG CXCDP\n" +
            "ON CXCDP.c_Documento = DP.CorrelativoAnticipoCxC\n" +
            "AND CXCDP.c_Concepto = 'A_P'\n" +
            "WHERE TB.PedidoWeb <> '' AND DP.Amount > 0 \n" +
            "";
            
            Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE).executeQuery(mSQL);
            
            pRsDetPagos.get(0).populate(Datos);
            
            if (gDebugMode) System.out.println("Verificando si Datos.first()" + String.valueOf(Datos.first()));
            
            return Datos.first();
            
        } catch (SQLException ex) {
            if (gDebugMode) System.out.println("Error Buscando Datos pago web" + pTransID);
            gLogger.EscribirLog(ex, "Buscando Datos pago web " + pTransID);
            return false;
        }
        
    }
    
    public void ConstruirRegistrosDeVentasADM(Boolean pReprocesarFallidos)
    {
        
        String mUltDoc = new String(); Double mUltTot = 0.0; String mUltFec = new String();
        String mEtapa;
        
        java.util.Date DocTime = new java.util.Date();
        
        mEtapa = "Iniciando Ventas ADM...";
        
        try {
            
            FilteredRowSet RsVentas = null, RsItems = null, RsDetPagos = null,
            RsImpuestos = null, RsDocumentosFiscales = null;
            
            List<FilteredRowSet> lRsVentas = Arrays.asList(new FilteredRowSet[]{ RsVentas }), 
            lRsItems = Arrays.asList(new FilteredRowSet[]{ RsItems }),
            lRsDetPagos = Arrays.asList(new FilteredRowSet[]{ RsDetPagos }),
            lRsImpuestos = Arrays.asList(new FilteredRowSet[]{ RsImpuestos }),
            lRsDocumentosFiscales = Arrays.asList(new FilteredRowSet[]{ RsDocumentosFiscales });
            
            ResultSet RsVentaFinal = null; ResultSet RsItemsFinal = null; ResultSet RsDetPagFinal = null;
            
            Double MontoExento; Object tmpVal; Double Cont;
            
            Double mTasaOrigen = 1.0;
            Double mTasaDestino = 1.0;
            Double mTasaConversion = 1.0; 
            
            Integer mLimiteDec = 0;
            
            //if (pReprocesarFallidos)
                //MarcarRegistros(RegistrosPendientesPorLote, true);
            
            if (ObtenerDatosVentasADM(lRsVentas, lRsItems, lRsDetPagos, lRsImpuestos, lRsDocumentosFiscales, pReprocesarFallidos))
            {
                if (true) //ObtenerDataSourceVentasFinal(RsVentaFinal, RsItemsFinal, RsDetPagFinal))
                {
                    
                    if (gTestMode) { // PRUEBAS
                        
                    }
                    else // Real Processing - Procesamiento Real - ConstruirRegistrosDeVentas:
                    {
                    
                    Double Signo;
                    
                    RsVentas = lRsVentas.get(0);
                    RsItems = lRsItems.get(0);
                    RsDetPagos = lRsDetPagos.get(0);
                    RsImpuestos = lRsImpuestos.get(0);
                    RsDocumentosFiscales = lRsDocumentosFiscales.get(0);
                    
                    ResultSetMetaData FieldData = null;
                    FieldData = RsVentas.getMetaData();

                    SimpleDateFormat dtFmt = new SimpleDateFormat("yyyyMMdd");
                    SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMddHHmmss");
                    SimpleDateFormat dtFmtLog = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    
                    while (RsVentas.next())
                    {
                        
                        NextTrans: {
                        
                        if (RsVentas.getString("c_Concepto").toUpperCase().equalsIgnoreCase("DEV"))
                            Signo = (-1.0);
                        else
                            Signo = 1D;
                            
                        mUltDoc = RsVentas.getString("TransID");
                        mUltTot = RsVentas.getDouble("n_Total");
                        mUltFec = RsVentas.getString("d_Fecha");
                        
                        DocTime = RsVentas.getDate("d_Fecha"); //DateTimeFrom(RsVentas.getDate("d_Fecha"), RsVentas.getDate("d_Fecha"));
                        
                        mTasaDestino = BuscarTasaTransaccionADM(RsVentas, DocTime, RsVentas.getString("TransID"));
                        
                        if (mTasaDestino <= 0)
                        {
                            gLogger.EscribirLog(mUltDoc + " [" + dtFmtLog.format(DocTime) + "] - " + "Hubo un error al obtener la tasa de conversión a moneda alterna " + 
                            "o es inválida para la fecha del documento (Valor Encontrado = " + mTasaDestino.toString() + ").");
                            // Controlar Error de Registro de Ventas.
                            MarcarFallido(RsVentas.getString("TransID"));
                            break NextTrans;
                        }
                        
                        mTasaConversion = (mTasaOrigen / mTasaDestino);
                        
                        /*for (int i = 1; i <= RsVentas.getMetaData().getColumnCount(); i++) {
                            System.out.println(RsVentas.getMetaData().getColumnLabel(i) + 
                            " => " + 
                            RsVentas.getObject(i).toString());
                        }*/
                        
                        JCoStructure TestStruc = null;
                        JCoTable Transaction = null;
                        JCoTable TransactionDiscount = null;
                        JCoTable RetailLineItem = null;
                        JCoTable LineItemVoid = null;
                        JCoTable LineItemDiscount = null;
                        JCoTable LineItemTax = null;
                        JCoTable Tender = null;
                        JCoTable LineItemExt = null;
                        JCoTable TransactionExt = null;
                        JCoTable Additionals = null;

                        JCoTable Return = null;
                        
                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_LOCKWAIT", 0);

                        TestStruc = gCnSAP.fTransVentas.getImportParameterList().getStructure("I_SOURCEDOCUMENTLINK");

                        TestStruc.setValue("KEY", new String());
                        TestStruc.setValue("TYPE", new String());
                        TestStruc.setValue("LOGICALSYSTEM", new String());

                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_SOURCEDOCUMENTLINK", TestStruc);
                        gCnSAP.fTransVentas.getImportParameterList().setValue("I_COMMIT", "X");
                        
                        Transaction = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTION");
                        TransactionDiscount = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTIONDISCOUNT");
                        RetailLineItem = gCnSAP.fTransVentas.getTableParameterList().getTable("RETAILLINEITEM");
                        LineItemVoid = gCnSAP.fTransVentas.getTableParameterList().getTable("LINEITEMVOID");
                        LineItemDiscount = gCnSAP.fTransVentas.getTableParameterList().getTable("LINEITEMDISCOUNT");
                        LineItemTax = gCnSAP.fTransVentas.getTableParameterList().getTable("LINEITEMTAX");
                        Tender = gCnSAP.fTransVentas.getTableParameterList().getTable("TENDER");
                        LineItemExt = gCnSAP.fTransVentas.getTableParameterList().getTable("LINEITEMEXT");
                        TransactionExt = gCnSAP.fTransVentas.getTableParameterList().getTable("TRANSACTIONEXT");
                        Additionals = gCnSAP.fTransVentas.getTableParameterList().getTable("ADDITIONALS");
                        
                        Transaction.clear();
                        TransactionDiscount.clear();
                        RetailLineItem.clear();
                        LineItemVoid.clear();
                        LineItemDiscount.clear();
                        LineItemTax.clear();
                        Tender.clear();
                        LineItemExt.clear();
                        TransactionExt.clear();
                        Additionals.clear();
                        
                        // EMPEZAR A LLENAR LOS DATOS DE LA TRANSACCIÓN
                        
                        mEtapa = "Llenando Datos Cabecero Ventas ADM...";
                        
                        // DATOS DEL DOCUMENTO - CABECERO
                        
                        Transaction.appendRow();
                        
                        /*if (!gCodLocalidadSAP.isEmpty())
                            Transaction.setValue("RETAILSTOREID", Left(gCodLocalidadSAP, 10));
                        else
                            Transaction.setValue("RETAILSTOREID",  Left(RsVentas.getString("c_Sucursal"), 10));*/
                        
                        String TransactionSequenceNumber;
                        
                        if (VentaADM_NumeroTransaccionSinPrefijo)
                        {
                            TransactionSequenceNumber = RsVentas.getString("c_Documento").replaceFirst(VentaADM_IndiceNumericoLocalidad, "");
                        }
                        else
                        {
                            TransactionSequenceNumber = RsVentas.getString("c_Documento");
                        }
                        
                        String PedidoWeb = BuscarPedidoWebVentaADM(RsVentas.getString("TransID"));
                        
                        Boolean OrigenWeb = !PedidoWeb.isEmpty(); //RsVentas.getString("c_Observacion").toUpperCase().contains("WEB [");
                        
                        if (gCodLocalidadSAP.equalsIgnoreCase("*") && gCodLocalidadStellar.equalsIgnoreCase("*"))
                            Transaction.setValue("RETAILSTOREID", Left(RsVentas.getString("c_CodLocalidad"), 10));
                        else
                            Transaction.setValue("RETAILSTOREID", Left(gCodLocalidadSAP, 10));
                        
                        String ShortDoc = RsVentas.getString("c_Concepto") + RsVentas.getString("c_Documento");
                        
                        Transaction.setValue("BUSINESSDAYDATE", Left(dtFmt.format(RsVentas.getDate("d_Fecha")), 8));
                        Transaction.setValue("TRANSACTIONTYPECODE", "1001");
                        Transaction.setValue("WORKSTATIONID", Left(VentaADM_POS_ID, 10));
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                            Transaction.setValue("TRANSACTIONSEQUENCENUMBER", Left(TransactionSequenceNumber, 20));
                        else if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV"))
                            Transaction.setValue("TRANSACTIONSEQUENCENUMBER", Left("D" + TransactionSequenceNumber, 20));
                        
                        Transaction.setValue("BEGINDATETIMESTAMP", Left(dtFmtLong.format(RsVentas.getDate("d_Fecha")), 14));
                        
                        Transaction.setValue("ENDDATETIMESTAMP", Left(dtFmtLong.format(RsVentas.getDate("d_Fecha")), 14));
                        Transaction.setValue("DEPARTMENT", new String());
                        Transaction.setValue("OPERATORQUALIFIER", new String());
                        Transaction.setValue("OPERATORID", Left(RsVentas.getString("c_CodVendedor"), 30));
                        
                        if (!SincronizarEnMultiMoneda)
                        {
                            Transaction.setValue("TRANSACTIONCURRENCY", gMonedaSAPPredeterminada);
                            Transaction.setValue("TRANSACTIONCURRENCY_ISO", new String());
                        }
                        else
                        {
                            Transaction.setValue("TRANSACTIONCURRENCY", SincronizarEnMultiMoneda_Param_TransactionCurrency);
                            Transaction.setValue("TRANSACTIONCURRENCY_ISO", SincronizarEnMultiMoneda_Param_TransactionCurrency_ISO);                            
                        }
                        
                        Transaction.setValue("PARTNERQUALIFIER", new String());
                        Transaction.setValue("PARTNERID", new String());
                        
                        mEtapa = "Llenando Datos Extras Ventas ADM...";
                        
                        // DATOS ADICIONALES DE LA TRANSACCIÓN.
                        
                        int nPosRowExt = 0;
                        
                        nPosRowExt++;
                        TransactionExt.appendRow();
                        //TransactionExt.setRow(nPosRowExt);
                        
                        TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        TransactionExt.setValue("FIELDGROUP", new String());
                        TransactionExt.setValue("FIELDNAME", "CLI_RIFCLI");
                        TransactionExt.setValue("FIELDVALUE", Left(RsVentas.getString("c_RIF"), 40));
                        
                        nPosRowExt++;
                        TransactionExt.appendRow();
                        //TransactionExt.setRow(nPosRowExt);
                        
                        TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        TransactionExt.setValue("FIELDGROUP", new String());
                        TransactionExt.setValue("FIELDNAME", "CLI_NOMBRE");
                        TransactionExt.setValue("FIELDVALUE", Left(RsVentas.getString("c_Descripcion"), 40));
                        
                        nPosRowExt++;
                        TransactionExt.appendRow();
                        //TransactionExt.setRow(nPosRowExt);
                        
                        TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                        TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                        TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                        TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                        TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                        TransactionExt.setValue("FIELDGROUP", new String());
                        TransactionExt.setValue("FIELDNAME", "StellarDoc");
                        TransactionExt.setValue("FIELDVALUE", Left((OrigenWeb ? "WEB" : "ADM") + "_" + RsVentas.getString("TransID"), 40));
                        
                        if (SincronizarEnMultiMoneda)
                        {
                            
                            mLimiteDec = Integer.parseInt(SincronizarenMultiMoneda_Limite_Decimales);
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);

                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "Cod_Moneda_Alterna");
                            TransactionExt.setValue("FIELDVALUE", Left(SincronizarEnMultiMoneda_CodMoneda, 40));
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);

                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "Tasa_Moneda_Alterna");
                            TransactionExt.setValue("FIELDVALUE", Left(RawDouble(mTasaDestino), 40));
                            
                        } else {
                            mLimiteDec = 8;
                        }
                        
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV")
                        && RsVentas.getString("c_Relacion").length() >= 5)
                        {
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "StellarFDV");
                            TransactionExt.setValue("FIELDVALUE", Left((OrigenWeb ? "WEB" : "ADM") + "_" + "VEN" + 
                            RsVentas.getString("c_Relacion").substring(5), 40));
                            
                        }
                        
                        ResultSet DatosFiscales_DocumentoRelacion = null;
                        
                        mEtapa = "Buscando Datos Fiscales Ventas ADM...";
                        
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV")
                        && RsVentas.getString("c_Relacion").length() >= 5)
                            DatosFiscales_DocumentoRelacion = DatosFiscalesDocRelVentasADM(RsVentas.getString("c_CodLocalidad") + 
                            "VEN" + RsVentas.getString("c_Relacion").substring(5));
                        
                        // DATOS FISCALES
                        
                        RsDocumentosFiscales.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID") );

                        if (RsDocumentosFiscales.first())
                        {
                            
                            mEtapa = "Llenando Datos Fiscales Ventas ADM...";
                            
//                            FieldData = RsDocumentosFiscales.getMetaData();
//                            RsDocumentosFiscales.beforeFirst();
//                            while (RsDocumentosFiscales.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsDocumentosFiscales.getObject(i).toString());
//                                }
//                            }
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_INDCNS");
                            TransactionExt.setValue("FIELDVALUE", "1");
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_SERIMP");
                            TransactionExt.setValue("FIELDVALUE", Left(RsDocumentosFiscales.getString("cu_SerialImpresora"), 40));
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_NRODOC");
                            TransactionExt.setValue("FIELDVALUE", Left(RsDocumentosFiscales.getString("cu_DocumentoFiscal"), 40));
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_RPRT_Z");
                            if (ExisteCampoTabla(RsDocumentosFiscales, "cu_ZFiscal"))
                                TransactionExt.setValue("FIELDVALUE", Left(RsDocumentosFiscales.getString("cu_ZFiscal"), 40));
                            else
                                TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "REF_SERIMP");
                            if (DatosFiscales_DocumentoRelacion == null)
                                TransactionExt.setValue("FIELDVALUE", new String());
                            else
                                TransactionExt.setValue("FIELDVALUE", Left(DatosFiscales_DocumentoRelacion.getString("cu_SerialImpresora"), 40));
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "REF_NRODOC");
                            if (DatosFiscales_DocumentoRelacion == null)
                                TransactionExt.setValue("FIELDVALUE", new String());
                            else
                                TransactionExt.setValue("FIELDVALUE", Left(DatosFiscales_DocumentoRelacion.getString("cu_DocumentoFiscal"), 40));
                            
                        }
                        else
                        {
                            
                            if (gValidarDocumentoFiscal && !RsVentas.getBoolean("bu_Impresa")) 
                            {
                                System.out.println(RsVentas.getString("TransID") + " - " + "La transaccion no está impresa fiscalmente.");
                                gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "La transaccion no está impresa fiscalmente.");
                                // Controlar Error de Registro de Ventas.
                                MarcarFallidoVentasADM(RsVentas.getString("TransID"));
                                break NextTrans;
                            }
                            
                            mEtapa = "Llenando Datos Fiscales Vacios como Fallback Ventas ADM...";
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_INDCNS");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_SERIMP");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_NRODOC");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "FIS_RPRT_Z");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "REF_SERIMP");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                            nPosRowExt++;
                            TransactionExt.appendRow();
                            //TransactionExt.setRow(nPosRowExt);
                            
                            TransactionExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            TransactionExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            TransactionExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            TransactionExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            TransactionExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            TransactionExt.setValue("FIELDGROUP", new String());
                            TransactionExt.setValue("FIELDNAME", "REF_NRODOC");
                            TransactionExt.setValue("FIELDVALUE", new String());
                            
                        }
                        
                        // Comentando Impuestos por Transacción. Hasta ahora no son necesarios.
                        //RsImpuestos.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID") );

                        //if (RsImpuestos.first())
                        //{
//                            FieldData = RsImpuestos.getMetaData();
//                            RsImpuestos.beforeFirst();
//                            while (RsImpuestos.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsImpuestos.getObject(i).toString());
//                                }
//                            }
                        //}
                        //else
                        //{
                            
                        //}
                        
                        if (gEnviarTransactionDiscount) 
                        {
                            
                            mEtapa = "Llenando Datos Descuento Cabecero Ventas ADM...";
                            
                            if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN") 
                            && RsVentas.getDouble("n_Descuento") > 0) 
                            {
                                
                                TransactionDiscount.appendRow();
                                
                                TransactionDiscount.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                TransactionDiscount.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                TransactionDiscount.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                TransactionDiscount.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                TransactionDiscount.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                
                                TransactionDiscount.setValue("DISCOUNTSEQUENCENUMBER", "1");
                                TransactionDiscount.setValue("DISCOUNTTYPECODE", "3401");
                                TransactionDiscount.setValue("DISCOUNTREASONCODE", new String());
                                TransactionDiscount.setValue("REDUCTIONAMOUNT", Left(RawDouble(Round(
                                RsVentas.getDouble("n_Descuento") * mTasaConversion, mLimiteDec)), 28));
                                TransactionDiscount.setValue("STOREFINANCIALLEDGERACCOUNTID", new String());
                                TransactionDiscount.setValue("DISCOUNTID", new String());
                                TransactionDiscount.setValue("DISCOUNTIDQUALIFIER", new String());
                                TransactionDiscount.setValue("BONUSBUYID", new String());
                                TransactionDiscount.setValue("OFFERID", new String());
                                
                            }
                            
                        }
                        
                        // DETALLE DE PAGOS
                        
                        int nPosTender = 0;
                        
                        if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN")) 
                        {
                            
                            mEtapa = "Llenando Datos de Pagos Ventas ADM...";
                            
                            FilteredRowSet RsDetallePago = null;
                            
                            List<FilteredRowSet> lRsDetallePago = Arrays.asList(new FilteredRowSet[]{ RsDetallePago });
                            
                            if (gDebugMode)
                                System.out.println(RsVentas.getString("TransID") + " " + "Origen Web: " + String.valueOf(OrigenWeb));
                            
                            if (OrigenWeb)
                            {

                                if (gDebugMode)
                                    System.out.println("Buscando Datos pagos web.");
                                
                                Boolean mDetPagWeb = BuscarDatosPagosWeb(lRsDetallePago, RsVentas.getString("TransID"));
                                
                                if (mDetPagWeb)
                                {
                                    RsDetallePago = lRsDetallePago.get(0);
                                } 
                                else
                                {
                                    RsDetallePago = RsDetPagos;
                                }
                                
                            } else 
                            {
                                RsDetallePago = RsDetPagos;
                            }
                            
                            /*if (gDebugMode)
                                    System.out.println("Que pasa con RsDetallePago: " + String.valueOf((RsDetallePago == null)));]*/
                            
                            RsDetallePago.setFilter(new StringColumnFilter(
                            RsVentas.getString("TransID"), "TransID"));
                            
                            if (RsDetallePago.last())
                            {
                                
    //                            FieldData = RsDetallePago.getMetaData();
    //                            RsDetallePago.beforeFirst();
    //                            while (RsDetallePago.next()) {
    //                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
    //                                    System.out.println(FieldData.getColumnLabel(i) + 
    //                                    " => " + 
    //                                    RsDetallePago.getObject(i).toString());
    //                                }
    //                            }
                                
                                // Verificamos si se quiere manejar una forma de pago resumida
                                // por el total de la factura. Para ello la variable 
                                // gFormaPagoFacturaSAP_SinDetallePago debe tener un codigo de
                                // forma de pago en SAP establecido con el cual sera enviado
                                // ese unico registro. Este mecanismo generalmente solo es usado
                                // como contingencia, lo mas probable es que dicha variable siempre
                                // esté en blanco o con un *, lo cual indica que se debe sincronizar
                                // el detalle de pago, ignorando montos por encima a causa de vuelto y donaciones.
                                
                                if (!(gFormaPagoFacturaSAP_SinDetallePago.isEmpty() || 
                                gFormaPagoFacturaSAP_SinDetallePago.equalsIgnoreCase("*")))
                                { // Si tiene un codigo especifico definido (No esta vacía ni es "*").
                                    
                                    mEtapa = "Llenando Datos Forma de Pago Generica Ventas ADM...";
                                    
                                    // 1 Solo registro con el monto de la venta.
                                    
                                    nPosTender++;
                                    Tender.appendRow();
                                    
                                    Tender.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                    Tender.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                    Tender.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                    Tender.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                    Tender.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                    Tender.setValue("TENDERSEQUENCENUMBER", String.valueOf(nPosTender));
                                    
                                    String TenderCurrency = gMonedaSAPPredeterminada;
                                    
                                    String TenderTypeCode = gFormaPagoFacturaSAP_SinDetallePago;
                                    
                                    Tender.setValue("TENDERTYPECODE", TenderTypeCode);
                                    Tender.setValue("TENDERAMOUNT", Left(RawDouble(Round(
                                    RsVentas.getDouble("n_Total") * mTasaConversion, mLimiteDec)), 28));
                                    Tender.setValue("TENDERCURRENCY", TenderCurrency);
                                    Tender.setValue("TENDERCURRENCY_ISO", new String());
                                    Tender.setValue("TENDERID", Left(TenderTypeCode, 32));
                                    Tender.setValue("ACCOUNTNUMBER", new String());
                                    Tender.setValue("REFERENCEID", new String());
                                    
                                }
                                else // Caso común. Enviar todo el detalle de pago.
                                {                                    
                                    
                                    mEtapa = "Llenando Datos Formas de Pago Ventas ADM...";
                                    
                                    int DetPagRowCount = RsDetallePago.getRow();
                                    
                                    double mVueltoRestante = RsVentas.getDouble("n_Vuelto");
                                    double mVueltoLn = 0;
                                    double mPagoLn = 0;
                                    double mRestanteDonacion = 0; //RsVentas.getDouble("n_MontoDonacion");
                                    
                                    RsDetallePago.beforeFirst();
                                    
                                    while (RsDetallePago.next())
                                    {
                                        
                                        if (RsDetallePago.getDouble("MontoPago") > 0){ // Omitir Vueltos
                                        
                                        String mMoneda = RsDetallePago.getString("MonedaPago");
                                        Double mMontoLn = RsDetallePago.getDouble("MontoPago");
                                        
                                        String TenderCurrency = null;
                                        String LineCurrency = null;
                                        
                                        if (ListaAsociacionMoneda.containsKey(mMoneda))
                                        {
                                            LineCurrency = ListaAsociacionMoneda.get(
                                            mMoneda).toString();
                                        }
                                        
                                        if (LineCurrency == null) LineCurrency = "";
                                        
                                        String TenderTypeCode = null;
                                        
                                        // Primero buscar Llave compuesta de moneda y denominación.
                                        if (ListaAsociacionFormaPago.containsKey(
                                        mMoneda + ";" + RsDetallePago.getString("DenominacionPago")))
                                        {
                                            TenderTypeCode = ListaAsociacionFormaPago.get(
                                            mMoneda + ";" + RsDetallePago.getString("DenominacionPago")).toString();
                                        } else if (ListaAsociacionFormaPago.containsKey(
                                        RsDetallePago.getString("DenominacionPago"))){ // sino existe se intenta buscar solo por código de denominación.
                                            TenderTypeCode = ListaAsociacionFormaPago.get(
                                            RsDetallePago.getString("DenominacionPago")).toString();
                                        }                                        
                                        
                                        //if ((RsDetallePago.getString("DenominacionPago").equalsIgnoreCase("Efectivo") ||
                                        //DetPagRowCount == 1)) // && Round(RsDetallePago.getDouble("n_Factor"), 8) == 1)
                                        if (mVueltoRestante > 0)
                                        {
                                            
                                            /*if (LineCurrency.equalsIgnoreCase(gMonedaSAPPredeterminada) && 
                                            RsDetallePago.getString("DenominacionPago").equalsIgnoreCase("Efectivo"))
                                            {   
                                                
                                                // Se determinó que el POS le resta el vuelto al Efectivo de la
                                                // moneda predeterminada. En estos casos entonces, no restamos.
                                                // Esto da pie a muchas inconsistencias pero bueno, asi esta hecho.
                                                // Hay que resolver de esta manera.
                                                
                                                mVueltoLn = 0;
                                                mPagoLn = Round(mMontoLn, 8);
                                                
                                                // Es MAS!, en casos extraños pudiera quedar el Row con monto negativo...
                                                // Lo que significa que hay un vuelto que habría que restarle a otra línea.
                                                // entonces corregimos de la siguiente manera:
                                                
                                                if (mPagoLn < 0)
                                                {   // Modificamos el vuelto original por el vuelto Restante (el monto negativo)
                                                    mVueltoRestante = Math.abs(mPagoLn);
                                                }
                                                else
                                                {   // Si el monto quedo positivo entonces no quedo mas vuelto por restar a ninguna otra forma de pago.
                                                    mVueltoRestante = 0;
                                                }
                                                
                                            }
                                            else
                                            {*/   
                                                // Si hay vuelto, sea lo que sea, restarselo
                                                // ya que el BUSINESS no se la resto. Caso Efectivo en Otras Monedas...
                                                mVueltoLn = (Round(mMontoLn - mVueltoRestante, 8) >= 0 ? 
                                                Round(mVueltoRestante, 8) : Round(mMontoLn, 8));
                                            //}
                                            
                                            mPagoLn = Round(mMontoLn - mVueltoLn, 8);
                                            mVueltoRestante = Round(mVueltoRestante - mVueltoLn, 8);
                                            
                                        }
                                        else
                                        {
                                            mVueltoLn = 0;
                                            mPagoLn = Round(mMontoLn, 8);
                                        }
                                        
                                        if (mRestanteDonacion > 0) {
                                            
                                            if (mRestanteDonacion > mPagoLn)
                                            {
                                                mRestanteDonacion = Round(mRestanteDonacion - mPagoLn, 8);
                                                mPagoLn = 0;
                                            }
                                            else
                                            {
                                                mPagoLn = Round(mPagoLn - mRestanteDonacion, 8);
                                                mRestanteDonacion = 0;
                                            }
                                            
                                        }
                                        
                                        //mPagoLn = Round(mPagoLn, 2); // Mejor enviar el pago exactamente como es.
                                        
                                        if (mPagoLn > 0) // Monto final del pago - Vuelto - Donaciones.
                                        {
                                            
                                            nPosTender++;
                                            Tender.appendRow();
                                            
                                            Tender.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                            Tender.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                            Tender.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                            Tender.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                            Tender.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                            Tender.setValue("TENDERSEQUENCENUMBER", String.valueOf(nPosTender));                                            
                                            
                                            // TenderCurrency = LineCurrency;
                                            
                                            // TRANSFERIR COMO MONEDA PREDETERMINADA FACTORIZADA.
                                            
                                            TenderCurrency = gMonedaSAPPredeterminada;
                                            
                                            /*// En vez de marcar como fallidos, enviemosle lo que ellos consideren como efectivo
                                            // de manera predeterminada para no trancar el proceso.
                                            if (TenderTypeCode == null)
                                                TenderTypeCode = "3101"; // Efectivo*/
                                            
                                            if (TenderTypeCode == null || TenderCurrency == null)
                                            {
                                                System.out.println(RsVentas.getString("TransID") + " - " + "Una de la(s) formas de pago / moneda del pago de la factura no está categorizada para envio a SAP.");
                                                gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "Una de la(s) formas de pago / moneda del pago de la factura no está categorizada para envio a SAP.");
                                                // Controlar Error Registro de Ventas.
                                                MarcarFallidoVentasADM(RsVentas.getString("TransID"));
                                                break NextTrans;
                                            }
                                            
                                            Tender.setValue("TENDERTYPECODE", TenderTypeCode);
                                            
                                            /*Tender.setValue("TENDERAMOUNT", Left(String.valueOf(Round(RsDetPagos.getDouble("n_Monto") -
                                            ((RsDetPagos.getString("c_CodDenominacion").equalsIgnoreCase("Efectivo") ||
                                            DetPagRowCount == 1) && Round(RsDetPagos.getDouble("n_Factor"), 8) == 1 ? 
                                            RsVentas.getDouble("n_Vuelto"): 0), 8)), 28));*/
                                            
                                            Tender.setValue("TENDERAMOUNT", Left(RawDouble(Round(mPagoLn * mTasaConversion, mLimiteDec)), 28));
                                            Tender.setValue("TENDERCURRENCY", TenderCurrency);
                                            Tender.setValue("TENDERCURRENCY_ISO", new String());
                                            Tender.setValue("TENDERID", Left(RsDetallePago.getString("DenominacionPago"), 32));
                                            Tender.setValue("ACCOUNTNUMBER", new String());
                                            Tender.setValue("REFERENCEID", Left(RsDetallePago.getString("ReferenciaPago"), 35));
                                            
                                        }
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            else
                            {
                                System.out.println(RsVentas.getString("TransID") + " - " + "No se encontró detalle de pago.");
                                gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "No se encontró detalle de pago.");
                                MarcarFallidoVentasADM(RsVentas.getString("TransID"));
                                break NextTrans;
                            }
                            
                        }
                        else if (RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV")) 
                        {
                            
                            mEtapa = "Llenando Datos Forma de Pago Generica Devoluciones ADM...";
                            
                            nPosTender++;
                            Tender.appendRow();
                            
                            Tender.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            Tender.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            Tender.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            Tender.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            Tender.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            Tender.setValue("TENDERSEQUENCENUMBER", String.valueOf(nPosTender));
                            
                            String TenderCurrency = gMonedaSAPPredeterminada;
                            
                            String TenderTypeCode = gFormaPagoDevolucionSAP;
                            
                            Tender.setValue("TENDERTYPECODE", TenderTypeCode);
                            Tender.setValue("TENDERAMOUNT", Left(RawDouble(Round(
                            RsVentas.getDouble("n_Total") * Signo * mTasaConversion, mLimiteDec)), 28));
                            Tender.setValue("TENDERCURRENCY", TenderCurrency);
                            Tender.setValue("TENDERCURRENCY_ISO", new String());
                            Tender.setValue("TENDERID", Left(TenderTypeCode, 32));
                            Tender.setValue("ACCOUNTNUMBER", new String());
                            Tender.setValue("REFERENCEID", new String());
                            
                        }
                        
                        // DETALLE DE TRANSACCION - PRODUCTOS
                        
                        RsItems.setFilter(new StringColumnFilter(RsVentas.getString("TransID"), "TransID") );
                        
                        int nPosDcto = 0;
                        int nPosImp = 0;
                        
                        HashMap DocItmList = new HashMap();
                        HashMap DocItmList_Info;
                        
                        Double SignoItem = 1D;
                        
                        mEtapa = "Llenando Datos Detalle Productos Ventas ADM...";
                        
                        if (RsItems.first())
                        {
                            
//                            FieldData = RsItems.getMetaData();
//                            RsItems.beforeFirst();
//                            while (RsItems.next()) {
//                                for (int i = 1; i <= FieldData.getColumnCount(); i++) {
//                                    System.out.println(FieldData.getColumnLabel(i) + 
//                                    " => " + 
//                                    RsItems.getObject(i).toString());
//                                }
//                            }
                            
                            RsItems.beforeFirst();
                            
                            while(RsItems.next())
                            {
                            
                            RetailLineItem.appendRow();
                            
                            RetailLineItem.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            RetailLineItem.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            RetailLineItem.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            RetailLineItem.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            RetailLineItem.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            RetailLineItem.setValue("RETAILSEQUENCENUMBER", RsItems.getString("c_Linea"));
                            
                            // Support for LineItemVoid
                            
                            if(RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN") && RsItems.getDouble("n_Cantidad") > 0)
                            {
                                
                                if (!DocItmList.containsKey(RsItems.getString("c_CodArticulo") + "|" + 
                                String.valueOf(Round(RsItems.getDouble("n_Precio"), 2))))
                                {
                                    
                                    DocItmList_Info = new HashMap();
                                    DocItmList_Info.put("Cod", RsItems.getString("c_CodArticulo"));
                                    DocItmList_Info.put("Current_Ln", RsItems.getString("c_Linea"));
                                    DocItmList_Info.put("Cant_Acum", RsItems.getDouble("n_Cantidad"));
                                    DocItmList_Info.put("Price", RsItems.getDouble("n_Precio"));
                                    HashMap All_Ln_Info = new HashMap();
                                    HashMap Ln_Info = new HashMap();
                                    Ln_Info.put("Ln", DocItmList_Info.get("Current_Ln"));
                                    Ln_Info.put("Remaining_Cant", DocItmList_Info.get("Cant_Acum"));
                                    All_Ln_Info.put(RsItems.getString("c_Linea"), Ln_Info);
                                    HashMap All_R_Info = new HashMap();
                                    DocItmList_Info.put("All_Ln_Info", All_Ln_Info);
                                    DocItmList_Info.put("All_R_Info", All_R_Info);
                                    DocItmList.put(RsItems.getString("c_CodArticulo") + "|" + 
                                    String.valueOf(Round(RsItems.getDouble("n_Precio"), 2)), DocItmList_Info);
                                    
                                } else {
                                    
                                    DocItmList_Info = ((HashMap<String, Object>) DocItmList.get(RsItems.getString("c_CodArticulo") + "|" + 
                                    String.valueOf(Round(RsItems.getDouble("n_Precio"), 2))));
                                    DocItmList_Info.replace("Cant_Acum", ((Double) DocItmList_Info.get("Cant_Acum")) + 
                                    RsItems.getDouble("n_Cantidad"));
                                    HashMap Ln_Info = new HashMap();
                                    Ln_Info.put("Ln", RsItems.getString("c_Linea"));
                                    Ln_Info.put("Remaining_Cant", RsItems.getDouble("n_Cantidad"));
                                    if (((HashMap)(DocItmList_Info.get("All_Ln_Info"))).size() <= 0)
                                        DocItmList_Info.replace("Current_Ln", RsItems.getString("c_Linea"));
                                    ((HashMap)(DocItmList_Info.get("All_Ln_Info"))).put(RsItems.getString("c_Linea"), Ln_Info);
                                    
                                }
                                
                            } else if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN") && RsItems.getDouble("n_Cantidad") < 0)
                            {
                                
                                DocItmList_Info = ((HashMap) DocItmList.get(RsItems.getString("c_CodArticulo") + "|" + 
                                String.valueOf(Round(RsItems.getDouble("n_Precio"), 2))));
                                
                                HashMap All_Ln_Info = ((HashMap)(DocItmList_Info.get("All_Ln_Info")));
                                HashMap All_R_Info = ((HashMap)(DocItmList_Info.get("All_R_Info")));
                                
                                Double Cant_R = Math.abs(RsItems.getDouble("n_Cantidad"));
                                DocItmList_Info.replace("Cant_Acum", Round(((Double) DocItmList_Info.get("Cant_Acum") - Cant_R), 6));
                                
                                while (Cant_R != 0)
                                {
                                    
                                    String Current_Ln = DocItmList_Info.get("Current_Ln").toString();
                                    /*try { assert (All_Ln_Info.containsKey(Current_Ln)); } catch ( AssertionError ae ) 
                                    { 
                                        if(ae != null)
                                            System.out.println(ae); 
                                    };*/
                                    Double Cant_Ln = 0D;
                                    if (All_Ln_Info.containsKey(Current_Ln))
                                        Cant_Ln = Round((Double)((HashMap) All_Ln_Info.get(Current_Ln)).get("Remaining_Cant"), 6);
                                    
                                    //if (!All_R_Info.containsKey(Current_Ln))
                                    //{
                                        //
                                        
                                        LineItemVoid.appendRow();

                                        LineItemVoid.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                        LineItemVoid.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                        LineItemVoid.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                        LineItemVoid.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                        LineItemVoid.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                        LineItemVoid.setValue("RETAILSEQUENCENUMBER", RsItems.getString("c_Linea"));
                                        
                                        LineItemVoid.setValue("VOIDEDLINE", Current_Ln);
                                        LineItemVoid.setValue("VOIDFLAG", "X");
                                        
                                        //All_R_Info.put(Current_Ln, Current_Ln);
                                        All_R_Info.putIfAbsent(Current_Ln, Current_Ln);
                                    //}
                                    
                                    /*try { assert (Round((Cant_R - Cant_Ln), 6) != 0D); } catch ( AssertionError ae ) 
                                    { 
                                        System.out.println(ae); 
                                    };*/
                                    
                                    if (Cant_R < Cant_Ln && Cant_Ln != 0D)
                                    {
                                        ((HashMap) All_Ln_Info.get(Current_Ln)).replace("Remaining_Cant", Round((Cant_Ln - Cant_R), 6));
                                        Cant_R = 0D;
                                    } else {
                                        
                                        Cant_R = Round((Cant_R - Cant_Ln), 6);
                                        All_Ln_Info.remove(Current_Ln);
                                        if (All_Ln_Info.size() > 0)
                                        {
                                            Current_Ln = ((HashMap) All_Ln_Info.values().toArray()[0]).get("Ln").toString();
                                            DocItmList_Info.replace("Current_Ln", Current_Ln);
                                        } else {
                                            Cant_R = 0D;
                                        }
                                    }
                                    
                                }
                                
                            }
                            
                            if (RsItems.getDouble("n_Cantidad") < 0 &&
                            RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                { RetailLineItem.setValue("RETAILTYPECODE", "2901"); SignoItem = -1D; }
                            else if ((RsItems.getDouble("n_Cantidad") * Signo) > 0 &&
                            RsVentas.getString("c_Concepto").equalsIgnoreCase("DEV"))
                                { RetailLineItem.setValue("RETAILTYPECODE", "2902"); SignoItem = -1D; }
                            else
                                if (new BigDecimal(RsItems.getDouble("n_Impuesto")).compareTo(BigDecimal.ZERO) == 0)
                                    if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                        RetailLineItem.setValue("RETAILTYPECODE", "2201");
                                    else
                                        RetailLineItem.setValue("RETAILTYPECODE", "2202");
                                else
                                    if (RsVentas.getString("c_Concepto").equalsIgnoreCase("VEN"))
                                        RetailLineItem.setValue("RETAILTYPECODE", "2001");
                                    else
                                        RetailLineItem.setValue("RETAILTYPECODE", "2801");
                            
                            RetailLineItem.setValue("RETAILREASONCODE", new String());
                            RetailLineItem.setValue("ITEMIDQUALIFIER", "1");
                            RetailLineItem.setValue("ITEMID", Left(RsItems.getString("c_CodArticulo"), 18));
                            RetailLineItem.setValue("RETAILQUANTITY", Left(RawDouble(Round(
                            RsItems.getDouble("n_Cantidad") * Signo * SignoItem, 8)), 13));
                            RetailLineItem.setValue("SALESUNITOFMEASURE", gUnidadMedidaSAP);
                            RetailLineItem.setValue("SALESUNITOFMEASURE_ISO", new String());
                            RetailLineItem.setValue("SALESAMOUNT", Left(RawDouble(Round(
                            RsItems.getDouble("n_Subtotal") * Signo * SignoItem * mTasaConversion, mLimiteDec)), 28));
                            RetailLineItem.setValue("NORMALSALESAMOUNT", Left(RawDouble(Round(
                            RsItems.getDouble("n_Cantidad") * Math.abs(RsItems.getDouble("n_Precio_Original")) *
                            Signo * SignoItem * mTasaConversion, mLimiteDec)), 28));
                            RetailLineItem.setValue("COST", new String());
                            RetailLineItem.setValue("BATCHID", new String());
                            RetailLineItem.setValue("SERIALNUMBER", new String());
                            RetailLineItem.setValue("PROMOTIONID", new String());
                            RetailLineItem.setValue("ITEMIDENTRYMETHODCODE", new String());
                            RetailLineItem.setValue("ACTUALUNITPRICE", Left(RawDouble(Math.abs(Round(
                            RsItems.getDouble("n_Precio_Original") * mTasaConversion, mLimiteDec))), 28));
                            RetailLineItem.setValue("UNITS", new String());
                            RetailLineItem.setValue("SCANTIME", new String());
                            
                            ResultSet Cat = BuscarCategoriasProductoADM(RsItems.getString("c_CodArticulo"));
                            
                            LineItemExt.appendRow();
                            
                            LineItemExt.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                            LineItemExt.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                            LineItemExt.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                            LineItemExt.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                            LineItemExt.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                            LineItemExt.setValue("RETAILSEQUENCENUMBER", RetailLineItem.getValue("RETAILSEQUENCENUMBER"));
                            LineItemExt.setValue("FIELDGROUP", new String());
                            LineItemExt.setValue("FIELDNAME", "ART_DEPART");
                            if (Cat == null)
                                LineItemExt.setValue("FIELDVALUE", new String());
                            else
                                LineItemExt.setValue("FIELDVALUE", Left(Cat.getString("c_Departamento"), 40));
                            
                            /*if (RsItems.getDouble("Impuesto") != 0)
                            {*/
                                
                                nPosImp++;
                                LineItemTax.appendRow();
                                
                                LineItemTax.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                LineItemTax.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                LineItemTax.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                LineItemTax.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                LineItemTax.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                LineItemTax.setValue("RETAILSEQUENCENUMBER", RsItems.getString("c_Linea"));
                                LineItemTax.setValue("TAXSEQUENCENUMBER", String.valueOf(nPosImp));
                                
                                Double ImpuestoProducto = 0D;
                                
                                if (RsItems.getDouble("n_Subtotal") != 0)
                                    ImpuestoProducto = Round(Math.abs(RsItems.getDouble("n_Impuesto") * 100 / RsItems.getDouble("n_Subtotal")), 2);
                                
                                String TaxTypeCode = null;
                                DecimalFormatSymbols taxFmtSym = new DecimalFormatSymbols();
                                taxFmtSym.setDecimalSeparator('.');
                                taxFmtSym.setGroupingSeparator(',');
                                DecimalFormat taxFmt = new DecimalFormat("00.00", taxFmtSym);
                                
                                if (gTaxListCodes != null)
                                {
                                    if (gTaxListCodes.containsKey(taxFmt.format(ImpuestoProducto)))
                                        TaxTypeCode = gTaxListCodes.get(taxFmt.format(ImpuestoProducto)).toString();
                                } else 
                                {
                                    if (gTaxList1.containsValue(taxFmt.format(ImpuestoProducto)))
                                        TaxTypeCode = "4301";
                                    else if (gTaxList2.containsValue(taxFmt.format(ImpuestoProducto)))
                                        TaxTypeCode = "4301";
                                    else if (gTaxList3.containsValue(taxFmt.format(ImpuestoProducto)))
                                        TaxTypeCode = "4303";
                                }
                                
                                if (TaxTypeCode == null)
                                {
                                    System.out.println(RsVentas.getString("TransID") + " - " + "No se encontró asociacion de Codigo de Impuesto.");
                                    gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "No se encontró asociacion de Codigo de Impuesto.");
                                    // Controlar Error Registro de Ventas.
                                    MarcarFallidoVentasADM(RsVentas.getString("TransID"));
                                    break NextTrans;
                                }
                                
                                LineItemTax.setValue("TAXTYPECODE", TaxTypeCode);
                                LineItemTax.setValue("TAXAMOUNT", Left(RawDouble(Round(
                                RsItems.getDouble("n_Impuesto") * Signo * SignoItem * mTasaConversion, mLimiteDec)), 28));
                                
                            //}
                            
                            Double DescuentoLn = Round(RsItems.getDouble("n_Precio_Original") - RsItems.getDouble("n_Precio"), 8);
                            
                            //if (Math.abs(RsItems.getDouble("Descuento")) > 0)
                            if (DescuentoLn > 0)
                            {
                                
                                nPosDcto++;
                                LineItemDiscount.appendRow();
                                
                                LineItemDiscount.setValue("RETAILSTOREID", Transaction.getValue("RETAILSTOREID"));
                                LineItemDiscount.setValue("BUSINESSDAYDATE", Transaction.getValue("BUSINESSDAYDATE"));
                                LineItemDiscount.setValue("TRANSACTIONTYPECODE", Transaction.getValue("TRANSACTIONTYPECODE"));
                                LineItemDiscount.setValue("WORKSTATIONID", Transaction.getValue("WORKSTATIONID"));
                                LineItemDiscount.setValue("TRANSACTIONSEQUENCENUMBER", Transaction.getValue("TRANSACTIONSEQUENCENUMBER"));
                                LineItemDiscount.setValue("RETAILSEQUENCENUMBER", RawDouble(RsItems.getDouble("c_Linea")));
                                LineItemDiscount.setValue("DISCOUNTSEQUENCENUMBER", String.valueOf(nPosDcto));
                                LineItemDiscount.setValue("DISCOUNTTYPECODE", "3101");
                                LineItemDiscount.setValue("DISCOUNTREASONCODE", new String());
                                LineItemDiscount.setValue("REDUCTIONAMOUNT", Left(RawDouble(Round(Math.abs(
                                DescuentoLn) * mTasaConversion, mLimiteDec)), 28));
                                LineItemDiscount.setValue("STOREFINANCIALLEDGERACCOUNTID", new String());
                                LineItemDiscount.setValue("DISCOUNTID", new String());
                                LineItemDiscount.setValue("DISCOUNTIDQUALIFIER", new String());
                                LineItemDiscount.setValue("BONUSBUYID", new String());
                                LineItemDiscount.setValue("OFFERID", new String());
                                
                            }
                            
                            }
                            
                        }
                        else
                        {
                            System.out.println(RsVentas.getString("TransID") + " - " + "No se encontraron productos.");
                            gLogger.EscribirLog(RsVentas.getString("TransID") + " - " + "No se encontraron productos.");
                            // Controlar Error Registro de Ventas.
                            MarcarFallidoVentasADM(RsVentas.getString("TransID"));
                            break NextTrans;
                        }
                        
                        // TODOS LOS DATOS ESTAN ESTABLECIDOS.
                        // EJECUTAR LA FUNCION REMOTA.
                        
                        Boolean SendError = false;
                        
                        try{
                            gCnSAP.fTransVentas.execute(gCnSAP.ABAP_RFC);
                        } catch (JCoException ex){
                            System.out.println("Error al ejecutar la función de envío de transacciones administrativas.");
                            System.out.println(json.toJson(new Messages('E', ex.getMessage())));
                            gLogger.EscribirLog(ex, "Error al ejecutar la función de envío de transacciones administrativas.");
                            SendError = true;
                            //System.exit(0);
                        }

                        Return = gCnSAP.fTransVentas.getTableParameterList().getTable("RETURN");
                        
                        Boolean TransaccionEnviada = false;
                        Boolean TransaccionProcesada = false;
                        
                        int ReturnRowCount = Return.getNumRows();
                        
                        if (!SendError)
                            if (ReturnRowCount <= 0)
                            {
                                // Envío exitoso sin ningún mensaje en particular.
                                TransaccionEnviada = true;
                            }
                            else
                            {
                                
                                TransaccionEnviada = true;
                                
                                // Revisar Mensajes.
                                
                                String RetFullLog = "";
                                String RetLog = "";
                                
                                for (int i = 0; i < ReturnRowCount; i++) 
                                {
                                    
                                    Return.setRow(i);
                                    
                                    String RetLnLog = "";
                                    
                                    if (gDebugMode)
                                    {
                                        RetLnLog = GetLines() +
                                        "TYPE => " + Return.getString("TYPE") + GetTab() +
                                        "ID => " + Return.getString("ID") + GetTab() +
                                        "NUMBER => " + Return.getString("NUMBER") + GetTab() +
                                        "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
                                        "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
                                        "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
                                        "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
                                        "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
                                        "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
                                        "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
                                        "ROW => " + Return.getString("ROW") + GetTab() +
                                        "FIELD => " + Return.getString("FIELD") + GetTab() +
                                        "SYSTEM => " + Return.getString("SYSTEM");
                                        RetFullLog += RetLnLog;
                                    }
                                    
                                    if (Return.getString("TYPE").equalsIgnoreCase("E") ||
                                    Return.getString("TYPE").equalsIgnoreCase("A")) {
                                        
                                        TransaccionEnviada = false;
                                        
                                        if (!gDebugMode)
                                            RetLnLog = GetLines() +
                                            "TYPE => " + Return.getString("TYPE") + GetTab() +
                                            "ID => " + Return.getString("ID") + GetTab() +
                                            "NUMBER => " + Return.getString("NUMBER") + GetTab() +
                                            "LOG_NO => " + Return.getString("LOG_NO") + GetTab() +
                                            "LOG_MSG_NO => " + Return.getString("LOG_MSG_NO") + GetTab() +
                                            "MESSAGE_V1 => " + Return.getString("MESSAGE_V1") + GetTab() +
                                            "MESSAGE_V2 => " + Return.getString("MESSAGE_V2") + GetTab() +
                                            "MESSAGE_V3 => " + Return.getString("MESSAGE_V3") + GetTab() +
                                            "MESSAGE_V4 => " + Return.getString("MESSAGE_V4") + GetTab() +
                                            "PARAMETER => " + Return.getString("PARAMETER") + GetTab() +
                                            "ROW => " + Return.getString("ROW") + GetTab() +
                                            "FIELD => " + Return.getString("FIELD") + GetTab() +
                                            "SYSTEM => " + Return.getString("SYSTEM");
                                        
                                        RetLog += RetLnLog;
                                        
                                    }
                                    
                                }
                                
                            }
                        
                        if (TransaccionEnviada) 
                        {
                            
                            int RetRows = 0;
                            
                            RetRows = gConexion.createStatement().executeUpdate(
                            "UPDATE [VAD10].[DBO].[MA_VENTAS] SET cs_Sync_SxS = '" + gCorrelativo + "' " +
                            "WHERE (c_CodLocalidad + c_Concepto + c_Documento) = '" + RsVentas.getString("TransID") + "'");
                            
                            if (RetRows > 0) TransaccionProcesada = true;
                            
                            if (!TransaccionProcesada) {
                                System.out.println("Transacción [" + RsVentas.getString("TransID") + "] no pudo ser marcada como procesada, pero si fue enviada.");
                                gLogger.EscribirLog("Transacción [" + RsVentas.getString("TransID") + "] no pudo ser marcada como procesada, pero si fue enviada.");
                            }
                            
                        }
                        
                    // Próxima Transacción.
                     
                    }
                    
                    }
                    
                }
                
                System.out.println("Lote de Ventas Administrativas Finalizado con Exito.");
                
                }
                
            }
            else
            {
                System.out.println("No se encontraron mas transacciones de ventas administrativas pendientes por sincronizar.");
            }
            
        } catch(Exception Ex) { // ConstruirRegistrosDeVentas:Exception - ConstruirRegistrosDeVentas:Error
            System.out.println(Ex.getMessage());
            System.out.println(mEtapa);
            gLogger.EscribirLog(Ex, "Error al procesar registros de Ventas o Devoluciones Administrativas. Etapa: " + mEtapa);
            if (!mUltDoc.isEmpty())
                gLogger.EscribirLog("Documento en proceso => " + mUltDoc + GetTab() + 
                "Fecha => " + mUltFec + GetTab() +
                "Total => " + String.valueOf(mUltTot));
        }
        
    }
    
    public void ConstruirRegistrosDeVentasADM()
    {
        ConstruirRegistrosDeVentasADM(false);
    }
    
    public void testConnection(){
        SapConnection JCo = new SapConnection(this.odbc); 
        JCo.verificarConexion();
        JCo.unregister();             
    }
    
    public void downloadIDOCStoStellar(String desti, String date, String plu, String ean, String wgr){
        SapConnection JCo = new SapConnection(this.odbc);
        JCo.rfcIDOCS(desti, date, plu, ean, wgr);
        JCo.unregister();
    }    
    
    public Boolean InitializeSAPConnection(String pConexionDestino)
    {
        gCnSAP = new SapConnection(pConexionDestino);
        if (gCnSAP.pingResult || (gTestMode && gModalidad.equalsIgnoreCase("1"))) {
            return true;
        }
        else
        {
            gLogger.EscribirLog("No se pudo establecer conexión con SAP. Verifique la conexión y los parámetros de entrada");
            return false;
        }
    }
    
    public void TestStellarConnection(String Server){
        
        try {
            
            dbConnection mDBCn = new dbConnection(Server, "49333", "VAD10", "SA", "", "SQLSRV");
            Connection mCn = mDBCn.getConnection();
            
            mCn.setAutoCommit(false);
            
            Statement mCmd = mCn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet mRs = mCmd.executeQuery("SELECT * FROM MA_PRODUCTOS\n" +
            "WHERE c_Codigo = '012563'");
            
            mRs.next();
            
            Boolean Test1= mDBCn.ExisteCampoTabla(mRs, "c_Descri");
            
            if(Test1) {
                
                mRs.updateNString("c_Descri", "4545454554%%");
                //System.out.println(mRs.rowUpdated());
                mRs.updateRow();
                //System.out.println(mRs.rowUpdated());
                
            }
            
            mCn.commit();
            mRs.close();
            //mCn.close();
            
            mRs = mCmd.executeQuery("SELECT * FROM MA_PRODUCTOS\n" +
            "WHERE c_Codigo = '012563'");
            
            mRs.first();
            
            System.out.println(mRs.getNString("c_Codigo") + "\n" + mRs.getNString("c_Descri"));
            
        } catch (SQLException ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    
    
    /**
     * @param args the command line arguments
     */    
    public static void main(String[] args) {
        // TODO code application logic here
        StellarSAP_JCo SAPco = new StellarSAP_JCo(args);
        System.exit(0);
    }
    
    public static HashMap ConvertirCadenadeAsociacion(String pCadena, String pSeparador){
        try {
            
            HashMap tmp = new HashMap();
            String[] ParClaveValor = null;
            
            String[] split1 = pCadena.split(pSeparador);
            
            for (Object Item : split1) {
                ParClaveValor = Item.toString().split(":", 2);
                if (!tmp.containsKey(ParClaveValor[0])) tmp.put(ParClaveValor[0], ParClaveValor[1]);
            }
            
            if (tmp.isEmpty()) throw new Exception("NoData");
            
            return tmp;
            
        } catch (Exception e) {
            return null;
        }
        
    }
    
    public static HashMap ConvertirCadenadeAsociacion(String pCadena){
        return ConvertirCadenadeAsociacion(pCadena, "\\|");
    }
    
    public static double Val(String str) {
        StringBuilder validStr = new StringBuilder();
        boolean seenDot = false;   // when this is true, dots are not allowed
        boolean seenDigit = false; // when this is true, signs are not allowed
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == '.' && !seenDot) {
                seenDot = true;
                validStr.append(c);
            } else if ((c == '-' || c == '+') && !seenDigit) {
                validStr.append(c);
            } else if (Character.isDigit(c)) {
                seenDigit = true;
                validStr.append(c);
            } else if (Character.isWhitespace(c)) {
                // just skip over whitespace
            } else {
                // invalid character
                break;
            }
        }
        return Double.parseDouble(validStr.toString());
    }
    
    private static String getComputerName()
    {
        Map<String, String> env = System.getenv();
        if (env.containsKey("COMPUTERNAME"))
            return env.get("COMPUTERNAME");
        else if (env.containsKey("HOSTNAME"))
            return env.get("HOSTNAME");
        else
            return "";
    }
    
    public static Boolean EstablecerConexion(List<Connection> pCn, String pServidor, String pPort, String pBD, 
    long pCnnTOut, List<String> pUser, List<String> pPassword, Boolean AuthReset, 
    List<String> NewUser, List<String> NewPassword)
    {
        
        Retry:
        
        while(true) { 
            
            try {
                
                String mCadena;
                String mServidor;
                
                dbConnection dbc = new dbConnection(pServidor, pPort, pBD, pUser.get(0).toString(), pPassword.get(0).toString(), "SQLSRV");
                
                pCn.set(0, dbc.getConnection());
                
                return true;
                
            } catch (Exception Ex) {

                if (AuthReset) {

                    if (Ex instanceof SQLException)
                    if (((SQLException)Ex).getErrorCode() == 18456) {

                        String tmp; Boolean seguir;
                        tmp = JOptionPane.showInputDialog("Ingrese nuevamente el usuario para la conexión al servidor SQL");
                        seguir = !tmp.isEmpty();

                        if (seguir){
                            pUser.set(0, tmp);
                            tmp = JOptionPane.showInputDialog("Ingrese nuevamente la contraseña para la conexión al servidor SQL");
                            pPassword.set(0, tmp);
                        }

                        if (seguir){
                            try {
                                NewUser.set(0, enc.Encode(pUser.get(0).toString()));
                            } catch (Exception ex2) {
                                Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex2);
                            }
                            try {
                                NewPassword.set(0, enc.Encode(pPassword.get(0).toString()));
                            } catch (Exception ex3) {
                                Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex3);
                            }

                            continue Retry;
                        }

                    }

                }
                else {

                    gLogger.EscribirLog(Ex, "");

                }
            }
            
            return false;
            
        }
        
    }
    
    public static String Left(String pString, int pLen){
        if (pString == null) return new String();
        if (pLen <= 0) pLen = 1;
        if (pString.length() >= pLen)
            return pString.substring(0, (pLen));
        else
            return pString;
    }
    
    public static String Left(Object WrappedString, int pLen){
        return Left(WrappedString.toString(), pLen);
    }
    
    public String CtrlC(String pText) { Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(pText), new StringSelection(pText)); return pText; }

    public String GetLines(long HowMany)
    {
        
        long HowManyTabs; String GetLines = new String();
        
        HowManyTabs = HowMany;
        
        for (int i = 1; i <= HowManyTabs; i++){
            GetLines += "\n";
        }
        
        return GetLines;
        
    }
    
    public String GetLines() { return GetLines(1); }
    
    public String GetTab(long HowMany)
    {
        
        long HowManyTabs; String GetTab = new String();
        
        HowManyTabs = HowMany;
        
        for (int i = 1; i <= HowManyTabs; i++){
            GetTab +=  "\t";
        }
        
        return GetTab;
        
    }
    
    public String GetTab() { return GetTab(1); }
    
    public Boolean isDBNull(Object pValue) { return (pValue == null); }

    public Object isDBNull(Object pValue, Object pDefaultValueReturned) {
        if (isDBNull(pValue))
            return pDefaultValueReturned;
        else
            return pValue;
    }
    
    public static Double Round(Double Value, int Places) 
    {
        if (Places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(Value));
        bd = bd.setScale(Places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
    
    public static String RawDouble(Double Value)
    {
        DecimalFormat Fmt = new DecimalFormat("0.0##################", 
        DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        Fmt.setMaximumFractionDigits(340);
        String toReturn = Fmt.format(Value);
        return toReturn;
    }
    
    public static java.util.Date DateSerial(int pYear, int pMonth, int pDay) {
        
        Calendar aDate = Calendar.getInstance();
        aDate.clear();
        aDate.set(pYear, pMonth, pDay);
        
        return aDate.getTime();
        
    }
    
    public static java.util.Date TimeSerial(int pHour, int pMinute, int pSecond) {
        
        Calendar aTime = Calendar.getInstance();
        aTime.clear();
        aTime.set(0, 0, 0, pHour, pMinute, pSecond);
        
        return aTime.getTime();
        
    }
    
    public static java.util.Date DateAddInterval(java.util.Date pDate, int pCalendarFieldType, int pAmount) {
        
        Calendar aDate = Calendar.getInstance();
        aDate.setTime(pDate);
        aDate.add(pCalendarFieldType, pAmount);
        
        return aDate.getTime();
        
    }
    
    public static java.util.Date DateTimeFrom(java.util.Date date, java.util.Date time) {

        Calendar aDate = Calendar.getInstance();
        aDate.setTime(date);

        Calendar aTime = Calendar.getInstance();
        aTime.setTime(time);

        Calendar aDateTime = Calendar.getInstance();
        aDateTime.set(Calendar.DAY_OF_MONTH, aDate.get(Calendar.DAY_OF_MONTH));
        aDateTime.set(Calendar.MONTH, aDate.get(Calendar.MONTH));
        aDateTime.set(Calendar.YEAR, aDate.get(Calendar.YEAR));
        aDateTime.set(Calendar.HOUR, aTime.get(Calendar.HOUR));
        aDateTime.set(Calendar.MINUTE, aTime.get(Calendar.MINUTE));
        aDateTime.set(Calendar.SECOND, aTime.get(Calendar.SECOND));

        return aDateTime.getTime();

    }   
    
    public static Calendar CalendarFrom(Date date){ 
      Calendar cal = Calendar.getInstance();
      cal.setTime(date);
      return cal;
    }
    
    public static Boolean EstablecerConexion(Connection pCn, String pServidor, String pPort, String pBD, 
    long pCnnTOut, String pUser, String pPassword)
    {
        //return true; 
        return EstablecerConexion(Arrays.asList(new Connection[] { pCn }), pServidor, pPort, pBD, pCnnTOut, 
        Arrays.asList(new String[] { pUser }), Arrays.asList(new String[] { pPassword }), false, null, null);
    }
    
    public static Object ViewRs(FilteredRowSet Rs){
        FileWriter xs;
        try {
            if (Rs.first()) Rs.beforeFirst();
            String sf = "C:\\Tmp\\rs1.xml";
            File f = new File(sf);
            xs = new FileWriter(f);
            Rs.writeXml(xs);
        } catch (SQLException ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(StellarSAP_JCo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    public String eliminarCeros(String variable) 
    {

        if (variable != null && !variable.isEmpty()) {
            return variable.replaceFirst("^0*", "");
        }

        return "";

    }
        
    public Double BuscarTasaTransaccionPOS(FilteredRowSet pRsVentas, java.util.Date pDocTime, String pTransID)
    {
        
        Double TasaTmp = 1.0;
        
        if (!SincronizarEnMultiMoneda) { return TasaTmp; }
        
        ResultSet Datos;
        String mSQL;
        
        SimpleDateFormat dtFmtShort = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMdd HH:mm:ss.000");
        
        try {
            
            if (SincronizarEnMultiMoneda_POS_Modalidad_Tasa.equals("3"))
            {
                
                mSQL = 
                "SELECT TOP 1 isNULL(HIS.n_FactorPeriodo, -1.0) AS FactorEncontrado \n" +
                "FROM VAD10.DBO.MA_MONEDAS MON LEFT JOIN VAD10.DBO.MA_HISTORICO_MONEDAS HIS \n" +
                "ON MON.c_CodMoneda = HIS.c_CodMoneda \n" +
                "AND HIS.d_FechaCambioActual <= '" + dtFmtLong.format(pDocTime) + "' \n" +
                "AND CAST(HIS.d_FechaCambioActual AS DATE) = '" + dtFmtShort.format(pDocTime) + "' \n" +
                "WHERE MON.c_CodMoneda = '" + SincronizarEnMultiMoneda_CodMoneda + "' \n" +
                "AND CAST(HIS.d_FechaCambioActual AS DATE) = '" + dtFmtShort.format(pDocTime) + "' \n" +
                "ORDER BY HIS.d_FechaCambioActual DESC, HIS.ID DESC \n";

                Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);

                if (Datos.first())
                {
                    TasaTmp = Datos.getDouble("FactorEncontrado");
                }

                if (TasaTmp <= 0) TasaTmp = -1.0;

            }
            
            if (SincronizarEnMultiMoneda_POS_Modalidad_Tasa.equals("2"))
            {
                
                mSQL = 
                "SELECT TOP 1 isNULL(HIS.n_FactorPeriodo, MON.n_Factor) AS FactorEncontrado \n" +
                "FROM VAD10.DBO.MA_MONEDAS MON LEFT JOIN VAD10.DBO.MA_HISTORICO_MONEDAS HIS \n" +
                "ON MON.c_CodMoneda = HIS.c_CodMoneda \n" +
                "AND HIS.d_FechaCambioActual <= '" + dtFmtLong.format(pDocTime) + "' \n" +
                "WHERE MON.c_CodMoneda = '" + SincronizarEnMultiMoneda_CodMoneda + "' \n" +
                "ORDER BY HIS.d_FechaCambioActual DESC, HIS.ID DESC \n";

                Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);

                if (Datos.first())
                {
                    TasaTmp = Datos.getDouble("FactorEncontrado");
                }

                if (TasaTmp <= 0) TasaTmp = -1.0;

            }
            
            if (SincronizarEnMultiMoneda_POS_Modalidad_Tasa.equals("1")
            || (SincronizarEnMultiMoneda_POS_Modalidad_Tasa.equals("3") && TasaTmp <= 0))
            {
                
                if (ExisteCampoTabla(pRsVentas, "n_FactorMonedaAdicional"))
                {
                    TasaTmp = pRsVentas.getDouble("n_FactorMonedaAdicional");
                } else { 
                    TasaTmp = -1.0; 
                }
                
                if (TasaTmp <= 0)
                {
                    
                    mSQL = 
                    "SELECT TOP 1 isNULL(HIS.n_Factor, MON.n_Factor) AS FactorEncontrado \n" +
                    "FROM VAD10.DBO.MA_MONEDAS MON LEFT JOIN VAD20.DBO.MA_DETALLEPAGO HIS \n" +
                    "ON MON.c_CodMoneda = HIS.c_CodMoneda \n" +
                    "AND (CAST(CAST(HIS.d_Fecha AS DATE) AS DATETIME) + CAST(CAST(HIS.f_Hora AS TIME) AS DATETIME)) <= '" + dtFmtLong.format(pDocTime) + "' \n" +
                    "WHERE MON.c_CodMoneda = '" + SincronizarEnMultiMoneda_CodMoneda + "' \n" +
                    "ORDER BY (CAST(CAST(HIS.d_Fecha AS DATE) AS DATETIME) + CAST(CAST(HIS.f_Hora AS TIME) AS DATETIME)) DESC, HIS.ID DESC \n";

                    Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);
                    
                    if (Datos.first())
                    {
                        TasaTmp = Datos.getDouble("FactorEncontrado");
                    }
                    
                    if (TasaTmp <= 0) TasaTmp = -1.0;
                    
                }
                
            }
            
            return TasaTmp;
            
        } catch (SQLException ex) {
            if (gDebugMode) System.out.println("Error Buscando Tasa de conversión a moneda alterna " + pTransID);
            gLogger.EscribirLog(ex, "Error Buscando Tasa de conversión a moneda alterna " + pTransID);
            return -1.0;
        } catch (Exception Any) 
        {
            if (gDebugMode) System.out.println("Error Buscando Tasa de conversión a moneda alterna " + pTransID);
            gLogger.EscribirLog(Any, "Error Buscando Tasa de conversión a moneda alterna " + pTransID);
            return -1.0;
        }
        
    }
    
    public Double BuscarTasaTransaccionADM(FilteredRowSet pRsVentas, java.util.Date pDocTime, String pTransID)
    {
        
        Double TasaTmp = 1.0;
        
        if (!SincronizarEnMultiMoneda) { return TasaTmp; }
        
        ResultSet Datos;
        String mSQL;
        
        SimpleDateFormat dtFmtShort = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dtFmtLong = new SimpleDateFormat("yyyyMMdd HH:mm:ss.000");
        
        try {
            
            if (SincronizarEnMultiMoneda_ADM_Modalidad_Tasa.equals("3"))
            {
                
                mSQL = 
                "SELECT TOP 1 isNULL(HIS.n_FactorPeriodo, -1.0) AS FactorEncontrado \n" +
                "FROM VAD10.DBO.MA_MONEDAS MON LEFT JOIN VAD10.DBO.MA_HISTORICO_MONEDAS HIS \n" +
                "ON MON.c_CodMoneda = HIS.c_CodMoneda \n" +
                "AND HIS.d_FechaCambioActual <= '" + dtFmtLong.format(pDocTime) + "' \n" +
                "AND CAST(HIS.d_FechaCambioActual AS DATE) = '" + dtFmtShort.format(pDocTime) + "' \n" +
                "WHERE MON.c_CodMoneda = '" + SincronizarEnMultiMoneda_CodMoneda + "' \n" +
                "AND CAST(HIS.d_FechaCambioActual AS DATE) = '" + dtFmtShort.format(pDocTime) + "' \n" +
                "ORDER BY HIS.d_FechaCambioActual DESC, HIS.ID DESC \n";
                
                Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);

                if (Datos.first())
                {
                    TasaTmp = Datos.getDouble("FactorEncontrado");
                }

                if (TasaTmp <= 0) TasaTmp = -1.0;

            }
            
            if (SincronizarEnMultiMoneda_ADM_Modalidad_Tasa.equals("1"))
            {
                
                mSQL = 
                "SELECT TOP 1 isNULL(HIS.n_FactorPeriodo, MON.n_Factor) AS FactorEncontrado \n" +
                "FROM VAD10.DBO.MA_MONEDAS MON LEFT JOIN VAD10.DBO.MA_HISTORICO_MONEDAS HIS \n" +
                "ON MON.c_CodMoneda = HIS.c_CodMoneda \n" +
                "AND HIS.d_FechaCambioActual <= '" + dtFmtLong.format(pDocTime) + "' \n" +
                "WHERE MON.c_CodMoneda = '" + SincronizarEnMultiMoneda_CodMoneda + "' \n" +
                "ORDER BY HIS.d_FechaCambioActual DESC, HIS.ID DESC \n";

                Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);

                if (Datos.first())
                {
                    TasaTmp = Datos.getDouble("FactorEncontrado");
                }

                if (TasaTmp <= 0) TasaTmp = -1.0;

            }
            
            if (SincronizarEnMultiMoneda_ADM_Modalidad_Tasa.equals("2")
            || (SincronizarEnMultiMoneda_ADM_Modalidad_Tasa.equals("3") && TasaTmp <= 0))
            {
                
                if (ExisteCampoTabla(pRsVentas, "n_FactorMonedaAdicional"))
                {
                    TasaTmp = pRsVentas.getDouble("n_FactorMonedaAdicional");
                } else { 
                    TasaTmp = -1.0; 
                }
                
                if (TasaTmp <= 0)
                {
                    
                    mSQL = 
                    "SELECT TOP 1 isNULL(HIS.n_Factor, MON.n_Factor) AS FactorEncontrado \n" +
                    "FROM VAD10.DBO.MA_MONEDAS MON LEFT JOIN VAD20.DBO.MA_DETALLEPAGO HIS \n" +
                    "ON MON.c_CodMoneda = HIS.c_CodMoneda \n" +
                    "AND (CAST(CAST(HIS.d_Fecha AS DATE) AS DATETIME) + CAST(CAST(HIS.f_Hora AS TIME) AS DATETIME)) <= '" + dtFmtLong.format(pDocTime) + "' \n" +
                    "WHERE MON.c_CodMoneda = '" + SincronizarEnMultiMoneda_CodMoneda + "' \n" +
                    "ORDER BY (CAST(CAST(HIS.d_Fecha AS DATE) AS DATETIME) + CAST(CAST(HIS.f_Hora AS TIME) AS DATETIME)) DESC, HIS.ID DESC \n";

                    Datos = gConexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY).executeQuery(mSQL);
                    
                    if (Datos.first())
                    {
                        TasaTmp = Datos.getDouble("FactorEncontrado");
                    }
                    
                    if (TasaTmp <= 0) TasaTmp = -1.0;
                    
                }
                
            }
            
            return TasaTmp;
            
        } catch (SQLException ex) {
            if (gDebugMode) System.out.println("Error Buscando Tasa de conversión a moneda alterna " + pTransID);
            gLogger.EscribirLog(ex, "Error Buscando Tasa de conversión a moneda alterna " + pTransID);
            return -1.0;
        } catch (Exception Any) 
        {
            if (gDebugMode) System.out.println("Error Buscando Tasa de conversión a moneda alterna " + pTransID);
            gLogger.EscribirLog(Any, "Error Buscando Tasa de conversión a moneda alterna " + pTransID);
            return -1.0;
        }
        
    }
    
}
