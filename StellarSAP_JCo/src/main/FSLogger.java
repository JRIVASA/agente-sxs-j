/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

/**
 *
 * @author programacion
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FSLogger
{
    private String fileName;

    // si pasamos la ruta de un archivo, su utilizará ese para hacer el log
    public FSLogger(String file)
    {
        fileName = file;
    }

    // caso contrario se utiliza uno por defecto
    public FSLogger()
    {
        fileName = "C:/log.txt";
    }

    public void EscribirLog(String logText)
    {
        
        File file = new File(fileName);
        FileWriter fr = null;
        BufferedWriter br = null;
        PrintWriter pr = null;
        
        try
        {
            // to append to file, you need to initialize FileWriter using below constructor
            fr = new FileWriter(file, true);
            br = new BufferedWriter(fr);
            pr = new PrintWriter(br);
            pr.println(java.time.LocalDateTime.now().toString() + " - " + logText);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                pr.close();
                br.close();
                fr.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        
    }

    public void EscribirLog(Exception ex, String PreMsg)
    {
        
        File file = new File(fileName);
        FileWriter fr = null;
        BufferedWriter br = null;
        PrintWriter pr = null;

        try 
        {
            
            // to append to file, you need to initialize FileWriter using below constructor
            fr = new FileWriter(file, true);
            br = new BufferedWriter(fr);
            pr = new PrintWriter(br);

            pr.println("--------------------------------------------------------------------------------");

            if (!(PreMsg == null || PreMsg.isEmpty())) pr.println("Log Message: " + PreMsg);
            
            if (ex != null) 
            {

                pr.println(java.time.LocalDateTime.now().toString() + " - Exception (" + ex.getClass().getTypeName() + ")");
                pr.println("Message: " + ex.getMessage());
                pr.println("StackTrace: " + Arrays.toString(ex.getStackTrace()));

            }
            
        } catch (IOException ex1) {
            Logger.getLogger(FSLogger.class.getName()).log(Level.SEVERE, null, ex1);
            ex1.printStackTrace();
        }
        finally
        {
            try
            {
                pr.close();
                br.close();
                fr.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

    }

}

