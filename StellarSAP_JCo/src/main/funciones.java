//import com.sap.conn.jco.JCoException;
//import com.sap.conn.jco.JCoFunction;
//import com.sap.conn.jco.JCoTable;
//import java.io.PrintStream;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import javax.swing.JTextField;
//
//public class funciones
//{
//  public funciones() {}
//  
//  public void AsignarParametrosABAP()
//  {
//    String str = "";
//    StringBuffer localStringBuffer = new StringBuffer(1000);
//    
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//      str = str + localStringBuffer.append("select sc.* from sap_conexion sc inner join sap_posdm_configuration spc on (sc.id = spc.value::integer) where\tspc.key = 'connection_posdm_active'");
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        int i = localResultSet.getInt("conectype");
//        
//        Globals.JCOclientelang = localResultSet.getString("lang");
//        Globals.JCOclienteclient = localResultSet.getString("client");
//        
//        if (i == 1) {
//          Globals.JCOclientesaprouter = localResultSet.getString("saprouter");
//        }
//        else {
//          Globals.JCOclientesaprouter = "";
//        }
//        Globals.JCOclienteuser = localResultSet.getString("sapuser");
//        Globals.JCOclientepasswd = localResultSet.getString("sappasswd");
//        Globals.JCOclientesysnr = localResultSet.getString("sysnr");
//        Globals.JCOclienteashost = localResultSet.getString("ashost");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {}
//  }
//  
//
//  public void CrearABAPfile()
//  {
//    java.io.FileWriter localFileWriter = null;
//    java.io.BufferedWriter localBufferedWriter = null;
//    try
//    {
//      localFileWriter = new java.io.FileWriter(Globals.ABAP_AS + ".jcoDestination");
//      localBufferedWriter = new java.io.BufferedWriter(localFileWriter);
//      
//      AsignarParametrosABAP();
//      
//      localBufferedWriter.write("jco.client.lang=" + Globals.JCOclientelang + "\n");
//      localBufferedWriter.write("jco.client.client=" + Globals.JCOclienteclient + "\n");
//      
//      if (!Globals.JCOclientesaprouter.equals("")) {
//        localBufferedWriter.write("jco.client.saprouter=" + Globals.JCOclientesaprouter + "\n");
//      }
//      localBufferedWriter.write("jco.client.passwd=" + Globals.JCOclientepasswd + "\n");
//      localBufferedWriter.write("jco.client.user=" + Globals.JCOclienteuser + "\n");
//      localBufferedWriter.write("jco.client.sysnr=" + Globals.JCOclientesysnr + "\n");
//      localBufferedWriter.write("jco.client.ashost=" + Globals.JCOclienteashost);
//      
//      localBufferedWriter.newLine();
//      localBufferedWriter.close();
//    }
//    catch (Exception localException) {}
//  }
//  
//
//
//  public int ValidarConfigConnPanel(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8)
//  {
//    if ((paramInt == -1) || (paramString1.equals("")) || (paramString2.equals("")) || (paramString3.equals("")) || (paramString4.equals("")) || (paramString5.length() == 0) || (paramString6.equals("")) || (paramString7.equals("")) || (paramString8.equals("")))
//    {
//
//
//      return 0;
//    }
//    return 1;
//  }
//  
//  public String GuardarConfigEnvioPanel()
//  {
//    String str1 = "";
//    StringBuffer localStringBuffer1 = new StringBuffer(1000);
//    StringBuffer localStringBuffer2 = new StringBuffer(1000);
//    
//    String str2 = Globals.ConfigFieldTienda.getText().toString();
//    String str3 = Globals.ConfigFieldTransxEnvio.getText().toString();
//    String str4 = Globals.ConfigFieldEnvioxMin.getText().toString();
//    String str5 = Globals.ConfigFieldBarraFija.getText().toString();
//    String str6 = "0";
//    String str7 = "0";
//    String str8 = "000000000000000000000000000000";
//    
//    String str9 = Globals.ConfigFieldTimeOut.getText().toString();
//    int i = 0;
//    
//    String str10 = Globals.ConfigFieldNumberSendAttempts.getText().toString();
//    
//
//    String str11 = Globals.ConfigFieldMaxDaysSend.getText().toString();
//    int j = 0;
//    
//
//    if (str2.equals("")) {
//      return "DEBE INGRESAR EL CODIGO DE LA TIENDA.";
//    }
//    
//
//
//
//
//
//
//    if (str4.equals("")) {
//      return "DEBE INDICAR EL TIEMPO DE ESPERA ENTRE ENVIO.";
//    }
//    
//    if (Integer.parseInt(str4) == 0) {
//      return "DEBE INGRESAR EL TIEMPO DE ESPERA ENTRE ENVIO EXPRESADO EN MINUTOS Y MAYOR QUE CERO.";
//    }
//    
//
//
//
//
//
//
//
//
//
//    if (str3.equals("")) {
//      return "DEBE INDICAR EL NUMERO DE TRANSACCIONES A PROCESAR POR ENVIO.";
//    }
//    
//    if (Integer.parseInt(str3) == 0) {
//      return "DEBE INGRESAR EL NUMERO DE TRANSACCIONES A PROCESAR POR ENVIO EXPRESADO EN UN NUMERO MAYOR QUE CERO.";
//    }
//    
//
//
//
//    if (str9.equals("")) {
//      return "DEBE INGRESAR EL TIMEOUT PARA LAS TRANSACCIONES EXPRESADO EN MINUTOS.";
//    }
//    
//
//    if (Integer.parseInt(str9) == 0) {
//      return "DEBE INGRESAR EL TIMEOUT PARA LAS TRANSACCIONES EXPRESADO EN MINUTOS Y MAYOR QUE CERO.";
//    }
//    i = Integer.parseInt(str9);
//    i = i * 60 * 1000;
//    
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//    if (str10.equals("")) {
//      return "DEBE INDICAR EL NUMERO DE INTENTOS PARA ENVÍO DE TRANSACCIONES";
//    }
//    if (Integer.parseInt(str10) == 0) {
//      return "DEBE INGRESAR EL NUMERO DE INTENTOS PARA ENVÍO DE TRANSACCIONES EXPRESADO EN UN NUMERO MAYOR QUE CERO.";
//    }
//    
//
//
//    if (Globals.ConfigCheckBoxSendPreviousTrans.isSelected()) {
//      if (Globals.ConfigCheckBoxSendAllDays.isSelected()) {
//        str11 = "-1";
//        j = 1;
//      } else {
//        if (str11.equals("")) {
//          return "SI ESTA SELECCIONADO EL ENVÍO DE TRANSACCIONES DE VENTAS ANTERIORES, DEBE SELECCIONAR LA CANTIDAD DE DIAS ANTERIORES A ENVIAR.";
//        }
//        if (Integer.parseInt(str11) == 0) {
//          return "SI ESTA SELECCIONADO EL ENVÍO DE TRANSACCIONES DE VENTAS ANTERIORES, DEBE SELECCIONAR LA CANTIDAD DE DIAS ANTERIORES A ENVIAR, EXPRESADO EN UN NUMERO MAYOR QUE CERO";
//        }
//        j = 1;
//      }
//    }
//    
//
//
//
//    if (Globals.ConfigCheckBoxEnvioActivo.isSelected()) {
//      str6 = "1";
//    }
//    if (Globals.ConfigCheckBoxBarraFija.isSelected()) {
//      str7 = "1";
//      if (str5.equals("")) {
//        return "DEBE INGRESAR EL CODIGO DE BARRA FIJA";
//      }
//    }
//    str8 = str6 + str7 + str8;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      if (ExistConfigEnvio(1) == 0) {
//        str1 = str1 + localStringBuffer1.append("INSERT INTO sap_configuracion(id, tienda, min_x_trans, trans_x_envio, barrafija, configflag) ").toString();
//        
//
//
//        str1 = str1 + localStringBuffer2.append("VALUES(").append(1).append(",'").append(str2).append("',").append(str4).append(",").append(str3).append(",'").append(str5).append("','").append(str8).append("'::BIT(32))").toString();
//
//      }
//      else
//      {
//
//        str1 = str1 + localStringBuffer1.append("UPDATE sap_configuracion SET tienda ='").append(str2).append("', min_x_trans=").append(str4).append(", trans_x_envio=").append(str3).append(", barrafija='").append(str5).append("', configflag='").append(str8).append("' where id =").append(1).toString();
//      }
//      
//
//
//
//
//
//
//
//      localStatement.executeUpdate(str1);
//      
//
//
//      if (ExistConfigPosdm(Globals.Config_TimeOut) == 0) {
//        str1 = "insert into sap_posdm_configuration(key, value, created_at, updated_at) values('" + Globals.Config_TimeOut + "','" + i + "', current_timestamp, current_timestamp); ";
//      } else {
//        str1 = "update sap_posdm_configuration set value = '" + i + "', updated_at = current_timestamp where key = '" + Globals.Config_TimeOut + "'; ";
//      }
//      localStatement.executeUpdate(str1);
//      
//
//
//
//      if (ExistConfigPosdm(Globals.Config_NumeroIntentosEnvios) == 0) {
//        str1 = "insert into sap_posdm_configuration(key, value, created_at, updated_at) values('" + Globals.Config_NumeroIntentosEnvios + "','" + str10 + "', current_timestamp, current_timestamp); ";
//      } else {
//        str1 = "update sap_posdm_configuration set value = '" + str10 + "', updated_at = current_timestamp where key = '" + Globals.Config_NumeroIntentosEnvios + "'; ";
//      }
//      localStatement.executeUpdate(str1);
//      
//
//      if (ExistConfigPosdm(Globals.Config_EnvioTransAnt) == 0) {
//        str1 = "insert into sap_posdm_configuration(key, value, created_at, updated_at) values('" + Globals.Config_EnvioTransAnt + "','" + j + "', current_timestamp, current_timestamp); ";
//      } else {
//        str1 = "update sap_posdm_configuration set value = '" + j + "', updated_at = current_timestamp where key = '" + Globals.Config_EnvioTransAnt + "'; ";
//      }
//      localStatement.executeUpdate(str1);
//      
//
//      if (str11.equals("")) {
//        str11 = "0";
//      }
//      if (ExistConfigPosdm(Globals.Config_MaxDiasEnvio) == 0) {
//        str1 = "insert into sap_posdm_configuration(key, value, created_at, updated_at) values('" + Globals.Config_MaxDiasEnvio + "','" + str11 + "', current_timestamp, current_timestamp); ";
//      } else {
//        str1 = "update sap_posdm_configuration set value = '" + str11 + "', updated_at = current_timestamp where key = '" + Globals.Config_MaxDiasEnvio + "'; ";
//      }
//      localStatement.executeUpdate(str1);
//      
//
//
//      localStatement.execute("END");
//      
//
//      localStatement.close();
//      localConnection.close();
//      
//      ModificarCronTab();
//      
//      return "CONFIGURACION GUARDADA EXITOSAMENTE";
//    }
//    catch (Exception localException)
//    {
//      return "OPERACION FALLIDA: " + localException.getMessage();
//    }
//  }
//  
//
//  public String GuardarNewConexionSap()
//  {
//    String str1 = "";
//    
//    String str2 = "";
//    String str3 = "";
//    int i = 0;
//    String str4 = "";
//    
//
//    StringBuffer localStringBuffer1 = new StringBuffer(1000);
//    StringBuffer localStringBuffer2 = new StringBuffer(1000);
//    StringBuffer localStringBuffer3 = new StringBuffer(1000);
//    
//    String str5 = "CONEXION 1";
//    TypeComboBox localTypeComboBox = (TypeComboBox)Globals.ConfigComboConexion.getSelectedItem();
//    
//    int j = localTypeComboBox.getValue();
//    String str6 = Globals.ConfigFieldLang.getText().toString();
//    String str7 = Globals.ConfigFieldCliente.getText().toString();
//    String str8 = Globals.ConfigFieldRouter.getText().toString();
//    
//    String str9 = new String(Globals.ConfigFieldPass.getPassword());
//    String str10 = Globals.ConfigFieldUser.getText().toString();
//    String str11 = Globals.ConfigFieldSysnr.getText().toString();
//    String str12 = Globals.ConfigFieldHost.getText().toString();
//    
//    if (ValidarConfigConnPanel(j, str5, str6, str7, str8, str9, str10, str11, str12) == 0)
//    {
//      return "DEBE LLENAR TODOS LOS CAMPOS";
//    }
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//
//
//
//
//
//      str4 = "select value as id from sap_posdm_configuration where key = 'connection_posdm_active'";
//      ResultSet localResultSet = localStatement.executeQuery(str4);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("id");
//      }
//      
//
//
//      if (ExistConfigConn(i) == 0)
//      {
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//        str1 = str1 + localStringBuffer1.append("INSERT INTO sap_conexion(conectype,name,lang,client,saprouter,sappasswd,sapuser,sysnr,ashost) ").toString();
//        
//
//
//        str1 = str1 + localStringBuffer2.append("VALUES(").append(j).append(",'").append(str5).append("','").append(str6).append("','").append(str7).toString();
//        
//
//        str1 = str1 + localStringBuffer3.append("','").append(str8).append("','").append(str9).append("','").append(str10).append("','").append(str11).append("','").append(str12).append("')").toString();
//        
//
//
//
//        str2 = "INSERT";
//      } else {
//        str1 = str1 + localStringBuffer1.append("UPDATE sap_conexion SET conectype =").append(j).append(", name='").append(str5).append("', lang='").append(str6).append("', client='").toString();
//        
//
//
//        str1 = str1 + localStringBuffer2.append(str7).append("', saprouter='").append(str8).append("', sappasswd='").append(str9).append("', sapuser='").toString();
//        
//
//
//        str1 = str1 + localStringBuffer3.append(str10).append("', sysnr='").append(str11).append("', ashost='").append(str12).append("' where id = ").append(i).toString();
//        
//
//        str2 = "UPDATE";
//      }
//      
//      localStatement.executeUpdate(str1);
//      localStatement.execute("END");
//      
//      if (str2 == "INSERT") {
//        str1 = "select currval('sap_conexion_id_seq')";
//        localResultSet = localStatement.executeQuery(str1);
//        
//        while (localResultSet.next()) {
//          i = localResultSet.getInt("currval");
//        }
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      CrearABAPfile();
//      str3 = GuardarConexionActiva(i);
//      return "CONFIGURACION GUARDADA EXITOSAMENTE\n" + str3;
//    }
//    catch (Exception localException) {
//      System.out.println(str1);
//      return "OPERACION FALLIDA: " + localException.getMessage();
//    }
//  }
//  
//
//  public int ExistConfigEnvio(int paramInt)
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select count(*) as cant from sap_configuracion where id=1";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("cant");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//  public int ExistConfigConn(int paramInt)
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//      String str = "select count(*) as cant from sap_conexion where id = " + paramInt;
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("cant");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//  public String ShowConfigEnvio(int paramInt)
//  {
//    String str1 = "";
//    StringBuffer localStringBuffer = new StringBuffer(1000);
//    String str2 = "00000000000000000000000000000000";
//    int i = 0;
//    
//    int j = 0;
//    int k = 0;
//    
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      str1 = str1 + localStringBuffer.append("select * from sap_configuracion where id=").append(paramInt).toString();
//      
//      ResultSet localResultSet = localStatement.executeQuery(str1);
//      
//      while (localResultSet.next()) {
//        Globals.ConfigFieldTienda.setText(localResultSet.getString("tienda"));
//        
//        Globals.ConfigFieldTransxEnvio.setText(localResultSet.getString("trans_x_envio"));
//        
//        Globals.ConfigFieldEnvioxMin.setText(localResultSet.getString("min_x_trans"));
//        
//        Globals.ConfigFieldBarraFija.setText(localResultSet.getString("barrafija"));
//        
//        str2 = localResultSet.getString("configflag");
//        if (str2.substring(0, 1).equals("1")) {
//          Globals.ConfigCheckBoxEnvioActivo.setSelected(true);
//        }
//        if (str2.substring(1, 2).equals("1")) {
//          Globals.ConfigCheckBoxBarraFija.setSelected(true);
//          Globals.ConfigFieldBarraFija.setEnabled(true);
//        }
//      }
//      
//
//
//      str1 = "select value from sap_posdm_configuration where key = '" + Globals.Config_TimeOut + "';";
//      localResultSet = localStatement.executeQuery(str1);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("value");
//        
//        i = i / 60 / 1000;
//        
//        Globals.ConfigFieldTimeOut.setText(String.valueOf(i));
//      }
//      
//
//      str1 = "select value from sap_posdm_configuration where key = '" + Globals.Config_NumeroIntentosEnvios + "';";
//      localResultSet = localStatement.executeQuery(str1);
//      
//      while (localResultSet.next()) {
//        j = localResultSet.getInt("value");
//        Globals.ConfigFieldNumberSendAttempts.setText(String.valueOf(j));
//      }
//      
//
//
//
//      str1 = "select value from sap_posdm_configuration where key = '" + Globals.Config_MaxDiasEnvio + "';";
//      localResultSet = localStatement.executeQuery(str1);
//      
//      Globals.ConfigCheckBoxSendPreviousTrans.setSelected(false);
//      Globals.ConfigCheckBoxSendPreviousTrans.setEnabled(true);
//      
//      Globals.ConfigCheckBoxSendAllDays.setSelected(false);
//      Globals.ConfigCheckBoxSendAllDays.setEnabled(false);
//      
//      Globals.ConfigFieldMaxDaysSend.setEnabled(false);
//      Globals.ConfigFieldMaxDaysSend.setText("");
//      
//      while (localResultSet.next()) {
//        k = localResultSet.getInt("value");
//        if (k == -1) {
//          Globals.ConfigCheckBoxSendPreviousTrans.setSelected(true);
//          Globals.ConfigCheckBoxSendAllDays.setSelected(true);
//          Globals.ConfigCheckBoxSendAllDays.setEnabled(true);
//          Globals.ConfigFieldMaxDaysSend.setEnabled(false);
//        }
//        else if (k > 0) {
//          Globals.ConfigCheckBoxSendPreviousTrans.setSelected(true);
//          Globals.ConfigFieldMaxDaysSend.setEnabled(true);
//          Globals.ConfigFieldMaxDaysSend.setText(String.valueOf(k));
//          Globals.ConfigCheckBoxSendAllDays.setEnabled(true);
//        }
//      }
//      
//
//
//      localStatement.close();
//      localConnection.close();
//      
//      return "";
//    } catch (Exception localException) {
//      return "NO SE PUDO CARGAR LA CONFIGURACION DESDE LA BD: " + localException.getMessage();
//    }
//  }
//  
//
//  public String ShowConexionSap(int paramInt)
//  {
//    String str = "";
//    StringBuffer localStringBuffer = new StringBuffer(1000);
//    
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      str = str + localStringBuffer.append("select * from sap_conexion where id=").append(paramInt).toString();
//      
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        Globals.ConfigComboConexion.setSelectedIndex(localResultSet.getInt("conectype") + 1);
//        
//        Globals.ConfigFieldLang.setText(localResultSet.getString("lang"));
//        Globals.ConfigFieldCliente.setText(localResultSet.getString("client"));
//        
//        Globals.ConfigFieldRouter.setText(localResultSet.getString("saprouter"));
//        
//        Globals.ConfigFieldUser.setText(localResultSet.getString("sapuser"));
//        
//
//
//
//
//        Globals.ConfigFieldPass.setText(localResultSet.getString("sappasswd"));
//        
//        Globals.ConfigFieldSysnr.setText(localResultSet.getString("sysnr"));
//        Globals.ConfigFieldHost.setText(localResultSet.getString("ashost"));
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return "";
//    } catch (Exception localException) {
//      return "NO SE PUDO CARGAR LA CONFIGURACION DESDE LA BD: " + localException.getMessage();
//    }
//  }
//  
//
//  public void LimpiarPanelConexionSap()
//  {
//    Globals.ConfigComboConexion.setSelectedIndex(-1);
//    Globals.ConfigFieldLang.setText("");
//    Globals.ConfigFieldCliente.setText("");
//    Globals.ConfigFieldRouter.setText("");
//    Globals.ConfigFieldPass.setText("");
//    Globals.ConfigFieldUser.setText("");
//    Globals.ConfigFieldSysnr.setText("");
//    Globals.ConfigFieldHost.setText("");
//  }
//  
//  public void LimpiarPanelEnvioSap() {
//    Globals.ConfigFieldTienda.setText("");
//    Globals.ConfigFieldTransxEnvio.setText("");
//    Globals.ConfigFieldEnvioxMin.setText("");
//    Globals.ConfigFieldTimeOut.setText("");
//    Globals.ConfigFieldNumberSendAttempts.setText("");
//    Globals.ConfigCheckBoxSendPreviousTrans.setSelected(false);
//    Globals.ConfigCheckBoxSendAllDays.setSelected(false);
//    Globals.ConfigFieldMaxDaysSend.setText("");
//    Globals.ConfigCheckBoxEnvioActivo.setSelected(false);
//    Globals.ConfigCheckBoxBarraFija.setSelected(false);
//    Globals.ConfigFieldBarraFija.setText("");
//  }
//  
//  public String GetTienda() {
//    String str1 = "";
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str2 = "select tienda from sap_configuracion ;";
//      ResultSet localResultSet = localStatement.executeQuery(str2);
//      
//      while (localResultSet.next()) {
//        str1 = localResultSet.getString("tienda");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return str1;
//    }
//    catch (Exception localException) {}
//    return str1;
//  }
//  
//
//  public String GetBarraFija()
//  {
//    String str1 = "";
//    String str2 = "";
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str3 = "select substring(configflag, 2, 1) as flag, barrafija from sap_configuracion where id = 1 ;";
//      ResultSet localResultSet = localStatement.executeQuery(str3);
//      
//      while (localResultSet.next()) {
//        str1 = localResultSet.getString("flag");
//        str2 = localResultSet.getString("barrafija");
//        
//        if ((str1.equals("1")) && (!str2.equals(""))) {
//          return str2;
//        }
//      }
//      localStatement.close();
//      localConnection.close();
//      
//      return "";
//    }
//    catch (Exception localException) {}
//    return "";
//  }
//  
//
//  public int EnvioActivo()
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select substring(configflag from 1 for 1) as active from sap_configuracion where id=1";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("active");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//  public int ObtenerStatusTransaccion(String paramString, int paramInt1, int paramInt2)
//  {
//    int i = -1;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//
//
//      String str = "select * from sap_transacciones where bdate = " + paramString + " and termnmbr=" + paramInt1 + " and transnmbr=" + paramInt2;
//      
//
//
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("status");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//  public int ObtenerStatusTotales(String paramString)
//  {
//    int i = -1;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select * from sap_estadisticas_" + paramString;
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("status_enviado");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    return 0;
//  }
//  
//
//
//  public void ObtenerDevolucion(String paramString, int paramInt1, int paramInt2)
//  {
//    int i = Integer.parseInt(paramString);
//    String str2 = "";
//    
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str1 = "select date_origen, (substring(date_origen from 7 for 2)::INTEGER + 2000)::TEXT||'/'|| substring(date_origen from 4 for 2)||'/'|| substring(date_origen from 1 for 2) as dateformat, termnmbr_origen, transnmbr_origen from ejdevolucion_general where termnmbr_destino = " + paramInt1 + " and transnmbr_destino = " + paramInt2 + " and bdate_destino = " + i;
//      
//
//
//
//      ResultSet localResultSet = localStatement.executeQuery(str1);
//      
//      while (localResultSet.next()) {
//        int j = localResultSet.getInt("termnmbr_origen");
//        int k = localResultSet.getInt("transnmbr_origen");
//        str2 = localResultSet.getString("dateformat");
//        
//        String[] arrayOfString = { "bash", "-c", ". /home/tplinux/posdm/sap_devolucion.sh  " + str2 + " " + j + " " + k };
//        try
//        {
//          ProcessBuilder localProcessBuilder = new ProcessBuilder(arrayOfString);
//          Process localProcess = localProcessBuilder.start();
//        }
//        catch (Exception localException2) {}
//      }
//      
//
//
//
//
//
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException1) {}
//  }
//  
//
//
//
//
//
//  public void ModificarCronTab()
//  {
//    int i = 0;
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select min_x_trans from sap_configuracion";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("min_x_trans");
//        
//        if (i > 0)
//        {
//          String[] arrayOfString = { "bash", "-c", ". /home/tplinux/posdm/configcrontab.sh  " + i };
//          System.out.println("entro a correj el shell");
//          try {
//            ProcessBuilder localProcessBuilder = new ProcessBuilder(arrayOfString);
//            Process localProcess = localProcessBuilder.start();
//          }
//          catch (Exception localException2) {
//            System.out.println("ERROR: " + localException2.toString());
//            localException2.printStackTrace();
//          }
//        }
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException1) {}
//  }
//  
//
//
//
//
//  public int CountTransaccionesPendientes(String paramString)
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      Statement localStatement = localConnection.createStatement();
//      
//
//
//
//      String str = "select count(*) as count from sap_transacciones where bdate = " + paramString + " and status=0 ";
//      
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("count");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {}
//    
//
//
//
//    return i;
//  }
//  
//
//
//
//  public int CountTransaccionesProcesadasSinEnviar(String paramString)
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      Statement localStatement = localConnection.createStatement();
//      
//
//
//      String str = "select count(*) as count from sap_transacciones where bdate = " + paramString + " and status not in (1,2,999); ";
//      
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("count");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {}
//    
//
//
//
//    return i;
//  }
//  
//
//
//  public int TotalesEnviados(String paramString)
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select status_enviado from sap_estadisticas_" + paramString;
//      
//      System.out.println(str);
//      
//
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("status_enviado");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException)
//    {
//      System.out.println(localException.getMessage());
//      return i;
//    }
//    
//    return i;
//  }
//  
//
//
//
//
//  public int ExisteFlagTotales(String paramString)
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      Statement localStatement = localConnection.createStatement();
//      
//
//
//
//
//
//      String str = "select count(*) as count from sap_transacciones where bdate = " + paramString + " and status=999 and transnmbr=999 and termnmbr=999 ";
//      
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("count");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {}
//    
//
//
//
//    return i;
//  }
//  
//
//
//
//  public String SetTotalesFlag(String paramString)
//  {
//    if (ExisteFlagTotales(paramString) > 0)
//    {
//      return "Ya se encontro un flag de totales para el businessdate: " + paramString;
//    }
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      Statement localStatement = localConnection.createStatement();
//      
//
//
//
//
//
//
//
//
//
//
//      String str = "INSERT INTO sap_transacciones(datetime, bdate, status, termnmbr, transnmbr) VALUES (extract(epoch from '" + FechaLog("yyyy-MM-dd HH:mm:ss") + "'::timestamp)::integer" + "," + paramString + ",999,999,999)";
//      
//
//
//
//      System.out.println(str);
//      localStatement.executeUpdate(str);
//      
//      localStatement.execute("END");
//      localStatement.close();
//      localConnection.close();
//      
//      return "Flag actualizado exitosamente";
//    }
//    catch (Exception localException)
//    {
//      return localException.getMessage();
//    }
//  }
//  
//
//
//  public int ActualizarStatusTotales(String paramString, int paramInt1, int paramInt2)
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      if (paramInt2 == 1) {
//        i = paramInt2;
//      } else if (paramInt2 < 0) {
//        if (paramInt1 < 0) {
//          i = paramInt1 - paramInt2;
//        } else {
//          i = paramInt2;
//        }
//      }
//      String str = "UPDATE sap_estadisticas_" + paramString + " SET status_enviado=" + i;
//      
//      localStatement.executeUpdate(str);
//      
//      localStatement.execute("END");
//      localStatement.close();
//      localConnection.close();
//      
//      return 0;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//  public int TransaccionIgnorada(String paramString, int paramInt1, int paramInt2)
//  {
//    int i = 0;
//    int j = 0;
//    int k = 0;
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//      String str = "SELECT count(*) as cont FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TRANSACTION') " + "AS (campo1 character varying, campo2 character varying, campo3 character varying) ";
//      
//
//
//
//
//
//
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("cont");
//      }
//      
//      str = "SELECT count(*) as cont FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TRANSACTIONCONTROL') " + "AS (campo1 character varying, campo2 character varying, campo3 character varying, " + "campo4 character varying, campo5 character varying)";
//      
//
//
//
//
//
//
//
//      localResultSet = localStatement.executeQuery(str);
//      while (localResultSet.next()) {
//        j = localResultSet.getInt("cont");
//      }
//      
//      k = i + j;
//      localStatement.close();
//      localConnection.close();
//      
//
//
//
//      return k;
//    }
//    catch (Exception localException) {}
//    return 0;
//  }
//  
//  public String ProbarConexion()
//  {
//    try {
//      Globals.destination = com.sap.conn.jco.JCoDestinationManager.getDestination(Globals.ABAP_AS);
//      
//      Globals.function = Globals.destination.getRepository().getFunction("/POSDW/BAPI_POSTR_CREATE");
//      
//      if (Globals.function == null) {
//        return "/POSDW/BAPI_POSTR_CREATE no encontrado en SAP";
//      }
//      
//      return "PRUEBA EXITOSA";
//    }
//    catch (JCoException localJCoException) {
//      return localJCoException.getMessage().toString();
//    }
//  }
//  
//  public void enviar_totales_automatico() {
//    String str1 = String.valueOf(BusinessDateActual());
//    
//    int i = 1;
//    
//    EjecutarEnvioConTimeOut localEjecutarEnvioConTimeOut = new EjecutarEnvioConTimeOut();
//    
//
//    int j = ValorConfiguracion(Globals.Config_TimeOut);
//    
//
//
//    int m = 0;
//    String str2 = "";
//    
//    LogTrans(str1, "\n\n", "log_aut");
//    
//
//
//
//    LogTrans(str1, "ENVIANDO TRANSACCIONES DE TOTALES PARA EL BUSINESSDATE: " + str1 + " INTENTO " + i, "log_aut");
//    
//
//
//
//    try
//    {
//      int k = ObtenerStatusTotales(str1);
//      
//
//
//      m = ValidarEnvioTrans(str1, 999, 999, k);
//      
//
//      if ((m == -1) || (m == 0))
//      {
//        str2 = "Error consultando el parametro numero de intentos de envio de transaccion. No se puede enviar la transaccion de totales businessdate: " + str1 + " status: " + k;
//        LogTrans(str1, str2, "log_aut");
//      }
//      
//      if (m == 1) {
//        localEjecutarEnvioConTimeOut.runTaskInLessThanGivenMs(j, "OcurrioTimeOut", str1, 999, 999, k, 1, "totales");
//      }
//      
//      if (m == 2)
//      {
//        str2 = "La transaccion llego al maximo numero de intentos de envio. No se puede enviar la transaccion de totales businessdate: " + str1 + " status: " + k;
//        LogTrans(str1, str2, "log_aut");
//      }
//    }
//    catch (Exception localException) {
//      LogTrans(str1, localException.getMessage(), "log_aut");
//      return;
//    }
//  }
//  
//
//
//  public void CallBapi_EnviarTotales(String paramString, int paramInt1, int paramInt2)
//    throws JCoException
//  {
//    String str1 = GetTienda();
//    String str2 = "";
//    String str3 = "";
//    
//    String str4 = Globals.NombreTablaTemp;
//    
//    if (paramInt2 == 2) {
//      LogTrans(paramString, "\n\n", "log_php");
//      LogTrans(paramString, "Enviando TOTALES del dia : " + paramString, "log_php");
//    }
//    if (paramInt2 == 1) {
//      LogTrans(paramString, "\n\n", "log_aut");
//      LogTrans(paramString, "Enviando TOTALES del dia : " + paramString, "log_aut");
//    }
//    if (paramInt2 == 0) {
//      Globals.ta.append("Enviando TOTALES del dia : " + paramString + "\n");
//      LogTrans(paramString, "\n\n", "log_app");
//      LogTrans(paramString, "Enviando TOTALES del dia : " + paramString, "log_app");
//    }
//    
//    if (str1.equals("")) {
//      str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> ERROR: NO SE ENCONTRO LA TIENDA";
//      if (paramInt2 == 2) {
//        LogTrans(paramString, str2, "log_php");
//        System.out.println(str2);
//      }
//      if (paramInt2 == 1) {
//        LogTrans(paramString, str2, "log_aut");
//      }
//      if (paramInt2 == 0) {
//        Globals.ta.append("NO SE ENCONTRO LA TIENDA\n");
//        LogTrans(paramString, str2, "log_app");
//      }
//      
//      ActualizarStatusTotales(paramString, paramInt1, -1);
//      return;
//    }
//    
//
//
//
//
//    StringBuffer localStringBuffer = new StringBuffer();
//    
//
//    String str5 = str1;
//    String str6 = paramString.substring(0, 8);
//    String str7 = "1602";
//    
//    String str8 = "";
//    String str9 = "";
//    String str10 = "";
//    String str11 = "";
//    String str12 = "";
//    String str13 = "";
//    String str14 = "VES";
//    String str15 = "";
//    String str16 = "";
//    String str17 = "";
//    String str18 = "";
//    
//
//
//    String str19 = "";
//    String str20 = "";
//    String str21 = "";
//    
//
//
//
//
//
//
//    int m = 0;
//    int n = 0;
//    
//    int i1 = (int)Math.pow(10.0D, 2.0D);
//    try
//    {
//      com.sap.conn.jco.JCoDestination localJCoDestination = com.sap.conn.jco.JCoDestinationManager.getDestination(Globals.ABAP_AS);
//      JCoFunction localJCoFunction = localJCoDestination.getRepository().getFunction("/POSDW/BAPI_POSTR_CREATE");
//      
//      if (localJCoFunction == null) {
//        if (paramInt2 == 2) {
//          str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> ERROR: no se encontro /POSDW/BAPI_POSTR_CREATE en SAP.";
//          LogTrans(paramString, str2, "log_php");
//          System.out.println(str2);
//          str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> RESUTADO DEL ENVIO: -1";
//          LogTrans(paramString, str2, "log_php");
//          ActualizarStatusTotales(paramString, paramInt1, -1);
//          return;
//        }
//        if (paramInt2 == 1) {
//          str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> ERROR: no se encontro /POSDW/BAPI_POSTR_CREATE en SAP.";
//          LogTrans(paramString, str2, "log_aut");
//          str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> RESUTADO DEL ENVIO: -1";
//          LogTrans(paramString, str2, "log_aut");
//          ActualizarStatusTotales(paramString, paramInt1, -1);
//          return;
//        }
//        if (paramInt2 == 0) {
//          Globals.ta.append("\t no se encontro /POSDW/BAPI_POSTR_CREATE en SAP.");
//          str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> ERROR: no se encontro /POSDW/BAPI_POSTR_CREATE en SAP.";
//          LogTrans(paramString, str2, "log_app");
//          str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> RESUTADO DEL ENVIO: -1";
//          LogTrans(paramString, str2, "log_app");
//          return;
//        }
//      }
//      
//      localJCoFunction.getImportParameterList().setValue("I_COMMIT", "X");
//      
//      JCoTable localJCoTable1 = localJCoFunction.getTableParameterList().getTable("TRANSACTION");
//      JCoTable localJCoTable2 = localJCoFunction.getTableParameterList().getTable("RETAILTOTALS");
//      JCoTable localJCoTable3 = localJCoFunction.getTableParameterList().getTable("ADDITIONALS");
//      JCoTable localJCoTable4 = localJCoFunction.getTableParameterList().getTable("POSTVOIDDETAILS");
//      try
//      {
//        Class.forName("org.postgresql.Driver");
//        Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//        Statement localStatement1 = localConnection.createStatement();
//        Statement localStatement2 = localConnection.createStatement();
//        
//        String str26 = "";
//        String str27 = "";
//        
//
//
//
//        String str28 = "";
//        str26 = "select sap_fecha_reconversion from sap_fecha_reconversion()";
//        ResultSet localResultSet2 = localStatement1.executeQuery(str26);
//        while (localResultSet2.next()) {
//          str28 = localResultSet2.getString("sap_fecha_reconversion");
//        }
//        if (Integer.parseInt(str6) < Integer.parseInt(str28)) {
//          str14 = "VEF";
//        }
//        
//
//        str26 = "SELECT sap_retail_totales('" + paramString + "');";
//        
//        localStatement1.executeQuery(str26);
//        
//
//        localStringBuffer.append("------------------------------------------------------------------------------- \n");
//        
//
//
//
//
//        str26 = "SELECT businessdaydate, \t   workstationid, \t   transactionsequencenumber, \t   min(begindatetimestamp) as begindatetimestamp, \t   max(enddatetimestamp) as enddatetimestamp FROM show_sap_retail_totales('" + paramString + "','ONLINE') " + "GROUP BY businessdaydate, workstationid, transactionsequencenumber " + "ORDER BY businessdaydate, workstationid, transactionsequencenumber ";
//        
//
//
//
//
//
//
//
//
//
//        localResultSet2 = localStatement1.executeQuery(str26);
//        int i;
//        ResultSet localResultSet1; double d; int j; while (localResultSet2.next()) {
//          m = 1;
//          str6 = localResultSet2.getString("businessdaydate");
//          
//          str7 = "1602";
//          i = localResultSet2.getInt("workstationid");
//          
//          str8 = localResultSet2.getString("transactionsequencenumber");
//          
//          str9 = localResultSet2.getString("begindatetimestamp");
//          
//          str10 = localResultSet2.getString("enddatetimestamp");
//          
//
//          localJCoTable1.appendRow();
//          localJCoTable1.setValue("RETAILSTOREID", str5);
//          localJCoTable1.setValue("BUSINESSDAYDATE", str6);
//          localJCoTable1.setValue("TRANSACTIONTYPECODE", str7);
//          
//          localJCoTable1.setValue("WORKSTATIONID", i);
//          localJCoTable1.setValue("TRANSACTIONSEQUENCENUMBER", str8);
//          
//          localJCoTable1.setValue("BEGINDATETIMESTAMP", str9);
//          
//          localJCoTable1.setValue("ENDDATETIMESTAMP", str10);
//          
//          localJCoTable1.setValue("DEPARTMENT", str11);
//          localJCoTable1.setValue("OPERATORQUALIFIER", str12);
//          
//          localJCoTable1.setValue("OPERATORID", str13);
//          localJCoTable1.setValue("TRANSACTIONCURRENCY", str14);
//          
//          localJCoTable1.setValue("TRANSACTIONCURRENCY_ISO", str15);
//          
//          localJCoTable1.setValue("PARTNERQUALIFIER", str16);
//          
//          localJCoTable1.setValue("PARTNERID", str17);
//          
//
//          localStringBuffer.append("\n TRANSACTION  TOTAL POS: " + i + " BUSINESSDAYDATE: " + str6 + "\n");
//          
//          localStringBuffer.append("RETAILSTOREID: " + str5 + "\n");
//          
//          localStringBuffer.append("BUSINESSDAYDATE: " + str6 + "\n");
//          
//          localStringBuffer.append("TRANSACTIONTYPECODE: " + str7 + "\n");
//          
//          localStringBuffer.append("WORKSTATIONID: " + i + "\n");
//          
//          localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str8 + "\n");
//          
//          localStringBuffer.append("BEGINDATETIMESTAMP: " + str9 + "\n");
//          
//          localStringBuffer.append("ENDDATETIMESTAMP: " + str10 + "\n");
//          
//          localStringBuffer.append("DEPARTMENT: " + str11 + "\n");
//          
//          localStringBuffer.append("OPERATORQUALIFIER: " + str12 + "\n");
//          
//          localStringBuffer.append("OPERATORID: " + str13 + "\n");
//          
//          localStringBuffer.append("TRANSACTIONCURRENCY: " + str14 + "\n");
//          
//          localStringBuffer.append("TRANSACTIONCURRENCY_ISO: " + str15 + "\n");
//          
//          localStringBuffer.append("PARTNERQUALIFIER: " + str16 + "\n");
//          
//          localStringBuffer.append("PARTNERID: " + str17 + "\n");
//          
//          str27 = "SELECT retailtypecode, sum(salesamount) AS salesamount, sum(lineitemcount) AS lineitemcount FROM show_sap_retail_totales('" + paramString + "','ONLINE') " + "WHERE businessdaydate='" + str6 + "' AND workstationid=" + i + "GROUP BY retailtypecode " + "ORDER BY retailtypecode";
//          
//
//
//
//
//
//
//          localResultSet1 = localStatement2.executeQuery(str27);
//          
//
//          while (localResultSet1.next()) {
//            str18 = localResultSet1.getString("retailtypecode");
//            
//            d = localResultSet1.getDouble("salesamount");
//            
//            j = localResultSet1.getInt("lineitemcount");
//            
//            d = Math.rint(d / 100.0D * i1) / i1;
//            
//
//            localJCoTable2.appendRow();
//            localJCoTable2.setValue("RETAILSTOREID", str5);
//            localJCoTable2.setValue("BUSINESSDAYDATE", str6);
//            
//            localJCoTable2.setValue("TRANSACTIONTYPECODE", str7);
//            
//            localJCoTable2.setValue("WORKSTATIONID", i);
//            localJCoTable2.setValue("TRANSACTIONSEQUENCENUMBER", str8);
//            
//            localJCoTable2.setValue("RETAILTYPECODE", str18);
//            
//            localJCoTable2.setValue("SALESAMOUNT", d);
//            localJCoTable2.setValue("LINEITEMCOUNT", j);
//            
//
//            localStringBuffer.append("\n RETAILTOTAL POS: " + i + " BUSINESSDAYDATE: " + str6 + "\n");
//            
//            localStringBuffer.append("RETAILSTOREID: " + str5 + "\n");
//            
//            localStringBuffer.append("BUSINESSDAYDATE: " + str6 + "\n");
//            
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str7 + "\n");
//            
//            localStringBuffer.append("WORKSTATIONID: " + i + "\n");
//            
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str8 + "\n");
//            
//            localStringBuffer.append("RETAILTYPECODE: " + str18 + "\n");
//            
//            localStringBuffer.append("SALESAMOUNT: " + d + "\n");
//            
//            localStringBuffer.append("LINEITEMCOUNT: " + j + "\n");
//          }
//        }
//        
//
//
//
//
//
//
//
//
//        str26 = "SELECT businessdaydate, workstationid, transactionsequencenumber, businessdaydatevoided, transactionsequencenumbervoided, begindatetimestampvoided, enddatetimestampvoided FROM show_sap_retail_totales('" + paramString + "','VOIDING');";
//        
//
//
//
//
//
//
//
//        localResultSet2 = localStatement1.executeQuery(str26);
//        while (localResultSet2.next())
//        {
//          n = 1;
//          str6 = localResultSet2.getString("businessdaydate");
//          
//          str7 = "1401";
//          i = localResultSet2.getInt("workstationid");
//          
//          str8 = localResultSet2.getString("transactionsequencenumber");
//          
//          str9 = localResultSet2.getString("begindatetimestampvoided");
//          
//          str10 = localResultSet2.getString("enddatetimestampvoided");
//          
//          str21 = "Z001";
//          String str22 = str5;
//          String str23 = localResultSet2.getString("businessdaydatevoided");
//          
//          int k = i;
//          String str24 = localResultSet2.getString("transactionsequencenumbervoided");
//          
//          String str25 = "";
//          
//          localJCoTable1.appendRow();
//          localJCoTable1.setValue("RETAILSTOREID", str5);
//          localJCoTable1.setValue("BUSINESSDAYDATE", str6);
//          localJCoTable1.setValue("TRANSACTIONTYPECODE", str7);
//          
//          localJCoTable1.setValue("WORKSTATIONID", i);
//          localJCoTable1.setValue("TRANSACTIONSEQUENCENUMBER", str8);
//          
//          localJCoTable1.setValue("BEGINDATETIMESTAMP", str9);
//          
//          localJCoTable1.setValue("ENDDATETIMESTAMP", str10);
//          
//          localJCoTable1.setValue("DEPARTMENT", str11);
//          localJCoTable1.setValue("OPERATORQUALIFIER", str12);
//          
//          localJCoTable1.setValue("OPERATORID", str13);
//          localJCoTable1.setValue("TRANSACTIONCURRENCY", str14);
//          
//          localJCoTable1.setValue("TRANSACTIONCURRENCY_ISO", str15);
//          
//          localJCoTable1.setValue("PARTNERQUALIFIER", str16);
//          
//          localJCoTable1.setValue("PARTNERID", str17);
//          
//
//          localStringBuffer.append("\n (ANULANDO) TRANSACTION  TOTAL POS: " + i + " BUSINESSDAYDATE: " + str6 + "\n");
//          
//          localStringBuffer.append("RETAILSTOREID: " + str5 + "\n");
//          
//          localStringBuffer.append("BUSINESSDAYDATE: " + str6 + "\n");
//          
//          localStringBuffer.append("TRANSACTIONTYPECODE: " + str7 + "\n");
//          
//          localStringBuffer.append("WORKSTATIONID: " + i + "\n");
//          
//          localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str8 + "\n");
//          
//          localStringBuffer.append("BEGINDATETIMESTAMP: " + str9 + "\n");
//          
//          localStringBuffer.append("ENDDATETIMESTAMP: " + str10 + "\n");
//          
//          localStringBuffer.append("DEPARTMENT: " + str11 + "\n");
//          
//          localStringBuffer.append("OPERATORQUALIFIER: " + str12 + "\n");
//          
//          localStringBuffer.append("OPERATORID: " + str13 + "\n");
//          
//          localStringBuffer.append("TRANSACTIONCURRENCY: " + str14 + "\n");
//          
//          localStringBuffer.append("TRANSACTIONCURRENCY_ISO: " + str15 + "\n");
//          
//          localStringBuffer.append("PARTNERQUALIFIER: " + str16 + "\n");
//          
//          localStringBuffer.append("PARTNERID: " + str17 + "\n");
//          
//          localJCoTable3.appendRow();
//          localJCoTable3.setValue("RETAILSTOREID", str5);
//          localJCoTable3.setValue("BUSINESSDAYDATE", str6);
//          localJCoTable3.setValue("TRANSACTIONTYPECODE", str7);
//          
//          localJCoTable3.setValue("WORKSTATIONID", i);
//          localJCoTable3.setValue("TRANSACTIONSEQUENCENUMBER", str8);
//          
//          localJCoTable3.setValue("TRAININGFLAG", str19);
//          localJCoTable3.setValue("KEYEDOFFLINEFLAG", str20);
//          
//          localJCoTable3.setValue("TRANSREASONCODE", str21);
//          
//
//          localStringBuffer.append("\n (ANULANDO) ADDITIONALS TOTAL POS: " + i + " BUSINESSDAYDATE: " + str6 + "\n");
//          
//          localStringBuffer.append("RETAILSTOREID: " + str5 + "\n");
//          
//          localStringBuffer.append("BUSINESSDAYDATE: " + str6 + "\n");
//          
//          localStringBuffer.append("TRANSACTIONTYPECODE: " + str7 + "\n");
//          
//          localStringBuffer.append("WORKSTATIONID: " + i + "\n");
//          
//          localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str8 + "\n");
//          
//          localStringBuffer.append("TRAININGFLAG: " + str19 + "\n");
//          
//          localStringBuffer.append("KEYEDOFFLINEFLAG: " + str20 + "\n");
//          
//          localStringBuffer.append("TRANSREASONCODE: " + str21 + "\n");
//          
//          localJCoTable4.appendRow();
//          localJCoTable4.setValue("RETAILSTOREID", str5);
//          localJCoTable4.setValue("BUSINESSDAYDATE", str6);
//          
//          localJCoTable4.setValue("TRANSACTIONTYPECODE", str7);
//          
//          localJCoTable4.setValue("WORKSTATIONID", i);
//          localJCoTable4.setValue("TRANSACTIONSEQUENCENUMBER", str8);
//          
//          localJCoTable4.setValue("VOIDEDRETAILSTOREID", str22);
//          
//          localJCoTable4.setValue("VOIDEDBUSINESSDAYDATE", str23);
//          
//          localJCoTable4.setValue("VOIDEDWORKSTATIONID", k);
//          
//          localJCoTable4.setValue("VOIDEDTRANSACTIONSEQUENCENUMBE", str24);
//          
//
//          localJCoTable4.setValue("VOIDFLAG", str25);
//          
//
//          localStringBuffer.append("\n (ANULANDO) POSTVOIDDETAILS TOTAL POS: " + i + " BUSINESSDAYDATE: " + str6 + "\n");
//          
//          localStringBuffer.append("RETAILSTOREID: " + str5 + "\n");
//          
//          localStringBuffer.append("BUSINESSDAYDATE: " + str6 + "\n");
//          
//          localStringBuffer.append("TRANSACTIONTYPECODE: " + str7 + "\n");
//          
//          localStringBuffer.append("WORKSTATIONID: " + i + "\n");
//          
//          localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str8 + "\n");
//          
//          localStringBuffer.append("VOIDEDRETAILSTOREID: " + str22 + "\n");
//          
//          localStringBuffer.append("VOIDEDBUSINESSDAYDATE: " + str23 + "\n");
//          
//          localStringBuffer.append("VOIDEDWORKSTATIONID: " + k + "\n");
//          
//          localStringBuffer.append("VOIDEDTRANSACTIONSEQUENCENUMBE: " + str24 + "\n");
//          
//          localStringBuffer.append("VOIDFLAG: " + str25 + "\n");
//        }
//        
//
//
//
//
//
//        if (n == 1)
//        {
//
//
//
//          str26 = "SELECT businessdaydate, workstationid, transactionsequencenumber, min(begindatetimestamp) as begindatetimestamp, max(enddatetimestamp) as enddatetimestamp FROM show_sap_retail_totales('" + paramString + "','OFFLINE') " + "GROUP BY businessdaydate, workstationid, transactionsequencenumber " + "ORDER BY businessdaydate, workstationid, transactionsequencenumber ";
//          
//
//
//
//
//
//
//
//
//
//          localResultSet2 = localStatement1.executeQuery(str26);
//          
//          while (localResultSet2.next()) {
//            m = 1;
//            str6 = localResultSet2.getString("businessdaydate");
//            
//            str7 = "1602";
//            i = localResultSet2.getInt("workstationid");
//            
//            str8 = localResultSet2.getString("transactionsequencenumber");
//            
//            str9 = localResultSet2.getString("begindatetimestamp");
//            
//            str10 = localResultSet2.getString("enddatetimestamp");
//            
//
//            localJCoTable1.appendRow();
//            localJCoTable1.setValue("RETAILSTOREID", str5);
//            localJCoTable1.setValue("BUSINESSDAYDATE", str6);
//            
//            localJCoTable1.setValue("TRANSACTIONTYPECODE", str7);
//            
//            localJCoTable1.setValue("WORKSTATIONID", i);
//            localJCoTable1.setValue("TRANSACTIONSEQUENCENUMBER", str8);
//            
//            localJCoTable1.setValue("BEGINDATETIMESTAMP", str9);
//            
//            localJCoTable1.setValue("ENDDATETIMESTAMP", str10);
//            
//            localJCoTable1.setValue("DEPARTMENT", str11);
//            localJCoTable1.setValue("OPERATORQUALIFIER", str12);
//            
//            localJCoTable1.setValue("OPERATORID", str13);
//            localJCoTable1.setValue("TRANSACTIONCURRENCY", str14);
//            
//            localJCoTable1.setValue("TRANSACTIONCURRENCY_ISO", str15);
//            
//            localJCoTable1.setValue("PARTNERQUALIFIER", str16);
//            
//            localJCoTable1.setValue("PARTNERID", str17);
//            
//
//            localStringBuffer.append("\n (OFFLINE) TRANSACTION TOTAL POS: " + i + " BUSINESSDAYDATE: " + str6 + "\n");
//            
//            localStringBuffer.append("RETAILSTOREID: " + str5 + "\n");
//            
//            localStringBuffer.append("BUSINESSDAYDATE: " + str6 + "\n");
//            
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str7 + "\n");
//            
//            localStringBuffer.append("WORKSTATIONID: " + i + "\n");
//            
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str8 + "\n");
//            
//            localStringBuffer.append("BEGINDATETIMESTAMP: " + str9 + "\n");
//            
//            localStringBuffer.append("ENDDATETIMESTAMP: " + str10 + "\n");
//            
//            localStringBuffer.append("DEPARTMENT: " + str11 + "\n");
//            
//            localStringBuffer.append("OPERATORQUALIFIER: " + str12 + "\n");
//            
//            localStringBuffer.append("OPERATORID: " + str13 + "\n");
//            
//            localStringBuffer.append("TRANSACTIONCURRENCY: " + str14 + "\n");
//            
//            localStringBuffer.append("TRANSACTIONCURRENCY_ISO: " + str15 + "\n");
//            
//            localStringBuffer.append("PARTNERQUALIFIER: " + str16 + "\n");
//            
//            localStringBuffer.append("PARTNERID: " + str17 + "\n");
//            
//            str27 = "SELECT retailtypecode, sum(salesamount) AS salesamount, sum(lineitemcount) AS lineitemcount FROM show_sap_retail_totales('" + paramString + "','OFFLINE') " + "WHERE businessdaydate='" + str6 + "' AND workstationid=" + i + "GROUP BY retailtypecode " + "ORDER BY retailtypecode ";
//            
//
//
//
//
//
//
//
//
//            localResultSet1 = localStatement2.executeQuery(str27);
//            
//
//            while (localResultSet1.next()) {
//              str18 = localResultSet1.getString("retailtypecode");
//              
//              d = localResultSet1.getDouble("salesamount");
//              
//              j = localResultSet1.getInt("lineitemcount");
//              
//              d = Math.rint(d / 100.0D * i1) / i1;
//              
//
//
//              localJCoTable2.appendRow();
//              localJCoTable2.setValue("RETAILSTOREID", str5);
//              
//              localJCoTable2.setValue("BUSINESSDAYDATE", str6);
//              
//              localJCoTable2.setValue("TRANSACTIONTYPECODE", str7);
//              
//              localJCoTable2.setValue("WORKSTATIONID", i);
//              
//              localJCoTable2.setValue("TRANSACTIONSEQUENCENUMBER", str8);
//              
//
//              localJCoTable2.setValue("RETAILTYPECODE", str18);
//              
//              localJCoTable2.setValue("SALESAMOUNT", d);
//              localJCoTable2.setValue("LINEITEMCOUNT", j);
//              
//
//
//              localStringBuffer.append("\n (OFFLINE) RETAILTOTAL POS: " + i + " BUSINESSDAYDATE: " + str6 + "\n");
//              
//              localStringBuffer.append("RETAILSTOREID: " + str5 + "\n");
//              
//              localStringBuffer.append("BUSINESSDAYDATE: " + str6 + "\n");
//              
//              localStringBuffer.append("TRANSACTIONTYPECODE: " + str7 + "\n");
//              
//              localStringBuffer.append("WORKSTATIONID: " + i + "\n");
//              
//              localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str8 + "\n");
//              
//              localStringBuffer.append("RETAILTYPECODE: " + str18 + "\n");
//              
//              localStringBuffer.append("SALESAMOUNT: " + d + "\n");
//              
//              localStringBuffer.append("LINEITEMCOUNT: " + j + "\n");
//            }
//          }
//        }
//        
//
//
//
//
//
//        localStringBuffer.append("------------------------------------------------------------------------------- \n");
//        if (paramInt2 == 2) {
//          LogTrans(paramString, localStringBuffer.toString(), "log_totals_php");
//        }
//        if (paramInt2 == 1) {
//          LogTrans(paramString, localStringBuffer.toString(), "log_totals_aut");
//        }
//        if (paramInt2 == 0) {
//          LogTrans(paramString, localStringBuffer.toString(), "log_totals_app");
//        }
//        
//        localStatement1.close();
//        localConnection.close();
//        
//        if ((m != 0) || (n != 0)) {
//          try
//          {
//            localJCoFunction.execute(localJCoDestination);
//            str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> TOTALES ENVIADOS CON EXITO.";
//            if (paramInt2 == 2) {
//              LogTrans(paramString, str2, "log_php");
//              
//              str2 = "Totales Enviados Con Exito. Bdate: " + paramString;
//              str3 = "<br>Totales Enviados Con Exito. Bdate: " + paramString;
//              LogTransWeb(str4, str2, "log");
//              LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//              System.out.println(str2);
//            }
//            if (paramInt2 == 1) {
//              LogTrans(paramString, str2, "log_aut");
//            }
//            if (paramInt2 == 0) {
//              Globals.ta.append("\t TOTALES ENVIADOS CON EXITO..\n");
//              LogTrans(paramString, str2, "log_app");
//            }
//            
//            JCoTable localJCoTable5 = localJCoFunction.getTableParameterList().getTable("RETURN");
//            for (int i2 = 0; i2 < localJCoTable5.getNumRows(); i2++) {
//              localJCoTable5.setRow(i2);
//              
//              str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> " + localJCoTable5.getString("TYPE") + '\t' + localJCoTable5.getString("MESSAGE");
//              if (paramInt2 == 2) {
//                LogTrans(paramString, str2, "log_php");
//              }
//              if (paramInt2 == 1) {
//                LogTrans(paramString, str2, "log_aut");
//              }
//              if (paramInt2 == 0) {
//                Globals.ta.append("\t" + localJCoTable5.getString("TYPE") + '\t' + localJCoTable5.getString("MESSAGE") + "\n");
//                LogTrans(paramString, str2, "log_app");
//              }
//            }
//            
//            if (paramInt2 == 2) {
//              if (m != 0)
//              {
//                str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> transaccion de totales tuvo posicion de venta";
//                LogTrans(paramString, str2, "log_php");
//              }
//              else if (n != 0)
//              {
//                str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> transaccion tuvo transacciones anuladas";
//                LogTrans(paramString, str2, "log_php");
//              }
//            }
//            if (paramInt2 == 1) {
//              if (m != 0)
//              {
//                str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> transaccion de totales tuvo posicion de venta";
//                LogTrans(paramString, str2, "log_aut");
//              }
//              else if (n != 0)
//              {
//                str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> transaccion tuvo transacciones anuladas";
//                LogTrans(paramString, str2, "log_aut");
//              }
//            }
//            if (paramInt2 == 0) {
//              if (m != 0)
//              {
//                str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> transaccion de totales tuvo posicion de venta";
//                LogTrans(paramString, str2, "log_app");
//              }
//              else if (n != 0)
//              {
//                str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> transaccion tuvo transacciones anuladas";
//                LogTrans(paramString, str2, "log_app");
//              }
//            }
//            
//            ActualizarStatusTotales(paramString, paramInt1, 1);
//            return;
//          }
//          catch (com.sap.conn.jco.AbapException localAbapException) {
//            str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> ERROR: " + localAbapException.toString();
//            str3 = "<br> --> ERROR: " + localAbapException.toString();
//            if (paramInt2 == 2) {
//              LogTrans(paramString, str2, "log_php");
//              LogTransWeb(str4, str2, "error");
//              LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//              System.out.println(str2);
//            }
//            if (paramInt2 == 1) {
//              LogTrans(paramString, str2, "log_aut");
//            }
//            if (paramInt2 == 0) {
//              Globals.ta.append("\t" + localAbapException.toString() + "\n");
//              LogTrans(paramString, str2, "log_app");
//            }
//            ActualizarStatusTotales(paramString, paramInt1, -1);
//            return;
//          }
//        }
//        str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> LA TRANSACCION DE TOTALES NO LLENO NINGUN SEGMENTO";
//        str3 = "<br> --> LA TRANSACCION DE TOTALES NO LLENO NINGUN SEGMENTO";
//        if (paramInt2 == 2) {
//          LogTrans(paramString, str2, "log_php");
//          LogTransWeb(str4, str2, "error");
//          LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//          System.out.println(str2);
//        }
//        if (paramInt2 == 1) {
//          LogTrans(paramString, str2, "log_aut");
//        }
//        if (paramInt2 == 0) {
//          Globals.ta.append("\tLA TRANSACCION DE TOTALES NO LLENO NINGUN SEGMENTO\n");
//          LogTrans(paramString, str2, "log_app");
//        }
//        
//        ActualizarStatusTotales(paramString, paramInt1, 2);
//        return;
//      }
//      catch (Exception localException) {
//        str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> ERROR: " + localException.getMessage();
//        str3 = "<br> --> ERROR: " + localException.getMessage();
//        if (paramInt2 == 2) {
//          LogTrans(paramString, str2, "log_php");
//          LogTransWeb(str4, str2, "error");
//          LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//          System.out.println(str2);
//        }
//        if (paramInt2 == 1) {
//          LogTrans(paramString, str2, "log_aut");
//        }
//        if (paramInt2 == 0) {
//          Globals.ta.append("\t" + localException.getMessage() + "\n");
//          LogTrans(paramString, str2, "log_app");
//        }
//        
//        ActualizarStatusTotales(paramString, paramInt1, -1); return;
//      }
//      return;
//    }
//    catch (JCoException localJCoException) {
//      str2 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t --> ERROR: " + localJCoException.getMessage();
//      str3 = "<br> --> ERROR: " + localJCoException.getMessage();
//      if (paramInt2 == 2) {
//        LogTrans(paramString, str2, "log_php");
//        LogTransWeb(str4, str2, "error");
//        LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//        System.out.println(str2);
//      }
//      if (paramInt2 == 1) {
//        LogTrans(paramString, str2, "log_aut");
//      }
//      if (paramInt2 == 0) {
//        Globals.ta.append("\t" + localJCoException.getMessage() + "\n");
//        LogTrans(paramString, str2, "log_app");
//      }
//    }
//  }
//  
//  public int ActualizarStatusTransaccion(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
//  {
//    String str2 = "";
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      String str1 = "SELECT save_sap_totales('" + paramString + "', " + paramInt1 + ", " + paramInt2 + ")";
//      
//      localStatement.executeQuery(str1);
//      
//      if ((paramInt4 == 1) || (paramInt4 == 2)) {
//        if (paramInt4 == 1) {
//          str2 = ", senddate = current_timestamp ";
//        }
//        i = paramInt4;
//      }
//      else if (paramInt4 < 0) {
//        if (paramInt3 < 0) {
//          i = paramInt3 - paramInt4;
//        } else {
//          i = paramInt4;
//        }
//      }
//      
//
//
//
//
//      str1 = "UPDATE sap_transacciones SET status=" + i + str2 + " where bdate = " + paramString + " and termnmbr=" + paramInt1 + " and transnmbr=" + paramInt2;
//      
//
//
//
//
//      localStatement.executeUpdate(str1);
//      
//      localStatement.execute("END");
//      localStatement.close();
//      localConnection.close();
//      
//      return 0;
//    }
//    catch (Exception localException) {
//      System.out.println(localException.getMessage()); }
//    return -1;
//  }
//  
//  public void callBapi_transacciones_automaticas(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
//    throws JCoException
//  {
//    String str1 = GetTienda();
//    String str2 = GetBarraFija();
//    String str3 = "";
//    String str4 = "";
//    
//    String str5 = Globals.NombreTablaTemp;
//    
//
//    str3 = "\nEnviando Tr: " + paramInt2 + " del Pos: " + paramInt1 + " del dia : " + paramString + " con status: " + paramInt3;
//    if (paramInt4 == 2) {
//      LogTrans(paramString, str3, "log_php");
//    }
//    
//
//
//
//
//    if (paramInt4 == 0) {
//      Globals.ta.append("Enviando Tr: " + paramInt2 + " del Pos: " + paramInt1 + " del dia : " + paramString + " con status: " + paramInt3 + "\n");
//      LogTrans(paramString, str3, "log_app");
//    }
//    
//    if (str1.equals(""))
//    {
//      if (paramInt4 == 2) {
//        str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> ERROR: NO SE ENCONTRO LA TIENDA";
//        str4 = "<br> --> ERROR: NO SE ENCONTRO LA TIENDA";
//        LogTrans(paramString, str3, "log_php");
//        LogTransWeb(str5, str3, "error");
//        LogEnvioAppWeb(Globals.NombreTablaTemp, str4, Globals.NumeroTransaccionActual);
//        System.out.println(str3);
//      }
//      if (paramInt4 == 1) {
//        str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> ERROR: NO SE ENCONTRO LA TIENDA";
//        LogTrans(paramString, str3, "log_aut");
//      }
//      if (paramInt4 == 0) {
//        Globals.ta.append("NO SE ENCONTRO LA TIENDA\n");
//        LogTrans(paramString, str3, "log_app");
//      }
//      
//      ActualizarStatusTransaccion(paramString, paramInt1, paramInt2, paramInt3, -1);
//      return;
//    }
//    
//    try
//    {
//      com.sap.conn.jco.JCoDestination localJCoDestination = com.sap.conn.jco.JCoDestinationManager.getDestination(Globals.ABAP_AS);
//      JCoFunction localJCoFunction = localJCoDestination.getRepository().getFunction("/POSDW/BAPI_POSTR_CREATE");
//      if (localJCoFunction == null)
//      {
//        if (paramInt4 == 2) {
//          str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> ERROR: no se encontro /POSDW/BAPI_POSTR_CREATE en SAP";
//          str4 = "<br> --> ERROR: no se encontro /POSDW/BAPI_POSTR_CREATE en SAP";
//          LogTrans(paramString, str3, "log_php");
//          LogTransWeb(str5, str3, "error");
//          LogEnvioAppWeb(Globals.NombreTablaTemp, str4, Globals.NumeroTransaccionActual);
//          System.out.println(str3);
//          
//          str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> RESUTADO DEL ENVIO: -1";
//          LogTrans(paramString, str3, "log_php");
//        }
//        
//
//
//        if (paramInt4 == 1) {
//          str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> ERROR: no se encontro /POSDW/BAPI_POSTR_CREATE en SAP";
//          LogTrans(paramString, str3, "log_aut");
//          
//          str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> RESUTADO DEL ENVIO: -1";
//          LogTrans(paramString, str3, "log_aut");
//        }
//        
//
//
//        if (paramInt4 == 0) {
//          Globals.ta.append("\t no se encontro /POSDW/BAPI_POSTR_CREATE en SAP.");
//          str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> ERROR: no se encontro /POSDW/BAPI_POSTR_CREATE en SAP";
//          LogTrans(paramString, str3, "log_app");
//          
//          str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> RESUTADO DEL ENVIO: -1";
//          LogTrans(paramString, str3, "log_app");
//        }
//        
//
//
//        ActualizarStatusTransaccion(paramString, paramInt1, paramInt2, paramInt3, -1);
//        return;
//      }
//      
//      localJCoFunction.getImportParameterList().setValue("I_COMMIT", "X");
//      
//      JCoTable localJCoTable1 = localJCoFunction.getTableParameterList().getTable("TRANSACTION");
//      JCoTable localJCoTable2 = localJCoFunction.getTableParameterList().getTable("TRANSACTIONDISCOUNT");
//      JCoTable localJCoTable3 = localJCoFunction.getTableParameterList().getTable("TRANSACTIONTAX");
//      JCoTable localJCoTable4 = localJCoFunction.getTableParameterList().getTable("RETAILLINEITEM");
//      JCoTable localJCoTable5 = localJCoFunction.getTableParameterList().getTable("LINEITEMVOID");
//      JCoTable localJCoTable6 = localJCoFunction.getTableParameterList().getTable("LINEITEMDISCOUNT");
//      JCoTable localJCoTable7 = localJCoFunction.getTableParameterList().getTable("LINEITEMTAX");
//      JCoTable localJCoTable8 = localJCoFunction.getTableParameterList().getTable("TENDER");
//      JCoTable localJCoTable9 = localJCoFunction.getTableParameterList().getTable("LINEITEMEXT");
//      JCoTable localJCoTable10 = localJCoFunction.getTableParameterList().getTable("TRANSACTIONEXT");
//      JCoTable localJCoTable11 = localJCoFunction.getTableParameterList().getTable("ADDITIONALS");
//      
//      int i = 0;
//      int j = 0;
//      
//
//
//
//      StringBuffer localStringBuffer = new StringBuffer();
//      
//      try
//      {
//        Class.forName("org.postgresql.Driver");
//        Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//        Statement localStatement1 = localConnection.createStatement();
//        Statement localStatement2 = localConnection.createStatement();
//        
//
//        String str6 = str1;
//        String str7 = "";
//        String str8 = "1001";
//        int k = paramInt1;
//        
//        String str9 = paramInt2 + "";
//        String str10 = "";
//        String str11 = "";
//        String str12 = "";
//        String str13 = "";
//        String str14 = "";
//        String str15 = "VES";
//        String str16 = "";
//        String str17 = "";
//        String str18 = "";
//        
//
//
//        String str19 = "";
//        String str20 = "";
//        
//        String str21 = "";
//        String str22 = "";
//        String str23 = "";
//        String str24 = "";
//        String str25 = "";
//        
//
//
//        String str26 = "";
//        String str27 = "";
//        String str28 = "1";
//        String str29 = "";
//        
//
//        String str30 = "ST";
//        String str31 = "";
//        
//
//        String str32 = "";
//        String str33 = "";
//        String str34 = "";
//        String str35 = "";
//        String str36 = "";
//        
//        String str37 = "";
//        String str38 = "";
//        
//
//
//        String str39 = "x";
//        
//
//
//        String str40 = "";
//        
//
//
//
//        String str41 = "";
//        
//        String str42 = "VES";
//        String str43 = "";
//        String str44 = "";
//        String str45 = "";
//        String str46 = "";
//        
//
//        String str47 = "";
//        String str48 = "";
//        String str49 = "";
//        
//        String str50 = "";
//        String str51 = "";
//        String str52 = "";
//        
//
//        int i4 = (int)Math.pow(10.0D, 2.0D);
//        
//        String str53 = "";
//        
//
//
//
//        String str54 = "";
//        str53 = "select sap_fecha_reconversion from sap_fecha_reconversion()";
//        ResultSet localResultSet1 = localStatement1.executeQuery(str53);
//        while (localResultSet1.next()) {
//          str54 = localResultSet1.getString("sap_fecha_reconversion");
//        }
//        
//        ObtenerDevolucion(paramString, paramInt1, paramInt2);
//        
//
//
//        localStringBuffer.append("----------------- SEGMENTO  POS: " + paramInt1 + " TRANS: " + paramInt2 + "--------------- \n");
//        
//        str53 = " select distinct(to_char(to_timestamp(datetime), 'YYYYMMDD'))::TEXT as bdate  FROM ej" + paramString + "  WHERE termnmbr=" + paramInt1 + " and transnmbr=" + paramInt2;
//        
//
//        localResultSet1 = localStatement1.executeQuery(str53);
//        while (localResultSet1.next()) {
//          str7 = localResultSet1.getString("bdate");
//        }
//        
//        if (Integer.parseInt(str7) < Integer.parseInt(str54)) {
//          str15 = "VEF";
//          str42 = "VEF";
//        }
//        localStringBuffer.append("\nTHE CURRENCY LABEL IS TENDERCURRENCY " + str42 + " AND TRANSACTIONCURRENCY " + str15 + " \n");
//        
//        str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TRANSACTION') " + "AS (campo1 character varying, campo2 character varying, campo3 character varying)";
//        
//
//
//
//
//
//
//
//
//        localResultSet1 = localStatement1.executeQuery(str53);
//        
//        while (localResultSet1.next()) {
//          i = 1;
//          str10 = localResultSet1.getString("campo1");
//          str11 = localResultSet1.getString("campo2");
//          str14 = localResultSet1.getString("campo3");
//          
//
//          localStringBuffer.append("\n TRANSACTION  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//          
//          localJCoTable1.appendRow();
//          localJCoTable1.setValue("RETAILSTOREID", str6);
//          
//          localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//          localJCoTable1.setValue("BUSINESSDAYDATE", str7);
//          
//          localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//          localJCoTable1.setValue("TRANSACTIONTYPECODE", str8);
//          
//
//
//
//
//          localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//          localJCoTable1.setValue("WORKSTATIONID", k);
//          
//          localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//          localJCoTable1.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//          
//
//
//
//
//          localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//          localJCoTable1.setValue("BEGINDATETIMESTAMP", str10);
//          
//
//          localStringBuffer.append("BEGINDATETIMESTAMP: " + str10 + "\n");
//          localJCoTable1.setValue("ENDDATETIMESTAMP", str11);
//          
//
//          localStringBuffer.append("ENDDATETIMESTAMP: " + str11 + "\n");
//          localJCoTable1.setValue("DEPARTMENT", str12);
//          
//          localStringBuffer.append("DEPARTMENT: " + str12 + "\n");
//          localJCoTable1.setValue("OPERATORQUALIFIER", str13);
//          
//
//          localStringBuffer.append("OPERATORQUALIFIER: " + str13 + "\n");
//          localJCoTable1.setValue("OPERATORID", str14);
//          
//          localStringBuffer.append("OPERATORID: " + str14 + "\n");
//          localJCoTable1.setValue("TRANSACTIONCURRENCY", str15);
//          
//
//          localStringBuffer.append("TRANSACTIONCURRENCY: " + str15 + "\n");
//          localJCoTable1.setValue("TRANSACTIONCURRENCY_ISO", str16);
//          
//
//          localStringBuffer.append("TRANSACTIONCURRENCY_ISO: " + str16 + "\n");
//          localJCoTable1.setValue("PARTNERQUALIFIER", str17);
//          
//
//          localStringBuffer.append("PARTNERQUALIFIER: " + str17 + "\n");
//          localJCoTable1.setValue("PARTNERID", str18);
//          
//          localStringBuffer.append("PARTNERID: " + str18 + "\n");
//        }
//        int i5;
//        if (i != 0)
//        {
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TRANSACTIONDISCOUNT') " + " AS (campo1 character varying, campo2 numeric);";
//          
//
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          i5 = 1;
//          int m; double d1; while (localResultSet1.next()) {
//            m = i5;
//            str19 = localResultSet1.getString("campo1");
//            d1 = localResultSet1.getLong("campo2");
//            d1 = Math.rint(d1 / 100.0D * i4) / i4;
//            
//
//
//
//            localStringBuffer.append("\n TRANSACTIONDISCOUNT  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable2.appendRow();
//            localJCoTable2.setValue("RETAILSTOREID", str6);
//            
//
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable2.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable2.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable2.setValue("WORKSTATIONID", k);
//            
//
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable2.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable2.setValue("DISCOUNTSEQUENCENUMBER", m);
//            
//
//
//            localStringBuffer.append("DISCOUNTSEQUENCENUMBER: " + m + "\n");
//            localJCoTable2.setValue("DISCOUNTTYPECODE", str19);
//            
//
//            localStringBuffer.append("DISCOUNTTYPECODE: " + str19 + "\n");
//            localJCoTable2.setValue("DISCOUNTREASONCODE", str20);
//            
//
//            localStringBuffer.append("DISCOUNTREASONCODE: " + str20 + "\n");
//            localJCoTable2.setValue("REDUCTIONAMOUNT", d1);
//            
//
//            localStringBuffer.append("REDUCTIONAMOUNT: " + d1 + "\n");
//            localJCoTable2.setValue("STOREFINANCIALLEDGERACCOUNTID", str21);
//            
//
//
//            localStringBuffer.append("STOREFINANCIALLEDGERACCOUNTID: " + str21 + "\n");
//            localJCoTable2.setValue("DISCOUNTID", str22);
//            
//
//            localStringBuffer.append("DISCOUNTID: " + str22 + "\n");
//            localJCoTable2.setValue("DISCOUNTIDQUALIFIER", str23);
//            
//
//            localStringBuffer.append("DISCOUNTIDQUALIFIER: " + str23 + "\n");
//            localJCoTable2.setValue("BONUSBUYID", str24);
//            
//
//            localStringBuffer.append("BONUSBUYID: " + str24 + "\n");
//            localJCoTable2.setValue("OFFERID", str25);
//            
//            localStringBuffer.append("OFFERID: " + str25 + "\n");
//            i5 += 1;
//          }
//          
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TRANSACTIONTAX') " + " AS (campo1 character varying, campo2 numeric);";
//          
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          i5 = 1;
//          int i2; double d6; while (localResultSet1.next()) {
//            i2 = i5;
//            str40 = localResultSet1.getString("campo1");
//            
//            d6 = localResultSet1.getDouble("campo2");
//            d6 = Math.rint(d6 / 100.0D * i4) / i4;
//            
//
//
//            localStringBuffer.append("\n TRANSACTIONTAX  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable3.appendRow();
//            localJCoTable3.setValue("RETAILSTOREID", str6);
//            
//
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable3.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable3.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable3.setValue("WORKSTATIONID", k);
//            
//
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable3.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable3.setValue("TAXSEQUENCENUMBER", i2);
//            
//
//            localStringBuffer.append("TAXSEQUENCENUMBER: " + i2 + "\n");
//            localJCoTable3.setValue("TAXTYPECODE", str40);
//            
//            localStringBuffer.append("TAXTYPECODE: " + str40 + "\n");
//            localJCoTable3.setValue("TAXAMOUNT", d6);
//            
//            localStringBuffer.append("TAXAMOUNT: " + d6 + "\n");
//            i5 += 1;
//          }
//          
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'RETAILLINEITEM') " + "AS (campo1 numeric, campo2 character varying, campo3 character varying, " + "campo4 numeric, campo5 numeric, campo6 numeric, campo7 numeric)";
//          
//
//
//
//
//
//
//
//
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          int n;
//          while (localResultSet1.next()) {
//            n = localResultSet1.getInt("campo1");
//            str26 = localResultSet1.getString("campo2");
//            
//            if (str2.equals("")) {
//              str29 = localResultSet1.getString("campo3");
//            } else {
//              str29 = str2;
//            }
//            double d2 = localResultSet1.getDouble("campo4");
//            double d3 = localResultSet1.getDouble("campo5");
//            double d4 = localResultSet1.getDouble("campo6");
//            double d5 = localResultSet1.getDouble("campo7");
//            
//            d2 = Math.rint(d2 / 1000.0D * i4) / i4;
//            
//
//            d3 = Math.rint(d3 / 100.0D * i4) / i4;
//            
//            d4 = Math.rint(d4 / 100.0D * i4) / i4;
//            
//
//            d5 = Math.rint(d5 / 100.0D * i4) / i4;
//            
//
//
//
//            localStringBuffer.append("\n RETAILLINEITEM  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable4.appendRow();
//            localJCoTable4.setValue("RETAILSTOREID", str6);
//            
//
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable4.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable4.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable4.setValue("WORKSTATIONID", k);
//            
//
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable4.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable4.setValue("RETAILSEQUENCENUMBER", n);
//            
//
//            localStringBuffer.append("RETAILSEQUENCENUMBER: " + n + "\n");
//            localJCoTable4.setValue("RETAILTYPECODE", str26);
//            
//
//            localStringBuffer.append("RETAILTYPECODE: " + str26 + "\n");
//            localJCoTable4.setValue("RETAILREASONCODE", str27);
//            
//
//            localStringBuffer.append("RETAILREASONCODE: " + str27 + "\n");
//            localJCoTable4.setValue("ITEMIDQUALIFIER", str28);
//            
//
//            localStringBuffer.append("ITEMIDQUALIFIER: " + str28 + "\n");
//            localJCoTable4.setValue("ITEMID", str29);
//            
//            localStringBuffer.append("ITEMID: " + str29 + "\n");
//            localJCoTable4.setValue("RETAILQUANTITY", d2);
//            
//
//            localStringBuffer.append("RETAILQUANTITY: " + d2 + "\n");
//            localJCoTable4.setValue("SALESUNITOFMEASURE", str30);
//            
//
//            localStringBuffer.append("SALESUNITOFMEASURE: " + str30 + "\n");
//            localJCoTable4.setValue("SALESUNITOFMEASURE_ISO", str31);
//            
//
//            localStringBuffer.append("SALESUNITOFMEASURE_ISO: " + str31 + "\n");
//            localJCoTable4.setValue("SALESAMOUNT", d3);
//            
//            localStringBuffer.append("SALESAMOUNT: " + d3 + "\n");
//            localJCoTable4.setValue("NORMALSALESAMOUNT", d4);
//            
//
//            localStringBuffer.append("NORMALSALESAMOUNT: " + d4 + "\n");
//            localJCoTable4.setValue("COST", str32);
//            
//            localStringBuffer.append("COST: " + str32 + "\n");
//            localJCoTable4.setValue("BATCHID", str33);
//            
//            localStringBuffer.append("BATCHID: " + str33 + "\n");
//            localJCoTable4.setValue("SERIALNUMBER", str34);
//            
//            localStringBuffer.append("SERIALNUMBER: " + str34 + "\n");
//            localJCoTable4.setValue("PROMOTIONID", str35);
//            
//            localStringBuffer.append("PROMOTIONID: " + str35 + "\n");
//            localJCoTable4.setValue("ITEMIDENTRYMETHODCODE", str36);
//            
//
//            localStringBuffer.append("ITEMIDENTRYMETHODCODE: " + str36 + "\n");
//            localJCoTable4.setValue("ACTUALUNITPRICE", d5);
//            
//
//            localStringBuffer.append("ACTUALUNITPRICE: " + d5 + "\n");
//            localJCoTable4.setValue("UNITS", str37);
//            
//            localStringBuffer.append("UNITS: " + str37 + "\n");
//            localJCoTable4.setValue("SCANTIME", str38);
//            
//            localStringBuffer.append("SCANTIME: " + str38 + "\n");
//          }
//          
//
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'LINEITEMVOID') " + "AS (campo1 numeric, campo2 numeric);";
//          
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          while (localResultSet1.next()) {
//            n = localResultSet1.getInt("campo1");
//            int i1 = localResultSet1.getInt("campo2");
//            
//
//            localStringBuffer.append("\n LINEITEMVOID  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable5.appendRow();
//            localJCoTable5.setValue("RETAILSTOREID", str6);
//            
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable5.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable5.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable5.setValue("WORKSTATIONID", k);
//            
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable5.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable5.setValue("RETAILSEQUENCENUMBER", n);
//            
//
//            localStringBuffer.append("RETAILSEQUENCENUMBER: " + n + "\n");
//            localJCoTable5.setValue("VOIDEDLINE", i1);
//            
//            localStringBuffer.append("VOIDEDLINE: " + i1 + "\n");
//            localJCoTable5.setValue("VOIDFLAG", str39);
//            
//            localStringBuffer.append("VOIDFLAG: " + str39 + "\n");
//          }
//          
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'LINEITEMDISCOUNT') " + "AS (campo1 numeric, campo2 numeric, campo3 character varying, campo4 numeric);";
//          
//
//
//
//
//
//
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          while (localResultSet1.next()) {
//            n = localResultSet1.getInt("campo1");
//            m = localResultSet1.getInt("campo2");
//            str19 = localResultSet1.getString("campo3");
//            d1 = localResultSet1.getDouble("campo4");
//            
//            d1 = Math.rint(d1 / 100.0D * i4) / i4;
//            
//
//
//
//            localStringBuffer.append("\n LINEITEMDISCOUNT  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable6.appendRow();
//            localJCoTable6.setValue("RETAILSTOREID", str6);
//            
//
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable6.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable6.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable6.setValue("WORKSTATIONID", k);
//            
//
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable6.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable6.setValue("RETAILSEQUENCENUMBER", n);
//            
//
//            localStringBuffer.append("RETAILSEQUENCENUMBER: " + n + "\n");
//            localJCoTable6.setValue("DISCOUNTSEQUENCENUMBER", m);
//            
//
//            localStringBuffer.append("DISCOUNTSEQUENCENUMBER: " + m + "\n");
//            localJCoTable6.setValue("DISCOUNTTYPECODE", str19);
//            
//
//            localStringBuffer.append("DISCOUNTTYPECODE: " + str19 + "\n");
//            localJCoTable6.setValue("DISCOUNTREASONCODE", str20);
//            
//
//            localStringBuffer.append("DISCOUNTREASONCODE: " + str20 + "\n");
//            localJCoTable6.setValue("REDUCTIONAMOUNT", d1);
//            
//
//            localStringBuffer.append("REDUCTIONAMOUNT: " + d1 + "\n");
//            localJCoTable6.setValue("STOREFINANCIALLEDGERACCOUNTID", str21);
//            
//
//
//            localStringBuffer.append("STOREFINANCIALLEDGERACCOUNTID: " + str21 + "\n");
//            localJCoTable6.setValue("DISCOUNTID", str22);
//            
//            localStringBuffer.append("DISCOUNTID: " + str22 + "\n");
//            localJCoTable6.setValue("DISCOUNTIDQUALIFIER", str23);
//            
//
//            localStringBuffer.append("DISCOUNTIDQUALIFIER: " + str23 + "\n");
//            localJCoTable6.setValue("BONUSBUYID", str24);
//            
//            localStringBuffer.append("BONUSBUYID: " + str24 + "\n");
//            localJCoTable6.setValue("OFFERID", str25);
//            
//            localStringBuffer.append("OFFERID: " + str25 + "\n");
//          }
//          
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'LINEITEMTAX') " + "AS (campo1 numeric, campo2 numeric, campo3 character varying, campo4 numeric);";
//          
//
//
//
//
//
//
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          while (localResultSet1.next()) {
//            n = localResultSet1.getInt("campo1");
//            i2 = localResultSet1.getInt("campo2");
//            str40 = localResultSet1.getString("campo3");
//            
//
//            d6 = localResultSet1.getDouble("campo4");
//            
//            d6 = Math.rint(d6 / 100.0D * i4) / i4;
//            
//
//
//            localStringBuffer.append("\n LINEITEMTAX  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable7.appendRow();
//            localJCoTable7.setValue("RETAILSTOREID", str6);
//            
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable7.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable7.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable7.setValue("WORKSTATIONID", k);
//            
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable7.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable7.setValue("RETAILSEQUENCENUMBER", n);
//            
//
//            localStringBuffer.append("RETAILSEQUENCENUMBER: " + n + "\n");
//            localJCoTable7.setValue("TAXSEQUENCENUMBER", i2);
//            
//
//            localStringBuffer.append("TAXSEQUENCENUMBER: " + i2 + "\n");
//            localJCoTable7.setValue("TAXTYPECODE", str40);
//            
//            localStringBuffer.append("TAXTYPECODE: " + str40 + "\n");
//            localJCoTable7.setValue("TAXAMOUNT", d6);
//            
//            localStringBuffer.append("TAXAMOUNT: " + d6 + "\n");
//          }
//          
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TENDER') " + "AS (campo1 numeric, campo2 character varying, campo3 numeric, " + "campo4 character varying, campo5 character varying, campo6 character varying);";
//          
//
//
//
//
//
//
//
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          i5 = 1;
//          while (localResultSet1.next()) {
//            int i3 = i5;
//            str41 = localResultSet1.getString("campo2");
//            double d7 = localResultSet1.getDouble("campo3");
//            str44 = localResultSet1.getString("campo4");
//            str45 = localResultSet1.getString("campo5");
//            str46 = localResultSet1.getString("campo6");
//            
//            d7 = Math.rint(d7 / 100.0D * i4) / i4;
//            
//
//
//            localStringBuffer.append("\n TENDER  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable8.appendRow();
//            localJCoTable8.setValue("RETAILSTOREID", str6);
//            
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable8.setValue("BUSINESSDAYDATE", str7);
//            
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable8.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable8.setValue("WORKSTATIONID", k);
//            
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable8.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable8.setValue("TENDERSEQUENCENUMBER", i3);
//            
//
//            localStringBuffer.append("TENDERSEQUENCENUMBER: " + i3 + "\n");
//            localJCoTable8.setValue("TENDERTYPECODE", str41);
//            
//            localStringBuffer.append("TENDERTYPECODE: " + str41 + "\n");
//            localJCoTable8.setValue("TENDERAMOUNT", d7);
//            
//            localStringBuffer.append("TENDERAMOUNT: " + d7 + "\n");
//            localJCoTable8.setValue("TENDERCURRENCY", str42);
//            
//            localStringBuffer.append("TENDERCURRENCY: " + str42 + "\n");
//            localJCoTable8.setValue("TENDERCURRENCY_ISO", str43);
//            
//
//            localStringBuffer.append("TENDERCURRENCY_ISO: " + str43 + "\n");
//            localJCoTable8.setValue("TENDERID", str44);
//            
//            localStringBuffer.append("TENDERID: " + str44 + "\n");
//            localJCoTable8.setValue("ACCOUNTNUMBER", str45);
//            
//            localStringBuffer.append("ACCOUNTNUMBER: " + str45 + "\n");
//            localJCoTable8.setValue("REFERENCEID", str46);
//            
//            localStringBuffer.append("REFERENCEID: " + str46 + "\n");
//            i5 += 1;
//          }
//          
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'LINEITEMEXT') " + "AS (campo1 numeric, campo2 character varying, campo3 character varying);";
//          
//
//
//
//
//
//
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          while (localResultSet1.next()) {
//            n = localResultSet1.getInt("campo1");
//            str48 = localResultSet1.getString("campo2");
//            str49 = localResultSet1.getString("campo3");
//            
//
//            localStringBuffer.append("\n LINEITEMEXT  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable9.appendRow();
//            localJCoTable9.setValue("RETAILSTOREID", str6);
//            
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable9.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable9.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable9.setValue("WORKSTATIONID", k);
//            
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable9.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable9.setValue("RETAILSEQUENCENUMBER", n);
//            
//
//            localStringBuffer.append("RETAILSEQUENCENUMBER: " + n + "\n");
//            localJCoTable9.setValue("FIELDGROUP", str47);
//            
//            localStringBuffer.append("FIELDGROUP: " + str47 + "\n");
//            localJCoTable9.setValue("FIELDNAME", str48);
//            
//            localStringBuffer.append("FIELDNAME: " + str48 + "\n");
//            localJCoTable9.setValue("FIELDVALUE", str49);
//            
//            localStringBuffer.append("FIELDVALUE: " + str49 + "\n");
//          }
//          
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TRANSACTIONEXT') " + "AS (campo1 character varying, campo2 character varying)";
//          
//
//
//
//
//
//
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          while (localResultSet1.next()) {
//            str48 = localResultSet1.getString("campo1");
//            str49 = localResultSet1.getString("campo2");
//            
//
//            localStringBuffer.append("\n LINEITEMEXT  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable10.appendRow();
//            localJCoTable10.setValue("RETAILSTOREID", str6);
//            
//
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable10.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable10.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable10.setValue("WORKSTATIONID", k);
//            
//
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable10.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable10.setValue("FIELDGROUP", str47);
//            
//            localStringBuffer.append("FIELDGROUP: " + str47 + "\n");
//            localJCoTable10.setValue("FIELDNAME", str48);
//            
//            localStringBuffer.append("FIELDNAME: " + str48 + "\n");
//            localJCoTable10.setValue("FIELDVALUE", str49);
//            
//            localStringBuffer.append("FIELDVALUE: " + str49 + "\n");
//          }
//          
//
//
//
//
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'ADDITIONALS') " + "AS (campo1 character varying)";
//          
//
//
//          localResultSet1 = localStatement1.executeQuery(str53);
//          
//          while (localResultSet1.next()) {
//            str51 = localResultSet1.getString("campo1");
//            
//
//            localStringBuffer.append("\n LINEITEMEXT  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable11.appendRow();
//            localJCoTable11.setValue("RETAILSTOREID", str6);
//            
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable11.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable11.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable11.setValue("WORKSTATIONID", k);
//            
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable11.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable11.setValue("TRAININGFLAG", str50);
//            
//            localStringBuffer.append("TRAININGFLAG: " + str50 + "\n");
//            localJCoTable11.setValue("KEYEDOFFLINEFLAG", str51);
//            
//
//            localStringBuffer.append("KEYEDOFFLINEFLAG: " + str51 + "\n");
//            localJCoTable11.setValue("TRANSREASONCODE", str52);
//            
//
//            localStringBuffer.append("TRANSREASONCODE: " + str52 + "\n");
//          }
//        }
//        
//
//        str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TRANSACTIONCONTROL') " + "AS (campo1 character varying, campo2 character varying, campo3 character varying, " + "campo4 character varying, campo5 character varying)";
//        
//
//
//
//
//
//
//
//
//
//        localResultSet1 = localStatement1.executeQuery(str53);
//        
//        while (localResultSet1.next()) {
//          j = 1;
//          str9 = localResultSet1.getString("campo1");
//          str8 = localResultSet1.getString("campo2");
//          str10 = localResultSet1.getString("campo3");
//          str11 = localResultSet1.getString("campo4");
//          str14 = localResultSet1.getString("campo5");
//          
//
//          localStringBuffer.append("\n TRANSACTIONCONTROL  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//          localJCoTable1.appendRow();
//          localJCoTable1.setValue("RETAILSTOREID", str6);
//          
//          localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//          localJCoTable1.setValue("BUSINESSDAYDATE", str7);
//          
//          localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//          localJCoTable1.setValue("TRANSACTIONTYPECODE", str8);
//          
//
//          localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//          localJCoTable1.setValue("WORKSTATIONID", k);
//          
//          localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//          localJCoTable1.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//          
//
//          localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//          localJCoTable1.setValue("BEGINDATETIMESTAMP", str10);
//          
//
//          localStringBuffer.append("BEGINDATETIMESTAMP: " + str10 + "\n");
//          localJCoTable1.setValue("ENDDATETIMESTAMP", str11);
//          
//
//          localStringBuffer.append("ENDDATETIMESTAMP: " + str11 + "\n");
//          localJCoTable1.setValue("DEPARTMENT", str12);
//          
//          localStringBuffer.append("DEPARTMENT: " + str12 + "\n");
//          localJCoTable1.setValue("OPERATORQUALIFIER", str13);
//          
//
//          localStringBuffer.append("OPERATORQUALIFIER: " + str13 + "\n");
//          localJCoTable1.setValue("OPERATORID", str14);
//          
//          localStringBuffer.append("OPERATORID: " + str14 + "\n");
//          localJCoTable1.setValue("TRANSACTIONCURRENCY", str15);
//          
//
//          localStringBuffer.append("TRANSACTIONCURRENCY: " + str15 + "\n");
//          localJCoTable1.setValue("TRANSACTIONCURRENCY_ISO", str16);
//          
//
//          localStringBuffer.append("TRANSACTIONCURRENCY_ISO: " + str16 + "\n");
//          localJCoTable1.setValue("PARTNERQUALIFIER", str17);
//          
//
//          localStringBuffer.append("PARTNERQUALIFIER: " + str17 + "\n");
//          localJCoTable1.setValue("PARTNERID", str18);
//          
//          localStringBuffer.append("PARTNERID: " + str18 + "\n");
//          
//          str53 = "SELECT * FROM SAP_SEGMENTOS('" + paramString + "'," + paramInt1 + "," + paramInt2 + ",'TRANSACTIONCONTROLEXT') " + "AS (campo1 character varying, campo2 character varying)";
//          
//
//
//
//
//
//
//
//          ResultSet localResultSet2 = localStatement2.executeQuery(str53);
//          
//          while (localResultSet2.next()) {
//            str48 = localResultSet2.getString("campo1");
//            str49 = localResultSet2.getString("campo2");
//            
//
//            localStringBuffer.append("\n TRANSACTIONCONTROLEXT  POS: " + paramInt1 + " TRANS: " + paramInt2 + "\n");
//            localJCoTable10.appendRow();
//            localJCoTable10.setValue("RETAILSTOREID", str6);
//            
//
//            localStringBuffer.append("RETAILSTOREID: " + str6 + "\n");
//            localJCoTable10.setValue("BUSINESSDAYDATE", str7);
//            
//
//            localStringBuffer.append("BUSINESSDAYDATE: " + str7 + "\n");
//            localJCoTable10.setValue("TRANSACTIONTYPECODE", str8);
//            
//
//            localStringBuffer.append("TRANSACTIONTYPECODE: " + str8 + "\n");
//            localJCoTable10.setValue("WORKSTATIONID", k);
//            
//
//            localStringBuffer.append("WORKSTATIONID: " + k + "\n");
//            localJCoTable10.setValue("TRANSACTIONSEQUENCENUMBER", str9);
//            
//
//            localStringBuffer.append("TRANSACTIONSEQUENCENUMBER: " + str9 + "\n");
//            localJCoTable10.setValue("FIELDGROUP", str47);
//            
//            localStringBuffer.append("FIELDGROUP: " + str47 + "\n");
//            localJCoTable10.setValue("FIELDNAME", str48);
//            
//            localStringBuffer.append("FIELDNAME: " + str48 + "\n");
//            localJCoTable10.setValue("FIELDVALUE", str49);
//            
//            localStringBuffer.append("FIELDVALUE: " + str49 + "\n");
//          }
//        }
//        
//
//        localStringBuffer.append("------------------------------------------------------------------------------- \n");
//        localStatement1.close();
//        localConnection.close();
//        
//
//        if (TransaccionIgnorada(paramString, paramInt1, paramInt2) > 0)
//        {
//          if (paramInt4 == 2) {
//            LogTrans(paramString, localStringBuffer.toString(), "log_trans_php");
//          }
//          if (paramInt4 == 1) {
//            LogTrans(paramString, localStringBuffer.toString(), "log_trans_aut");
//          }
//          if (paramInt4 == 0) {
//            LogTrans(paramString, localStringBuffer.toString(), "log_trans_app");
//          }
//          try
//          {
//            str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> enviando transaccion";
//            if (paramInt4 == 2) {
//              LogTrans(paramString, str3, "log_php");
//            }
//            if (paramInt4 == 1) {
//              LogTrans(paramString, str3, "log_aut");
//            }
//            if (paramInt4 == 0) {
//              LogTrans(paramString, str3, "log_app");
//            }
//            
//            localJCoFunction.execute(localJCoDestination);
//            
//            str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> transaccion enviada con exito";
//            if (paramInt4 == 2) {
//              LogTrans(paramString, str3, "log_php");
//              
//              str3 = "Transaccion enviada con exito. Bdate: " + paramString + " pos: " + paramInt1 + " trans: " + paramInt2;
//              str4 = "<br>Transaccion enviada con exito. Bdate: " + paramString + " pos: " + paramInt1 + " trans: " + paramInt2;
//              LogTransWeb(str5, str3, "log");
//              LogEnvioAppWeb(Globals.NombreTablaTemp, str4, Globals.NumeroTransaccionActual);
//              System.out.println(str3);
//            }
//            if (paramInt4 == 1) {
//              LogTrans(paramString, str3, "log_aut");
//            }
//            if (paramInt4 == 0) {
//              Globals.ta.append("\t TRANSACCION ENVIADA CON EXITO..\n");
//              LogTrans(paramString, str3, "log_app");
//            }
//            
//            JCoTable localJCoTable12 = localJCoFunction.getTableParameterList().getTable("RETURN");
//            for (i5 = 0; i5 < localJCoTable12.getNumRows(); i5++)
//            {
//              localJCoTable12.setRow(i5);
//              str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t" + localJCoTable12.getString("TYPE") + '\t' + localJCoTable12.getString("MESSAGE");
//              if (paramInt4 == 2) {
//                LogTrans(paramString, str3, "log_php");
//              }
//              if (paramInt4 == 1) {
//                LogTrans(paramString, str3, "log_aut");
//              }
//              if (paramInt4 == 0) {
//                Globals.ta.append("\t" + localJCoTable12.getString("TYPE") + '\t' + localJCoTable12.getString("MESSAGE") + "\n");
//                LogTrans(paramString, str3, "log_app");
//              }
//            }
//            
//            if (paramInt4 == 2) {
//              if (i != 0)
//              {
//                str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> transaccion " + paramInt2 + " del pos " + paramInt1 + " tuvo posicion de venta";
//                LogTrans(paramString, str3, "log_php");
//              }
//              else if (j != 0)
//              {
//                str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> transaccion " + paramInt2 + " del pos " + paramInt1 + " tuvo transacciones de control";
//                LogTrans(paramString, str3, "log_php");
//              }
//            }
//            if (paramInt4 == 1) {
//              if (i != 0)
//              {
//                str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> transaccion " + paramInt2 + " del pos " + paramInt1 + " tuvo posicion de venta";
//                LogTrans(paramString, str3, "log_aut");
//              }
//              else if (j != 0)
//              {
//                str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> transaccion " + paramInt2 + " del pos " + paramInt1 + " tuvo transacciones de control";
//                LogTrans(paramString, str3, "log_aut");
//              }
//            }
//            if (paramInt4 == 0) {
//              if (i != 0)
//              {
//                str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> transaccion " + paramInt2 + " del pos " + paramInt1 + " tuvo posicion de venta";
//                LogTrans(paramString, str3, "log_app");
//              }
//              else if (j != 0)
//              {
//                str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> transaccion " + paramInt2 + " del pos " + paramInt1 + " tuvo transacciones de control";
//                LogTrans(paramString, str3, "log_app");
//              }
//            }
//            
//            ActualizarStatusTransaccion(paramString, paramInt1, paramInt2, paramInt3, 1);
//            Globals.ContadorTransacciones += 1;
//            
//            return;
//          }
//          catch (com.sap.conn.jco.AbapException localAbapException) {
//            str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> ERROR:" + localAbapException.toString();
//            str4 = "<br> --> ERROR:" + localAbapException.toString();
//            if (paramInt4 == 2) {
//              LogTrans(paramString, str3, "log_php");
//              LogTransWeb(str5, str3, "error");
//              LogEnvioAppWeb(Globals.NombreTablaTemp, str4, Globals.NumeroTransaccionActual);
//              System.out.println(str3);
//            }
//            if (paramInt4 == 1) {
//              LogTrans(paramString, str3, "log_aut");
//            }
//            if (paramInt4 == 0) {
//              Globals.ta.append("\t" + localAbapException.toString() + "\n");
//              LogTrans(paramString, str3, "log_app");
//            }
//            ActualizarStatusTransaccion(paramString, paramInt1, paramInt2, paramInt3, -1);
//            return;
//          }
//        }
//        str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> transaccion " + paramInt2 + " del pos " + paramInt1 + " ignorada";
//        str4 = "<br> --> transaccion " + paramInt2 + " del pos " + paramInt1 + " ignorada";
//        if (paramInt4 == 2) {
//          LogTrans(paramString, str3, "log_php");
//          LogTransWeb(str5, str3, "error");
//          LogEnvioAppWeb(Globals.NombreTablaTemp, str4, Globals.NumeroTransaccionActual);
//          System.out.println(str3);
//        }
//        if (paramInt4 == 1) {
//          LogTrans(paramString, str3, "log_aut");
//        }
//        if (paramInt4 == 0) {
//          Globals.ta.append("\t transaccion " + paramInt2 + " del pos " + paramInt1 + " ignorada" + "\n");
//          LogTrans(paramString, str3, "log_app");
//        }
//        
//        ActualizarStatusTransaccion(paramString, paramInt1, paramInt2, paramInt3, 2);
//        return;
//      }
//      catch (Exception localException)
//      {
//        str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> ERROR:" + localException.getMessage();
//        str4 = "<br> --> ERROR:" + localException.getMessage();
//        if (paramInt4 == 2) {
//          LogTrans(paramString, str3, "log_php");
//          LogTransWeb(str5, str3, "error");
//          LogEnvioAppWeb(Globals.NombreTablaTemp, str4, Globals.NumeroTransaccionActual);
//          System.out.println(str3);
//        }
//        if (paramInt4 == 1) {
//          LogTrans(paramString, str3, "log_aut");
//        }
//        if (paramInt4 == 0) {
//          Globals.ta.append("\t" + localException.getMessage() + "\n");
//          LogTrans(paramString, str3, "log_app");
//        }
//        
//        ActualizarStatusTransaccion(paramString, paramInt1, paramInt2, paramInt3, -1); return;
//      }
//      return;
//    }
//    catch (JCoException localJCoException) {
//      str3 = "[" + FechaLog("ddMMyy HH:mm:ss") + "] \t\t --> ERROR:" + localJCoException.getMessage();
//      str4 = "<br> --> ERROR:" + localJCoException.getMessage();
//      if (paramInt4 == 2) {
//        LogTrans(paramString, str3, "log_php");
//        LogTransWeb(str5, str3, "error");
//        LogEnvioAppWeb(Globals.NombreTablaTemp, str4, Globals.NumeroTransaccionActual);
//        System.out.println(str3);
//      }
//      if (paramInt4 == 1) {
//        LogTrans(paramString, str3, "log_aut");
//      }
//      if (paramInt4 == 0) {
//        Globals.ta.append("\t" + localJCoException.getMessage() + "\n");
//        LogTrans(paramString, str3, "log_app");
//      }
//      
//      ActualizarStatusTransaccion(paramString, paramInt1, paramInt2, paramInt3, -1);
//    }
//  }
//  
//
//
//
//  public void enviar_transacciones_automatico()
//  {
//    Globals.ContadorTransacciones = 0;
//    
//    int i = BusinessDateActual();
//    String str2 = String.valueOf(i);
//    
//
//
//
//    if (str2 == "") {
//      LogTrans(str2, "NO SE ENCONTRO BUSINESSDATE ACTUAL", "log");
//      return;
//    }
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//
//
//      String str1 = "select termnmbr, transnmbr, status from sap_transacciones where bdate = " + i + " and status not in (1,2) order by status DESC, transnmbr DESC ";
//      
//
//
//      ResultSet localResultSet = localStatement.executeQuery(str1);
//      
//      while (localResultSet.next()) {
//        int j = localResultSet.getInt("termnmbr");
//        int k = localResultSet.getInt("transnmbr");
//        int m = localResultSet.getInt("status");
//        callBapi_transacciones_automaticas(str2, j, k, m, 1);
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {
//      LogTrans(str2, localException.getMessage(), "log");
//      return;
//    }
//  }
//  
//  public int LimiteTransacciones()
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select trans_x_envio from sap_configuracion where id = 1;";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("trans_x_envio");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//
//  public String FechaLog(String paramString)
//  {
//    java.util.Date localDate = new java.util.Date();
//    
//    java.text.SimpleDateFormat localSimpleDateFormat = new java.text.SimpleDateFormat(paramString);
//    String str = localSimpleDateFormat.format(localDate);
//    return str;
//  }
//  
//
//  public int ExisteTabla(String paramString)
//  {
//    int i = 0;
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select count(*) as count from pg_tables where tablename = '" + paramString + "'";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("count");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {}
//    
//    return i;
//  }
//  
//
//  public int BusinessDateActual()
//  {
//    int i = 0;
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//      String str = "select bdate from businessdate order by seq desc limit 1";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("bdate");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {
//      LogTrans("error", localException.getMessage(), "app");
//    }
//    
//
//    return i;
//  }
//  
//  public String SetTransaccionQuery(int paramInt1, int paramInt2, int paramInt3)
//  {
//    String str1 = "";
//    String str2 = "";
//    if (paramInt1 != -1)
//    {
//
//
//
//
//
//
//
//      str1 = "select to_char(to_timestamp(datetime), 'DD/MM/YY HH24:MI') as fecha, termnmbr, transnmbr, (CASE WHEN status=1 THEN 'ENVIADA' WHEN status=0 THEN 'NO ENVIADA' WHEN status=2 THEN 'IGNORADA' ELSE 'REENVIANDO' END) as status, priority - 1 as timeout from sap_transacciones ";
//      
//
//
//
//
//
//
//
//      str2 = " where status not in(999) and bdate = " + paramInt1;
//    }
//    
//
//
//
//    if (paramInt2 != -1) {
//      if (str2.equals("")) {
//        str2 = str2 + " where termnmbr = " + paramInt2;
//      } else {
//        str2 = str2 + " and termnmbr = " + paramInt2;
//      }
//    }
//    
//    if (paramInt3 != -1) {
//      if (str2.equals("")) {
//        if (paramInt3 == 0)
//          str2 = str2 + " where status = 0";
//        if (paramInt3 == 1)
//          str2 = str2 + " where status = 1";
//        if (paramInt3 == 2)
//          str2 = str2 + " where status = 2";
//        if (paramInt3 == 3)
//          str2 = str2 + " where status < 0";
//      } else {
//        if (paramInt3 == 0)
//          str2 = str2 + " and status = 0";
//        if (paramInt3 == 1)
//          str2 = str2 + " and status = 1";
//        if (paramInt3 == 2)
//          str2 = str2 + " and status = 2";
//        if (paramInt3 == 3) {
//          str2 = str2 + " and status < 0";
//        }
//      }
//    }
//    if (!str1.equals(""))
//    {
//      str1 = str1 + str2 + " ORDER BY termnmbr ASC, status ASC, priority ASC, transnmbr ASC";
//    } else {
//      str1 = str1 + str2;
//    }
//    
//    return str1;
//  }
//  
//
//
//
//  public String ActualizarTablaTransacciones()
//  {
//    int i = -1;
//    int j = -1;
//    int k = -1;
//    String str = "";
//    
//    TypeComboBox localTypeComboBox1 = (TypeComboBox)Globals.ComboBusinessdate.getSelectedItem();
//    TypeComboBox localTypeComboBox2 = (TypeComboBox)Globals.ComboStatus.getSelectedItem();
//    TypeComboBox localTypeComboBox3 = (TypeComboBox)Globals.ComboPos.getSelectedItem();
//    
//    i = localTypeComboBox1.getValue();
//    j = localTypeComboBox2.getValue();
//    k = localTypeComboBox3.getValue();
//    
//    Globals.TransModel.RemoveAll();
//    
//    if (i == -1)
//    {
//      str = DatatoTrasaccionTable(-1, -1, -1);
//      return str;
//    }
//    
//
//    str = DatatoTrasaccionTable(i, k, j);
//    return str;
//  }
//  
//
//
//
//
//
//  public String SetComboQuery(String paramString)
//  {
//    String str = "";
//    
//
//
//
//
//
//    if (paramString.equals("BUSINESSDATE"))
//    {
//
//
//
//
//
//
//
//
//
//
//
//      str = "select bdate as id, \tsubstring(bdate::text from 1 for 4)||'-'|| \tsubstring(bdate::text from 5 for 2)||'-'|| \tsubstring(bdate::text from 7 for 2)||' '|| \tsubstring(bdate::text from 9 for 2) as descripcion \tfrom sap_transacciones group by bdate order by bdate desc";
//    }
//    
//
//
//
//
//
//
//    if (paramString.equals("STATUS")) {
//      str = "(SELECT 0 as id, 'NO ENVIADA' as descripcion) UNION ALL (SELECT 1 as id, 'ENVIADA' as descripcion) UNION ALL (SELECT 2 as id, 'IGNORADA' as descripcion) UNION ALL (SELECT 3 as id, 'REENVIANDO' as descripcion)";
//    }
//    
//
//
//
//    if (paramString.equals("POS")) {
//      str = "select termnmbr as id, termdesc as descripcion from tcf where used='1'";
//    }
//    
//
//    if (paramString.equals("CONEXION")) {
//      str = "(SELECT 0 as id, 'ANTENA' as descripcion) UNION ALL (SELECT 1 as id, 'INTERNET' as descripcion)";
//    }
//    
//
//    if (paramString.equals("TOTALESMES"))
//    {
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//      str = "select DISTINCT(substring(bdate::TEXT from 1 for 4)||substring(bdate::TEXT from 5 for 2)) as id, (CASE WHEN substring(bdate::TEXT  from 5 for 2) = '01' THEN 'ENERO 'WHEN substring(bdate::TEXT  from 5 for 2) = '02' THEN 'FEBRER0 ' WHEN substring(bdate::TEXT  from 5 for 2) = '03' THEN 'MARZO 'WHEN substring(bdate::TEXT  from 5 for 2) = '04' THEN 'ABRIL 'WHEN substring(bdate::TEXT  from 5 for 2) = '05' THEN 'MAYO 'WHEN substring(bdate::TEXT  from 5 for 2) = '06' THEN 'JUNIO 'WHEN substring(bdate::TEXT  from 5 for 2) = '07' THEN 'JULIO 'WHEN substring(bdate::TEXT  from 5 for 2) = '08' THEN 'AGOSTO 'WHEN substring(bdate::TEXT  from 5 for 2) = '09' THEN 'SEPTIEMBRE 'WHEN substring(bdate::TEXT  from 5 for 2) = '10' THEN 'OCTUBRE 'WHEN substring(bdate::TEXT  from 5 for 2) = '11' THEN 'NOVIEMBRE 'WHEN substring(bdate::TEXT  from 5 for 2) = '12' THEN 'DICIEMBRE 'ELSE ' ERROR' END)||substring(bdate::TEXT from 1 for 4) as descripcion FROM sap_transacciones GROUP BY bdate ORDER BY substring(bdate::TEXT from 1 for 4)||substring(bdate::TEXT from 5 for 2) ASC;";
//    }
//    
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//    if (paramString.equals("TOTALESSTATUS")) {
//      str = "(SELECT 1 as id, 'ENVIADOS' as descripcion) UNION ALL (SELECT 0 as id, 'NO ENVIADOS' as descripcion) ";
//    }
//    
//
//    return str;
//  }
//  
//  public void LoadComboBox(String paramString)
//  {
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str1 = SetComboQuery(paramString);
//      ResultSet localResultSet = localStatement.executeQuery(str1);
//      
//      if (paramString.equals("BUSINESSDATE")) {
//        Globals.ComboBusinessdate.addItem(new TypeComboBox(-1, "SELECCIONE"));
//      }
//      if (paramString.equals("STATUS"))
//        Globals.ComboStatus.addItem(new TypeComboBox(-1, "SELECCIONE"));
//      if (paramString.equals("CONEXION")) {
//        Globals.ConfigComboConexion.addItem(new TypeComboBox(-1, "SELECCIONE"));
//      }
//      if (paramString.equals("POS"))
//        Globals.ComboPos.addItem(new TypeComboBox(-1, "SELECCIONE"));
//      if (paramString.equals("TOTALESMES")) {
//        Globals.ComboTotalMes.addItem(new TypeComboBox(-1, "SELECCIONE"));
//      }
//      if (paramString.equals("TOTALESSTATUS")) {
//        Globals.ComboTotalStatus.addItem(new TypeComboBox(-1, "SELECCIONE"));
//      }
//      
//      while (localResultSet.next()) {
//        int i = localResultSet.getInt("id");
//        String str2 = localResultSet.getString("descripcion");
//        
//        if (paramString.equals("BUSINESSDATE")) {
//          Globals.ComboBusinessdate.addItem(new TypeComboBox(i, str2));
//        }
//        if (paramString.equals("STATUS")) {
//          Globals.ComboStatus.addItem(new TypeComboBox(i, str2));
//        }
//        if (paramString.equals("CONEXION")) {
//          Globals.ConfigComboConexion.addItem(new TypeComboBox(i, str2));
//        }
//        if (paramString.equals("POS"))
//          Globals.ComboPos.addItem(new TypeComboBox(i, str2));
//        if (paramString.equals("TOTALESMES")) {
//          Globals.ComboTotalMes.addItem(new TypeComboBox(i, str2));
//        }
//        if (paramString.equals("TOTALESSTATUS")) {
//          Globals.ComboTotalStatus.addItem(new TypeComboBox(i, str2));
//        }
//      }
//      
//      localResultSet.close();
//      localResultSet.close();
//    }
//    catch (Exception localException) {}
//  }
//  
//
//
//
//  public String DatatoTrasaccionTable(int paramInt1, int paramInt2, int paramInt3)
//  {
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str1 = SetTransaccionQuery(paramInt1, paramInt2, paramInt3);
//      
//
//      if (!str1.equals("")) {
//        ResultSet localResultSet = localStatement.executeQuery(str1);
//        while (localResultSet.next()) {
//          String str2 = localResultSet.getString("fecha");
//          String str3 = localResultSet.getString("termnmbr");
//          String str4 = localResultSet.getString("transnmbr");
//          String str5 = localResultSet.getString("status");
//          String str6 = localResultSet.getString("timeout");
//          Object[] arrayOfObject = { str2, str3, str4, str5, str6, new Boolean(false) };
//          
//          Globals.TransModel.insertData(arrayOfObject);
//        }
//        localResultSet.close();
//        localResultSet.close();
//      }
//      
//
//      return "";
//    }
//    catch (Exception localException)
//    {
//      return "ERROR BUSCANDO LA CONSULTA: " + localException.toString();
//    }
//  }
//  
//
//  public String SetTotalesQuery(int paramInt1, int paramInt2)
//  {
//    String str1 = "";
//    String str2 = "";
//    if (paramInt1 != -1)
//    {
//
//
//
//
//
//
//
//
//      str1 = "select bdate as id, substring(bdate::text from 1 for 4)||'-'||substring(bdate::text from 5 for 2)||'-'||substring(bdate::text from 7 for 2)||' '||substring(bdate::text from 9 for 2) as businessdate, 'TODOS' as status from sap_transacciones where bdate::text like '" + paramInt1 + "%' " + "group by bdate order by bdate DESC;";
//    }
//    
//
//
//
//
//
//
//    if (paramInt2 != -1) {
//      if (paramInt2 == 1)
//      {
//
//
//
//
//
//
//
//        str1 = "select bdate as id, substring(bdate::text from 1 for 4)||'-'||substring(bdate::text from 5 for 2)||'-'||substring(bdate::text from 7 for 2)||' '||substring(bdate::text from 9 for 2) as businessdate, 'ENVIADO' as status from sap_transacciones where bdate::text like '" + paramInt1 + "%' " + "group by bdate order by bdate DESC;";
//
//
//
//
//
//
//
//
//      }
//      else
//      {
//
//
//
//
//
//
//
//        str1 = "select bdate as id, substring(bdate::text from 1 for 4)||'-'||substring(bdate::text from 5 for 2)||'-'||substring(bdate::text from 7 for 2)||' '||substring(bdate::text from 9 for 2) as businessdate, 'NO ENVIADO' as status from sap_transacciones where bdate::text like '" + paramInt1 + "%' " + "group by bdate order by bdate DESC;";
//      }
//    }
//    
//
//
//
//
//
//
//
//
//
//    str1 = str1 + str2;
//    
//
//    return str1;
//  }
//  
//
//  public String EstadisticaTotalesProcesado(String paramString)
//  {
//    String str1 = "NO";
//    String str2 = "";
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      str2 = "select status_procesado from sap_estadisticas_" + paramString;
//      ResultSet localResultSet = localStatement.executeQuery(str2);
//      
//      while (localResultSet.next()) {
//        if (localResultSet.getInt("status_procesado") != 0) {
//          str1 = "SI";
//        }
//      }
//      
//
//      return str1;
//    }
//    catch (Exception localException) {}
//    
//
//    return "NO EXISTEN";
//  }
//  
//
//  public String EstadisticaTotalesStatus(String paramString)
//  {
//    int i = 0;
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select status_enviado from sap_estadisticas_" + paramString;
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("status_enviado");
//      }
//      
//      if (i == 0) {
//        return "NO ENVIADO";
//      }
//      return "ENVIADO";
//    }
//    catch (Exception localException) {}
//    return "NO ENVIADO";
//  }
//  
//  public String DatatoTotalesTable(int paramInt1, int paramInt2)
//  {
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str1 = SetTotalesQuery(paramInt1, paramInt2);
//      
//      if (!str1.equals("")) {
//        ResultSet localResultSet = localStatement.executeQuery(str1);
//        while (localResultSet.next()) {
//          String str2 = localResultSet.getString("id");
//          String str3 = localResultSet.getString("businessdate");
//          String str4 = EstadisticaTotalesProcesado(str2);
//          
//          String str5 = EstadisticaTotalesStatus(str2);
//          
//          Object[] arrayOfObject;
//          if (localResultSet.getString("status").equals("TODOS"))
//          {
//            arrayOfObject = new Object[] { str3, str4, str5, new Boolean(false) };
//            
//            Globals.TotalesModel.insertData(arrayOfObject);
//          } else if (localResultSet.getString("status").equals(str5))
//          {
//            arrayOfObject = new Object[] { str3, str4, str5, new Boolean(false) };
//            
//            Globals.TotalesModel.insertData(arrayOfObject);
//          }
//        }
//        
//
//
//
//
//
//
//        localResultSet.close();
//        localResultSet.close();
//      }
//      
//
//      return "";
//    }
//    catch (Exception localException)
//    {
//      return "ERROR BUSCANDO LA CONSULTA: " + localException.toString();
//    }
//  }
//  
//
//  public void LogTrans(String paramString1, String paramString2, String paramString3)
//  {
//    String str = "/home/tplinux/posdm/log-trans/" + paramString3 + "." + paramString1;
//    
//    java.io.FileWriter localFileWriter = null;
//    java.io.PrintWriter localPrintWriter = null;
//    try {
//      localFileWriter = new java.io.FileWriter(str, true);
//      localPrintWriter = new java.io.PrintWriter(localFileWriter);
//      localPrintWriter.println(paramString2);
//      localFileWriter.close();
//    }
//    catch (Exception localException) {
//      localException.printStackTrace();
//    }
//  }
//  
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//  public int ValorConfiguracion(String paramString)
//  {
//    int i = 0;
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select value from sap_posdm_configuration where key = '" + paramString + "';";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("value");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//  public int ActualizarPrioridadTransaccion(String paramString, int paramInt1, int paramInt2)
//  {
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "UPDATE sap_transacciones  SET priority=priority + 1  where bdate = " + paramString + " and termnmbr=" + paramInt1 + " and transnmbr=" + paramInt2;
//      
//
//
//
//
//      localStatement.executeUpdate(str);
//      
//      localStatement.execute("END");
//      localStatement.close();
//      localConnection.close();
//      
//      return 0;
//    }
//    catch (Exception localException) {
//      System.out.println(localException.getMessage()); }
//    return -1;
//  }
//  
//  public int ActualizarPrioridadTotal(String paramString)
//  {
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "UPDATE sap_transacciones  SET priority=priority + 1  where bdate = " + paramString + " and termnmbr=" + 999 + " and transnmbr=" + 999 + " and status=" + 999;
//      
//
//
//
//
//
//      localStatement.executeUpdate(str);
//      
//      localStatement.execute("END");
//      localStatement.close();
//      localConnection.close();
//      
//      return 0;
//    }
//    catch (Exception localException) {
//      System.out.println(localException.getMessage()); }
//    return -1;
//  }
//  
//  public int ExistConfigPosdm(String paramString)
//  {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select count(*) as cant from sap_posdm_configuration where key = '" + paramString + "'";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("cant");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//
//
//
//
//
//
//  public int EnviarTransaccionWeb(String paramString, int paramInt)
//  {
//    EjecutarEnvioConTimeOut localEjecutarEnvioConTimeOut = new EjecutarEnvioConTimeOut();
//    
//
//    int m = ValorConfiguracion(Globals.Config_TimeOut);
//    String str2 = "";
//    String str3 = "";
//    String str4 = "";
//    int n = 0;
//    
//    int i1 = 0;
//    
//    Globals.NombreTablaTemp = paramString;
//    Globals.CantidadTransaccionesEnviar = CuantasTransacciones(paramString);
//    Globals.NumeroTransaccionActual = 0;
//    
//    int i2 = 0;
//    int i3 = 0;
//    int i4 = 0;
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str5 = "select bdate, posnmbr, transnmbr, status from " + paramString + " order by transnmbr desc";
//      ResultSet localResultSet = localStatement.executeQuery(str5);
//      
//      LogTransWeb(paramString, "Enviar Transaccion Iniciado", "log");
//      
//
//
//
//
//
//
//
//
//
//
//      while (localResultSet.next()) {
//        Globals.NumeroTransaccionActual += 1;
//        String str1 = localResultSet.getString("bdate");
//        int i = localResultSet.getInt("posnmbr");
//        int j = localResultSet.getInt("transnmbr");
//        int k = localResultSet.getInt("status");
//        
//        if (n == 0) {
//          str2 = "Realizando prueba de conexion con el Servidor SAP";
//          LogTrans(str1, "\n" + str2, "log_php");
//          
//          LogTransWeb(paramString, str2, "log");
//          System.out.println(str2);
//          str4 = ProbarConexion();
//          if (str4 == "PRUEBA EXITOSA") {
//            n = 1;
//            str2 = "Prueba Exitosa";
//            LogTrans(str1, str2, "log_php");
//            LogTransWeb(paramString, str2, "log");
//            System.out.println(str2);
//          } else {
//            str2 = "\nError de conexion con el servidor SAP. Se intentara la conexion con el servidor en la proxima transaccion.\nBdate: " + str1 + " Pos: " + i + " Trans: " + j + " Status: " + k + " tipo de operacion: " + paramInt + "(1=Venta/Control 2=Total)\n";
//            str3 = "<br>Error de conexion con el servidor SAP. Se intentara la conexion con el servidor en la proxima transaccion.Bdate: " + str1 + " Pos: " + i + " Trans: " + j + " Status: " + k + " tipo de operacion: " + paramInt + "(1=Venta/Control 2=Total)<br>";
//            LogTrans(str1, str4 + str2, "log_php");
//            LogTransWeb(paramString, str4, "log");
//            LogTransWeb(paramString, str4 + str2, "error");
//            LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//            System.out.println(str2);
//          }
//        }
//        
//        if (str4 == "PRUEBA EXITOSA") {
//          if (paramInt == 1)
//          {
//            if ((i == 999) && (j == 999) && (k == 999)) {
//              str2 = "Se esta enviando datos de transaccion de total con bandera de transaccion de venta/control. Bandera tipo(1=Venta/Control 2=Total)\n Bdate: " + str1 + " Pos: " + i + " Trans: " + j + " Bandera Tipo de Operacion: " + paramInt;
//              str3 = "<br>Se esta enviando datos de transaccion de total con bandera de transaccion de venta/control. Bandera tipo(1=Venta/Control 2=Total) Bdate: " + str1 + " Pos: " + i + " Trans: " + j + " Bandera Tipo de Operacion: " + paramInt;
//              System.out.println(str2);
//              LogTransWeb(paramString, str2, "error");
//              LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//            }
//            else {
//              i1 = ValidarEnvioTrans(str1, i, j, k);
//              
//
//              if ((i1 == -1) || (i1 == 0))
//              {
//                str2 = "Error consultando el parametro numero de intentos de envio de transaccion. No se puede enviar la transaccion businessdate: " + str1 + " pos: " + i + " trans: " + j + " status: " + k;
//                str3 = "<br>Error consultando el parametro numero de intentos de envio de transaccion. No se puede enviar la transaccion businessdate: " + str1 + " pos: " + i + " trans: " + j + " status: " + k;
//                LogTrans(str1, str2, "log_php");
//                LogTransWeb(paramString, str2, "log");
//                LogTransWeb(paramString, str2, "error");
//                LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//              }
//              
//              if (i1 == 1) {
//                localEjecutarEnvioConTimeOut.runTaskInLessThanGivenMs(m, "OcurrioTimeOut", str1, i, j, k, 2, "transacciones");
//              }
//              
//              if (i1 == 2)
//              {
//                str2 = "La transaccion llego al maximo numero de intentos de envio. No se puede enviar la transaccion businessdate: " + str1 + " pos: " + i + " trans: " + j + " status: " + k;
//                str3 = "<br>La transaccion llego al maximo numero de intentos de envio. No se puede enviar la transaccion businessdate: " + str1 + " pos: " + i + " trans: " + j + " status: " + k;
//                LogTrans(str1, str2, "log_php");
//                LogTransWeb(paramString, str2, "log");
//                LogTransWeb(paramString, str2, "error");
//                LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//              }
//            }
//          }
//          
//          if (paramInt == 2)
//          {
//            if ((i != 999) && (j != 999) && (k != 999)) {
//              str2 = "Se esta enviando datos de transaccion de venta/control con bandera de transaccion de Total. Bandera tipo(1=Venta/Control 2=Total)\n Bdate: " + str1 + " Pos: " + i + " Trans: " + j + " Bandera Tipo de Operacion: " + paramInt;
//              str3 = "<br>Se esta enviando datos de transaccion de venta/control con bandera de transaccion de Total. Bandera tipo(1=Venta/Control 2=Total) Bdate: " + str1 + " Pos: " + i + " Trans: " + j + " Bandera Tipo de Operacion: " + paramInt;
//              System.out.println(str2);
//              LogTransWeb(paramString, str2, "error");
//              LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//            }
//            else {
//              i1 = ValidarEnvioTrans(str1, i, j, k);
//              
//
//              if ((i1 == -1) || (i1 == 0))
//              {
//                str2 = "Error consultando el parametro numero de intentos de envio de transaccion. No se puede enviar la transaccion de totales businessdate: " + str1 + " status: " + k;
//                str3 = "<br>Error consultando el parametro numero de intentos de envio de transaccion. No se puede enviar la transaccion de totales businessdate: " + str1 + " status: " + k;
//                LogTrans(str1, str2, "log_php");
//                LogTransWeb(paramString, str2, "log");
//                LogTransWeb(paramString, str2, "error");
//                LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//              }
//              
//              if (i1 == 1)
//              {
//                i2 = TotalesEnviados(str1);
//                i3 = CountTransaccionesPendientes(str1);
//                i4 = CountTransaccionesProcesadasSinEnviar(str1);
//                
//                if (i2 == 1) {
//                  str2 = "Totales ya enviados. Bussinesdate: " + str1 + " Status: " + k + "\n";
//                  str3 = "<br>Totales ya enviados. Bussinesdate: " + str1 + " Status: " + k;
//                  LogTrans(str1, str2, "log_php");
//                  LogTransWeb(paramString, str2, "log");
//                  LogTransWeb(paramString, str2, "error");
//                  LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//                }
//                if (i3 > 0) {
//                  str2 = "Existen transacciones pendientes por enviar, no se puede enviar los totales. Businessdate: " + str1 + " Status: " + k + "\n";
//                  str3 = "<br>Existen transacciones pendientes por enviar, no se puede enviar los totales. Businessdate: " + str1 + " Status: " + k;
//                  LogTrans(str1, str2, "log_php");
//                  LogTransWeb(paramString, str2, "log");
//                  LogTransWeb(paramString, str2, "error");
//                  LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//                }
//                else if (i4 > 0) {
//                  str2 = "Existen transacciones procesadas que no han podido ser enviadas, no se puede enviar los totales. Businessdate: " + str1 + " Status: " + k + "\n";
//                  str3 = "<br>Existen transacciones procesadas que no han podido ser enviadas, no se puede enviar los totales. Businessdate: " + str1 + " Status: " + k;
//                  LogTrans(str1, str2, "log_php");
//                  LogTransWeb(paramString, str2, "log");
//                  LogTransWeb(paramString, str2, "error");
//                  LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//                }
//                
//
//                if ((i2 == 0) && (i3 == 0) && (i4 == 0)) {
//                  localEjecutarEnvioConTimeOut.runTaskInLessThanGivenMs(m, "OcurrioTimeOut", str1, 999, 999, k, 2, "totales");
//                }
//              }
//              
//              if (i1 == 2)
//              {
//                str2 = "La transaccion llego al maximo numero de intentos de envio. No se puede enviar la transaccion de totales businessdate: " + str1 + " status: " + k;
//                str3 = "<br>La transaccion llego al maximo numero de intentos de envio. No se puede enviar la transaccion de totales businessdate: " + str1 + " status: " + k;
//                LogTrans(str1, str2, "log_php");
//                LogTransWeb(paramString, str2, "log");
//                LogTransWeb(paramString, str2, "error");
//                LogEnvioAppWeb(Globals.NombreTablaTemp, str3, Globals.NumeroTransaccionActual);
//              }
//            }
//          }
//        }
//      }
//      
//
//      str5 = "drop table " + paramString;
//      localStatement.executeUpdate(str5);
//      localStatement.execute("END");
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return 0;
//    }
//    catch (Exception localException)
//    {
//      str2 = "El proceso termino con errores";
//      LogTransWeb(paramString, str2, "fin");
//      System.out.println(localException.getMessage());
//      LogTransWeb(paramString, localException.getMessage(), "error"); }
//    return -1;
//  }
//  
//
//  public void LogTransWeb(String paramString1, String paramString2, String paramString3)
//  {
//    String str = "/home/tplinux/posdm/log-trans/appweb/" + paramString1 + "." + paramString3;
//    java.io.FileWriter localFileWriter = null;
//    java.io.PrintWriter localPrintWriter = null;
//    try {
//      localFileWriter = new java.io.FileWriter(str, true);
//      localPrintWriter = new java.io.PrintWriter(localFileWriter);
//      localPrintWriter.println(paramString2);
//      localFileWriter.close();
//    }
//    catch (Exception localException) {
//      localException.printStackTrace();
//    }
//  }
//  
//  public int ValidarEnvioTrans(String paramString, int paramInt1, int paramInt2, int paramInt3) {
//    int i = 0;
//    int j = 0;
//    int k = 0;
//    
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//      String str = "select priority from sap_transacciones where bdate = " + paramString + " and termnmbr = " + paramInt1 + " and transnmbr = " + paramInt2 + " and status = " + paramInt3;
//      
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("priority");
//      }
//      
//
//      str = "select value from sap_posdm_configuration where key = '" + Globals.Config_NumeroIntentosEnvios + "'";
//      
//      localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        j = localResultSet.getInt("value") + 1;
//      }
//      
//      if (i <= j) {
//        k = 1;
//      } else {
//        k = 2;
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return k;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//
//  public int SeleccionarBusinessDate(int paramInt)
//  {
//    int i = 0;
//    String str1 = "";
//    int j = 0;
//    int k = 0;
//    int m = 0;
//    int n = 0;
//    int i1 = 0;
//    String str2 = "";
//    
//
//    if (paramInt != -1) {
//      str2 = " and (\t\t(\t\t\tsubstring(bdate::text from 1 for 4)||'-'||\t\t\tsubstring(bdate::text from 5 for 2)||'-'||\t\t\tsubstring(bdate::text from 7 for 2)::varchar\t\t)::date) >= (\t\tselect\t\t\t(\t\t\t\t(\t\t\t\t\tsubstring(bdate::text from 1 for 4)||'-'||\t\t\t\t\tsubstring(bdate::text from 5 for 2)||'-'||\t\t\t\t\tsubstring(bdate::text from 7 for 2)::varchar\t\t\t\t)::date - " + paramInt + "\t\t\t)" + "\t\tfrom" + "\t\t\tbusinessdate" + "\t\twhere" + "\t\t\tseq in(select max(seq) from businessdate)" + ") ";
//    }
//    
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str3 = "select bdate from businessdate where \t( \t\t(\t\t\tsubstring(bdate::text from 1 for 4)||'-'||\t\t\tsubstring(bdate::text from 5 for 2)||'-'||\t\t\tsubstring(bdate::text from 7 for 2)::varchar\t\t)::date\t)  <=\t(\t\tselect\t\t\t( \t\t\t\t(\t\t\t\t\tsubstring(bdate::text from 1 for 4)||'-'||\t\t\t\t\tsubstring(bdate::text from 5 for 2)||'-'||\t\t\t\t\tsubstring(bdate::text from 7 for 2)::varchar\t\t\t\t)::date\t\t\t)\t\tfrom \t\t\tbusinessdate\t\twhere \t\t\tseq in(select max(seq) from businessdate) )" + str2 + " order by seq asc;";
//      
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//      ResultSet localResultSet = localStatement.executeQuery(str3);
//      LogTrans("log", "\n" + str3, "log_sql");
//      
//      while (localResultSet.next()) {
//        if (i1 == 0) {
//          i = localResultSet.getInt("bdate");
//          str1 = String.valueOf(i);
//          j = TotalesEnviados(str1);
//          k = CountTransaccionesPendientes(str1);
//          m = CountTransaccionesProcesadasSinEnviar(str1);
//          n = ExisteFlagTotales(str1);
//          
//          if (j != 1) {
//            if ((n > 0) && (k == 0)) {
//              i1 = 1;
//            }
//            else if ((k != 0) || (m != 0)) {
//              i1 = 1;
//            }
//          }
//        }
//      }
//      
//
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {
//      LogTrans("error", localException.getMessage(), "app");
//    }
//    
//
//    return i;
//  }
//  
//  public void LogEnvioAppWeb(String paramString1, String paramString2, int paramInt)
//  {
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str;
//      if (ExisteTabla(paramString1 + "_log") == 0) {
//        str = "CREATE TABLE " + paramString1 + "_log (id serial NOT NULL, mensaje VARCHAR, posicion SMALLINT, bandleido smallint)";
//        
//        localStatement.executeUpdate(str);
//      }
//      
//      if (ExisteLog(paramString1 + "_log", paramInt) == 0) {
//        str = "insert into " + paramString1 + "_log (mensaje, posicion, bandleido) values('" + paramString2 + "'," + paramInt + ", 0);";
//      }
//      else {
//        str = "update " + paramString1 + "_log set mensaje = '" + paramString2 + "' where posicion = " + paramInt + " and bandleido = 0;";
//      }
//      
//      localStatement.executeUpdate(str);
//      
//      localStatement.execute("END");
//      localStatement.close();
//      localConnection.close();
//
//    }
//    catch (Exception localException)
//    {
//
//      LogTransWeb(paramString1, localException.getMessage(), "error");
//    }
//  }
//  
//
//  public int CuantasTransacciones(String paramString)
//  {
//    int i = 0;
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select count(*) as count from '" + paramString + "'";
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("count");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//    }
//    catch (Exception localException) {}
//    
//    return i;
//  }
//  
//  public int ExisteLog(String paramString, int paramInt) {
//    int i = 0;
//    try {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      String str = "select count(*) as cant from " + paramString + " where posicion = " + paramInt;
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        i = localResultSet.getInt("cant");
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return i;
//    }
//    catch (Exception localException) {}
//    
//    return -1;
//  }
//  
//
//
//
//
//  public String ShowConexionSapActiva()
//  {
//    String str = "";
//    StringBuffer localStringBuffer = new StringBuffer(1000);
//    
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//
//      str = str + localStringBuffer.append("select sc.* from sap_conexion sc inner join sap_posdm_configuration spc on (sc.id = spc.value::integer) where\tspc.key = 'connection_posdm_active'");
//      ResultSet localResultSet = localStatement.executeQuery(str);
//      
//      while (localResultSet.next()) {
//        Globals.ConfigComboConexion.setSelectedIndex(localResultSet.getInt("conectype") + 1);
//        
//        Globals.ConfigFieldLang.setText(localResultSet.getString("lang"));
//        Globals.ConfigFieldCliente.setText(localResultSet.getString("client"));
//        
//        Globals.ConfigFieldRouter.setText(localResultSet.getString("saprouter"));
//        
//        Globals.ConfigFieldUser.setText(localResultSet.getString("sapuser"));
//        
//
//
//
//
//        Globals.ConfigFieldPass.setText(localResultSet.getString("sappasswd"));
//        
//        Globals.ConfigFieldSysnr.setText(localResultSet.getString("sysnr"));
//        Globals.ConfigFieldHost.setText(localResultSet.getString("ashost"));
//      }
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return "";
//    } catch (Exception localException) {
//      return "NO SE PUDO CARGAR LA CONFIGURACION DESDE LA BD: " + localException.getMessage();
//    }
//  }
//  
//
//
//  public String GuardarConexionActiva(int paramInt)
//  {
//    String str = "";
//    StringBuffer localStringBuffer1 = new StringBuffer(1000);
//    StringBuffer localStringBuffer2 = new StringBuffer(1000);
//    try
//    {
//      Class.forName("org.postgresql.Driver");
//      Connection localConnection = DriverManager.getConnection(Globals.url, Globals.user, Globals.password);
//      
//      Statement localStatement = localConnection.createStatement();
//      
//      if (ExistConfigPosdm("connection_posdm_active") == 0) {
//        str = "insert into sap_posdm_configuration(key, value, created_at, updated_at) values('connection_posdm_active','" + paramInt + "',current_timestamp,current_timestamp)";
//      }
//      if (ExistConfigPosdm("connection_posdm_active") > 0)
//      {
//        str = "update sap_posdm_configuration set value = '" + paramInt + "', updated_at = current_timestamp where key = 'connection_posdm_active'";
//      }
//      
//
//      localStatement.executeUpdate(str);
//      localStatement.execute("END");
//      
//      localStatement.close();
//      localConnection.close();
//      
//      return "LA CONEXION SE HA CONFIGURADO COMO ACTIVA DE MANERA EXITOSA";
//    }
//    catch (Exception localException) {
//      System.out.println(str);
//      return "OPERACION FALLIDA, NO SE HA PODIDO GUARDAR LA CONEXION COMO ACTIVA: " + localException.getMessage();
//    }
//  }
//}
